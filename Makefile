build:
	docker build -f ./Dockerfile.primo-marketing \
                    --label org.label-schema.vcs-url=`git config --get remote.origin.url` \
                    --label org.label-schema.vcs-ref=`git show --oneline -s|cut -d' ' -f 1` \
                    --label org.label-schema.build-date=`date '+%FT%T%z'` \
                    --label org.label-schema.version=`jq -r .version package.json` \
                    -t primo-marketing .

run:
	docker run -it --rm -p 80:3000 --name primo-marketing-1 -e NODE_ENV=qa primo-marketing
