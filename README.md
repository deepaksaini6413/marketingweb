# Primo connect - WEB application

## Description
Make free international calls to over 4 billion people around the world including:

- Call India mobile phones free
- USA, Brazil, Canada, & Mexico land & mobiles
- China, Thailand, and Singapore land & mobiles
- Landlines in UK, France, Germany, and much of Europe

Calling works without data in over 60 countries on the Primo voice network. You can use your cellular voice minutes to make calls when your not near WIFI or out of data. We go where you go.

Personal US Phone Numbers - Get a free unique USA phone number with your Primo Account.

Unlimited Calls & Texts - You can receive free unlimited inbound calls and SMS to your Primo phone number.

It gets better, Primo is also a messaging app.

Connect with friends and take Primo to the next level with video, messaging, file sharing and more. 

You'll earn free international talk & text minutes when you refer your friends.

Some messenger apps can't communicate with mobile phones around the world. Some alternatives to phone service can't do video, file sharing, or chat. Primo does both in one easy to use ad free service. Try us today and stay connected to anyone anywhere with Primo.

## Installation
Clone the repository to your preferred location.
Run following commands
`npm install` - This will install all dependencies in node_modules folder
`gulp deploy` - This will create build folder with all dependencies


## Running
In order to run the server execute `node server.js` command in root folder of the project.


## Testing
In order to test the application we need to run `gulp deploy_e2e` shell command. This will inject all Istanbul dependencies, so that coverage report can be generated.
In the root folder run `npm test` command. This command will execute all Protractor tests located in **public/test/e2e** folder

## Deployment
After performing tests you might want to deploy the application.
Deployment build is created by using `gulp deploy` command.