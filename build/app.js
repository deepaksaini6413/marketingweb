/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

var app = angular.module("Primo",
  [
    'ui.router',
    'ng.deviceDetector',
    'ngAnimate',
    'ngMaterial',
    'ngMessages',
    'ngSanitize',
    'ngCookies',
    'config',
    'angular-loading-bar',
    'pascalprecht.translate',
    'mparticleWidget',
    'braintree-angular',
    'ngMenuSidenav',
    'flow',
    'ngCsv',
    'sticky',
    'ui.bootstrap',
    'angucomplete-alt',
    'ngToast',
    'angular-search-and-select'
  ]);

app.constant('clientTokenPath', '/get-client-token');
app.config(["$stateProvider", "$urlRouterProvider", "config", "$httpProvider", "$translateProvider", "$mdThemingProvider", "deviceDetectorProvider", "mparticleWidgetProvider", "flowFactoryProvider", "$compileProvider", "$locationProvider", function($stateProvider, $urlRouterProvider,
                    config,
                    $httpProvider, $translateProvider,
                    $mdThemingProvider,
                    deviceDetectorProvider,
                    mparticleWidgetProvider,
                    flowFactoryProvider,
                    $compileProvider,
                    $locationProvider) {

  // Disabling Debug Data
  $compileProvider.debugInfoEnabled(false);

  $locationProvider.html5Mode(true);


  mparticleWidgetProvider.init({
    app_key: config.mparticle.key,
    isSandbox: config.mparticle.isSandbox
  });

  //translate provider
  $translateProvider.useSanitizeValueStrategy(null);
  $translateProvider.useStaticFilesLoader({
    prefix: 'resources/i18n/',
    suffix: '.json'
  });


  //theme provider
  $mdThemingProvider.definePalette('primoPrimary', {
    '50': '#515151',
    '100': '#444444',
    '200': '#373737',
    '300': '#2a2a2a',
    '400': '#1e1e1e',
    '500': '#111',
    '600': '#040404',
    '700': '#000000',
    '800': '#000000',
    '900': '#000000',
    'A100': '#5d5d5d',
    'A200': '#6a6a6a',
    'A400': '#777777',
    'A700': '#000000',
    'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                        // on this palette should be dark or light
    'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
      '200', '300', '400', 'A100'],
    'contrastLightColors': undefined    // could also specify this if default was 'dark'
  });

  $mdThemingProvider.definePalette('primoAccent', {
    '50': '#39feff',
    '100': '#20feff',
    '200': '#06feff',
    '300': '#00ebec',
    '400': '#00d1d2',
    '500': '#00b8b9',
    '600': '#009f9f',
    '700': '#008586',
    '800': '#006c6c',
    '900': '#005353',
    'A100': '#53feff',
    'A200': '#6cfeff',
    'A400': '#86feff',
    'A700': '#003939',
    'contrastDefaultColor': 'light'
  });
  //theme configuration
  $mdThemingProvider.theme('default')
    .primaryPalette('primoPrimary')
    .accentPalette('primoAccent');

  $mdThemingProvider.alwaysWatchTheme(true);

  $urlRouterProvider.otherwise(function($injector) {
    var $state = $injector.get('$state');
    $state.go("/");
  });

  $httpProvider.interceptors.push('sessionInterceptor');

  $stateProvider
    .state("login", {
      url: "/login?:hashkey",
      isPublic: true,
      isNavVisible: true,
      isAuthPage: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/login.html",
          controller: 'LoginController'
        }
      }
    })
    .state("registration", {
      url: "/sign-up",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/register.html",
          controller: "RegisterController"
        }
      }
    })
    .state("verifyMSISDN", {
      url: "/verify-msisdn/:username/:msisdn/:country",
      isPublic: true,
      isNavVisible: true,
      isAuthPage: true,
      onEnter: ["AuthService", function(AuthService) {
        AuthService.verifyMSISDN();
      }]
    })
    .state("forgotPassword", {
      url: "/forgot-password",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/forgot-password.html",
          controller: "ForgotPasswordController"
        }
      }
    })
    .state("passwordReset", {
      url: "/password/reset/:key",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/forgot-password.html",
          controller: "ForgotPasswordController"
        }
      }
    })
    
    .state("forgotUsername", {
      url: "/forgot-username",
      isPublic: true,
      isNavVisible: true,
      isAuthPage: true,
      onEnter: ["AuthService", function(AuthService) {
        AuthService.forgotUsername();
      }]
    })
    .state("tos", {
      url: "/terms-of-service",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/tos.html",
          controller: "TosController"
        }
      }
    })
    .state("privacyPolicy", {
      url: "/privacy-policy",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/privacy-policy.html",
          controller: "PrivacyController"
        }
      }
    })
    .state("/", {
      url: "/",
      isPublic: false,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/home.html",
          controller: 'HomeController'
        }
      }
    })
    .state("landing-page", {
      url: "/landing-page",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/landing.html",
          controller: 'HomeController'
        }
      }
    })
    .state("/textbooks", {
      url: "/textbooks",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/home.html",
          controller: 'HomeController'
        }
      }
    })
    .state("dashboard", {
      url: "/dashboard",
      isPublic: false,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/dashboard.html",
          controller: 'DashboardController'
        }
      }
    })
    .state("profile", {
      url: "/profile",
      isPublic: false,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/account-profile.html",
          controller: 'AccountProfileController'
        }
      }
    })
    .state("changePassword", {
      url: "/change-password",
      isPublic: false,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/account-change-password.html",
          controller: 'AccountChangePasswordController'
        }
      }
    })
    .state("inviteFriends", {
      url: "/invite-friends",
      isPublic: false,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/account-invite-friends.html",
          controller: 'AccountInviteFriendsController'
        }
      }
    })
    .state("faq", {
      url: "/faq/:selected",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/account-faq.html",
          controller: 'AccountFAQController',
          resolve: {
            articles: ["FaqService", function(FaqService) {
              return FaqService.getFAQ().then(function(res) {
                return res;
              })
            }]
          }
        }
      }
    })
    
    // Core Start From Here
    .state("news", {
      url: "/news",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/news.html",
          controller: "NewsController"
        }
      }
    })
    .state("newsDetail", {
      url: "/news/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.news.html",
          controller: "NewsController"
        }
      }
    })

    .state("jobs", {
      url: "/jobs/",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/jobs.html",
          controller: "JobsController"
        }
      }
    })

    .state("jobDetail", {
      url: "/job/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.job.html",
          controller: "JobsController"
        }
      }
    })

    /*** scholarships ***/
    .state("scholarship", {
      url: "/scholarships",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/scholarships.html",
          controller: "ScholarshipController"
        }
      }
    })
    .state("scholarshipDetail", {
      url: "/scholarship/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.scholarship.html",
          controller: "ScholarshipController"
        }
      }
    })
    .state("scholarshipSearch", {
      url: "/scholarships-search",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/scholarships.search.html",
          controller: "ScholarshipController"
        }
      }
    })
    /*** colleges ***/
    .state("colleges", {
      url: "/colleges",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.html",
          controller: "CollegeController"
        }
      }
    })
    .state("collegeDetail", {
      url: "/college/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.college.html",
          controller: "CollegeController"
        }
      }
    })
    .state("collegeSearch", {
      url: "/colleges/search",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.search.html",
          controller: "CollegeController"
        }
      }
    })
    .state("/campus-life", {
      url: "/colleges/:campuslife",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.campus-life.html",
          controller: "ArticleController"
        }
      }
    })
    .state("/types-of-scholarships", {
      url: "/scholarships/:campuslife",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.campus-life.html",
          controller: "ArticleController"
        }
      }
    })
    .state("/become-a-writer", {
      url: "/news/article/:campuslife",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.campus-life.html",
          controller: "ArticleController"
        }
      }
    })
    .state("/add-event-acricle", {
      url: "/events/:campuslife",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.campus-life.html",
          controller: "ArticleController"
        }
      }
    })
    .state("/post-a-job", {
      url: "/jobs/:campuslife",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.campus-life.html",
          controller: "ArticleController"
        }
      }
    })
    .state("collegeSearchWithTitle", {
      url: "/college-search/:title",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.search.html",
          controller: "CollegeController"
        }
      }
    })
    .state("collegeSearchWithTitleState", {
      url: "/college-search/:title/:state",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.search.html",
          controller: "CollegeController"
        }
      }
    })
    .state("collegeSearchWithTitleStateCountry", {
      url: "/college-search/:title/:state/:country",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.search.html",
          controller: "CollegeController"
        }
      }
    })
    /*** books ***/
    .state("books", {
      url: "/books",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/books.html",
          controller: "BookController"
        }
      }
    })
    .state("bookDetail", {
      url: "/book/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.book.html",
          controller: "BookController"
        }
      }
    })
    .state("bookSearch", {
      url: "/book-search/:title",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/search.book.html",
          controller: "BookController"
        }
      }
    })
    /*** events ***/
    .state("events", {
      url: "/events",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/events.html",
          controller: "EventController"
        }
      }
    })
    .state("eventDetail", {
      url: "/event/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.event.html",
          controller: "EventController"
        }
      }
    })

}]);

angular.module("config", [])
.constant("config", {"primo":{"host":"http://174.138.42.98/ucclaravel/public/index.php/api/","username":"intl_ott_api","password":"Xeb96d496Qa1f5","mediumImagePath":"http://174.138.42.98/ucclaravel/public/images/profile/thumbnail/150/","smallImagePath":"http://174.138.42.98/ucclaravel/public/images/category/thumbnail/300/","fullImagePath":"http://174.138.42.98/ucclaravel/public/images/category/thumbnail/800/","noImagePath":"resources/images/no-photo.jpg"},"google":{"analytics":{"trackingCode":"UA-89045079-2"}},"mparticle":{"key":"1158d52cbe59f74aba3dbe6352425071","isSandbox":true}});

app.run(["$injector", "$window", "$location", "$rootScope", "config", "SessionService", "$mdDialog", "UiService", "$state", "AuthService", "_", function ($injector, $window, $location, $rootScope, config, SessionService, $mdDialog, UiService, $state, AuthService, _) {
  //google analytics
  $window.ga('create', config.google.analytics.trackingCode, 'auto');
  $window.ga('require', 'ecommerce');

  $rootScope.$on('$stateChangeSuccess', function (event) {
    $window.ga('send', 'pageview', $location.path());
  });

  $rootScope.$on('destroy-session', function () {
    SessionService.destroySession();
  });

  SessionService.restoreSession();

  // Load UI config
  UiService.init();

  $rootScope.$on('$stateChangeStart', function (event, next, stateParamsOpts) {

    //close all opened md dialogs
    $mdDialog.cancel();

    //prevent access to login page when authenticated
    if (next.name === 'login' && SessionService.isAuthenticated()) {
      event.preventDefault();
      return $state.go('/');
    }

    if (next.isAuthPage) {
      UiService.isAuthPage(true);
    } else {
      UiService.isAuthPage(false);
    }

    if (next.isPublic === false) {
      
      if(next.name === '/'){
        event.preventDefault();
        return $state.go("landing-page");
      }

      if (SessionService.isAuthenticated()) { //authenticated user is transferred to last state
        if ($rootScope.lastState) {
          event.preventDefault();
          var lastState = $rootScope.lastState;
          delete $rootScope.lastState;
          return $state.go(lastState);
        }
      } else { //non authenticated user is transferred to login
        event.preventDefault();
        $rootScope.lastState = next.name;
        $rootScope.stateParams = stateParamsOpts || {};
        return AuthService.login();
      }
    }
  });

}]);

app.factory('sessionInterceptor', ["$rootScope", "$q", "$filter", "config", function ($rootScope, $q, $filter, config) {
  var sessionInterceptor = {
    request: function (cfg) {
      if (cfg.url == '/create-ticket' || cfg.url.includes('world-')) {
        cfg.headers['Content-Type'] = 'application/json';
      } else {
        cfg.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      }

      if (cfg.url.includes('/books/create')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/books-update')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/events/create')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/events-update')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/classified/create')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/classified-update')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/jobs/create')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/jobs-update')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/profile-image-update')) {
        cfg.headers['Content-Type'] = undefined;
      }

      return cfg;
    },
    response: function (response) {
      var deferred = $q.defer();
      if (response.data && response.data.error_codes && response.data.error_codes.length > 0) {
        //when invalid login token is received destroy session
        if (response.data && response.data.error_codes &&
          response.data.error_codes[0] == 'SE0007' ||
          response.data.error_codes[0] == 'SE0013' ||
          response.data.error_codes[0] == 'VV0031') {
          if (response.data.error_codes[0] == 'VV0031') {
            $rootScope.alert = {
              show: true,
              context: {type: 'danger', msg: response.data.user_errors}
            };
          }
          $rootScope.$broadcast('destroy-session');
          return $q.reject(response);
        }
        else if (response.data.error_codes[0] == 'AC0001' || //this is not an error, its epay user first login sms sent as password message
          response.data.error_codes[0] == 'AC0002' //first epay success login
        ) {

          if (response.data.error_codes[0] == 'AC0001') {
            $rootScope.alert = {
              show: true,
              context: {type: 'success', msg: response.data.user_errors}
            };
          }

          return $q.resolve(response);
        }
        //display error message
        else if (response.data && response.data.user_errors && response.data.user_errors.length > 0) {
          $rootScope.alert = {
            show: true,
            context: {type: 'danger', msg: response.data.user_errors[0]}
          };
          deferred.reject(response);
        }
      } else if (response.config.url.includes('GetPartnerToken')) {
        //braintree needs a client token as a single paramtere received from response...
        if (response.data.tokens && response.data.tokens.braintree) {
          response.data = response.data.tokens.braintree.token;
        }
        deferred.resolve(response);
      } else {
        deferred.resolve(response);
      }
      return deferred.promise;
    },
    responseError: function (rejection) {
      try {
        //when received invalid login token destroy session
        if (rejection.data.error_codes[0] == 'SE0013' || rejection.data.error_codes[0] == 'VV0031') {
          if (rejection.data.error_codes[0] == 'VV0031') {
            $rootScope.alert = {
              show: true,
              context: {type: 'danger', msg: rejection.data.user_errors}
            };
          }
          $rootScope.$broadcast('destroy-session');
          return $q.reject(rejection);
        } else if (rejection.data.error_codes[0] == 'AC0001' || //this is not an error, its epay user first login sms sent as password message
          rejection.data.error_codes[0] == 'AC0002' //first epay success login
        ) {

          if (rejection.data.error_codes[0] == 'AC0001') {
            $rootScope.alert = {
              show: true,
              context: {type: 'success', msg: rejection.data.user_errors}
            };
          }

          return $q.resolve(rejection);
        } else {
          // Error handler
          if (rejection.data && rejection.data && rejection.data.user_errors) {
            $rootScope.alert = {
              show: true,
              context: {type: 'danger', msg: UtilHelperService.uniCodeToString(rejection.data.user_errors[0])}
            };
          }
          return $q.reject(rejection);

        }

      } catch (e) {
        $rootScope.alert = {show: true, context: {type: 'danger', msg: $filter('translate')("FATAL_ERROR")}};

        return $q.reject(rejection);
      }
    }
  };

  return sessionInterceptor;
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller('RewardsListController', ["$scope", "$filter", "$state", "$rootScope", "PrimoService", "data", function ($scope, $filter, $state, $rootScope, PrimoService, data) {
  $scope.rewardItems = [];

  //populate incomplete on top
  angular.forEach(data.incomplete, function (item) {
    item.type = 'incomplete';
    $scope.rewardItems.push(item);
  });

  //populate completed rewards
  angular.forEach(data.complete, function (item) {
    item.type = 'complete';
    $scope.rewardItems.push(item);
  });

  /**
   * determines selected item from local storage saved menu state
   * @type {boolean}
   */
  $scope.selected = false;

  $scope.isMSISDNVerified = function() {
    var retVal = false;
    angular.forEach($scope.rewardItems, function(val, key) {
      if ($scope.rewardItems[key].reward_name == "MSISDN_VERIFIED" && $scope.rewardItems[key].type == 'complete') {
        retVal = true;
      }
    });

    return retVal;
  };

  /**
   /**
   * Set selected item
   * @param item
   */
  $scope.select = function (item, index) {
    if (item.type == 'complete') {
      PrimoService.getRewardDetails({reward_name: item.reward_name}).then(function (data) {
        $scope.selected          = data.data.reward;
        $scope.selected[0].type  = item.type;
        $scope.selected[0].image = item.icon_base64_data;
      });
    } else {
      if (item.reward_name == "REFERRAL_REFERRER") {
        $state.go('dashboard');
      } else if (item.reward_name == "ANNIVERSARY") {
        //nothing to do here
      } else {
        $state.go('account.profile');
      }
    }
  };
}]);


app.constant('ACCESS_NUMBERS', {
  countries: [
    {name: "Argentina", number: "(54) 1152354341"},
    {name: "Australia", number: "(61) 386720142"},
    {name: "Austria", number: "(43) 12650508"},
    {name: "Bahrain", number: "(973) 16568306"},
    {name: "Belgium", number: "(32) 42441081"},
    {name: "Brazil", number: "(55) 1130422466"},
    {name: "Bulgaria", number: "(359) 24950561"},
    {name: "Canada", number: "(1) 5873180558"},
    {name: "Chile", number: "(56) 448909168"},
    {name: "Costa Rica", number: "(506) 40003918"},
    {name: "Croatia", number: "(385) 18000058"},
    {name: "Cyprus", number: "(357) 77788851"},
    {name: "Czech Republic", number: "(420) 225852059"},
    {name: "Denmark", number: "(45) 78772165"},
    {name: "Estonia", number: "(372) 6148072"},
    {name: "Finland", number: "(358) 974790029"},
    {name: "France", number: "(33) 180140066"},
    {name: "Georgia", number: "(995) 706777066"},
    {name: "Germany", number: "(49) 22198203418"},
    {name: "Indonesia", number: "(62) 2151388807"},
    {name: "Italy", number: "(39) 0289982286"},
    {name: "Japan", number: "(81) 350506413"},
    {name: "Kenya", number: "(254) 205231029"},
    {name: "Latvia", number: "(371) 67881694"},
    {name: "Lithuania", number: "(370) 37248964"},
    {name: "Luxembourg", number: "(352) 20301092"},
    {name: "Malaysia", number: "(60) 1111460012"},
    {name: "Mexico", number: "(52) 8992748137"},
    {name: "Netherlands", number: "(31) 635205014"},
    {name: "Nigeria", number: "(234) 14405241"},
    {name: "Norway", number: "(47) 21930647"},
    {name: "Panama", number: "(507) 8387841"},
    {name: "Poland", number: "(48) 123950846"},
    {name: "Portugal", number: "(351) 308802553"},
    {name: "Romania", number: "(40) 317807014"},
    {name: "Slovakia", number: "(421) 322601091"},
    {name: "Slovenia", number: "(386) 18280302"},
    {name: "South Africa", number: "(27) 878250121"},
    {name: "South Korea", number: "(82) 7076860023"},
    {name: "Spain", number: "(34) 960130328"},
    {name: "Sri Lanka", number: "(94) 115322970"},
    {name: "Sweden", number: "(46) 812410575"},
    {name: "Switzerland", number: "(41) 445133012"},
    {name: "Taiwan", number: "(886) 985646934"},
    {name: "Turkey", number: "(90) 2129881726"},
    {name: "Ukraine", number: "(380) 893239917"},
    {name: "United Kingdom", number: "(443) 309981206"},
    {name: "United States", number: "(1) 6123602050"}
  ],
  us_cities: [
    {name: "Houston", number: "(1) 3468000018"},
    {name: "Chicago", number: "(1) 2242144242"},
    {name: "Atlanta", number: "(1) 4705750300"},
    {name: "Phoenix", number: "(1) 6234325638"},
    {name: "New York", number: "(1) 3473541919"},
    {name: "Los Angeles", number: "(1) 3238018181"},
    {name: "Miami", number: "(1) 7867761000"},
    {name: "New Jersey", number: "(1) 8568861000"},
    {name: "Boston", number: "(1) 8577571000"},
    {name: "San Francisco", number: "(1) 4154161000"},
    {name: "Philadelphia ", number: "(1) 4849541000"},
    {name: "Seattle", number: "(1) 4254261000"}
  ]
});
app.constant('COUNTRIES', [
  {
    name: "Argentina",
    iso: "AR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Australia",
    iso: "au",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Austria",
    iso: "AT",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Bahrain",
    iso: "BH"
  },
  {
    name: "Bangladesh",
    iso: "BD",
    un: "BGD",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: false,
    unlimited_for_country: true
  },
  {
    name: "Belgium",
    iso: "BE",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Brazil",
    iso: "BR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Bulgaria",
    iso: "BG",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Canada",
    iso: "CA",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Chile",
    iso: "CL",
    un: "",
    rural_excluded: true,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "China",
    iso: "CN",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Colombia",
    iso: "CO",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Costa Rica",
    iso: "CR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Croatia",
    iso: "HR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Cyprus",
    iso: "CY",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Czech Republic",
    iso: "CZ",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Denmark",
    iso: "DK",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Dominican Republic",
    iso: "DO",
    un: "DOM",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: true
  },
  {
    name: "Estonia",
    iso: "EE",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Finland",
    iso: "FI",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "France",
    iso: "FR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Georgia",
    iso: "GE"
  },
  {
    name: "Germany",
    iso: "DE",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Greece",
    iso: "GR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Guadeloupe",
    iso: "GP",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Guam",
    iso: "GU",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Hong Kong",
    iso: "HK",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Hungary",
    iso: "HU",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Iceland",
    iso: "IS",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "India",
    iso: "IN",
    un: "IND",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: true,
    unlimited_for_country: true
  },
  {
    name: "Indonesia",
    iso: "ID"
  },
  {
    name: "Ireland",
    iso: "IE",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Israel",
    iso: "IL",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Italy",
    iso: "IT",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Japan",
    iso: "JP",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Kazakhstan",
    iso: "KZ",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Kenya",
    iso: "KE"
  },
  {
    name: "Latvia",
    iso: "LV",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Lithuania",
    iso: "LT"
  },
  {
    name: "Luxembourg",
    iso: "LU",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Malaysia",
    iso: "MY"
  },
  {
    name: "Malta",
    iso: "MT",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Martinique",
    iso: "MQ",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Mexico",
    iso: "MX",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Morocco",
    iso: "MA",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Netherlands",
    iso: "NL",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "New Zealand",
    iso: "NZ",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Nigeria",
    iso: "NG",
    un: "NGA",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: false,
    unlimited_for_country: true
  },
  {
    name: "Norway",
    iso: "NO",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Panama",
    iso: "PA",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Pakistan",
    iso: "PK",
    un: "PAK",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: false,
    unlimited_for_country: true
  },
  {
    name: "Paraguay",
    iso: "PY",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Peru",
    iso: "PE",
    un: "",
    rural_excluded: true,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Philippines",
    iso: "PH",
    un: "PHL",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: false,
    unlimited_for_country: true
  },
  {
    name: "Poland",
    iso: "PL",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Portugal",
    iso: "PT",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Puerto Rico",
    iso: "PR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Romania",
    iso: "RO",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Singapore",
    iso: "SG",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Slovakia",
    iso: "SK",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Slovenia",
    iso: "SI",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "South Africa",
    iso: "ZA",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "South Korea",
    iso: "KR",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Spain",
    iso: "ES",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Sri Lanka",
    iso: "LK"
  },
  {
    name: "Sweden",
    iso: "SE",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Switzerland",
    iso: "CH",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Taiwan",
    iso: "TW",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Thailand",
    iso: "TH",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Turkey",
    iso: "TR"
  },
  {
    name: "Ukraine",
    iso: "UA"
  },
  {
    name: "United States",
    iso: "US",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "U.S. Virgin Islands",
    iso: "VI",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "United Kingdom",
    iso: "GB",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Venezuela",
    iso: "VE",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Vietnam",
    iso: "VN",
    un: "VNM",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: false,
    unlimited_for_country: true
  },
]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * compareTo directive
 *
 * This directive is used to compare values between two elements.
 * It is used to compare password - confirm password fields on registration, reset password, etc...
 */
app.directive("compareTo", function () {
  return {
    restrict: "A",
    require: "ngModel",
    scope: {
      otherModelValue: "=compareTo"
    },
    link: function (scope, element, attributes, ngModel) {
      ngModel.$validators.compareTo = function (modelValue) {
        return modelValue == scope.otherModelValue;
      };

      scope.$watch("otherModelValue", function () {
        ngModel.$validate();
      });
    }
  };
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Display received alerts in human readable form
 */
app.filter('formatAlert', function () {
  return function (alerts) {
    var retVal = "";
    if (alerts.constructor === Array) {
      angular.forEach(alerts, function (alert) {
        alert = alert.replace(/\/r/g, '"');
        retVal += alert + '\n';
      });
    } else {
      retVal = alerts;
    }

    return retVal;
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Display Primo DID in formatted manner.
 * Data is received as plain number 15187778057 and we want number to be formatted as "+ 1 (518) 777 - 8057"
 *
 */
app.filter('formatDid', function () {
  /**
   * Check if this is a number. In case of a number we format the value, otherwise return as it is
   * @param did
   * @returns {boolean}
   */
  function isPSTN(did) {
    var regex = new RegExp('^[0-9]{1,6}');
    return regex.test(did);
  }

  return function (did) {
    if (did.indexOf('@') != -1) {
      did = did.substr(0, did.indexOf('@'));
    }

    if (isPSTN(did)) {
      did = did.replace(/\D/g, "")
        .replace(/(\d{1})(\d{3})(\d{3})(\d{4})/, "+$1 ($2) $3 - $4");
    }

    return did;
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Minutes are rounded to 2 decimals
 */
app.filter('formatPrimoCreditMinutes', ["$filter", function ($filter) {
  return function (rate) {
    if (!rate) {
      return $filter('translate')('SEARCH_COUNTRY_CHECK');
    }

    var retVal = (parseFloat(rate).toFixed(1));
    if (retVal == parseInt(retVal)) retVal = parseInt(retVal);
    return retVal;
  }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Display Primo Rates in cents. Rates are rounded to 2 decimals
 * If rates ends with .0 it is rounded to fixed integer value
 */
app.filter('formatRate', ["$filter", function ($filter) {
  return function (rate) {
    if (!rate) {
      return $filter('translate')('FREE');
    }

    var retVal = (parseFloat(rate * 100).toFixed(1));
    if (retVal == parseInt(retVal)) retVal = parseInt(retVal);
    return retVal + '¢/' + $filter('translate')('min');
  }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Display Primo Rates in cents. Rates are rounded to 2 decimals
 * If rates ends with .0 it is rounded to fixed integer value
 */
app.filter('formatSMSRate', ["$filter", function ($filter) {
  return function (rate) {
    if (!rate) {
      return $filter('translate')('FREE');
    }

    var retVal = (parseFloat(rate * 100).toFixed(1));
    if (retVal == parseInt(retVal)) retVal = parseInt(retVal);
    return retVal + '¢/' + $filter('translate')('msg');
  }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.filter('formatHHMMSS', function () {
  function pad(num) {
    return ("0" + num).slice(-2);
  }

  return function (secs) {
    var minutes = Math.floor(secs / 60);
    secs        = secs % 60;
    var hours   = Math.floor(minutes / 60);
    minutes     = minutes % 60;
    return pad(hours) + ":" + pad(minutes) + ":" + pad(secs);
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Directive used to filter international rates
 */
app.filter('filteredRates', function () {
  return function (input, criteria) {
    var result = [];
    var regex  = new RegExp(criteria, 'i');
    angular.forEach(input, function (rate) {
      if (regex.exec(rate.country)) {
        return result.push(rate);
      }
    });
    return result;
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.filter('realTime', function () {
  /**
   * Returns my current time offset
   * @returns {number}
   */
  function getMyTimezoneOffset() {
    var d = new Date();
    return d.getTimezoneOffset();
  }

  function getServerTimeOffset() {
    return moment().tz("America/Los_Angeles")._offset;
  }

  return function (time, filter) {
    return moment(time * 1000 + (Math.abs(getServerTimeOffset() + getMyTimezoneOffset()))*60*1000).format(filter);
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Return profile image to png or default profile avatar
 */
app.filter('profileImageFilter', function () {
  return function (data) {
    return (data) ? 'data:image/png;base64,' + data : 'resources/images/profile_avatar.png';
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Strips whitespace from a value
 */
app.filter('stripWhitespace', ["$filter", function ($filter) {
  return function (value) {
    if (value) {
      return value.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    }
  }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.filter('withinDatesFilter', function () {
  return function(items, field, timeStart, timeEnd) {
    timeEnd = timeEnd || Date.now(); 
    return items.filter(function(item) {
      return (parseInt(item[field]) * 1000 > timeStart && parseInt(item[field]) * 1000 < timeEnd);
    });
  }
});
app.service('AuthService', ["$mdDialog", "$state", "PrimoService", "mParticleService", "SessionService", "$q", "$rootScope", "$filter", "$timeout", function ($mdDialog, $state, PrimoService, mParticleService,
                                     SessionService, $q, $rootScope, $filter, $timeout) {
  return {
    doVerifyUsername: function (user, successHandler) {
      PrimoService.verifyUsername({username: user.username, language: 'en'}).then(function (response) {
        if (successHandler) {
          successHandler(response, response.data.error_codes && response.data.error_codes[0] === 'AC0001');
        }
      });
    },
    login: function ($event) {
      return $mdDialog.show({
        templateUrl: 'partials/login.html',
        controller: 'LoginController',
        targetEvent: $event,
        fullscreen: true,
        onRemoving: function () {
          checkState('login');
        }
      });
    },
    doLogin: function (user, successHandler) {

      // attach UUID to "user" object
      //user['registration_id'] = _guid();
      var deferred = $q.defer();
      PrimoService.login(user)
        .success(function (data, status, headers) {
          deferred.resolve(data);
        }).error(function(err){

          deferred.reject(err);
        });
        return deferred.promise;
    },
    register: function ($event) {
      return $mdDialog.show({
        templateUrl: 'partials/register.html',
        controller: 'RegisterController',
        targetEvent: $event,
        fullscreen: true,
        onRemoving: function () {
          checkState('register');
        }
      });
    },
    doRegister: function (user, successHandler) {
      PrimoService.register(user).success(function (data, status, headers) {
        PrimoService.login(user).success(function (data, status, headers) {
          PrimoService.getAccountInfo({
            username: data.alias,
            login_token: data.login_token
          }).success(function (accountData) {
            SessionService.storeSession(data, accountData);

            if (successHandler) {
              if (angular.isObject(successHandler)) {
                if (successHandler.hasOwnProperty('withMSISDN') && accountData.msisdn) {
                  successHandler.withMSISDN();
                } else if (successHandler.hasOwnProperty('withoutMSISDN') && !accountData.msisdn) {
                  successHandler.withoutMSISDN();
                }
              } else {
                successHandler();
              }
            }

          });

        });
      });
    },
    doClaimAccount: function (claimNumber, token, user, successHandler) {
      var deferred = $q.defer();

      //prepare data for claim account call
      var dataToSend = {
        username: claimNumber,
        new_username: user.username,
        new_password: user.password,
        email: user.email,
        first_name: user.first_name,
        last_name: user.last_name
      };

      PrimoService.claimAccount(dataToSend, claimNumber, token)
        .success(function (data) {
          PrimoService.getAccountInfo({
            username: data.alias,
            login_token: data.login_token
          }).success(function (accountData) {
            SessionService.storeSession(data, accountData);
            if (successHandler) {
              successHandler();
            }

            deferred.resolve();
          });
        });
      return deferred.promise;
    },
    verifyMSISDN: function ($event) {
      return $mdDialog.show({
        templateUrl: "partials/verify-msisdn.html",
        controller: 'VerifyMSISDNController',
        targetEvent: $event,
        fullscreen: true,
        onRemoving: function () {
          checkState('verifyMSISDN');
        }
      });
    },
    forgotPassword: function ($event) {
      return $mdDialog.show({
        templateUrl: "partials/forgot-password.html",
        controller: 'ForgotPasswordController',
        targetEvent: $event,
        fullscreen: true,
        onRemoving: function () {
          checkState('forgotPassword');
        }
      });
    },
    forgotUsername: function ($event) {
      return $mdDialog.show({
        templateUrl: "partials/forgot-username.html",
        controller: 'ForgotUsernameController',
        targetEvent: $event,
        fullscreen: true,
        onRemoving: function () {
          checkState('forgotUsername');
        }
      });
    }
  };

  function checkState(name) {
    $timeout(function () {
      if ($state.is(name)) {
        $state.go('/')
      }
    })
  };

  /**
   * Generate UUID used as registration_id
   * @returns {string}
   */
  function _guid() {
    var d    = new Date().getTime();
    var uuid = 'web-app-xxxxxxxx-xxxxxxxx-xxxxxxxx-xxxxxxxx-xxxxxxxx-xxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d     = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });

    return uuid;
  };

}]);


app.factory('Base64', function () {
  var keyStr = 'ABCDEFGHIJKLMNOP' +
    'QRSTUVWXYZabcdef' +
    'ghijklmnopqrstuv' +
    'wxyz0123456789+/' +
    '=';
  return {
    encode: function (input) {
      var output                 = "";
      var chr1, chr2, chr3       = "";
      var enc1, enc2, enc3, enc4 = "";
      var i                      = 0;

      do {
        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
          enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
          enc4 = 64;
        }

        output = output +
          keyStr.charAt(enc1) +
          keyStr.charAt(enc2) +
          keyStr.charAt(enc3) +
          keyStr.charAt(enc4);
        chr1   = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
      } while (i < input.length);

      return output;
    },

    decode: function (input) {
      var output                 = "";
      var chr1, chr2, chr3       = "";
      var enc1, enc2, enc3, enc4 = "";
      var i                      = 0;

      // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
      var base64test = /[^A-Za-z0-9\+\/\=]/g;
      if (base64test.exec(input)) {
        alert("There were invalid base64 characters in the input text.\n" +
          "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
          "Expect errors in decoding.");
      }
      input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

      do {
        enc1 = keyStr.indexOf(input.charAt(i++));
        enc2 = keyStr.indexOf(input.charAt(i++));
        enc3 = keyStr.indexOf(input.charAt(i++));
        enc4 = keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
          output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
          output = output + String.fromCharCode(chr3);
        }

        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";

      } while (i < input.length);

      return output;
    }
  };
});
app.service('CountryService', ["$http", "_", "COUNTRIES", function ($http, _, COUNTRIES) {

  var service = {
    load: function () {
      return $http({
        method: 'GET',
        url: 'resources/countries/countryCodes.json'
      });
    },
    getCountryCodes: function () {
      return JSON.parse(document.getElementById('resources/countries/countryCodes.json').innerHTML);
    },
    getCountryByName: function (name) {
      return _.find(COUNTRIES, {name: name})
    },
    getCountryCodeByName: function (name) {
      var country = service.getCountryByName(name);
      if (country) {
        return country.iso;
      }
    }
  };

  return service;

}]);
app.service('FaqService', ["PrimoService", "$window", "$q", function (PrimoService, $window, $q) {
  return {
    /**
     * Calls primo service to load faq
     * If faq is loaded in last hour return data from localStorage
     * @returns {*}
     */
    getFAQ: function () {
      var deferred = $q.defer();
      //if value is cached more than one hour it will be refreshed
      if (!$window.localStorage.faqRefreshDate || (Date.now() - $window.localStorage.faqRefreshDate > 3600 * 1000)) {
        PrimoService.getFAQ().success(function (data) {
          $window.localStorage.faq            = JSON.stringify(data);
          $window.localStorage.faqRefreshDate = Date.now();

          deferred.resolve(data);
        });
      } else {
        deferred.resolve(JSON.parse($window.localStorage.faq));
      }

      return deferred.promise;
    }
  }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.service("GAManager", ["$window", function ($window) {
  return {
    /**
     * Send custom event to ga
     * @param cat
     * @param action
     * @param label
     */
    sendEventMessage: function (cat, action, label) {
      ga('send', {
        hitType: 'event',
        eventCategory: cat,
        eventAction: action,
        eventLabel: label
      });
    },
    sendConversion: function (amount) {
      $window.google_trackConversion({
        google_conversion_id: '870417705',
        google_conversion_language: 'en',
        google_conversion_format: '3',
        google_conversion_color: 'ffffff',
        google_conversion_label: 'UrjiCJWA42wQqYqGnwM',
        google_conversion_value: amount,
        google_conversion_currency: 'USD',
        google_remarketing_only: false
      });
    }
  }
}]);
app.service('InternationalRatesService', ["PrimoService", "$window", "$q", function(PrimoService, $window, $q) {
  return {
    /**
     * Calls primo service to load international rates
     * If rates are loaded in last hour return data from localStorage
     * @returns {*}
     */
    init: function() {
    },
    
  }
}]);
app.factory('_', ["$window", function ($window) {
  if(!$window._){
    // If lodash is not available you can now provide a
    // mock service, try to load it from somewhere else,
    // redirect the user to a dedicated error page, ...
    console.warn('Lodash is not available!')
  }

  var _ = $window._;

  // delete global lodash to prevent direct usage
  delete($window._);

  // return original
  return _;

}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.service('mParticleService', ["SessionService", "$rootScope", function(SessionService, $rootScope) {
  var service = {
    sendSignInEvent: function() {
      mParticle.logEvent('Sign in', mParticle.EventType.Other);
      mParticle.setUserIdentity(SessionService.getPUID(), mParticle.IdentityType.CustomerId);
      mParticle.setUserIdentity(SessionService.getEmail(), mParticle.IdentityType.Email);
      mParticle.setUserAttribute("Username", $rootScope.userId);
      mParticle.setUserAttribute("$FirstName", SessionService.getFirstName());
      mParticle.setUserAttribute("$LastName", SessionService.getLastName());
      mParticle.setUserAttribute("Signin Date", Date.now());
      mParticle.setUserAttribute("Signin Channel", "Web");
      mParticle.setUserAttribute("Primo US Phone Number", SessionService.getDID());
      mParticle.setUserAttribute("$Mobile", SessionService.getMSISDN());

      mParticle.setUserAttribute("Country Code", SessionService.getMSISDNCountryCode());
      mParticle.setUserAttribute("Email Address", SessionService.getEmail());
      mParticle.setUserAttribute("Credits Balance", SessionService.getCreditBalance());
      mParticle.setUserAttribute("Minutes Balance", SessionService.getAvailableMinutes());

      mParticle.setUserAttribute("Play notification sound", true);
      mParticle.setUserAttribute("Display chat notifications", true);

      mParticle.setUserAttribute("Residence Place", SessionService.getResidencePlace());
      mParticle.setUserAttribute("Date", SessionService.getDidExpiration());

      mParticle.setUserAttribute("Reward Minutes Balance", SessionService.getRewardMinutes());
      mParticle.setUserAttribute("Plan Minutes Balance", SessionService.getPlanMinutes());

      mParticle.setUserAttribute("Rate Plan", SessionService.getCurrentPlan());
      mParticle.setUserAttribute("Rate Plan Expiration", SessionService.getCurrentPlanExpirationDate());


    },
    sendSignUpEvent: function() {
      mParticle.logEvent('Sign up', mParticle.EventType.Other);
      mParticle.setUserIdentity(SessionService.getPUID(), mParticle.IdentityType.CustomerId);
      mParticle.setUserIdentity(SessionService.getEmail(), mParticle.IdentityType.Email);
      mParticle.setUserAttribute("$FirstName", SessionService.getFirstName());
      mParticle.setUserAttribute("$LastName", SessionService.getLastName());
      mParticle.setUserAttribute("Signup Date", Date.now());
      mParticle.setUserAttribute("Signup Channel", "Web");
      mParticle.setUserAttribute("Primo US Phone Number", SessionService.getDID());
      mParticle.setUserAttribute("$Mobile", SessionService.getMSISDN());
    },
    logEvent: function(text, type, data) {
      if (text && data) {
        mParticle.logEvent(text, type, data);
      } else {
        mParticle.logEvent(text, type);
      }
    },
    logPageView: function() {
      if (mParticle.logPageView) mParticle.logPageView();
    },

    logSearchEvent: function(title, searchTerm) {
      window.mParticle.logEvent(
        title,
        mParticle.EventType.Search,
        {'Search Term': searchTerm}
      );
    },
    logDestinationEvent: function(destination) {
      window.mParticle.logEvent(
        'International Rates - View Destinations',
        mParticle.EventType.Other,
        {
          'Destination': destination

        });
    },
    logBuyCreditEvent: function(value) {
      window.mParticle.logEvent(
        'Primo Credit - Buy Now',
        mParticle.EventType.Other,
        {
          'Price': value

        });
    },
    logMonthlyBuyNowEvent: function(referrer, price) {
      window.mParticle.logEvent(
        'Monthly Minute Plans - Buy Now',
        mParticle.EventType.Other,
        {
          'Plan Referrer': referrer,
          'Price': price

        });
    },
    logEditProfile: function(data) {
      window.mParticle.logEvent('Edit Profile', mParticle.EventType.UserPreference);

      window.mParticle.setUserAttribute("$FirstName", data.first_name);
      window.mParticle.setUserAttribute("$LastName", data.last_name);
      window.mParticle.setUserIdentity($rootScope.userId, mParticle.IdentityType.Other);
      window.mParticle.setUserAttribute("Signup Date", Date.now());
      window.mParticle.setUserAttribute("Signup Channel", "Web");
      window.mParticle.setUserAttribute("$Mobile", data.msisdn);
      window.mParticle.setUserAttribute("Primo US Phone Number", data.direct_dial_in);
      window.mParticle.setUserAttribute("Country Code", data.home_country);
      window.mParticle.setUserAttribute("Email Address", data.email);
      window.mParticle.setUserAttribute("Birthplace", data.home_city);
      window.mParticle.setUserAttribute("Credits Balance", data.balance);
      window.mParticle.setUserAttribute("Minutes Balance", data.intl_minutes);
    },

    addToCartCredit: function(product, isRenewal) {
      window.mParticle.eCommerce.setCurrencyCode("USD");


      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
          // attributes
          {
            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal
          }
        );

      window.mParticle.eCommerce.Cart.add(mpProduct, true)
    },
    addToCartDid: function(product, isRenewal) {
      window.mParticle.eCommerce.setCurrencyCode("USD");


      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
          // attributes
          {
            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal
          }
        );

      window.mParticle.eCommerce.Cart.add(mpProduct, true)
    },
    addToCartUnlimited: function(product, isRenewal) {
      window.mParticle.eCommerce.setCurrencyCode("USD");


      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
          // attributes
          {
            //set for Primo Unlimited and Primo Monthly Minute Plans
            'Minutes': product.minutes.rated,

            //set for Primo Unlimited
            'Primo to Primo': product.minutes.cost,

            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal
          }
        );

      window.mParticle.eCommerce.Cart.add(mpProduct, true)
    },
    addToCartMonthly: function(product, isRenewal) {
      window.mParticle.eCommerce.setCurrencyCode("USD");

      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
// attributes
          {
            //set for Primo Unlimited and Primo Monthly Minute Plans
            'Minutes': product.minutes.rated,
            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal,
            //set for Primo Monthly Minute Plans
            'Per Minute Rate': product.minutes.cost
          }
        );

      window.mParticle.eCommerce.Cart.add(mpProduct, true)
    },
    removeFromCartCredit: function(product, isRenewal) {

      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
          // attributes
          {
            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal
          }
        );

      window.mParticle.eCommerce.Cart.remove(mpProduct, true)
    },
    removeFromCartDid: function(product, isRenewal) {
      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
          // attributes
          {
            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal
          }
        );

      window.mParticle.eCommerce.Cart.remove(mpProduct, true)
    },
    removeFromCartMonthly: function(product) {

      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity,
          null,
          null,
          null,
          null,
          null,
          {
            //set for Primo Unlimited and Primo Monthly Minute Plans
            'Minutes': product.minutes.rated,
            //set for Primo Monthly Minute Plans
            'Per Minute Rate': product.minutes.cost
          }
        );

      window.mParticle.eCommerce.Cart.remove(mpProduct, true)
    },
    removeFromCartUnlimited: function(product) {

      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity,
          null,
          null,
          null,
          null,
          null,
          {
            //set for Primo Unlimited and Primo Monthly Minute Plans
            'Minutes': product.minutes.rated,
            //set for Primo Unlimited
            'Primo to Primo': product.minutes.cost
          }
        );

      window.mParticle.eCommerce.Cart.remove(mpProduct, true)
    },

    logTransaction: function(data, total, items, isRenew) {
      window.mParticle.eCommerce.setCurrencyCode("USD");


      var mpProduct = items.map(function(item) {
        return window.mParticle.eCommerce
          .createProduct(
            item.name,
            item.id, //sku
            item.value, //price
            1, //quantity
            null,
            null,
            null,
            null,
            null,
// attributes
            {
              //set for Primo Unlimited and Primo Monthly Minute Plans
              'Minutes': item.minutes.rated,

              //set for Primo Unlimited
              'Primo to Primo': item.minutes.cost,

              //set for Primo Monthly Minute Plans
              'Per Minute Rate': item.minutes.cost
            }
          );
      });

      var transactionId = "";
      if (data && data.purchases && data.purchases[0] && data.purchases[0].transaction) {
        transactionId = data.purchases[0].transaction;
      }

      var transactionAttributes = window.mParticle.eCommerce
        .createTransactionAttributes(
          transactionId, // id
          total, // revenue
          0
        );


      // logs a Purchase using products in cart
      window.mParticle.eCommerce
        .logPurchase(transactionAttributes, mpProduct, true,
          {
            'Purchase Channel': 'Web',
            'Payment Method' : 'Braintree',
            'Auto-Renewal': isRenew
          }
        );
    }
  };

  return service;
}]);
app.service('phoneUtils', ["$window", function ($window) {
  return $window.phoneUtils;
}]);
app.service('PrimoService', ["$http", "$state", "$rootScope", "config", "Base64", "$httpParamSerializerJQLike", function($http, $state, $rootScope, config, Base64, $httpParamSerializerJQLike) {

  var token = localStorage.getItem('token');
  if(token){
    $http.defaults.headers.common['Authorization'] = token;
  }

  return {
    verifyUsername: function(user) {
      return $http({
        method: 'POST',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__VerifyUsername',
        data: $httpParamSerializerJQLike(user)
      });
    },
    getClientToken: function() {
      var user         = {};
      user.login_token = $rootScope.token;
      user.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/billingintl/3/primo/api/billingintl__ConveyCreditCardRegistration',
        data: $httpParamSerializerJQLike(user)
      });
    },
    login: function(user) {
      return $http({
        method: 'POST',
        url: config.primo.host + 'auth/login',
        data: $httpParamSerializerJQLike(user)
      });
    },
    logout: function() {
      var user         = {};
      user.login_token = $rootScope.token;
      user.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__LogoutPinlessAccount',
        data: $httpParamSerializerJQLike(user)
      });
    },
    
    verifyMSISDN: function(data) {
      data.login_token = $rootScope.token;
      data.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__VerifyMSISDN',
        data: $httpParamSerializerJQLike(data)
      });
    },
    getAccountInfo: function(user) {
      return $http({
        method: 'POST',
        url: config.primo.host + 'get-profile-info',
        data: $httpParamSerializerJQLike(user)
      });
    },
    
    setAccountInfo: function(user) {
      user.login_token = $rootScope.token;
      user.username    = $rootScope.userId;
      delete user.partner;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__SetPinlessAccountFields',
        data: $httpParamSerializerJQLike(user)
      });
    },
    
    searchUsers: function(user) {
      user.login_token = $rootScope.token;
      user.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/directoryintl/3/primo/api/directoryintl__SearchUserDirectory',
        data: $httpParamSerializerJQLike(user)
      });
    },
    changePassword: function(contact) {
      contact.login_token = $rootScope.token;
      contact.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__ChangePinlessAccountPassword',
        data: $httpParamSerializerJQLike(contact)
      });
   },
    inviteFriends: function(data) {
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      
      return $http.post(config.primo.host+'invite-friends-via-email',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    
    getPublicURLs: function(data) {
      return $http({
        method: 'GET',
        url: config.primo.host + '/pr/publicintl/3/primo/api/publicintl__GetPublicURLs'
      })
    },
    resetPassword: function(data) {
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__ResetPinlessAccountPassword',
        data: $httpParamSerializerJQLike(data)
      })
    },
    submitDealerApplication: function(data) {
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/dealer/3/primo/api/dealer__SubmitApplication',
        data: $httpParamSerializerJQLike(data)
      })
    },
    recoverUsername: function(data) {
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__RecoverUsername',
        data: $httpParamSerializerJQLike(data)
      })
    },
    deactivateAccount: function(data) {
      data.login_token = $rootScope.token;
      data.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__DeletePinlessAccount',
        data: $httpParamSerializerJQLike(data)
      })
    },
    getFAQ: function() {
      return $http({
        method: 'GET',
        url: '/list-faq'
      })
    },
    /*** Code Start From Here*/
    GetBookCategories: function(){
      return $http({
        method: 'GET',
        url: config.primo.host + 'books/get-book-categories',
      });
    },
    GetBookCategoriesWithParent: function(){
      return $http({
        method: 'GET',
        url: config.primo.host + 'books/get-book-categories-with-parent',
      });
    },
    // GetBooks: function(data){
    //   return $http({
    //     method: 'GET',
    //     url: config.primo.host + 'get-module/books?page='+data,
    //   });
    // },

    Books: function(lastpage) {
      return $http({
        method: 'GET',
        url: config.primo.host + 'get-module/books?page='+lastpage,
      });
          
    },
    getNews: function(){
      return $http.get(config.primo.host +'get-news').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    News: function(lastpage){
      return $http.get(config.primo.host +'get-module/news?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    singleNews: function(slug){
      return $http.get(config.primo.host +'get-single-module/news/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    newsActivity: function(id,action){
      return $http.get(config.primo.host +'news-activity/'+id+'/'+action).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    getNewsByCategory: function(slug){
      return $http.get(config.primo.host +'get-category-news/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    newsLoadMoreData: function(lastpage){
      return $http.get(config.primo.host +'get-news?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    /*** Jobs service ****/
    searchJobsBasedOnLocation: function(locations,data){
            var searchText= data.searchText ? data.searchText :'';
            var location= data.location ? data.location :'';
            var selectedExperience= data.selectedExperience ? data.selectedExperience :0;
            var selectedSalary= data.selectedSalary ? data.selectedSalary :0;

            return $http.post(config.primo.host + 'job-on-location', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Jobs: function(lastpage){
      return $http.get(config.primo.host+'get-module/jobs?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    jobDescriptionBySlug: function(slug){
      return $http.get(config.primo.host + 'job-description/' + slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    jobApply: function(data){
      return $http.post(config.primo.host + 'apply-job', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    jobAddToFavorite: function(data){
      return $http.post(config.primo.host + 'add-job-to-favourite', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    seeRecruiterNumber: function(jobId){
      var data={
                jobid:jobId
            };
         return $http.post(config.primo.host+'get-recuiter-details',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    globalJobSearch: function(searchText,location,selectedExperience,selectedSalary){
      var data={
                location:location,
                searchText:searchText,
                selectedSalary:selectedSalary,
                selectedExperience:selectedExperience
            };
            return $http.post(config.primo.host + 'jobs/search',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    Job: function(slug){
      return $http.get(config.primo.host + 'get-single-module/jobs/' + slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    UserJobs: function(lastpage){
      return $http.get(config.primo.host + 'get-module/jobs?page=' + lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    searchJob: function(data){
      return $http.post(config.primo.host + 'jobs/search', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    loadMoreData:function(lastpage, data) {
        return $http.post(config.primo.host + 'get-jobs?page=' + lastpage, $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    GetUserJobs: function(lastpage){
        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }

        return $http.get(config.primo.host+'get-user-jobs?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    CreateJobs: function(data){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'jobs/create', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    UpdateJobs: function(data,job_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'jobs-update/'+job_id+'/update', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    DeleteJobs: function(job_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      
      return $http.post(config.primo.host+'jobs/'+job_id+'/delete').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    /*****Scholarship Service *******/
    loadTopScholarship:function(){
          return $http.get(config.primo.host + 'get-module/scholarship?featured=true').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
      
    Scholarships: function(lastpage) {
         return $http.get(config.primo.host+'get-module/scholarship?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Scholarship: function(slug) {

          return $http.get(config.primo.host+'get-single-module/scholarship/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    searchScholarship: function(lastpage,college){
        return $http.get(config.primo.host+'get-module/scholarship?page='+lastpage+'&college='+college).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    testimonialsTeam: function(){
          // return $http.get(config.primo.host+'sampleData/testimonialsTeam.json').catch(function(response){
          //       if(response.status == 500){
          //         localStorage.removeItem('token'); 
          //         alert("Session has expired, please login again");
          //         $state.go('login')
          //       }
          //     });
    },

    /**** Colleges Service *****/
    getAllColleges: function(){
      return $http.get(config.primo.host+'get-all-colleges').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    leftNavigationColleges: function() {
       return $http.get(config.primo.host+'sampleData/leftSidenavigation.json').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    beseColleges: function(){
       return $http.get(config.primo.host+'sampleData/bestCollegeData.json').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    searchColleges: function(searchText){
        return $http.get(config.primo.host+'get-module/colleges?searchText='+searchText).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    sortCollegeByAlphabates: function(lastpage) {
       return $http.get(config.primo.host+'college-sort-data?sort=asend').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Colleges: function(lastpage) {
       return $http.get(config.primo.host+'get-module/colleges?page='+lastpage+"&best=true").catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    College: function(slug) {
        return $http.get(config.primo.host+'get-single-module/colleges/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    AddToWishList: function(id) {
        return $http.get(config.primo.host+'college/add-to-wishlist/'+id).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    searchCollegeByLocation: function(data){
        var data = {city:data.cureentAddress};
        return $http.post(config.primo.host+'get-module/colleges?page=1',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    collegeSearchPage: function(data){
      return $http.get(config.primo.host+'get-module/colleges?page='+lastpage+"&best=true").catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    searchCollegesWithTitleStateCountry: function(data,lastpage){
      return $http.get(config.primo.host+'get-module/colleges?page='+lastpage+"&title="+data.title+"&state="+data.state+"&country="+data.country).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    /**** book service *****/

    sendAMessage: function(bookId){
        return $http.get(config.primo.host+'sendMessage',{bookid:bookId}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    cateAndSubcatList: function(){
        return $http.get(config.primo.host+'sampleData/textbookLeftSidecategory.json').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    
    customeBroughtTextBooks: function(slug){
       return $http.get(config.primo.host+'sampleData/customeBoughtTextbook.json').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetBooks: function(lastpage,category_id) {
       return $http.get(config.primo.host+'get-module/books?page='+lastpage+'&category_id='+category_id).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    GetBooksByTitle: function(lastpage,title) {
       return $http.get(config.primo.host+'get-module/books?page='+lastpage+'&title='+title).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    GetBook: function(slug) {
        return $http.get(config.primo.host+'get-single-module/books/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    searchTextBook: function(searchText){
       return $http.get(config.primo.host+'sampleData/customeBoughtTextbook.json').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    loadMoreTextBooks: function(lastpage){
        return $http.get(config.primo.host+'get-module/books?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              }); 
    },

    GetUserBooks: function(lastpage){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
        return $http.get(config.primo.host+'get-user-books?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    CreateBooks: function(data){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'books/create', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    UpdateBooks: function(data,book_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'books-update/'+book_id+'/update', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    DeleteBooks: function(book_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      
      return $http.post(config.primo.host+'books/'+book_id+'/delete').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    
    /**** event service *****/

    Events: function(lastpage) {
       return $http.get(config.primo.host+'get-module/events?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Event: function(slug) {
        return $http.get(config.primo.host+'get-single-module/events/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    CreateEvent: function(data) {
        return $http.post(config.primo.host+'events/create',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    UserEvents: function(lastpage){
        return $http.get(config.primo.host+'get-module/events?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Update: function(data){
        return $http.post(config.primo.host+'events/'+data.id+'/update',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetUserEvents: function(lastpage){

        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }

        return $http.get(config.primo.host+'get-user-events?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    CreateEvents: function(data){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'events/create', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    UpdateEvents: function(data,event_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'events-update/'+event_id+'/update', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    DeleteEvents: function(event_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      
      return $http.post(config.primo.host+'events/'+event_id+'/delete').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    getEventsHost: function(data){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      return $http.get(config.primo.host+'events/get-events-host').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    AddEventHost: function(data){

        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }

        return $http.post(config.primo.host+'events/host/create',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    //searchEvents: function(data,lastpage = 1){
        // return $http.get(config.primo.host +'events/search?page='+lastpage+"&title="+data.title+"&state="+data.state+"&date="+data.date).catch(function(response){
        //         if(response.status == 500){
        //           localStorage.removeItem('token'); 
        //           alert("Session has expired, please login again");
        //           $state.go('login')
        //         }
        //       });
        
    //},

    /**** Profile Service *********/
    GetAll: function() {
            return $http.get(config.primo.host+'users').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetById: function(id) {
        return $http.get(config.primo.host+'users/' + id).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Profile: function(data) {
      
      return $http.post(config.primo.host+'get-profile-info',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    updateInfo: function(data){
        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }
        
        return $http.post(config.primo.host+'users',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    updateProfileImage: function(data){

      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      fd.append('image', data);
      
      return $http.post(config.primo.host+'profile-image-update', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetByUsername: function(username) {
        return $http.post(config.primo.host+'auth/login',username).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    CreateRegistration: function(data) {
        return $http.post(config.primo.host+'auth/register', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    UserUpdate: function(data) {
        return $http.put(config.primo.host+'users/' + user.id, $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Delete: function(id) {
        return $http.delete(config.primo.host+'users/' + id).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetState: function(){
        return $http.get(config.primo.host+'get/state').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetEducationLevel: function(){
        return $http.get(config.primo.host+'get/education-level').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetStateEducationlevelCollege: function(){
        return $http.get(config.primo.host+'get/signup-step-one').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    ForgetPasswordRequest: function (data) {
        return $http.post(config.primo.host +'password/email', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    ResetPasswordRequest: function (data) {
        return $http.post(config.primo.host +'password/reset', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    getInbox: function() {
        return $http.post(config.primo.host + 'get-inbox').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    getContactUser: function() {
        return $http.post(config.primo.host + 'get-contact-user').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    getGroup: function(userId) {
        var data = {user: userId};
        return $http.post(config.primo.host + 'get-group', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    sendMessage: function(data) {
        return $http.post(config.primo.host + 'send-message', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    /* Classified Service */

    GetUserClassified: function(lastpage){

        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }

        return $http({
          method: 'GET',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          url: config.primo.host + 'get-user-classified?page='+lastpage,
        });
    },

    CreateClassified: function(data){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'classified/create', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    UpdateClassified: function(data,classfied_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'classified-update/'+classfied_id+'/update', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    DeleteClassified: function(classfied_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      
      return $http.post(config.primo.host+'classified/'+classfied_id+'/delete').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    /* Get All Single Module By Id */

    GetSingleModuleById: function(module,id){

        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }

        return $http({
          method: 'GET',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          url: config.primo.host + 'get-single-module-by-id/'+module+'/'+id,
        });
    },
    
    /**** Get Article ******/

    getArticle: function(slug){
      return $http.get(config.primo.host+'get/article/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    }
  } 
}]);
app.service('PublicURLService', ["PrimoService", function (PrimoService) {
  return {
    load: function () {
      // return PrimoService.getPublicURLs().then(function (data) {
      //   return data;
      // })
    }
  }
}]);
app.service('selectColleges', ["$http", "$q", "config", function ($http, $q, config) {

  var urlBase = 'http://countrylistapi.apphb.com/api/';
  return ({
      getCountries: getCountries
  });

  function getCountries(searchKey, pageNumber) {
      var deferred = $q.defer();
      var request = $http({
                      method: "GET",
                      url: config.primo.host + "select-colleges",
                      params: { searchKey: searchKey, pageNumber: pageNumber }
                    });
      return (request.then(handleSuccess, handleError));

  }

  function handleError(response) {

      if (
          !angular.isObject(response.data) ||
          !response.data.message
          ) {
          return ($q.reject("An unknown error occurred."));
      }
      return ($q.reject(response.data.message));
  }


  function handleSuccess(response) {
      return (response.data);
  }

}]);
app.service('ServiceProductsService', ["PrimoService", "$window", "$q", function(PrimoService, $window, $q) {
  return {
    /**
     * Calls primo service to load international rates
     * If rates are loaded in last hour return data from localStorage
     * @returns {*}
     */
    loadDidAndCreditProducts: function(promo) {
      var deferred = $q.defer();
      var lastTime = $window.localStorage.didAndCreditProductsRefreshDate;
      var timePass = Date.now() - lastTime;

      console.log('didAndCreditProductsRefreshDate last time: ' + lastTime + ' and timePass: ' + timePass);
      //if value is cached more than one hour it will be refreshed
      if (!$window.localStorage.didAndCreditProductsRefreshDate || (timePass > 360000)) {
        console.log('didAndCreditProductsRefreshDate refreshing..');
        PrimoService.getServiceProducts(promo).then(function(data) {
          $window.localStorage.didAndCreditProducts            = JSON.stringify(data.data);
          $window.localStorage.didAndCreditProductsRefreshDate = Date.now();

          deferred.resolve(data.data);
        });
      } else {
        deferred.resolve(JSON.parse($window.localStorage.didAndCreditProducts));
      }

      return deferred.promise;
    },

    loadRatePlanProducts: function() {
      var deferred = $q.defer();
      var lastTime = $window.localStorage.ratePlanProductsRefreshDate;
      var timePass = Date.now() - lastTime;

      console.log('ratePlanProductsRefreshDate last time: ' + lastTime + ' and timePass: ' + timePass);

      if (!$window.localStorage.ratePlanProductsRefreshDate || (timePass > 360000)) {
        //get countries products
        PrimoService.getServiceProductsCountry().then(function(data) {
          $window.localStorage.ratePlanProducts            = JSON.stringify(data.data);
          $window.localStorage.ratePlanProductsRefreshDate = Date.now();
          deferred.resolve(data.data);
        });
      } else {
        deferred.resolve(JSON.parse($window.localStorage.ratePlanProducts));
      }

      return deferred.promise;
    },

    /**
     * Fetch the list of popular countries
     */
    getPopularCountries: function(promo) {
      var deferred = $q.defer();

      var retVal = [];
      this.getServiceProductsForCountryWithPromoCode(promo).then(function(data) {
        if (data.groups) {
          angular.forEach(data.groups[0].countries, function(val, key) {
            if (val.popular == true) {
              retVal.push(val);
            }
          });
        }
        deferred.resolve(retVal);
      });

      return deferred.promise;
    },

    /**
     * Returns a complete list of serviceProducts
     * @returns {Array}
     */
    getServiceProducts: function() {
      return ($window.localStorage.didAndCreditProducts) ? JSON.parse($window.localStorage.didAndCreditProducts) : [];
    },

    /**
     * Fetch DID products from local storage
     */
    getDidProducts: function(promo) {
      var deferred = $q.defer();
      var retVal   = [];
      this.loadDidAndCreditProducts(promo).then(function(data) {
        angular.forEach(data.groups, function(val, key) {
          if (val.group === "DID") {
            retVal = val.products;
            console.log("did product:: ");
            console.log(val.products);
          }
        });

        deferred.resolve(retVal);
      });
      return deferred.promise;
    },


    /**
     * Fetch the list of Credit products
     * @returns {*|promise}
     */
    getCreditProducts: function(promo) {
      var deferred = $q.defer();
      var retVal   = [];
      this.loadDidAndCreditProducts(promo).then(function(data) {
        angular.forEach(data.groups, function(val, key) {
          if (val.group === "CREDIT") {
            retVal = val.products;
          }
        });

        deferred.resolve(retVal);
      });
      return deferred.promise;
    },
    /**
     * Call services to fetch Service products with promotion for specific promo code
     * @param  {[type]} promo [description]
     * @return {[type]}       [description]
     */
    getServiceProductsForCountryWithPromoCode: function(promo) {
      var deferred = $q.defer();

      if (!$window.localStorage.ratePlanProductsRefreshDate || (Date.now() - $window.localStorage.ratePlanProductsRefreshDate > 3600 * 1000)) {
        //get countries products
        PrimoService.getServiceProductsCountry(promo).then(function(data) {
          $window.localStorage.ratePlanProducts            = JSON.stringify(data.data);
          $window.localStorage.ratePlanProductsRefreshDate = Date.now();
          deferred.resolve(data.data);
        });
      } else {
        deferred.resolve(JSON.parse($window.localStorage.ratePlanProducts));
      }
      return deferred.promise;
    },

    /**
     * Returns the list of rate plans
     * @returns {Array}
     */
    getRatePlanProducts: function() {
      return ($window.localStorage.ratePlanProducts) ? JSON.parse($window.localStorage.ratePlanProducts) : [];
    }
  }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.service("SessionService", ["$rootScope", "$window", "$injector", "UtilHelperService", function($rootScope, $window, $injector, UtilHelperService) {
  var service = {
    storeSession: function(data, user) {
      if (data.alias && data.refresh_token) {
        $rootScope.data = data;
      }

      if (data.login_token) {
        $rootScope.token = data.login_token;
      }

      if (data.alias) {
        $rootScope.userId = data.alias;
      }

      if (user) {
        $rootScope.user            = user;
        $rootScope.user.first_name = UtilHelperService.uniCodeToString($rootScope.user.first_name);
        $rootScope.user.last_name  = UtilHelperService.uniCodeToString($rootScope.user.last_name);
      }

      $window.localStorage.token  = $rootScope.token;
      $window.localStorage.userId = $rootScope.userId;
      $window.localStorage.data   = JSON.stringify($rootScope.data);
      $window.localStorage.user   = JSON.stringify($rootScope.user);
    },
    destroySession: function() {
      var state = $injector.get('$state');
      delete $rootScope.token;
      delete $rootScope.userId;
      delete $rootScope.user;

      delete $window.localStorage.token;
      delete $window.localStorage.data;
      delete $window.localStorage.userId;
      delete $window.localStorage.user;
      delete $window.localStorage.contacts;

      // Manual inject
      if (state.current.name !== '/') {
        state.go('/');
      }
    },
    updateSession: function(user) {
      //copy all properties from user
      angular.forEach(user, function(val, key) {
        $rootScope.user[key] = val;
      });
      $window.localStorage.user = JSON.stringify($rootScope.user);
    },
    restoreSession: function() {
      if ($window.localStorage.token) {
        $rootScope.token  = $window.localStorage.token;
        $rootScope.userId = $window.localStorage.userId;
        $rootScope.user   = JSON.parse($window.localStorage.user).data;
        //$rootScope.data   = JSON.parse($window.localStorage.data);

        return true;
      } else {
        return false;
      }
    },
    isAuthenticated: function() {
      if (!$rootScope.userId) {
        return this.restoreSession();
      } else {
        return true;
      }
    },
    getPUID: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.partner.PUID;
      }
    },
    getEmail: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.email;
      }
    },
    getUsername: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.alias;
      }
    },
    getFirstName: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.first_name;
      }
    },
    getLastName: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.last_name;
      }
    },
    getDID: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.did;
      }
    },
    getMSISDN: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.msisdn;
      }
    },
    getMSISDNCountryCode: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.msisdn_country_code;
      }
    },
    getCreditBalance: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.balance;
      }
    },
    getAvailableMinutes: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.intl_minutes;
      }
    },
    getRewardMinutes: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.free_minutes) ? user.free_minutes : 0;
      }
    },
    getPlanMinutes: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.plan_minutes) ? user.plan_minutes : 0;
      }
    },
    getDidExpiration: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.did_expires;
      }
    },
    getResidencePlace: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.residence_location_string) ? user.residence_location_string : "-";
      }
    },
    getCurrentPlan: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.current_plan) ? user.current_plan.short_title : "-";
      }
    },
    getCurrentPlanExpirationDate: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.current_plan && user.current_plan.expires) ? new Date(user.current_plan.expires * 1000) : "-";
      }
    },
    hasActiveUnlimitedPlan: function(ratePlan) {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.plan_state && user.plan_state == "Active" && user.current_plan && user.current_plan.plan != ratePlan.product);
      }
    }
  };

  return service;
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.service("UiService", ["$rootScope", "$window", "$translate", "SessionService", "$mdSidenav", "InternationalRatesService", "ServiceProductsService", "$mdDialog", "deviceDetector", "$mdMedia", "PublicURLService", "CountryService", function ($rootScope, $window, $translate, SessionService, $mdSidenav,
                                   InternationalRatesService,
                                   ServiceProductsService,
                                   $mdDialog,
                                   deviceDetector,
                                   $mdMedia,
                                   PublicURLService,
                                   CountryService) {
  return {
    init: function () {
      moment.tz.add('America/Los_Angeles|PST PDT|80 70|0101|1Lzm0 1zb0 Op0');
      //load country list
      $rootScope.countries = CountryService.getCountryCodes();

      //load international rates
      InternationalRatesService.init();

      $rootScope.deviceDetector = deviceDetector;

      //load public routes
      // PublicURLService.load().then(function (data) {
      //   $rootScope.publicURLs = data;
      // });

      // Alert-box UI params
      $rootScope.alert = {show: false, context: {}};

      // Logout fn
      $rootScope.logout = function () {
        SessionService.destroySession();
      };

      //initialize i18n
      $translate.use('en_EN');
      $rootScope.activeLanguage = 'en_EN';
      $rootScope.UiService      = this;
    },
    /**
     * Smooth scroll to element
     * @param el
     */
    scrollToElement: function (el, detract) {
      detract = detract || 150;
      $('html, body').animate({
        scrollTop: $(el).offset().top - detract
      }, 700, 'easeInOutCubic');
    },
    isAuthPage: function (isAuthPage) {
      $rootScope.isAuthPage = isAuthPage;
    },
    /**
     * Display alert message
     * @param title
     * @param text
     */
    showAlert: function (title, text) {
      $rootScope.alert = {
        show: true,
        context: {title: title, msg: text}
      };
    },

    /**
     * Return detected device type
     */
    isAndroidDevice: function () {
      return $rootScope.deviceDetector.raw.device.android;
    },
    isIOSDevice: function () {
      return ($rootScope.deviceDetector.raw.device.iphone || $rootScope.deviceDetector.raw.device.ipad);
    },
    showAlertDialog: function (controller, templateUrl, clb, filter) {
      $mdDialog.show({
        controller: controller,
        templateUrl: templateUrl,
        clickOutsideToClose: true,
        resolve: {
          filter: function () {
            return filter;
          }
        }
      }).then(function (val) {
        clb(val);
      }, function (val) {
        clb(val);
      });

    },
    showConfirmDialog: function (controller, templateUrl, clb) {
      $mdDialog.show({
        controller: controller,
        templateUrl: templateUrl,
        clickOutsideToClose: false
      }).then(function (answer) {
        clb(answer);
      }, function () {
        clb(false);
      });
    }
  };
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.service('UtilHelperService', function () {
  var service = {
    /**
     * Converts unicode to string
     * @param source
     * @returns {*}
     */
    uniCodeToString: function (source) {
      var r = /\\u([\d\w]{4})/gi;
      if (source) {
        source = source.replace(r, function (match, grp) {
          return String.fromCharCode(parseInt(grp, 16));
        });
        source = unescape(source);
      }
      return source;
    },
    /**
     * Converst unicode character to ascii format
     * @param str
     * @returns {string}
     */
    unicodeToAscii: function (str) {
      var out = "";
      for (var i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) < 0x80) {
          out += str.charAt(i);
        } else {
          var u = "" + str.charCodeAt(i).toString(16);
          out += "\\u" + (u.length === 2 ? "00" + u : u.length === 3 ? "0" + u : u);
        }
      }
      return out;
    },
    /**
     * Extracts username/pstn from string received from sip. Substring before first @ symbol
     * @param str
     * @returns {string}
     */
    extractUsernameFromSIP: function (str) {
      var retVal = str.substr(0, str.indexOf('@'));
      retVal     = retVal.replace('sip:', '');
      return retVal;
    },
    mergeToSIPuser: function (user, domain) {
      return user + '@' + domain;
    },
    sortAsc: function (messages) {
      messages.sort(function (a, b) {
        return parseFloat(a.date) - parseFloat(b.date);
      });

      return messages;
    },
    sortDesc: function (messages) {
      messages.sort(function (a, b) {
        return parseFloat(b.date) - parseFloat(a.date);
      });

      return messages;
    }
  };

  return service;
});
app.controller("ArticleController", ["$scope", "$rootScope", "$state", "$stateParams", "UiService", "$filter", "AuthService", "PrimoService", "config", "ngToast", "$sce", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, config,ngToast, $sce) {

  
    $scope.config = config;
    $scope.is_disable = false;
    
    function initController() {
        
        if($stateParams){
            if($stateParams.campuslife != undefined && $stateParams.campuslife){
                fetchArticle($stateParams.campuslife);
            }

            if($stateParams.campusvisit != undefined && $stateParams.campusvisit){
                fetchCollegesCampusVisitArticle();
            }
        }

    }

    function fetchArticle(slug){
        
        $rootScope.isProcessing=true;

        if(slug == 'campus-visit'){
            slug = 'campus-visit-101';
        }

        PrimoService.getArticle(slug).then(function (response){
            var res = response;
            $rootScope.isProcessing=false;
            if(res.status == 200){
              if(res.data.status == 1){
                if(res.data.article){
                  $scope.data = res.data.article
                }else{
                  ngToast.create("Page not found");
                  $state.go('/');
                }
              }
            }

            if(res.status == 0){
                ngToast.creat("Something went wrong");
            }

        });
    }

    $scope.deliberatelyTrustDangerousSnippet = function(data) {
         return $sce.trustAsHtml(data);
    };

    initController();

}]);
app.controller("BookController", ["$scope", "$rootScope", "$state", "$stateParams", "UiService", "$filter", "AuthService", "PrimoService", "$cookies", "config", "ngToast", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService,$cookies,config,ngToast) {

        $scope.config = config;  
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $rootScope.isProcessing=false;
        $scope.isDataFound=false;
        $scope.loadingImage='';
        $scope.filteredTodos = [];
        $scope.currentPage = 1;
        $scope.numPerPage = 10;
        $scope.maxSize = 5;
        $scope.search = [];
        initController();

        function initController() {
              
            $scope.loadingImage=$rootScope.loadingImage;
            $rootScope.isUserLoggedIn=true;
            $scope.changeTextBook ="Send A Message";
            var slug = $stateParams.slug;

            if(slug == undefined){
                loadGetAllTextBooks();
            }

            if($stateParams.title){
                $scope.title = $stateParams.title;
                $scope.search.title = $stateParams.title;
                searchBookByTitle();
            }

            if(slug){
                LoadSingleTextBook(slug);
            }
        }

        $scope.sendAMessage = function(item){
            $rootScope.isProcessing = true;
            var bookid = 1;
            PrimoService.sendAMessage(bookid).then(function(res){
                $rootScope.isProcessing = true;
                if(res.status==200){
                    //ngToast.create('Message sent successfully');
                    $scope.changeTextBook="Message sent";
                }
            });
        };

        $scope.getDataOnSubcat = function(catid,catname){
            $scope.selectedCtaName=catname;
            $rootScope.isProcessing=true;
            $scope.items=[];
            PrimoService.GetBooks(catid).then(function (response){
                var res = response;
                $rootScope.isProcessing=true;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                    $scope.items = res.data.Data;
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        //ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    //ngToast.create("Something went wrong");
                }
            });
        }


        $scope.seeBookProfile = function(book_id){
            if(book_id){
                //window.location.href='#!/textBooks/'+book_id;
                $location.path('textBooks/'+book_id);
            }
            else{
              //ngToast.create('Sorry No data found.');
            }
            
        }
         function cateAndSubcatList(){
             $rootScope.isProcessing=true;
            PrimoService.cateAndSubcatList().then(function (response){
                var res = response;
                $scope.itemList=[];
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                      $scope.itemList = res.data.Data;
                      $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                    }

                }
                if(res.status != 200){
                    //ngToast.create('Something went wrong');
                }
            });
         }

        $scope.searchTextBookById =function(category_id){
            $rootScope.isProcessing=true;
            PrimoService.searchTextBookById(category_id).then(function (response){
                var res = response;
                $scope.items=[];
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                      $scope.items = res.data.Data;
                      $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                    }

                }
                
                if(res.status != 200){
                    //ngToast.create('Something went wrong');
                }
            });
        }

        function customeBroughtTextBooks(){
            $rootScope.isProcessing=true;
            PrimoService.customeBroughtTextBooks($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                      $scope.boughtitems = res.data.Data;
                      $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                    }

                }
                
                if(res.status != 200){
                    //ngToast.create('Something went wrong');
                }
            });
        }

        function loadGetAllTextBooks()
        {
            $rootScope.isProcessing=true;
            PrimoService.GetBooks($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    // var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    // , end = begin + $scope.numPerPage;
                    // $scope.items = $scope.items.slice(begin, end);
        
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        //ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    //ngToast.create("Something went wrong");
                }
            });
        }

        function LoadSingleTextBook(slug){
            $rootScope.isProcessing=true;
            PrimoService.GetBook(slug).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result){
                        $scope.isDataFound=true;
                        $scope.item = res.data.result;
                    }
                    else{
                        $scope.isDataFound=false;
                        //ngToast.create('No data found');
                    }
                    
                }
                if(res.status != 200){
                    //ngToast.create('Something went wrong');
                }
            });
        }

        $scope.loadMore = function() 
        {
             console.log('loadMore');
                $scope.lastpage +=1;
                $rootScope.isProcessing=true;
                PrimoService.loadMoreTextBooks($scope.lastpage).then(function (response){
                    var res = response;
                    $rootScope.isProcessing=false;
                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.isDataFound=true;
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                        $scope.totalTextBook=$scope.items.length;
                    }
                    
                    if(res.status == 0){
                        //ngToast.create("Something went wrong");
                    }
                });
            }

        function  searchTextBookData(searchText){
            var slug = $stateParams.slug;
            $scope.searchText=slug;
           PrimoService.searchTextBook($scope.searchText).then(function(response){
                    if(response.status==200){
                        $rootScope.isProcessing=false;
                        if(response.data){
                          $scope.isDataFound=true;
                          $scope.searchTextBook=response.data.Data;
                          $scope.totalTextBook=$scope.searchTextBook.length;
                        }
                        else{
                            $scope.isDataFound=false;
                            $scope.totalCollege=0;
                            $scope.searchText='';
                        }
                    }
                    else{
                        $scope.totalCollege=0;
                        $scope.searchText='';
                        $rootScope.isProcessing=false;
                        //ngToast.create('cant load data from server');
                    }
                });
        }

         $scope.searchCollege = function(searchText){
            if($scope.searchText){
            $rootScope.isProcessing=true;
                PrimoService.searchTextBook($scope.searchText).then(function(response){
                    if(response.status==200){
                        //$rootScope.isProcessing=false;
                        if(response.data){
                          $scope.isDataFound=true;
                          $scope.searchTextBook=response.data.Data;
                          $scope.totalTextBook=$scope.searchTextBook.length;
                          if($scope.totalTextBook){
                            //window.location.href='#!/textBooks/search/'+$scope.searchText;
                            $location.path('textBooks/search/'+$scope.searchText);
                          }
                          $scope.searchText='';
                        }
                        else{
                            $scope.isDataFound=false;
                            $scope.totalCollege=0;
                            $scope.searchText='';
                        }
                    }
                    else{
                        $scope.totalCollege=0;
                        $scope.searchText='';
                        $rootScope.isProcessing=false;
                        //ngToast.create('cant load data from server');
                    }
                });
            }
            else{
                //ngToast.dismiss();
                //ngToast.create('Please Enter The Search Text');
            }
        }

        function searchBookByTitle()
        {
            $rootScope.isProcessing=true;
            PrimoService.GetBooksByTitle($scope.lastpage,$stateParams.title).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        //ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    //ngToast.create("Something went wrong");
                }
            });
        }

        $scope.searchBookByTitleLoadMore = function() 
        {
            $scope.lastpage +=1;
            $rootScope.isProcessing=true;
            PrimoService.GetBooksByTitle($scope.lastpage,$stateParams.title).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.isDataFound=true;
                        $scope.is_disable = true;
                    }

                    $scope.items = $scope.items.concat(res.data.result.data);
                    $scope.totalTextBook=$scope.items.length;
                }
                
                if(res.status == 0){
                    //ngToast.create("Something went wrong");
                }
            });
        }

        $scope.bookSearchByTitle = function(search){
          $lastpage = 1;
          if(search){
            $state.go('bookSearch',{title:search.title});
          }else{
            alert("Please fill the textbox first");
          }
          
        }

}]);

app.controller("CollegeController", ["$scope", "$rootScope", "$state", "$stateParams", "UiService", "$filter", "AuthService", "PrimoService", "$cookies", "config", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, $cookies, config) {
        
        $scope.config = config;
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $scope.sortAplhaVar = "true";
        $scope.leftCategoryList=[];
        $scope.sortAplhabates=false;
        $scope.collegeData=[];
        $scope.totalCollege=0;
        $rootScope.isProcessing=false;
        $scope.isDataFound=false;
        $scope.loadingImage='';
        $scope.currentPage = 1;
        $scope.numPerPage = 2;
        $scope.maxSize = 5;
        $scope.newItems=[];
        $scope.counter=0;

        initController();

        vm.pager = {};
        
        function initController() {

            //If serach College url hit

            if($stateParams.title){
                collegeSearchPage();
            }

            $scope.selectedCollegeId=window.localStorage.getItem('selectedCollegeOrUniversity');
            var slug = $stateParams.slug;
            if(slug == undefined){
                //loadColleges();
                loadTopScholarship();
            }else{
                LoadSinglePage(slug);
                loadNewsData();
                loadGetAllTextBooks();
                loadTopScholarship();
            }
            //call for getting left side list
            
            leftSideCollegeNavigation();

            //bestCollegesData();
            loadTopScholarship();
        }
        
        function showModal() {
          getLocation();
        }

        function getLocation() {
        if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition,showError);
        } else {
        ngToast.create("Geolocation is not supported by this browser.");
        }
        }

        function loadNewsData() {
            $rootScope.isProcessing=true;
            PrimoService.getNews().then(function(response) {
                var data = response.data;
                $rootScope.isProcessing=false;
                if (data.status == 1) {
                    $scope.isDataFound = true;
                    $scope.breakingNews = data.featured_news;
                    $scope.newsCategories = data.categories;
                    $scope.currentNews = data.news.data;
                    $scope.nextUrl = data.news.next_page_url;
                }
            });    
        }

        //for recent textbook added here:
        function loadGetAllTextBooks()
        {
            $rootScope.isProcessing=true;
            PrimoService.GetBooks($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    // var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    // , end = begin + $scope.numPerPage;
                    // $scope.items = $scope.items.slice(begin, end);
        
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    ngToast.create("Something went wrong");
                }
            });
        }

        // for top scholarship 
        function loadTopScholarship(){
            $rootScope.isProcessing=true;
            PrimoService.loadTopScholarship().then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    else{
                     $scope.is_disable = false;   
                    }
                    $scope.items = res.data.result;
                    $scope.states = res.data.states;
                }
                
                if(res.status == 0){
                    //$scope.isDataFound=false;
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
                
            });
        }

        function showPosition(position) {
          ngToast.dismiss();
          $scope.lat=position.coords.latitude;
          $scope.lng=position.coords.longitude;
          var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    var geocoder = geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                $scope.currentAddress=results[1].formatted_address;
                                $('#myModal').modal();
                                $scope.$apply();
                                //ngToast.create("Location: " + results[1].formatted_address);
                            }
                        }
                        else{
                            ngToast.create("Location:Not Found");
                        }
                    });
        }

        function showError(error) {
        switch(error.code) {
        case error.PERMISSION_DENIED:
          x.innerHTML = "User denied the request for Geolocation."
          break;
        case error.POSITION_UNAVAILABLE:
          x.innerHTML = "Location information is unavailable."
          break;
        case error.TIMEOUT:
          x.innerHTML = "The request to get user location timed out."
          break;
        case error.UNKNOWN_ERROR:
          x.innerHTML = "An unknown error occurred."
          break;
        }
        }


        $scope.searchSearchLocation = function(){
            $rootScope.isProcessing=true;
            var data={
                "lat":$scope.lat,
                "lng":$scope.lng,
                "cureentAddress":$scope.currentAddress
            };

            PrimoService.searchCollegeByLocation(data).then(function (response){
                if(response.status==200){
                    $rootScope.isProcessing=false;
                    if(response.data){
                      $scope.collegeData=response.data.Data;
                      $scope.totalCollege=$scope.collegeData.length;
                    }
                    else{
                        ngToast.dismiss();
                        ngToast.create('Data Not found!.');    
                    }
                }
                else{
                    $rootScope.isProcessing=false;  
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
            });
        }

        $scope.onKeySearch =function(text){
            if(text){}
            else{
                loadColleges();
            }    
        }

        $scope.seeCollegeByCategory = function(catId){
            showModal();
            //$rootScope.isProcessing=true;
            // PrimoService.seeCollegeByCategory(catId).then(function (response){
            //     if(response.status==200){
            //         $rootScope.isProcessing=false;
            //         if(response.data){
            //           $scope.collegeData=response.data.Data;
            //           $scope.totalCollege=$scope.collegeData.length;
            //         }
            //         else{
            //             ngToast.dismiss();
            //             ngToast.create('Data Not found!.');    
            //         }
            //     }
            //     else{
            //         $rootScope.isProcessing=false;
            //         ngToast.dismiss();
            //         ngToast.create('cant load data from server');
            //     }
            // });
        }

        function leftSideCollegeNavigation(){
            $rootScope.isProcessing=true;
            PrimoService.leftNavigationColleges().then(function (response){
                if(response.status==200){
                    $rootScope.isProcessing=false;
                    if(response.data){
                      $scope.leftCategoryList=response.data.Data;
                    }
                }
                else{
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
            });
        }

        function loadColleges()
        {
            $scope.counter++;
            $rootScope.isProcessing=true;
            $scope.items=[];
            $scope.newItems=[];
            PrimoService.Colleges($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    else{
                     $scope.is_disable = false;   
                    }
                    $scope.items = res.data.result;
                    $scope.totalCollege=res.data.total;
                    $scope.states = res.data.states;
                    setrating($scope.items);
                    $scope.isDataFound = true;
                    console.log($scope.items);
                }
                
                if(res.status == 0){
                    //$scope.isDataFound=false;
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
        }

        function setrating(data){
            $scope.newItems=[];
            for(var i=0;i<data.length;i++){
                $scope.newItems.push(
                {
                  "Id": $scope.items[i].id,
                  "title": $scope.items[i].title,
                  "college_rank": $scope.items[i].college_rank,
                  "college_image": $scope.items[i].college_image,
                  "address":$scope.items[i].address,
                  "time_period":$scope.items[i].time_period,
                  "no_of_students":$scope.items[i].no_of_students,
                  "net_price":$scope.items[i].net_price,
                  "rating":$scope.items[i].rating,
                  "votes":$scope.items[i].votes,
                  "reviews":$scope.items[i].reviews,
                  "slug":$scope.items[i].slug,
                  "className": ($scope.items[i].rating < 2 ? "bg_red" : (($scope.items[i].rating >= 2 && $scope.items[i].rating <= 3.5) ? "bg_yellow" : "bg_green"))
                });
            }

            $rootScope.saveSearchdata=$scope.newItems;
            if($scope.newItems.length > 0){
                //$scope.newItems=$scope.newItems.slice(0,5);
                //$scope.totalCollege=$scope.newItems.length;
            }else{

            }
        }

        function LoadSinglePage(slug){
            $rootScope.isProcessing=true;
            PrimoService.College(slug).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    $scope.item = res.data.result;
                    $scope.item.className = ($scope.item.rating < 2 ? "bg_red" : (($scope.item.rating >= 2 && $scope.item.rating <= 3.5) ? "bg_yellow" : "bg_green"));
                }
                console.log(res);    
            });
        }

        function loadTopScholarship(){
            $rootScope.isProcessing=true;
            PrimoService.loadTopScholarship().then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length)
                    {
                        $scope.isDataFound=true;
                        $scope.topScholarship = res.data.Data;
                    }
                    else{
                        $scope.isDataFound=false;
                    }
                    
                }
                
                if(res.status == 0){
                    ngToast.creat("Something went wrong");
                }
            });
        }

        $scope.loadMore = function() 
        {
             console.log('loadMore');
                $scope.lastpage +=1;
                $rootScope.isProcessing=true;
                PrimoService.Colleges($scope.lastpage).then(function (response){
                    var res = response;
                    $rootScope.isProcessing=false;
                    if(res.data.status == 1){
                        if(res.data.current_page == res.data.last_page)
                        {
                            $scope.is_disable = true;
                        }
                        $scope.totalCollege=res.data.to;
                        $scope.newItems = $scope.newItems.concat(res.data.result);
                    }
                    if(res.status == 0){
                        ngToast.dismiss();
                        ngToast.create("Something went wrong");
                    }
                });
             // $scope.newItems=$rootScope.saveSearchdata;
             // $scope.totalCollege=$scope.newItems.length;
             //$scope.newItems=$rootScope.saveSearchdata.slice(5,$rootScope.saveSearchdata.length -1);
        }

        $scope.addToWishList = function(item_id)
        {
            if(!$rootScope.isUserLoggedIn)
            {
                ngToast.create("Please login to add this school into your wishlist");
            }

            if($rootScope.isUserLoggedIn)
            {
                PrimoService.AddToWishList(item_id).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        // To Do task    
                    }
                    
                    if(res.status == 0){
                        ngToast.dismiss();
                        ngToast.create("Something went wrong");
                    }
                });
            }
        }
        $scope.sortAplha = function()
        {
            $scope.newItems=[];
            $scope.sortAplhabates=true;
            $scope.sortType = 'title';
            $rootScope.isProcessing=true;
            PrimoService.sortCollegeByAlphabates($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=true;
                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.items = res.data.result;
                    $scope.totalCollege=res.data.result.length;
                    setrating($scope.items);
                    $scope.isDataFound = true;
                    console.log($scope.items);
                }
                else{
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
                if(res.status == 0){
                    //$scope.isDataFound=false;
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
        }
        $scope.bestCollege= function (){
            //bestCollegesData();
            loadColleges();
        }

         function bestCollegesData(){
            $scope.collegeData=[];
            $scope.sortAplhabates=true;
            $rootScope.isProcessing=true;
            PrimoService.beseColleges().then(function(response){
                if(response.status==200){
                    $rootScope.isProcessing=false;
                    if(response.data){
                      $scope.collegeData=response.data.Data;
                      //$scope.totalCollege=$scope.collegeData.length;
                    }
                }
                else{
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
            });
        }

        $scope.searchCollege = function(){

            if($scope.searchText){
                $rootScope.isProcessing=true;
                $scope.items=[];
                $scope.newItems=[];
                PrimoService.Colleges($scope.lastpage).then(function (response){
                    var res = response;
                    $rootScope.isProcessing=false;
                    if(res.data.status == 1){
                        if(res.data.current_page == res.data.last_page)
                        {
                            $scope.is_disable = true;
                        }
                        else{
                         $scope.is_disable = false;   
                        }
                        $scope.items = res.data.result;
                        $scope.totalCollege=res.data.result.length;
                        setrating($scope.items);
                        $scope.isDataFound = true;
                        console.log($scope.items);
                    }
                    
                    if(res.status == 0){
                        //$scope.isDataFound=false;
                        $rootScope.isProcessing=false;
                        ngToast.dismiss();
                        ngToast.create("Something went wrong");
                    }
                });
            }
            else{
                 ngToast.dismiss();
                 ngToast.create('Please Enter The Search Text');
            }
        }

       $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
        , end = begin + $scope.numPerPage;
        //l$scope.totalCollege=$scope.newItems.length;
        $scope.newItems = $scope.items.slice(begin, end);
    
      });    

       $scope.searchCollege = function(search){
        debugger;
        $state.go('collegeSearch', { 'title':search.title, 'state':search.state });

       }

       $scope.collegeSearchPage = function(){

        $scope.isProcessing = true;
        $scope.counter++;
        $scope.items=[];
        $scope.newItems=[];
        var data = { title: $stateParams.title };
        PrimoService.collegeSearchPage(data).then(function (response){
            var res = response;
            $rootScope.isProcessing=false;
            if(res.data.status == 1){
                if(res.data.current_page == res.data.last_page)
                {
                    $scope.is_disable = true;
                }
                else{
                 $scope.is_disable = false;   
                }
                $scope.items = res.data.result;
                $scope.totalCollege=res.data.total;
                $scope.states = res.data.states;
                setrating($scope.items);
                $scope.isDataFound = true;
                console.log($scope.items);
            }
            
            if(res.status == 0){
                //$scope.isDataFound=false;
                $rootScope.isProcessing=false;
                ngToast.dismiss();
                ngToast.create("Something went wrong");
            }
        });

       }

}]);

app.filter('extractFirstAndSecondLetter', function() {
    return function(input) {
      var split = input.split(" ");

      var response;
      if(split.length >= 2)
      {
        response = split[0].charAt(0) + split[1].charAt(0);
      }

      if(split.length == 1)
      {
        response = split[0].substring(0,2);
      }
      return response.toUpperCase();
    }
});
app.controller("CollegeController", ["$scope", "$rootScope", "$state", "$stateParams", "UiService", "$filter", "AuthService", "PrimoService", "$cookies", "config", "ngToast", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, $cookies, config, ngToast) {
        
    
    $scope.config = config;
    $scope.data = {title:'',state:'',country:''};
    $scope.lastpage = 1;
    $scope.sortAplhabates=true;
    $scope.loadMoreOnlyTextButton = false;
    $scope.loadMoreButton = false;
    $scope.newItems=[];

    function initController() {
        
        if($stateParams.slug){
            LoadSinglePage($stateParams.slug);
        }else if ($stateParams.title){
            $scope.loadMoreButton = true;
            $scope.loadMoreOnlyTextButton = false;
            searchCollegesWithTitleStateCountry();
        }else{
            loadTopScholarship();
        }
    }        

    initController();

    function loadTopScholarship(){
        $rootScope.isProcessing=true;
        PrimoService.loadTopScholarship().then(function (response){
            var res = response;
            $rootScope.isProcessing=false;
            if(res.status == 200){
                if(res.data.result.data && res.data.result.data.length)
                {
                    $scope.isDataFound=false;
                    $scope.topScholarship = res.data.result.data;
                    $scope.states = res.data.states;
                    $scope.county_name = res.data.county_name;
                }
                else{
                    $scope.isDataFound=true;
                }
                
            }
            
            if(res.status == 0){
                ngToast.creat("Something went wrong");
            }
        });

    }


    $scope.searchCollegeForm = function(search){

        if(search){

            if(search.title && search.state && search.country){
                $state.go('collegeSearchWithTitleStateCountry',{'title':search.title,'state':search.state,'country':search.country});
            }else if(search.title && search.state){
                $state.go('collegeSearchWithTitleState',{'title':search.title,'state':search.state});
            }else {
                $state.go('collegeSearchWithTitle',{'title':search.title});   
            }

        }else{
            alert("Please fill atleast one field");
        } 
    }
    
    function searchCollegesWithTitleStateCountry(){
        $rootScope.isProcessing = true;
        
        if($stateParams.title){
            $scope.data.title = $stateParams.title;
        }

        if($stateParams.state){
            $scope.data.state = $stateParams.state;
        }

        if($stateParams.country){
            $scope.data.country = $stateParams.country;
        }

        if($stateParams){
            PrimoService.searchCollegesWithTitleStateCountry($scope.data,$scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                $scope.isDataFound=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.totalCollege = res.data.total;
                    $scope.newItems = $scope.newItems.concat(res.data.result);
                }
                if(res.status == 0){
                    $scope.isDataFound=true;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
        }
        
    }

    $scope.loadMore = function() 
        {
            $scope.loadMoreButton = true;
            $scope.loadMoreOnlyTextButton = false;

            if($stateParams.title){
                $scope.data.title = $stateParams.title;
            }

            if($stateParams.state){
                $scope.data.state = $stateParams.state;
            }

            if($stateParams.country){
                $scope.data.country = $stateParams.country;
            }

            $scope.lastpage +=1;
            $rootScope.isProcessing=true;

            PrimoService.searchCollegesWithTitleStateCountry($scope.data,$scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                $scope.isDataFound=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.totalCollege=res.data.to;
                    $scope.newItems = $scope.newItems.concat(res.data.result);
                }
                if(res.status == 0){
                    $scope.isDataFound=true;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
        }

    $scope.searchCollege = function(){
        $scope.loadMoreButton = false;
        $scope.loadMoreOnlyTextButton = true;
        debugger;
        if($scope.searchText){
            $rootScope.isProcessing=true;
            $scope.items=[];
            $scope.newItems=[];
            PrimoService.searchColleges($scope.searchText).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    else{
                     $scope.is_disable = false;   
                    }
                    $scope.items = res.data.result;
                    $scope.totalCollege=res.data.result.length;
                    setrating($scope.items);
                    $scope.isDataFound = false;
                    console.log($scope.items);
                }
                
                if(res.status == 0){
                    $scope.isDataFound = true;
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
        }
        else{
             ngToast.dismiss();
             ngToast.create('Please Enter The Search Text');
        }
    }

    function setrating(data){
            $scope.newItems=[];
            for(var i=0;i<data.length;i++){
                $scope.newItems.push(
                {
                  "Id": $scope.items[i].id,
                  "title": $scope.items[i].title,
                  "college_rank": $scope.items[i].college_rank,
                  "college_image": $scope.items[i].college_image,
                  "address":$scope.items[i].address,
                  "time_period":$scope.items[i].time_period,
                  "no_of_students":$scope.items[i].no_of_students,
                  "net_price":$scope.items[i].net_price,
                  "rating":$scope.items[i].rating,
                  "votes":$scope.items[i].votes,
                  "reviews":$scope.items[i].reviews,
                  "slug":$scope.items[i].slug,
                  "className": ($scope.items[i].rating < 2 ? "bg_red" : (($scope.items[i].rating >= 2 && $scope.items[i].rating <= 3.5) ? "bg_yellow" : "bg_green"))
                });
            }

            $rootScope.saveSearchdata=$scope.newItems;
            if($scope.newItems.length > 0){
                //$scope.newItems=$scope.newItems.slice(0,5);
                //$scope.totalCollege=$scope.newItems.length;
            }else{

            }
        }

    $scope.sortAplha = function()
    {
        $scope.sortAplhabates=true;
        $scope.sortType = 'title';
        $rootScope.isProcessing=true;
        PrimoService.sortCollegeByAlphabates($scope.lastpage).then(function (response){
            var res = response;
            $rootScope.isProcessing=true;
            if(res.data.status == 1){
                if(res.data.result.current_page == res.data.result.last_page)
                {
                    $scope.is_disable = true;
                }
                $scope.items = res.data.result;
                $scope.totalCollege=res.data.result.length;
                setrating($scope.items);
                $scope.isDataFound = false;
                console.log($scope.items);
            }
            else{
                $rootScope.isProcessing=false;
                ngToast.dismiss();
                ngToast.create('cant load data from server');
            }
            if(res.status == 0){
                $scope.isDataFound=true;
                $rootScope.isProcessing=false;
                ngToast.dismiss();
                ngToast.create("Something went wrong");
            }
        });
    }

    $scope.loadMoreOnlyTextButtonFun = function(){
       $scope.loadMoreButton = false;
            $scope.loadMoreOnlyTextButton = true;

            if($stateParams.title){
                $scope.data.title = $stateParams.title;
            }

            // if($stateParams.state){
            //     $scope.data.state = $stateParams.state;
            // }

            // if($stateParams.country){
            //     $scope.data.country = $stateParams.country;
            // }

            if($scope.searchText){
                $scope.data.title = $scope.searchText;
            }

            $scope.lastpage +=1;
            $rootScope.isProcessing=true;

            PrimoService.searchCollegesWithTitleStateCountry($scope.data,$scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                $scope.isDataFound=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.totalCollege=res.data.to;
                    $scope.newItems = $scope.newItems.concat(res.data.result);
                }
                if(res.status == 0){
                    $scope.isDataFound=true;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
    }

    function LoadSinglePage(slug){
        $rootScope.isProcessing=true;
        PrimoService.College(slug).then(function (response){
            var res = response;
            $rootScope.isProcessing=false;
            if(res.status == 200){
                $scope.item = res.data.result;
                $scope.item.className = ($scope.item.rating < 2 ? "bg_red" : (($scope.item.rating >= 2 && $scope.item.rating <= 3.5) ? "bg_yellow" : "bg_green"));
            }
            console.log(res);    
        });
    }

}]);

app.filter('extractFirstAndSecondLetter', function() {
    return function(input) {
      var split = input.split(" ");

      var response;
      if(split.length >= 2)
      {
        response = split[0].charAt(0) + split[1].charAt(0);
      }

      if(split.length == 1)
      {
        response = split[0].substring(0,2);
      }
      return response.toUpperCase();
    }
});

app.controller('CollegeSelectController', ["$rootScope", "$scope", "$mdDialog", "$filter", "selectColleges", function ($rootScope, $scope, $mdDialog, $filter,selectColleges) {

  $scope.answer = function (answer) {
    if (answer) {
      $mdDialog.hide($scope.inviteEmails);
    } else {
      $mdDialog.hide(false);
    }
  };

  var fetchingRecords = false;

    $scope.getCountries = function (searchKey, pagenumber) {

        if (fetchingRecords) return;
        fetchingRecords = true;

        selectColleges.getCountries(searchKey, pagenumber)
                    .then(function (result) {
                        if (pagenumber === 1) {
                            $scope.totalRecords = result.TotalRecords;
                            $scope.countries = result.Records;
                        }
                        else {
                            $scope.countries = $scope.countries.concat(result.Records);
                        }
                        fetchingRecords = false;
                    },
                    
                    function (errorMessage) {
                        window.console.warn(errorMessage);
                        fetchingRecords = false;
                    });
    };

    $scope.countries = [];

    $scope.country = {
        Key: "Alabama A & M University",
        Value: "1"
    };

    $scope.getCountries("", 1);

}]);
app.controller("EventController", ["$scope", "$rootScope", "$state", "$stateParams", "UiService", "$filter", "AuthService", "PrimoService", "$cookies", "config", "ngToast", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, $cookies, config, ngToast) {
        
        $scope.config = config;
        
        var vm          = this;
        vm.selected     = 'event';
        $scope.lastpage = 1;
        $scope.searchlastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $scope.sortAplhaVar = "true";
        $scope.leftCategoryList=[];
        $scope.sortAplhabates=false;
        $rootScope.isProcessing=false;
        $scope.isDataFound=false;
        $scope.loadingImage='';
        $scope.currentPage = 1;
        $scope.numPerPage = 2;
        $scope.maxSize = 5;
        $scope.newItems=[];
        $scope.counter=0;
        $scope.search = {};
        $scope.loadMoreButton = true;
        $scope.loadMoreWithSearchingButton = false;
        initController();

        function initController() {
            $scope.loadingImage=$rootScope.loaderImageUrl;
            var slug = $stateParams.slug;
            if(slug == undefined){
                LoadEvents();
            }else{
                LoadSinglePage(slug);
            }
        }

        function LoadEvents(){

            $scope.counter++;
            $rootScope.isProcessing=true;
            $scope.items=[];
            $scope.newItems=[];
            PrimoService.Events($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    else{
                     $scope.is_disable = false;   
                    }
                    $scope.items = res.data.result.data;
                    $scope.totalEvents=res.data.result.total;
                    $scope.isDataFound = true;
                    $scope.states = res.data.states;
                }
                
                if(res.status == 0){
                    //$scope.isDataFound=false;
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
            // PrimoService.Events($scope.lastpage).then(function (response){
            //     var res = response;

            //     if(res.data.status == 1){
            //         if(res.data.result.current_page == res.data.result.last_page)
            //         {
            //             $scope.is_disable = true;
            //         }
            //         $scope.items = res.data.result.data;
            //         console.log($scope.items);
            //     }
                
            //     if(res.status == 0){
            //         alert("Something went wrong");
            //     }
            // });
        }

        function LoadSinglePage(slug){
            
            PrimoService.Event(slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.item = res.data.result;
                }
                console.log(res);    
            });
        }

        $scope.loadMore = function() 
        {
            console.log('loadMore');
            $scope.lastpage +=1;

            PrimoService.Events($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    
                    $scope.items = $scope.items.concat(res.data.result.data);
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });
        }

        $scope.searchEvents = function(search){
            $scope.loadMoreWithSearchingButton = true;
            $scope.loadMoreButton = false;
            $scope.searchlastpage = 1;
            $scope.counter++; 
            $scope.search = search;

            if(search != undefined){
                $rootScope.isProcessing = true;
                PrimoService.searchEvents(search).then(function (response){
                    debugger;
                    $rootScope.isProcessing = false;

                    if(response.data.result.total > 0 ){

                        if(response.data.result.current_page == response.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }
                        else{

                            $scope.is_disable = false;   
                        }

                        $scope.items = response.data.result.data;
                        $scope.totalEvents = response.data.result.total;

                        console.log($scope.items);
                    }else{

                        $scope.totalEvents = 0;
                        $scope.items = [];
                    }

                });
            }else{
                ngToast.create({className: 'danger',content: 'Please fill atleast one option'});
            }

        }

        $scope.loadMoreWithSearching = function() 
        {
            $scope.searchlastpage +=1;
            PrimoService.searchEvents($scope.search,$scope.searchlastpage).then(function (response){
                $rootScope.isProcessing = false;

                    if(response.data.result.total > 0 ){

                        if(response.data.result.current_page == response.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }
                        else{

                            $scope.is_disable = false;   
                        }

                        $scope.items = $scope.items.concat(response.data.result.data);
                        $scope.totalEvents = response.data.result.total;

                        console.log($scope.items);
                    }else{

                        $scope.totalEvents = 0;
                        $scope.items = [];
                    }
            });
        }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller("HomeController", ["$scope", "$rootScope", "$state", "UiService", "$filter", "AuthService", "PrimoService", "config", "ngToast", function ($scope, $rootScope, $state, UiService, $filter, AuthService, PrimoService, config,ngToast) {

  
        var vm = this;
        $scope.config = config;
        $scope.is_disable = false;
        vm.user = null;
        vm.allUsers = [];
        vm.books = null;
        $scope.books = [];  
        $scope.lastpage = 1;
        $scope.selectedCategoryId = '';
        
        function initController() {
           //forceSSL();
            loadGetAllTextBooks();            
            loadBookCategories();
        }

        function loadGetAllTextBooks()
        {
            $rootScope.isProcessing=true;
            PrimoService.GetBooks($scope.lastpage,$scope.selectedCategoryId).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    ngToast.create("Something went wrong");
                }
            });
        }

        function loadBookCategories(){
            
            PrimoService.GetBookCategories().then(function (response){

                var res = response;
                if(res.data.status == 1){
                    $scope.itemList = res.data.result;
                }
                
                if(res.status == 0){
                    $scope.itemList = [];   
                }
            });
        }

        $scope.loadMore = function() 
        {
             console.log('loadMore');
                $scope.lastpage +=1;
                $rootScope.isProcessing=true;
                PrimoService.GetBooks($scope.lastpage,$scope.selectedCategoryId).then(function (response){
                    var res = response;
                    $rootScope.isProcessing=false;
                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.isDataFound=true;
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                        $scope.totalTextBook=$scope.items.length;
                    }
                    
                    if(res.status == 0){
                        ngToast.create("Something went wrong");
                    }
                });
        }


        $scope.searchBookByCategoryId = function(id){

            $scope.selectedCategoryId = id;
            $rootScope.isProcessing=true;
            PrimoService.GetBooks($scope.lastpage,$scope.selectedCategoryId).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    ngToast.create("Something went wrong");
                }
            });
        }

    $scope.bookSearchByTitle = function(search){
      
      if(search){
        $state.go('bookSearch',{title:search.title});
      }else{
        alert("Please fill the textbox first");
      }
      
    }
    
    initController();

}]);

app.controller("JobsController", ["$scope", "$rootScope", "$state", "$stateParams", "UiService", "$filter", "AuthService", "PrimoService", "$cookies", "config", "ngToast", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, $cookies, config,ngToast) {

        $scope.config = config;  
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $scope.Experiance=[];
        $scope.salary=[];
        $scope.loadingImage='';
        $scope.currentPage = 1;
        $scope.jobDescrption=[];
        $scope.numPerPage = 3;
        $scope.isDataFound=true;
        $scope.maxSize = 5;
        

        function getExperience(){
            $scope.Experiance.push(
                {key:1,value:1},
                {key:2,value:2},
                {key:3,value:3},
                {key:4,value:4},
                {key:5,value:5},
                {key:6,value:6},
                {key:7,value:7},
                {key:8,value:8},
                {key:9,value:9},
                {key:10,value:10}
                );

            $scope.salary.push(
                {key:1000,value:1000},
                {key:2000,value:2000},
                {key:3000,value:3000},
                {key:4000,value:4000},
                {key:5000,value:5000},
                {key:6000,value:6000},
                {key:7000,value:7000},
                {key:8000,value:8000},
                {key:9000,value:9000},
                {key:10000,value:10000}
                );

            //$scope.states.push({id:1,name:'Delhi'},{id:2,name:'Mumbai'},{id:3,name:'Surat'});
            // $scope.locations.push(
            //     {id:1,name:'Mumbai'},
            //     {id:2,name: 'Delhi'},
            //     {id:3,name: 'Punjab'},
            //     {id:4,name: 'Gurgaon'});
            //$scope.search.experience=$scope.Experiance[1].key;
        }

        function initController() {
            $scope.states=[];
            $scope.Experiance=[];
            $scope.locations=[];
            $scope.loadingImage=$rootScope.loaderImageUrl;
            var slug = $stateParams.slug;
            if(slug == undefined){
                loadJobs();
            }else{
                LoadSinglePage(slug);
                //jobDescriptionByID(slug);
            }
            $rootScope.isUserLoggedIn=true;
            getExperience();

            var jobId = $stateParams.id;
            if(jobId){
                jobDescriptionBySlug(jobId);
            }
        }

        $scope.addToFavouriteJob = function(event,jobId){
            $scope.jobAddToFavorite(jobId);
        }

        $scope.searchFreshness = function(event){
            if($scope.freshness){
                if(event.which==13 || event.keyCode==13){
                    //api data calling here
                    $scope.globalJobSearch($scope.freshness,'',0,0);
                    //$rootScope.isProcessing=true;
                    ngToast.dismiss();
                    ngToast.create('searched content');
                }
            }
            else{
                $rootScope.isProcessing=false;
                ngToast.dismiss();
                ngToast.create('Enter Freshness text here.');
            }
        }

        $scope.searchClickOnLocation = function(event,search){
            $scope.data=[];
            var totalLocation = document.getElementsByClassName('isCheck');
            for(var i=0;i<totalLocation.length;i++){
                if(totalLocation[i].checked){
                    $scope.data.push(totalLocation[i].value);
                }
            }
            console.log($scope.data+" bnkmkmnbm "+event.currentTarget.value);
            if($scope.data.length > 0){}
            else{
                $scope.items=$scope.savedJobs;
            }    

          if($scope.data.length){
            $rootScope.isProcessing=true;
            if($scope.search){
                var data={
                searchText:$scope.search.free_search,
                location:$scope.search.location,
                selectedExperience:$scope.search.experience,
                selectedSalary :$scope.search.salary
                };
            }
            else{
                var data={
                searchText:'',
                location:'',
                selectedExperience:0,
                selectedSalary :0
            };
            }
            
             PrimoService.searchJobsBasedOnLocation($scope.data,data).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                console.log(res);
                if(res.data.status == 1){
                    if(res.data.Data.length > 0){
                        $scope.items = res.data.jobs.data;
                        $scope.states = res.data.states;
                        $scope.isDataFound=true;
                    if(res.data.jobs.current_page == res.data.jobs.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    }
                    else{
                        ngToast.dismiss();
                        $scope.isDataFound=false;
                        $scope.items=[];
                        ngToast.create("Oop's No Job Found!.");
                    }
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });   
          }
        }

        function loadJobs()
        {
            $rootScope.isProcessing=true;
            PrimoService.Jobs($scope.lastpage).then(function (response){
                var res = response;
                console.log(res);
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.jobs.current_page == res.data.jobs.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    if(res.data.jobs.data.length > 0){
                      $scope.items = res.data.jobs.data;
                      $scope.totaljobLanding=$scope.items.length;
                      $scope.savedJobs=$scope.items;
                    }
                    else{
                        ngToast.create('No jobs Found');
                    }

                    if(res.data.states.length > 0){
                      $scope.locations= res.data.states;
                     $scope.states=  res.data.states;
                        // for(var i=0;i<res.data.states.length;i++){
                        //   $scope.states.push({key:res.data.states[i].name,value:res.data.states[i].name});      
                        // }
                    }
                    console.log($scope.items);
                }
                
                if(res.status == 0){
                    $rootScope.isProcessing=false;
                    ngToast.create("Something went wrong");
                }
            });
            // $rootScope.isProcessing=true;
            // PrimoService.Jobs().then(function (response){
            //     if(response.status==200){
            //         $rootScope.isProcessing=false;
            //         if(response.data){
            //           $scope.items=response.data.Data;
            //           $scope.savedJobs=$scope.items;
            //           $scope.items=$scope.savedJobs.slice(0,5);
            //           $scope.totaljobLanding=$scope.jobLandingData.length;
            //         }
            //     }
            //     else{
            //         $rootScope.isProcessing=false;
            //         window.alert('cant load data from server');
            //     }
            // });
        }


        function jobDescriptionBySlug(jobSlug)
        {
            $rootScope.isProcessing=true;
            PrimoService.jobDescriptionBySlug(jobSlug).then(function (response){
                if(response.status==200){
                    $rootScope.isProcessing=false;
                    if(response.data){
                      $scope.jobDescrption=response.data.Data;
                    }
                }
                else{
                    $rootScope.isProcessing=false;
                    window.alert('cant load data from server');
                }
            });
        }

        $scope.searchJob = function(searchData){
            $scope.globalJobSearch(searchData.free_search,searchData.location,searchData.experience,searchData.salary);
            // PrimoService.searchJob($scope.search).then(function(res) {
            //     if (res.data.status == 1) {
            //         if (res.data.jobs.current_page == res.data.jobs.last_page) {
            //             $scope.is_disable = true;
            //         }
            //         $scope.items = res.data.jobs.data;
            //         $scope.states = res.data.states;
            //     }
            // });
        }

        function LoadSinglePage(slug){
            
            PrimoService.Job(slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.item = res.data.result;
                }
                console.log(res);    
            });
        }

        $scope.loadMore = function() 
        {
             // console.log('loadMore');
             //    $scope.lastpage +=1;

             //    PrimoService.Jobs($scope.lastpage).then(function (response){
             //        var res = response;

             //        if(res.data.status == 1){
                        
             //            if(res.data.result.current_page == res.data.result.last_page)
             //            {
             //                $scope.is_disable = true;
             //            }

             //            $scope.items = $scope.items.concat(res.data.result.data);
             //        }
                    
             //        if(res.status == 0){
             //            alert("Something went wrong");
             //        }
             //    });
             $scope.items=$scope.savedJobs;
             $scope.totaljobLanding=$scope.items.length;
        }

        $scope.jobApply =function(jobId){
            if(!$rootScope.userId){
                ngToast.create("Please login first");
                return false;
            }
            var user = JSON.parse(localStorage.getItem('user'));

            //job apply here
            var data={
                userid :user.id,
                jobid:jobId
            };


            $rootScope.isProcessing=true;
            PrimoService.jobApply(data).then(function (response){
                ngToast.dismiss();
                if(response.data.status==1){
                    $rootScope.isProcessing=false;
                    ngToast.create(response.data.message);
                }
                if(response.data.status==0){
                    $rootScope.isProcessing=false;
                    ngToast.create(response.data.message);
                }
                // else{
                //     $rootScope.isProcessing=false;
                //     ngToast.create('Data cant be load from server');
                // }
            });
        }

        $scope.jobAddToFavorite= function(jobId){
            $rootScope.isProcessing=true;
             var data={
                userid :1,
                jobid:jobId
            };

            if(jobId){
                    PrimoService.jobAddToFavorite(data).then(function (response){
                        ngToast.dismiss();
                        if(response.data.status==1){
                            $rootScope.isProcessing=false;
                            ngToast.create(response.message);
                        }
                        if(response.data.status==0){
                            $rootScope.isProcessing=false;
                            angular.element(event.target).addClass('favoriteJob'); 
                            ngToast.create(response.data.message);
                        }
                        // else{
                        //     $rootScope.isProcessing=false;
                        //     ngToast.create('Data cant be load from server');
                        // }
                    });

                if(angular.element(event.target).hasClass('favoriteJob')){
                    angular.element(event.target).removeClass('favoriteJob');
                }
                else{
                 ngToast.dismiss();   
                 //ngToast.create('Job added into Favourite List.');   
                 angular.element(event.target).addClass('favoriteJob');   
                }
            }
        }
        //job seeRecruiterNumber
        $scope.seeRecruiterNumber =function(jobId){
            $rootScope.isProcessing=true;
            $('#myModal').modal();
            PrimoService.seeRecruiterNumber(jobId).then(function (response){
                if(response.data.status==1){
                    $rootScope.isProcessing=false;
                    $scope.recruiter=response.data.recruiter;
                }
                else{
                    $rootScope.isProcessing=false;
                    ngToast.create('Data cant be load from server');
                }
                $scope.$apply();
            });

        }

        $scope.globalJobSearch = function(searchText,location,selectedExperience,selectedSalary){
            if(searchText||location||selectedExperience||selectedSalary){
                $rootScope.isProcessing=true;
                PrimoService.globalJobSearch(searchText,location,selectedExperience,selectedSalary)
                .then(function(response){
                    
                    if(response.data.status==1){
                        $rootScope.isProcessing=false;
                        if(response.data.jobs.data){
                          $scope.searchText='';
                          $scope.isDataFound=true;
                          $scope.items=response.data.jobs.data;
                          $scope.totaljobs=response.data.jobs.data.length;
                        }
                        else{
                            $scope.isDataFound=false;
                            $scope.totaljobs=0;
                            $scope.searchText='';
                        }
                    }
                    else{
                        $scope.globalJobSearch=0;
                        $scope.searchText='';
                        $rootScope.isProcessing=false;
                        ngToast.create('cant load data from server');
                    }
                });
            }
            else{
                window.alert('Please Enter The Search Text');
            }
        }

        $scope.$watch('currentPage + numPerPage', function() {
            var begin = (($scope.currentPage - 1) * $scope.numPerPage)
            , end = begin + $scope.numPerPage;
            $scope.items = $scope.items.slice(begin, end);
        });

        initController();

}]);
app.controller("NewsController", ["$scope", "$rootScope", "$state", "$stateParams", "UiService", "$filter", "AuthService", "PrimoService", "$cookies", "config", "ngToast", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, $cookies, config,ngToast) {

        $scope.config = config;  
        var vm = this;
        $scope.is_disable = false;
        vm.user = null;
        vm.allUsers = [];
        vm.items = null;
        $scope.items = [];  
        $scope.lastpage = 1;
        $rootScope.isProcessing=false;
        $scope.loadingImage='';


        
        function initController() {
            
            if($stateParams.slug){
                LoadSinglePage($stateParams.slug);
            }else{
                loadNewsData();
            }
            loadNewsData();
        }

        function loadNewsData() {
            $rootScope.isProcessing=true;
            PrimoService.getNews().then(function(response) {
                var data = response.data;
                $rootScope.isProcessing=false;
                if (data.status == 1) {
                    $scope.isDataFound = true;
                    $scope.breakingNews = data.featured_news;
                    $scope.newsCategories = data.categories;
                    $scope.currentNews = data.news.data;
                    $scope.nextUrl = data.news.next_page_url;
                }
            });    
        }

        function loadNews() {
            $rootScope.isProcessing=true;
            if(angular.equals({}, $rootScope.globals.currentUser)){
                $scope.user = $rootScope.globals.currentUser.username;
            }
            
            PrimoService.News($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.items = res.data.result.data;
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });
        }
        
        function LoadSinglePage(slug){
            $rootScope.isProcessing=true;
            PrimoService.singleNews(slug).then(function (response){
                $rootScope.isProcessing=false;
                var res = response;
                if(res.data.status == 1){
                    $scope.item = res.data.news;
                    $scope.related_news = res.data.related_news;
                    $scope.stats = res.data.stats;
                }
            });
        }

        $scope.newsActivity = function(id,action){

            $rootScope.isProcessing=true;
            var newsObj = $cookies.getObject('newsactivity');

            if(newsObj != undefined){
                
                if(newsObj.indexOf(id) !== -1) {

                    alert("you already performed action");
                    return false;

                }else{
                    newsObj.push(id);
                    $cookies.putObject('newsactivity',newsObj);
                }

            }else{
                
                $cookies.putObject('newsactivity',[id]);
            }

            PrimoService.newsActivity(id,action).then(function (response){
                $rootScope.isProcessing=false;
                var res = response;

                if(res.data.status == 1){
                    $scope.stats = res.data.stats;
                }
                  
            });
        }

        $scope.loadMore = function() {
                $rootScope.isProcessing=true;
                $scope.lastpage +=1;

                PrimoService.News($scope.lastpage).then(function (response){
                    $rootScope.isProcessing=false;
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
            }


        $scope.getCategoryNews = function(slug) {
            $rootScope.isProcessing=true;
            $scope.lastpage = 1;
            $scope.currentNews = [];
            $rootScope.isProcessing=true;
            PrimoService.getNewsByCategory(slug).then(function(response) {
                $rootScope.isProcessing=false;
                var data = response.data;
                $rootScope.isProcessing=false;
                $scope.currentNews = data.news.data;
                $scope.newsCategory = data.category;
                if(data.news.data.length > 0){
                    $scope.isDataFound = true;
                }
                else{
                    $scope.isDataFound = false;
                }
                $scope.nextUrl = data.news.next_page_url;
            });
        }


        $scope.loadMoreNews = function() {
                
                $rootScope.isProcessing=true;
                $scope.lastpage +=1;

                $scope.loading = true;
                PrimoService.newsLoadMoreData($scope.lastpage).then(function(response) {
                    $rootScope.isProcessing=false;
                    var data = response.data;
                    $scope.loading = false;
                    if(data.status == 1){
                            
                        if(data.news.current_page == data.news.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        angular.forEach(data.news.data, function(value, key){
                            $scope.currentNews.push(value);
                        });
                    }

                    if(data.status == 0){
                        alert("Something went wrong");
                    }
                });
            }

    initController();

}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.controller("PrivacyController", function() {

});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.controller('RetailersController', ["$scope", "UiService", "PrimoService", "$rootScope", "$filter", function ($scope, UiService, PrimoService, $rootScope, $filter) {

  $scope.becomeRetailer = function () {
    UiService.showConfirmDialog('RetailersContactController', 'partials/retailers-contact-form.html', function (clb) {
      if (clb) {
        PrimoService.submitDealerApplication(clb).success(function (data) {
          $rootScope.alert = {
            show: true,
            context: {type: 'success', msg: $filter('translate')('SUCCESS_RETAIL_APP')}
          };
        });
      }
    });

  }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller('RetailersContactController', ["$rootScope", "$scope", "$mdDialog", "PrimoService", "$filter", function($rootScope, $scope, $mdDialog,
                                                      PrimoService,
                                                      $filter) {

  $scope.initUser = function() {
    $scope.user = {
      first_name: '',
      last_name: '',
      email: '',
      phone: '',
      company: '',
      address_1: '',
      city: '',
      state_province: '',
      postal_code: '',
      message: '',
      epay_pos: 0
    }
  };

  $scope.initUser();

  $scope.storeTypes = {
    "single": "Single Location",
    "multiple": "Multiple Location",
    "other": "Other"
  };

  $scope.source = {
    "Friend": "Friend",
    "Store": "Store",
    "A Primo Retailer": "A Primo Retailer",
    "Search Engine/Online": "Search Engine/Online",
    "Other": "Other"
  };

  $scope.answer = function() {
    PrimoService.submitDealerApplication($scope.user).success(function(data) {
      $rootScope.alert = {
        show: true,
        context: {type: 'success', msg: $filter('translate')('SUCCESS_RETAIL_APP')}
      };

      //reset user info on success
      $scope.initUser();
      $scope.dealerForm.$setPristine();
      $scope.dealerForm.$setUntouched();
    });
  };
}]);

app.controller("ScholarshipController", ["$scope", "$rootScope", "$state", "$stateParams", "UiService", "$filter", "AuthService", "PrimoService", "$cookies", "config", "ngToast", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, $cookies, config,ngToast) {

        $scope.config = config;
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $rootScope.isProcessing=false;
        $scope.isDataFound=false;

        initController();

        function initController() {
            $scope.loadingImage=$rootScope.loadingImage;
            var slug = $stateParams.slug;
            var currenturl = $state.current;

            if(slug){
                loadScholarships();
            }else if (currenturl.url == '/scholarships-search'){

                scholarshipsSearchPage();
            }else{
                LoadSinglePage(slug);
            }
            //testimonialsTeam();
            loadTopScholarship();
        }

        $scope.UCCHUB = function(clickAction){

        }

        function loadTopScholarship(){

            $rootScope.isProcessing=true;
            PrimoService.loadTopScholarship().then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.result.data && res.data.result.data.length)
                    {
                        $scope.isDataFound=true;
                        $scope.topScholarship = res.data.result.data;
                        
                    }
                    else{
                        $scope.isDataFound=false;
                    }
                    
                }
                
                if(res.status == 0){
                    ngToast.creat("Something went wrong");
                }
            });
        }

        $scope.startSearch = function(){
            $('#myModal').modal();
            // $rootScope.isProcessing=true;
            // PrimoService.startSearch().then(function(response){
            //     var res= response;
            //     $rootScope.isProcessing=false;
            //     if(res.status==200){
            //         if(res.data){
            //             //business logic here
            //         }
            //     }
            // });
        }

        $scope.collegeDictory = function(){
            $location.path('/colleges'); 
            // $rootScope.isProcessing=true;
            // PrimoService.collegeDictory().then(function(response){
            //     var res= response;
            //     $rootScope.isProcessing=false;
            //     if(res.status==200){
            //         if(res.data){
            //             //business logic here
            //         }
            //     }
            // });
        }

        $scope.matchMaker = function(){
            $rootScope.isProcessing=true;
            PrimoService.matchMaker().then(function(response){
                var res= response;
                $rootScope.isProcessing=false;
                if(res.status==200){
                    if(res.data){
                        //business logic here
                    }
                }
            });
        }

        function testimonialsTeam(){
            $rootScope.isProcessing=true;
            PrimoService.testimonialsTeam().then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length)
                    {
                        $scope.isDataFound=true;
                        $scope.testimonialsTeam = res.data.Data;
                    }
                    else{
                        $scope.isDataFound=false;
                    }
                    
                }
                
                if(res.status == 0){
                    ngToast.creat("Something went wrong");
                }
            });
        }

        function loadScholarships()
        {
            
            PrimoService.Scholarships($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.items = res.data.result.data;
                    
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });
        }

        function LoadSinglePage(slug){
            
            PrimoService.Scholarship(slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.item = res.data.result;
                }
                
            });
        }

        $scope.loadMore = function() 
        {
                $scope.lastpage +=1;
                PrimoService.Scholarships($scope.lastpage).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
            }

        function scholarshipsSearchPage(){
            $rootScope.isProcessing = true;
            $scope.items = [];

            PrimoService.Scholarships(1).then(function(response){
                if(response.data.status == 1){
                    $scope.items = response.data.result.data;
                    $scope.totalScholarship = response.data.result.total;
                    $scope.pagination = false;
                }
            });


            
            PrimoService.getAllColleges().then(function(response){
                $rootScope.isProcessing = false;
                if(response.data.status == 1){
                    $scope.colleges = response.data.result;
                }
            });
        }

        $scope.searchScholarship = function(search){

            $scope.lastpage = 1;
            $scope.isProcessing = true;
            $scope.items = [];
            
            PrimoService.searchScholarship($scope.lastpage,search.college.title).then(function(response){
                $scope.isProcessing = false;
                if(response.data.status == 1){

                    if(response.data.result.current_page == response.data.result.last_page)
                    {
                            $scope.is_disable = true;
                    }
                    $scope.pagination = true;
                    $scope.items = response.data.result.data;
                    $scope.totalScholarship = response.data.result.total;
                }
            });
        }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.controller('TosController', function() {
});

app.directive('activeLink', ['$location', function (location) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs, controller) {
      var clazz = attrs.activeLink;
      var path  = attrs.href || attrs.ngHref;
      path      = path || "";
      path      = path.substr(1, path.length); //remove hashtag

      scope.location = location;
      scope.$watch('location.path()', function (newPath) {
        //match top level menu links ie. match account side nave when /account/list is selected
        var regex   = new RegExp('^/.*/');
        var subPath = newPath.match(regex); //remove hashtag
        //remove trailing / from path
        subPath = (subPath && subPath[0]) ? subPath[0].substr(0, subPath[0].length - 1) : "nosub";

        if (path === newPath || path == subPath) {
          element.addClass(clazz);
        } else {
          element.removeClass(clazz);
        }
      });
    }
  };
}]);

app.directive('alerts', ["$timeout", "$filter", function ($timeout, $filter) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: "partials/alert-template.html",
    controller: ["$scope", function ($scope) {

      $scope.isVisible = false;
      $scope.message   = '';

      $scope.$watch('alert', function (val) {
        if (val == undefined || val.context == undefined || val.context.msg == undefined) return;

        $scope.isVisible = true;
        $scope.message   = $filter('formatAlert')(val.context.msg);

        $scope.color = val.context.type || 'success';

        //fadeout
        $timeout(function () {
          $scope.isVisible = false;
        }, 3000);

      });
    }]
  }
}]);
app.directive('footerView', function() {
  return {
    transclude: true,
    restrict: "E",
    templateUrl: "partials/footer-view.html",
    controller: ["$scope", "$rootScope", "$state", "UiService", "$filter", "PrimoService", "$window", "AuthService", function($scope, $rootScope, $state, UiService, $filter, PrimoService, $window, AuthService) {

      /**
       * Opens a dialog and submit values received from dialog
       */
      $scope.createTicketDialog = function() {
        UiService.showAlertDialog('AccountTicketCreateController', 'partials/account-ticket-create.html', function(val) {
          if (val) {
            PrimoService.createTicket(val).then(function() {
              $rootScope.alert = {
                show: true,
                context: {type: 'success', msg: $filter('translate')('SUCCESSFULLY_CREATED_TICKET')}
              };
            });
          }
        })
      };

      $scope.showDestinations = function() {
        UiService.showAlertDialog('FreeCountriesController', 'partials/free-countries-list.html', function() {
        }, 'sweet_60_rate_plan')
      };

      /**
       * Resolve download url based on access device
       */
      $scope.resolveDownloadUrl = function() {
        if (UiService.isAndroidDevice()) $window.open('https://play.google.com/store/apps/details?id=com.primo.mobile.android.app');
        if (UiService.isIOSDevice()) $window.open('https://itunes.apple.com/app/id976647948');
        $state.go('download');
      }

    }]
  }
});
app.directive('emptyResult', function() {
  return {
    restrict: 'E',
    transclude: true,
    templateUrl: "partials/empty-result.html",
    scope: {
      message: "@"
    }
  }
});
app.directive('gaSendEvent', ["mParticleService", function (mParticleService) {
  return {
    scope: {
      eventCategory: '@',
      eventAction: '@',
      eventLabel: '@',
      mparticleUnlimitedBuy: "="
    },
    link: function (scope, iElement, attrs) {
      iElement.bind('click', function () {
        //send to Google Analytics
        if (attrs.eventLabel) {
          ga('send', {
            hitType: 'event',
            eventCategory: attrs.eventCategory,
            eventAction: attrs.eventAction,
            eventLabel: attrs.eventLabel
          });
        }

        //send to mParticle
        if (attrs.mparticleLabel) {
          mParticleService.logEvent(attrs.mparticleLabel, mParticle.EventType.Other)
        }

        if (attrs.mparticleBuyCredit) {
          mParticleService.logBuyCreditEvent(attrs.mparticleBuyCredit)
        }

        if (attrs.mparticleMonthlyBuy) {
          var data = JSON.parse(attrs.mparticleMonthlyBuy);
          mParticleService.logMonthlyBuyNowEvent(data[0], data[1]);
        }

        if (attrs.mparticleUnlimitedBuy) {
          var data = JSON.parse(attrs.mparticleUnlimitedBuy);
          mParticleService.logMonthlyBuyNowEvent(data[0], data[1]);
        }
      });
    }
  };
}]);
app.directive('homeCarousel', ["UiService", function (UiService) {
  return {
    transclude: true,
    restrict: "E",
    templateUrl: "partials/home-carousel.html",
    controller: ["$scope", "$element", "$document", function ($scope, $element, $document) {

      $document.ready(function () {
        var slidesWrapper = $('.cd-hero-slider');

        //check if a .cd-hero-slider exists in the DOM
        if (slidesWrapper.length > 0) {
          var primaryNav           = $('.cd-primary-nav'),
              sliderNav            = $('.cd-slider-nav'),
              navigationMarker     = $('.cd-marker'),
              slidesNumber         = slidesWrapper.children('li').length,
              visibleSlidePosition = 0,
              autoPlayId,
              autoPlayDelay        = 5000;

          //upload videos (if not on mobile devices)
          uploadVideo(slidesWrapper);

          //autoplay slider
          setAutoplay(slidesWrapper, slidesNumber, autoPlayDelay);

          //on mobile - open/close primary navigation clicking/tapping the menu icon
          primaryNav.on('click', function (event) {
            if ($(event.target).is('.cd-primary-nav')) $(this).children('ul').toggleClass('is-visible');
          });

          //change visible slide
          sliderNav.on('click', 'li', function (event) {
            event.preventDefault();
            var selectedItem = $(this);
            if (!selectedItem.hasClass('selected')) {
              // if it's not already selected
              var selectedPosition = selectedItem.index(),
                  activePosition   = slidesWrapper.find('li.selected').index();

              if (activePosition < selectedPosition) {
                nextSlide(slidesWrapper.find('.selected'), slidesWrapper, sliderNav, selectedPosition);
              } else {
                prevSlide(slidesWrapper.find('.selected'), slidesWrapper, sliderNav, selectedPosition);
              }

              //this is used for the autoplay
              visibleSlidePosition = selectedPosition;

              updateSliderNavigation(sliderNav, selectedPosition);
              updateNavigationMarker(navigationMarker, selectedPosition + 1);
              //reset autoplay
              setAutoplay(slidesWrapper, slidesNumber, autoPlayDelay);
            }
          });

          $scope.prevSlide = function () {
            var index = slidesWrapper.find('.selected').index();
            if (index - 1 >= 0) {
              prevSlide(slidesWrapper.find('.selected'), slidesWrapper, sliderNav, index - 1);
            } else {
              nextSlide(slidesWrapper.find('.selected'), slidesWrapper, sliderNav, slidesWrapper.children().length - 1);
            }
          };

          $scope.nextSlide = function () {
            var index = slidesWrapper.find('.selected').index();
            if (index + 1 < slidesWrapper.children().length) {
              nextSlide(slidesWrapper.find('.selected'), slidesWrapper, sliderNav, index + 1);
            } else {
              prevSlide(slidesWrapper.find('.selected'), slidesWrapper, sliderNav, 0);
            }
          };
        }

        function nextSlide(visibleSlide, container, pagination, n) {
          visibleSlide.removeClass('selected from-left from-right').addClass('is-moving').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
            visibleSlide.removeClass('is-moving');
          });

          updateNavigationMarker(navigationMarker, n + 1);
          container.children('li').eq(n).addClass('selected from-right').prevAll().addClass('move-left');
          checkVideo(visibleSlide, container, n);
        }

        function prevSlide(visibleSlide, container, pagination, n) {
          updateNavigationMarker(navigationMarker, n + 1);
          visibleSlide.removeClass('selected from-left from-right').addClass('is-moving').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
            visibleSlide.removeClass('is-moving');
          });

          container.children('li').eq(n).addClass('selected from-left').removeClass('move-left').nextAll().removeClass('move-left');
          checkVideo(visibleSlide, container, n);
        }

        function updateSliderNavigation(pagination, n) {
          var navigationDot = pagination.find('.selected');
          navigationDot.removeClass('selected');
          pagination.find('li').eq(n).addClass('selected');
        }

        function setAutoplay(wrapper, length, delay) {
          if (wrapper.hasClass('autoplay')) {
            clearInterval(autoPlayId);
            autoPlayId = window.setInterval(function () {
              autoplaySlider(length)
            }, delay);
          }
        }

        function autoplaySlider(length) {
          if (visibleSlidePosition < length - 1) {
            nextSlide(slidesWrapper.find('.selected'), slidesWrapper, sliderNav, visibleSlidePosition + 1);
            visibleSlidePosition += 1;
          } else {
            prevSlide(slidesWrapper.find('.selected'), slidesWrapper, sliderNav, 0);
            visibleSlidePosition = 0;
          }
          updateNavigationMarker(navigationMarker, visibleSlidePosition + 1);
          updateSliderNavigation(sliderNav, visibleSlidePosition);
        }

        function uploadVideo(container) {
          container.find('.cd-bg-video-wrapper').each(function () {
            var videoWrapper = $(this);
            if (videoWrapper.is(':visible')) {
              // if visible - we are not on a mobile device
              var videoUrl = videoWrapper.data('video'),
                  video    = $('<video loop><source src="' + videoUrl + '.mp4" type="video/mp4" /><source src="' + videoUrl + '.webm" type="video/webm" /></video>');
              video.appendTo(videoWrapper);
              // play video if first slide
              if (videoWrapper.parent('.cd-bg-video.selected').length > 0) video.get(0).play();
            }
          });
        }

        function checkVideo(hiddenSlide, container, n) {
          //check if a video outside the viewport is playing - if yes, pause it
          var hiddenVideo = hiddenSlide.find('video');
          if (hiddenVideo.length > 0) hiddenVideo.get(0).pause();

          //check if the select slide contains a video element - if yes, play the video
          var visibleVideo = container.children('li').eq(n).find('video');
          if (visibleVideo.length > 0) visibleVideo.get(0).play();
        }

        function updateNavigationMarker(marker, n) {
          marker.removeClassPrefix('item').addClass('item-' + n);
        }

        $.fn.removeClassPrefix = function (prefix) {
          //remove all classes starting with 'prefix'
          this.each(function (i, el) {
            var classes  = el.className.split(" ").filter(function (c) {
              return c.lastIndexOf(prefix, 0) !== 0;
            });
            el.className = $.trim(classes.join(" "));
          });
          return this;
        };

      });

      $scope.discoverMore = function () {
        UiService.scrollToElement(angular.element($document[0].getElementById('primo-connect')), 70);
      }

    }]
  }
}]);
app.directive('msisdnControl', ["phoneUtils", "CountryService", "$http", "_", function (phoneUtils, CountryService, $http, _) {
  return {
    templateUrl: "partials/msisdn-control.html",
    restrict: 'E',
    require: "?ngModel",
    scope: {
      isDisabled: '=ngDisabled'
    },
    link: function (scope, element, attrs, ctrl) {

      scope.countries = CountryService.getCountryCodes();

      scope.model = {
        prefix: '',
        number: ''
      };

      var ngModelHolderName = attrs.ngModel.substr(0, attrs.ngModel.indexOf('.'));

      ctrl.$render = function () {

        if (ctrl.$viewValue && /[0-9]{7,}/.test(ctrl.$viewValue)) {
          updateRegionNumber(phoneUtils.getRegionCodeForNumber('+' + ctrl.$viewValue));
        } else {
          $http.jsonp('https://freegeoip.net/json/?callback=JSON_CALLBACK').success(function (response) {
            updateRegionNumber(response.country_code);
          }).error(function () {
            updateRegionNumber('US');
          });
        }

        function updateRegionNumber(regionCode) {
          if (!regionCode) {
            return;
          }

          var country = _.find(scope.countries, {iso: regionCode});

          updateParent(country.iso);

          var regionNumber = country.code;

          scope.model = {
            prefix: regionNumber,
            number: ctrl.$viewValue ? ctrl.$viewValue.slice(regionNumber.length) : ''
          }
        }

      };

      scope.$watch('model', function () {

        var rawNumber       = getRawNumber(),
            formattedNumber = '+' + rawNumber;

        var country = _.find(scope.countries, {code: scope.model.prefix});
        if (country) {
          updateParent(country.iso);
        }

        var isNumber = /^[0-9]{3,}$/.test(scope.model.number);
        ctrl.$setValidity('number', isNumber);

        var isPossible = isNumber ? phoneUtils.isPossibleNumber(formattedNumber) : false;
        ctrl.$setValidity('possible', isPossible);

        var isValid = isPossible ? phoneUtils.isValidNumber(formattedNumber) : false;
        ctrl.$setValidity('valid', isValid);

        if (isNumber && isPossible && isValid) {
          ctrl.$setViewValue(rawNumber);
        } else {
          ctrl.$setViewValue(null);
        }

      }, true);

      function updateParent(iso) {
        // force update parent model object with "msisdn_country_code
        scope.$parent[ngModelHolderName]['msisdn_country_code'] = iso;
        scope.$parent[ngModelHolderName]['country']             = iso;
        scope.$parent[ngModelHolderName]['new_country']         = iso;
        scope.$parent[ngModelHolderName]['new_msisdn']          = getRawNumber();
      }

      function getRawNumber() {
        return (scope.model.prefix || '') + (scope.model.number || '');
      }

    }
  }
}]);

/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.directive('navSide', function () {
  return {
    restrict: "E",
    templateUrl: "partials/nav-side.html",
    controller: ["$rootScope", "$scope", "$filter", "PrimoService", "SessionService", "$state", function ($rootScope, $scope, $filter, PrimoService, SessionService, $state) {
      $scope.state = $state;
      $scope.items = [
        {
          name: $filter('translate')('MY_ACCOUNT'),
          url: "account",
          image: "resources/images/nav-side/menu_myaccount.png",
          imageSelected: "resources/images/nav-side/menu_myaccount_sel.png"
        },
        {
          name: $filter('translate')('ADD_CREDIT'),
          url: "creditChoosePlan",
          image: "resources/images/nav-side/menu_credit.png",
          imageSelected: "resources/images/nav-side/menu/menu_credit.png"
        },
        {
          name: $filter('translate')('LOGOUT'),
          onclick: "logout()",
          image: "resources/images/account/menu/my_account_sign_out.png",
          imageSelected: "resources/images/account/menu/my_account_sign_out_sel.png"
        }
      ];

      $scope.$on('pps-get-sip-balance', function (ev, args) {
        PrimoService.getSIPBalanace().then(function (data) {
          $rootScope.user.balance      = data.data.balance;
          $rootScope.user.intl_minutes = data.data.intl_minutes;
          SessionService.updateSession($rootScope.user);
          return data;
        });
      });


      $scope.$on('pps-balance-new', function (ev, args) {
        $rootScope.user.balance      = data.bal;
        $rootScope.user.intl_minutes = data.intlmin;
        SessionService.updateSession($rootScope.user);
        return data;
      });
      /**
       * determines selected item from local storage saved menu state
       * @type {boolean}
       */
      $scope.selected = ($rootScope.menu && $rootScope.menu['mm']) ? $scope.items[$rootScope.menu['mm']] : $scope.items[0];
      /**
       * Set selected item
       * @param item
       */
      $scope.select = function (item) {
        $scope.selected = item;
      };


      /**
       * Invoke Primo service attach did method
       */
      $scope.attachDid = function () {
        PrimoService.attachUserDid({}).then(function (data) {
          //update rootscope user and session
          $rootScope.user.direct_dial_in = data.data.direct_dial_in;
          SessionService.updateSession($rootScope.user);
        });
      };
    }]
  }
});


app.directive('navTop', function () {
  return {
    transclude: true,
    restrict: "E",
    templateUrl: "partials/nav-top.html",
    controller: ["$scope", "$rootScope", "SessionService", "AuthService", "$state", "$stateParams", "config", "UiService", function ($scope, $rootScope, SessionService, AuthService, $state,$stateParams,config,UiService) {

      $scope.config   = config;
      $scope.state    = $state;
      
      if($rootScope.userId){
        $rootScope.user = JSON.parse(localStorage.getItem('user'));
      }

      $scope.init = function(){

        if($stateParams){

            if($stateParams.current != undefined && $stateParams.current){
                if($stateParams.current.name == 'colleges'){
                $scope.menuItems=[{'title':'College Search','url':'/college-search'},{'title':'Campus Life','url':'/colleges/campus-life'},{'title':'Campus Visit 101','url':'/colleges/campus-visit'}];
                $scope.currentMenuItem = 'Colleges';
                }else if($stateParams.current.name == 'scholarship'){
                    $scope.menuItems=[{'title':'Types of Scholarships','url':'/scholarships/types-of-scholarships'},{'title':'Financial Aid','url':'/scholarships/financial-aid'},{'title':'Student Loans','url':'/scholarships/students-loans'}];
                    $scope.currentMenuItem = 'Scholarship';
                }else if($stateParams.current.name == 'news'){
                    $scope.menuItems=[{'title':'Sections','url':'/'},{'title':'Become a Writer','url':'/news/article/become-a-writer'}];
                    $scope.currentMenuItem = 'News';
                }else if($stateParams.current.name == 'events'){
                    $scope.menuItems=[{'title':'Search','url':'/events-search'},{'title':'Category','url':'/'},{'title':'Add Event','url':'/events/add-event-article'}];
                    $scope.currentMenuItem = 'Events';
                }else if($stateParams.current.name == 'jobs'){
                    $scope.menuItems=[{'title':'Career Resources','url':'/career-resources'},{'title':'Post a Job','url':'/jobs/post-a-job'}];
                    $scope.currentMenuItem = 'Jobs';
                }else if ($stateParams.current.name == '/') {
                    $scope.menuItems=[{'title':'Colleges','url':'/colleges'},{'title':'Scholarship','url':'/scholarship'},{'title':'News','url':'/news'},{'title':'Events','url':'/events'},{'title':'Jobs','url':'/jobs'},{'title':'Textbooks','url':'/textbooks'}];
                }    
            }else{

                if(window.location.pathname == '/colleges'){
                $scope.menuItems=[{'title':'College Search','url':'/college-search'},{'title':'Campus Life','url':'/colleges/campus-life'},{'title':'Campus Visit 101','url':'/colleges/campus-visit'}];
                $scope.currentMenuItem = 'Colleges';
                }else if(window.location.pathname == '/scholarships'){
                    $scope.menuItems=[{'title':'Types of Scholarships','url':'/scholarships/types-of-scholarships'},{'title':'Financial Aid','url':'/scholarships/financial-aid'},{'title':'Student Loans','url':'/scholarships/students-loans'}];
                    $scope.currentMenuItem = 'Scholarship';
                }else if(window.location.pathname == '/news'){
                    $scope.menuItems=[{'title':'Sections','url':'/'},{'title':'Become a Writer','url':'/news/article/become-a-writer'}];
                    $scope.currentMenuItem = 'News';
                }else if(window.location.pathname == '/events'){
                    $scope.menuItems=[{'title':'Search','url':'/events-search'},{'title':'Category','url':'/'},{'title':'Add Event','url':'/events/add-event-article'}];
                    $scope.currentMenuItem = 'Events';
                }else if(window.location.pathname == '/jobs'){
                    $scope.menuItems=[{'title':'Career Resources','url':'/career-resources'},{'title':'Post a Job','url':'/jobs/post-a-job'}];
                    $scope.currentMenuItem = 'Jobs';
                }else if (window.location.pathname == '/') {
                    $scope.menuItems=[{'title':'Colleges','url':'/colleges'},{'title':'Scholarship','url':'/scholarship'},{'title':'News','url':'/news'},{'title':'Events','url':'/events'},{'title':'Jobs','url':'/jobs'},{'title':'Textbooks','url':'/textbooks'}];
                }

                //$scope.menuItems=[{'title':'Colleges','url':'/colleges'},{'title':'Scholarship','url':'/scholarship'},{'title':'News','url':'/news'},{'title':'Events','url':'/events'},{'title':'Jobs','url':'/jobs'},{'title':'Textbooks','url':'/'}];
            }
            
        }

      };

      $scope.openMenu = function ($mdOpenMenu, ev) {
        originatorEv = ev;
        $mdOpenMenu(ev);
      };

      $scope.logOut = function () {

        SessionService.destroySession();
        localStorage.removeItem('user');
        localStorage.removeItem('userId');
      };

      $scope.setHeadeMenuItem = function(currentMenuItem,value){

            $scope.currentMenuItem = currentMenuItem;

            $scope.setDRPMenuItem=["colleges","scholarship","news","events","jobs","textBooks"];

            switch(currentMenuItem){
                case 'colleges':
                $scope.menuItems=[{'title':'College Search','url':'/college-search'},{'title':'Campus Life','url':'/colleges/campus-life'},{'title':'Campus Visit 101','url':'/colleges/campus-visit'}];
                $scope.currentMenuItem = 'Colleges';
                $state.go("colleges");
                break;

                case 'scholarship':
                $scope.menuItems=[{'title':'Types of Scholarships','url':'/scholarships/types-of-scholarships'},{'title':'Financial Aid','url':'/scholarships/financial-aid'},{'title':'Student Loans','url':'/scholarships/students-loans'}];
                $scope.currentMenuItem = 'Scholarship';
                $state.go("scholarship");
                break;

                case 'news':
                $scope.menuItems=[{'title':'Sections','url':'/'},{'title':'Become a Writer','url':'/news/article/become-a-writer'}];
                $scope.currentMenuItem = 'News';
                $state.go("news");
                break;

                case 'events':
                $scope.menuItems=[{'title':'Search','url':'/events-search'},{'title':'Category','url':'/'},{'title':'Add Event','url':'/events/add-event-article'}];
                $scope.currentMenuItem = 'Events';
                $state.go("events");
                break;

                case 'jobs':
                $scope.menuItems=[{'title':'Career Resources','url':'/career-resources'},{'title':'Post a Job','url':'/jobs/post-a-job'}];
                $scope.currentMenuItem = 'Jobs';
                
                $state.go("jobs");
                break;

                case 'textBooks':
                $scope.menuItems=[{'title':'Sell/Post Textbook','url':'/'},{'title':'Textbook Resources','url':'/'}];
                $scope.currentMenuItem = 'textBooks';
                $state.go("/textbooks");
                break;
            }
        };

        $scope.changeCollege = function() {
              UiService.showConfirmDialog('CollegeSelectController', 'partials/college-select.html', function(clb) {
                if (clb) {
                  PrimoService.inviteFriends({invited_emails: clb}).then(function(response) {
                    $scope.$root.alert = {
                      show: true,
                      context: {type: 'success', msg: $filter('translate')('INVITE_SUCCESSFULLY SENT')}
                    }
                  });
                }
              })
        };

      $scope.login = AuthService.login;

      $scope.init();
    }],
    link: function (scope) {
      (function () {

        // Create mobile element
        // var mobile       = document.createElement('div');
        // mobile.className = 'nav-mobile';
        // document.querySelector('.nav').appendChild(mobile);

        // // hasClass
        // function hasClass(elem, className) {
        //   return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
        // }

        // // toggleClass
        // function toggleClass(elem, className) {
        //   var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
        //   if (hasClass(elem, className)) {
        //     while (newClass.indexOf(' ' + className + ' ') >= 0) {
        //       newClass = newClass.replace(' ' + className + ' ', ' ');
        //     }
        //     elem.className = newClass.replace(/^\s+|\s+$/g, '');
        //   } else {
        //     elem.className += ' ' + className;
        //   }
        // }

        // // Mobile nav function
        // var mobileNav     = document.querySelector('.nav-mobile');
        // var toggle        = document.querySelector('.nav-list');
        // mobileNav.onclick = function () {
        //   toggleClass(this, 'nav-mobile-open');
        //   toggleClass(toggle, 'nav-active');
        // };

        // $('.nav-item a:not([ng-click])').click(function () {
        //   toggleClass(this, 'nav-mobile-open');
        //   toggleClass(toggle, 'nav-active');
        // });

      })();
    }

    //test menu code
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.directive('primoCreditCard', function () {
  return {
    restrict: 'E',
    transclude: true,
    templateUrl: "partials/primo-credit-card.html",
    scope: {
      title: "@",
      titleImage: "@",
      text: "@",
      text2: "@",
      text3: "@",
      selected: "=selected",
      buttonAction: "@",
      mparticleLabel: "@",
      mparticleBuyCredit: "@"
    }
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.directive('primoMiniCard', function() {
  return {
    restrict: 'E',
    transclude: false,
    templateUrl: "partials/primo-mini-card.html",
    scope: {
      title: "@",
      titleImage: "@",
      buttonAction: "@"
    }
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 *
 * Directive used to display empty result Image and text. It is being displayed on all pages without results
 *
 */
app.directive('seeCountriesCard', function () {
  return {
    restrict: 'E',
    transclude: true,
    templateUrl: "partials/see-countries-card.html",
    controller: ["$scope", "UiService", function ($scope, UiService) {
      $scope.showDialog = function (filter) {
        UiService.showAlertDialog('FreeCountriesController', 'partials/free-countries-list.html', function () {
        }, filter)
      }
    }]
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */


app.directive('primoRateCard', function () {
  return {
    restrict: 'E',
    transclude: true,
    templateUrl: "partials/rate-card.html",
    scope: {
      title: "@",
      titleImage: "@",
      text: "@",
      text2: "@",
      text3: "@",
      buttonAction: "@",
      mparticleLabel: "@",
      mparticleMonthlyBuy: "="
    }
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller('AccountDeactivateController', ["$rootScope", "$scope", "PrimoService", function ($rootScope, $scope, PrimoService) {

  $scope.deactivate = function () {
    PrimoService.deactivateAccount({}).success(function () {
      $rootScope.logout();
    });
  }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller('AccountChangePasswordController', ["$scope", "PrimoService", "$filter", function ($scope, PrimoService, $filter) {

  $scope.changePassword = function () {
    var data = {
      old_password: $scope.checkSecret,
      new_password: $scope.secret
    };

    PrimoService.changePassword(data)
      .success(function () {
        $scope.$root.alert = {
          show: true,
          context: {type: 'success', msg: $filter('translate')('PASSWORD_CHANGE_SUCCESS')}
        };
      })
  };

}]);

app.controller('AccountInviteFriendsController', ["$rootScope", "$scope", "$mdDialog", "$filter", function ($rootScope, $scope, $mdDialog, $filter) {

  $scope.emailTemplateText = $filter('translate')('EMAIL_TEMPLATE_TEXT');

  $scope.email        = "";
  $scope.inviteEmails = [];

  //index of selected item
  $scope.selectedItemIndex = -1;

  $scope.answer = function (answer) {
    if (answer) {
      $mdDialog.hide($scope.inviteEmails);
    } else {
      $mdDialog.hide(false);
    }
  };

  /**
   * add to emails list
   */
  $scope.addToList = function () {
    //TODO: fetch limit from services
    if ($scope.inviteEmails.length == 5) {
      $rootScope.alert = {show: true, context: {type: 'info', msg: $filter('translate')("LIMIT_INVITE_PERIOD")}};
      return;
    }
    //already added email can not be added
    if ($scope.inviteEmails.indexOf($scope.email) !== -1) {
      $rootScope.alert = {show: true, context: {type: 'danger', msg: $filter('translate')("EMAIL_ALREADY_ADDED")}};
      return;
    }

    //evaluate agains email reges
    var patt = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
    var res  = new RegExp(patt).test($scope.email);

    //add to the list
    if (res) {
      $scope.inviteEmails.push($scope.email);
      $scope.email = "";
    }
  };

  /**
   * Remove element from array
   * @param index
   */
  $scope.removeFromList = function (index) {
    if ($scope.selectedItemIndex == index) {
      $scope.inviteEmails.splice(index, 1);
      $scope.selectedItemIndex = -1
    }
  };

  $scope.select = function (index) {
    $scope.selectedItemIndex = ($scope.selectedItemIndex != index) ? index : -1;
  };

  /**
   * checks if item is selected
   * @param index
   * @returns {boolean}
   */
  $scope.isSelected = function (index) {
    return $scope.selectedItemIndex == index;
  }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller('AccountHelpController', ["$rootScope", "$scope", function($rootScope, $scope) {
}]);
/**
 * Used to view/edit currently logged user details
 */
app.controller('AccountProfileController', ["$rootScope", "UiService", "$scope", "$q", "$http", "UtilHelperService", "PrimoService", "SessionService", "$filter", "AuthService", "config", "Base64", function($rootScope,
                                                    UiService,
                                                    $scope, $q, $http,
                                                    UtilHelperService,
                                                    PrimoService, SessionService, $filter, AuthService, config, Base64) {
        $rootScope.isProcessing = false;
        $scope.editProfile  = true;
        $scope.myMessages   = false;
        $scope.myContents   = false;
        $scope.disableForm  = true;
        $scope.addForm      = 'books';
        $scope.conditions   = {0 : "New", 1 : "Used"};
        $scope.trades       = {0 : "Money Transaction", 1 : "Money & Trade", 2 : "Trade"};
        $scope.want_sell    = {0 : "Want to sell", 1 : "Sold"};
        $scope.statusArr    = {0 : "Inactive", 1 : "Active"};
        $scope.industries   = {"Customer Service" : "Customer Service", "Manufacturing" : "Manufacturing","Retail":"Retail","Other":"Other"};
        $scope.job_types    = {"Full Time" : "Full Time", "Part Time" : "Part Time", "Other" : "Other"};
        $scope.locations    = {"Within campus" : "Within campus", "Outside campus" : "Outside campus","Online":"Online"};
        $scope.event_types  = {"School event" : "School event", "Other event" : "Other event"};
        $scope.publicArr    = {0 : "Public", 1 : "Private"};
        $scope.user         = {};
        $scope.data         = {};
        $scope.module       = "";
        $scope.lastpage     = 1;
        $scope.updateForm   = false;
        $scope.loadingImage ='';
        $rootScope.isProcessing=false;
        $scope.user           = JSON.parse(localStorage.getItem('user'));

        initController();
        
        function initController(){
            $scope.loadingImage="././images/loader.gif";
            
            $scope.selectedCollegeId=window.localStorage.getItem('selectedCollegeOrUniversity');
            $rootScope.isProcessing=true;
            var data = {userid:$scope.user.id};
            PrimoService.Profile(data).then(function(res){
                var response = res.data;
                if(response.status == 1){
                    $rootScope.isProcessing=false;
                    $scope.user.first_name=response.data.first_name;
                    $scope.user.last_name=response.data.last_name;
                    $scope.user.email=response.data.email;
                    $scope.user.gender=response.data.gender;
                    $scope.user.birthday=response.data.birthday;
                    $scope.user.gender=response.data.gender;
                    $scope.user.phone=response.data.phone ? response.data.phone: 'XXXXXXX-XXX';
                    $scope.user.education=response.data.education ? response.data.birthday :'None';
                    $scope.user.biography=response.data.biography;

                    $scope.user.scholarship_apply=response.data.scholarship_apply;
                    $scope.user.applied=response.data.job.applied.length;
                    $scope.user.favourite=response.data.job.favourite.length;
                    $scope.user.posted=response.data.job.posted.length;
                
                }

            
            });

        }

        $scope.Profile = function(){
            //return "Deepak Saini";
        }

        $scope.EditProfile = function(){
          $rootScope.isProcessing = true;
          $scope.myMessages   = false;
          $scope.myContents   = false;
          $scope.editProfile  = true;
        }
        /**Message module starts**/
        $scope.showingInbox = true;
        $scope.MyMessages = function(){
            $rootScope.isProcessing = true;
            $scope.myMessages   = true;
            $scope.myContents   = false;
            $scope.editProfile  = false;
            PrimoService.getInbox().then(function(response) {
                $scope.inboxList = response.inbox;
            });
        }

        $scope.showContact = function() {
            PrimoService.getContactUser().then(function(response) {
                if (response.status == 1) {
                    $scope.userContacts = response.users;
                    $scope.showingContact = true;
                    $scope.showingInbox = false;
                }else{
                    $socpe.userContacts = [];
                }
            });
        }

        $scope.showInbox = function() {
            $scope.showingInbox = true;            
            $scope.showingContact = false;
        }

        $scope.getGroup = function(userId) {
            PrimoService.getGroup(userId).then(function(response) {
                $scope.receiver = response.message[0].receiver;
                $scope.sender = response.message[0].sender;
                if (response.message[0].receiver.email == $scope.email) {
                    $scope.sender = response.message[0].receiver;
                    $scope.receiver = response.message[0].sender;
                }else if(response.message[0].sender.email == $scope.email){
                    $scope.sender = response.message[0].sender;
                    $scope.receiver = response.message[0].receiver;
                }
                $scope.chatStream = response.message[0].message;
                $scope.group_id = response.message[0].id;
                $scope.sender = response.message[0].sender;
                $scope.receiver = response.message[0].receiver;
            });
        }

        $scope.sendMsg = function(msg) {
            console.log(msg);
            var data = {group_id: $scope.group_id, message: msg.message};
            $('#get-msg').val('');
            PrimoService.sendMessage(data).then(function(response) {
                if (response.status == 1) {
                    if (response.message.length != 0) {
                        console.log(response.message.length);
                        $scope.chatStream.push(response.message);
                    }
                    $scope.fileThumbnail = false;
                    var list = $('#message');
                    var scrollHeight = list.prop('scrollHeight')
                    list.animate({scrollTop: scrollHeight}, 500);
                }
            });
            // var send = MAIN.sendMessage(data);
            // send.success(function(response) {
            //     console.log(response);
            //     if (response.status == 1) {
            //         $scope.emojiMessage.rawhtml = null;
            //         if (response.fileMsg.length != 0) {
            //             $scope.chatStream.push(response.fileMsg);
            //         }
            //         if (response.message.length != 0) {
            //             console.log(response.message.length);
            //             $scope.chatStream.push(response.message);
            //         }
            //         $scope.fileThumbnail = false;
            //         var list = $('#message');
            //         var scrollHeight = list.prop('scrollHeight')
            //         list.animate({scrollTop: scrollHeight}, 500);
            //     }
            // });
        }

        /**Message module ends**/


        $scope.MyContents = function(){
            $scope.enableForm = false;
          $rootScope.isProcessing = true;
          $scope.myMessages   = false;
          $scope.myContents   = true;
          $scope.editProfile  = false;
            
            BookService.UserBooks($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    $scope.module       = 'module/templates/book/list.view.html';
                    $scope.items        = res.data.result.data;
                }
                
                if(res.status == 0){
                    $scope.module       = 'module/templates/book/error.view.html';
                    alert("Something went wrong");
                }
            });

        }

        $scope.editForm = function(){
          $scope.disableForm = false;
        }
        
        $scope.saveForm = function(){
          $scope.disableForm = true;

          PrimoService.updateInfo($scope.user).then(function(response){
            console.log(response);
          });
          console.log($scope.user);
        }

        $scope.getUserBooks = function(){
            $scope.enableForm   = false;
          $rootScope.isProcessing = true;
            $scope.myMessages   = false;
            $scope.myContents   = true;
            $scope.editProfile  = false;
            
            BookService.UserBooks($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    $scope.module       = 'module/templates/book/list.view.html';
                    $scope.items        = res.data.result.data;
                }
                
                if(res.status == 0){
                    $scope.module       = 'module/templates/book/error.view.html';
                    alert("Something went wrong");
                }
            });
        }

        $scope.getUserClassified = function(){
            $scope.enableForm   = false;
            $rootScope.isProcessing = true;
            $scope.myMessages   = false;
            $scope.myContents   = true;
            $scope.editProfile  = false;
            
            BookService.UserBooks($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    $scope.module       = 'module/templates/book/list.view.html';
                    $scope.items        = res.data.result.data;
                }
                
                if(res.status == 0){
                    $scope.module       = 'module/templates/book/error.view.html';
                    alert("Something went wrong");
                }
            });
        }

        $scope.getUserJobs = function(){
            $scope.enableForm   = false;
            $rootScope.isProcessing = true;
            $scope.myMessages   = false;
            $scope.myContents   = true;
            $scope.editProfile  = false;
            
            JobService.UserJobs($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    $scope.module       = 'module/templates/job/list.view.html';
                    $scope.items        = res.data.result.data;
                }
                
                if(res.status == 0){
                    $scope.module       = 'module/templates/job/error.view.html';
                    alert("Something went wrong");
                }
            });
        }
        $scope.getUserEvents = function(){
            $scope.enableForm   = false;
            $rootScope.isProcessing = true;
            $scope.myMessages   = false;
            $scope.myContents   = true;
            $scope.editProfile  = false;
            
            EventService.UserEvents($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    $scope.module       = 'module/templates/event/list.view.html';
                    $scope.items        = res.data.result.data;
                }
                
                if(res.status == 0){
                    $scope.module       = 'module/templates/event/error.view.html';
                    alert("Something went wrong");
                }
            });
        }

        $scope.uploadImage =function(event){
         if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = (imagesrc) => {
              data.image = imagesrc.target.result;
              console.log(data.image);
            }
            reader.readAsDataURL(event.target.files[0]);
         }
        }
        
        $scope.Save = function(){
            BookService.Create($scope.data).then(function(response) {
                console.log(response);
            if (response.data.status == 1) {
                           
                        FlashService.Success('Book Created Successfully', true);
                        $scope.data = {};
                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating books process please contact to admin");
                        vm.dataLoading = false;
                    }
          });
        }

        $scope.saveJob = function(){
            JobService.Create($scope.data).then(function(response) {
                console.log(response);
                if (response.data.status == 1) {
                           
                        FlashService.Success('Job Created Successfully', true);
                        $scope.data = {};
                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating job please contact to admin");
                        vm.dataLoading = false;
                    }
            });
        }

        $scope.saveEvent = function(){
            EventService.Create($scope.data).then(function(response) {
                console.log(response);
                if (response.data.status == 1) {
                           
                        FlashService.Success('Event Created Successfully', true);
                        $scope.data = {};
                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating event please contact to admin");
                        vm.dataLoading = false;
                    }
            });
        }

        $scope.doEnable = function(){
            console.log("enableForm");
            $scope.enableForm = true;
            $scope.updateForm = false;
            $scope.data = {};
        }

        $scope.doDisable = function(){
            console.log("disableForm");
            $scope.enableForm = false;
        }

        $scope.loadMoreBooks = function(){
                $scope.lastpage +=1;

                BookService.Books($scope.lastpage).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
        }
        $scope.loadMoreJobs = function(){
                $scope.lastpage +=1;

                JobService.Jobs($scope.lastpage).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
        }

        $scope.loadMoreEvents = function(){
                $scope.lastpage +=1;

                EventService.Events($scope.lastpage).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
        }

        $scope.EditBook = function(book_slug){
            
            BookService.Book(book_slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.data = res.data.result;
                    $scope.data.trade_type = res.data.result.trade_type.toString();
                    $scope.data.status = res.data.result.status.toString();
                    $scope.data.sold = res.data.result.sold.toString();
                    $scope.data.condition = res.data.result.condition.toString();

                }
                   
            });
            $scope.enableForm = true;
            $scope.updateForm = true;
        }

        $scope.UpdateBook = function(){
            BookService.Update($scope.data).then(function(response) {
                console.log(response);
                if (response.data.status == 1) {
                        
                        FlashService.Success('Book Updated Successfully', true);

                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating books process please contact to admin");
                        vm.dataLoading = false;
                    }
            });
        }

        $scope.DeleteBook = function(){
            console.log("To Do");
        }

        $scope.EditEvent = function(event_slug){
            
            EventService.Event(event_slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.data = res.data.result;
                    $scope.data.event_type = res.data.result.event_type.toString();
                    $scope.data.public = res.data.result.public.toString();
                    $scope.data.status = res.data.result.status.toString();
                    $scope.data.start_date = "20-07-2017";
                }
                   
            });
            $scope.enableForm = true;
            $scope.updateForm = true;
        }

        $scope.UpdateEvent = function(){
            EventService.Update($scope.data).then(function(response) {
                console.log(response);
                if (response.data.status == 1) {
                        
                        FlashService.Success('Event Updated Successfully', true);

                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating books process please contact to admin");
                        vm.dataLoading = false;
                    }
            });
        }

        $scope.DeleteEvent = function(){
            console.log("To Do");
        }

        $scope.EditJob = function(job_slug){
            
            JobService.Job(job_slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.data = res.data.result;
                    $scope.data.job_type = res.data.result.job_type.toString();
                    $scope.data.status = res.data.result.status.toString();
                    $scope.data.industry = res.data.result.industry.toString();
                    $scope.data.job_location = res.data.result.job_location.toString();

                }
                   
            });
            $scope.enableForm = true;
            $scope.updateForm = true;
        }

        $scope.UpdateJob = function(){
            JobService.Update($scope.data).then(function(response) {
                console.log(response);
                if (response.data.status == 1) {
                        
                        FlashService.Success('Job Updated Successfully', true);

                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating books process please contact to admin");
                        vm.dataLoading = false;
                    }
            });
        }

    $scope.DeleteJob = function(){
        console.log("To Do");
    }
    $scope.logout = function(){
        AuthenticationService.ClearCredentials();
        $rootScope.isUserLoggedIn = false;
    }
    
    function moduleDiv(){
      return {
          restrict: 'EA',
          template: "module/templates/{{name}}/create.view.html"
        }
    }

}]);
/**
 * Used to view/edit currently logged user details
 */
app.controller('AccountProfileController', ["$rootScope", "UiService", "$scope", "$q", "$http", "UtilHelperService", "PrimoService", "SessionService", "$filter", "AuthService", "config", "Base64", "$uibModal", "ngToast", function($rootScope,
                                                    UiService,
                                                    $scope, $q, $http,
                                                    UtilHelperService,
                                                    PrimoService, SessionService, $filter, AuthService, config, Base64,$uibModal,ngToast) {
        
        $scope.config               = config;
        $rootScope.isProcessing     = false;
        $scope.user                 = {};
        $scope.data                 = {};
        $scope.user                 = JSON.parse(localStorage.getItem('user'));
        $scope.jobsLastPage         = 1;
        $scope.eventsLastPage       = 1;
        $scope.booksLastPage        = 1;
        $scope.classifiedLastPage   = 1;
        $scope.bookCategories       = [];
        $scope.errors               = [];
        $scope.industries           = {"Customer Service" : "Customer Service", "Manufacturing" : "Manufacturing","Retail":"Retail","Other":"Other"};
        $scope.job_types            = {"Full Time" : "Full Time", "Part Time" : "Part Time", "Other" : "Other"};
        $scope.locations            = {"Within campus" : "Within campus", "Outside campus" : "Outside campus","Online":"Online"};
        $scope.job_experince_array  = {0 : "0 Year", 1 : "1 Year",2:"2 Year",3 : "3 Year", 4 : "4 Year",5:"5 Year",6 : "6 Year", 7 : "7 Year",8:"8 Year",9 : "9 Year", 10 : "10 Year",'more':"10+ Year"};
        $scope.job_salary_type_array  = {1 : "Hourly", 2 : "Weekly",3:"Monthly",4 : "Quarterly", 5 : "Yearly"};
        $scope.in_campus_array       = {1 : "Yes", 2 : "No"};
        initController();
        
        function initController(){
            
            $scope.selectedCollegeId=window.localStorage.getItem('selectedCollegeOrUniversity');
            $rootScope.isProcessing=true;
            if($rootScope.userId){
                $rootScope.user = JSON.parse(localStorage.getItem('user'));
            }
            if($rootScope.user.id){
              var data = {userid:$rootScope.user.id};
            }else{
              var data = {userid:$rootScope.user.user_id};
            }
            
            PrimoService.Profile(data).then(function(res){
                
                var response = res.data;
                if(response.status == 1){
                    localStorage.setItem('user',JSON.stringify(response.data));
                    if($rootScope.userId){
                        $rootScope.user = JSON.parse(localStorage.getItem('user'));
                    }

                    $rootScope.isProcessing =false;
                    $scope.user.first_name      =   response.data.first_name;
                    $scope.user.last_name       =   response.data.last_name;
                    $scope.user.email           =   response.data.email;
                    $scope.user.gender          =   response.data.gender;
                    $scope.user.birthday        =   response.data.birthday;
                    $scope.user.gender          =   response.data.gender;
                    $scope.user.phone           =   response.data.phone ? response.data.phone: 'XXXXXXX-XXX';
                    $scope.user.education       =   response.data.education ? response.data.birthday :'None';
                    $scope.user.biography       =   response.data.biography;
                    $scope.user.scholarship_apply=  response.data.scholarship_apply;
                    $scope.user.applied         =   response.data.job.applied.length;
                    $scope.user.favourite       =   response.data.job.favourite.length;
                    $scope.user.posted          =   response.data.job.posted.length;
                    $scope.user.books           =   response.data.books;
                    $scope.user.classified      =   response.data.classified;
                    $scope.user.events          =   response.data.events;
                    $scope.user.jobs            =   response.data.job.posted;
                    $scope.user.image            =   response.data.image;
                }

            
            });

        }

        /**
         * load All books of current user
         * 
         */
        $scope.userBooksLoadMore = function(){

            $scope.booksLastPage +=1;
            $rootScope.isProcessing=true;
            PrimoService.GetUserBooks($scope.booksLastPage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_books_disable = true;
                    }

                    if(!res.data.result){
                        $scope.is_books_disable = true;
                    }

                    $scope.user.books = $scope.user.books.concat(res.data.result.data);
                }
            });
        }

        /**
         * load All events of current user
         * 
         */
        $scope.userEventsLoadMore = function(){
            $scope.eventsLastPage +=1;
            $rootScope.isProcessing=true;
            PrimoService.GetUserEvents($scope.eventsLastPage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_events_disable = true;
                    }

                    if(!res.data.result){
                        $scope.is_events_disable = true;
                    }

                    $scope.user.events = $scope.user.events.concat(res.data.result.data);
                }
            });
        }

        /**
         * load All classified of current user
         * 
         */

        $scope.userClassifiedLoadMore = function(){
            $scope.classifiedLastPage +=1;
            $rootScope.isProcessing   =true;
            PrimoService.GetUserClassified($scope.classifiedLastPage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_classified_disable = true;
                    }

                    if(!res.data.result){
                        $scope.is_classified_disable = true;
                    }

                    $scope.user.classified = $scope.user.classified.concat(res.data.result.data);
                }
            });
        }

        /**
         * load All Jobs of current user
         * 
         */

        $scope.userJobsLoadMore = function(){
            $scope.jobsLastPage +=1;
            $rootScope.isProcessing=true;
            PrimoService.GetUserJobs($scope.jobsLastPage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_jobs_disable = true;
                    }

                    if(!res.data.result){
                        $scope.is_jobs_disable = true;
                    }

                    $scope.user.jobs = $scope.user.jobs.concat(res.data.result.data);
                }
            });
        }

        /**
         * Add Book
         * 
         */

        $scope.userAddBooksPopup = function(){

            $uibModal.open({
            templateUrl: 'partials/add.books.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: ["$scope", "$uibModalInstance", "$log", function ($scope, $uibModalInstance, $log) {
                    PrimoService.GetBookCategoriesWithParent().then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.bookCategories = res.data.result;
                        }
                        
                        if(res.status == 0){
                            $scope.bookCategories = [];   
                        }
                    });

                    $scope.userAddBook = function () {
                        
                        $rootScope.isProcessing = true;

                        PrimoService.CreateBooks($scope.book).then(function(response){
                            $rootScope.isProcessing = false;

                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Book has been added");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }]
            });
        }

        /**
         * update Book
         * 
         */
        $scope.userUpdateBooksPopup = function(book_id){

            $uibModal.open({
            templateUrl: 'partials/update.books.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: ["$scope", "$uibModalInstance", "$log", function ($scope, $uibModalInstance, $log) {

                    PrimoService.GetBookCategoriesWithParent().then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.bookCategories = res.data.result;
                        }
                        
                        if(res.status == 0){
                            $scope.bookCategories = [];   
                        }
                    });

                    PrimoService.GetSingleModuleById('books',book_id).then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.book = res.data.result;
                            $scope.image_preview = $scope.book.image;
                        }
                        
                        if(res.status == 0){
                            $scope.book = [];   
                        }
                    });

                    $scope.userUpdateBook = function () {
                        
                        $rootScope.isProcessing = true;
                        
                        PrimoService.UpdateBooks($scope.book,$scope.book.id).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Book has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }]
            });
        }

        /**
         * Delete Book
         * 
         */
        $scope.userDeleteBooks = function(book_id){


            PrimoService.DeleteBooks(book_id).then(function(response){
                    $rootScope.isProcessing = false;
                    
                    if(response.status == 200){
                        if(response.data.status == 1){
                            initController();
                            alert("Book has been deleted.");
                        }

                        if(response.data.status == 0){
                            alert("You don't have permission to delete this book.");
                        }
                    }
                        
                });
        }

        /**
         * Add Events
         * 
         */

        $scope.userAddEventsPopup = function(){

            $uibModal.open({
            templateUrl: 'partials/add.events.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: ["$scope", "$uibModalInstance", "$log", function ($scope, $uibModalInstance, $log) {
                    
                    PrimoService.getEventsHost().then(function(response){
                        if(response.status == 200){
                                if(response.data.status == 1){
                                    $scope.hosts = response.data.result;

                                    if(response.data.result.length == 0){
                                        $uibModalInstance.dismiss('cancel');
                                        alert("Please add host first");
                                        
                                    }
                                }
                            }
                    });

                    setTimeout(function() {
                            $('#datetimepicker1').datetimepicker({minDate: new Date(),format:'YYYY-MM-DD HH:MM A'});
                            $('#datetimepicker2').datetimepicker({minDate: new Date(),format:'YYYY-MM-DD HH:MM A'});
                        }, 10);

                    $scope.userAddEvent = function () {
                        
                        var start_date = $('input[name=start_date]').val();

                        if(!start_date){
                            alert("Start date is required");
                            return false;
                        }
                        
                        var end_date = $('input[name=end_date]').val();
                        
                        if(!end_date){
                            alert("End date is required");
                            return false;
                        }

                        $scope.event['start_date'] = start_date;
                        $scope.event['end_date'] = end_date;

                        $rootScope.isProcessing = true;

                        PrimoService.CreateEvents($scope.event).then(function(response){
                            $rootScope.isProcessing = false;

                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Event has been added");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }]
            });
        }

        /**
         * update Events
         * 
         */
        $scope.userUpdateEventsPopup = function(event_id){

            $uibModal.open({
            templateUrl: 'partials/update.events.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: ["$scope", "$uibModalInstance", "$log", function ($scope, $uibModalInstance, $log) {

                    PrimoService.getEventsHost().then(function(response){
                        if(response.status == 200){
                                if(response.data.status == 1){
                                    $scope.hosts = response.data.result;

                                    if(response.data.result.length == 0){
                                        alert("Please add host first");
                                        $uibModalInstance.dismiss('cancel');
                                    }
                                }
                            }
                    });
                    
                    setTimeout(function() {
                            $('#datetimepicker1').datetimepicker({minDate: new Date(),format:'YYYY-MM-DD HH:MM A'});
                            $('#datetimepicker2').datetimepicker({minDate: new Date(),format:'YYYY-MM-DD HH:MM A'});
                        }, 10);

                    PrimoService.GetSingleModuleById('events',event_id).then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.event = res.data.result;
                            $scope.image_preview = $scope.event.image;
                        }
                        
                        if(res.status == 0){
                            $scope.event = [];   
                        }
                    });

                    $scope.userUpdateEvent = function () {
                        
                        var start_date = $('input[name=start_date]').val();

                        if(!start_date){
                            alert("Start date is required");
                            return false;
                        }
                        
                        var end_date = $('input[name=end_date]').val();
                        
                        if(!end_date){
                            alert("End date is required");
                            return false;
                        }

                        $scope.event['start_date'] = start_date;
                        $scope.event['end_date'] = end_date;

                        $rootScope.isProcessing = true;
                        
                        PrimoService.UpdateEvents($scope.event,$scope.event.id).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Event has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }]
            });
        }

        /**
         * Events Book
         * 
         */
        $scope.userDeleteEvents = function(event_id){


            PrimoService.DeleteEvents(event_id).then(function(response){
                    $rootScope.isProcessing = false;
                    
                    if(response.status == 200){
                        if(response.data.status == 1){
                            initController();
                            alert("Event has been deleted.");
                        }

                        if(response.data.status == 0){
                            alert("You don't have permission to delete this event.");
                        }
                    }
                        
                });
        }

        /**
         * Add Classified
         * 
         */

        $scope.userAddClassifiedPopup = function(){

            $uibModal.open({
            templateUrl: 'partials/add.classified.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: ["$scope", "$uibModalInstance", "$log", function ($scope, $uibModalInstance, $log) {
                    
                    $scope.userAddClassified = function () {
                        
                        $rootScope.isProcessing = true;

                        PrimoService.CreateClassified($scope.classified).then(function(response){
                            $rootScope.isProcessing = false;

                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Classified has been added");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }]
            });
        }

        /**
         * update Classified
         * 
         */
        $scope.userUpdateClassifiedPopup = function(classified_id){

            $uibModal.open({
            templateUrl: 'partials/update.classified.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: ["$scope", "$uibModalInstance", "$log", function ($scope, $uibModalInstance, $log) {

                    PrimoService.GetSingleModuleById('classifieds',classified_id).then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.classified = res.data.result;
                            $scope.image_preview = $scope.book.image;
                        }
                        
                        if(res.status == 0){
                            $scope.classified = [];   
                        }
                    });

                    $scope.userUpdateClassified = function () {
                        
                        $rootScope.isProcessing = true;
                        
                        PrimoService.UpdateClassified($scope.classified,$scope.classified.id).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Classified has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }]
            });
        }

        /**
         * Classified Book
         * 
         */
        $scope.userDeleteClassified = function(classified_id){


            PrimoService.DeleteClassified(classified_id).then(function(response){
                    $rootScope.isProcessing = false;
                    
                    if(response.status == 200){
                        if(response.data.status == 1){
                            initController();
                            alert("Classified has been deleted.");
                        }

                        if(response.data.status == 0){
                            alert("You don't have permission to delete this classified.");
                        }
                    }
                        
                });
        }

        /**
         * Add Jobs
         * 
         */

        $scope.userAddJobsPopup = function(){

            $uibModal.open({
            templateUrl: 'partials/add.jobs.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: ["$scope", "$uibModalInstance", "$log", function ($scope, $uibModalInstance, $log) {
                    
                    $scope.userAddJob = function () {
                        
                        $rootScope.isProcessing = true;

                        PrimoService.CreateJobs($scope.job).then(function(response){
                            $rootScope.isProcessing = false;

                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Job has been added");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }]
            });
        }

        /**
         * update Jobs
         * 
         */
        $scope.userUpdateJobsPopup = function(job_id){

            $uibModal.open({
            templateUrl: 'partials/update.jobs.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: ["$scope", "$uibModalInstance", "$log", function ($scope, $uibModalInstance, $log) {

                    PrimoService.GetSingleModuleById('jobs',job_id).then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.job = res.data.result;
                            $scope.image_preview = $scope.job.image;
                        }
                        
                        if(res.status == 0){
                            $scope.job = [];   
                        }
                    });

                    $scope.userUpdateJob = function () {
                        
                        $rootScope.isProcessing = true;
                        
                        PrimoService.UpdateJobs($scope.job,$scope.job.id).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Job has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }]
            });
        }

        /**
         * Jobs Book
         * 
         */
        $scope.userDeleteJobs = function(job_id){


            PrimoService.DeleteJobs(job_id).then(function(response){
                    $rootScope.isProcessing = false;
                    
                    if(response.status == 200){
                        if(response.data.status == 1){
                            initController();
                            alert("Job has been deleted.");
                        }

                        if(response.data.status == 0){
                            alert("You don't have permission to delete this job.");
                        }
                    }
                        
                });
        }

        /**
         * Add Events Host
         */
        $scope.AddEventHostPopup = function(){

            $uibModal.open({
            templateUrl: 'partials/add.host.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: ["$scope", "$uibModalInstance", "$log", function ($scope, $uibModalInstance, $log) {

                    $scope.AddEventHost = function () {
                        
                        $rootScope.isProcessing = true;
                        
                        PrimoService.AddEventHost($scope.host).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Host has been added");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }]
            });
        }

        /**
         * Invite Friends Module
         */

        $scope.inviteFriends = function() {
          UiService.showConfirmDialog('AccountInviteFriendsController', 'partials/account-invite-friends.html', function(clb) {
            if (clb) {
              PrimoService.inviteFriends({invited_emails: clb}).then(function(response) {
                $scope.$root.alert = {
                  show: true,
                  context: {type: 'success', msg: $filter('translate')('INVITE_SUCCESSFULLY SENT')}
                }
              });
            }
          })
        }

        /**
         * Update Profile
         */

        $scope.updateProfilePop = function() {
          $uibModal.open({
            templateUrl: 'partials/edit.profile.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: ["$scope", "$uibModalInstance", "$log", function ($scope, $uibModalInstance, $log) {

                    $scope.user.birthday = new Date($scope.user.birthday);

                    $scope.updateProfile = function () {
                        
                        $rootScope.isProcessing = true;
                        
                        PrimoService.updateInfo({birthday:$scope.user.birthday,
                                                first_name:$scope.user.first_name,
                                                last_name:$scope.user.last_name,
                                                gender:$scope.user.gender
                                                }).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Profile has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }]
            });
        }

        $scope.changeProfilePictureClickedEvent = function(){
            $uibModal.open({
            templateUrl: 'partials/edit.profile.image.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: ["$scope", "$uibModalInstance", "$log", function ($scope, $uibModalInstance, $log) {

                    
                    $scope.changeProfilePicture = function () {
                        console.log($scope.image);
                        data = $scope.image;
                        $rootScope.isProcessing = true;
                        
                        PrimoService.updateProfileImage(data).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Profile has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }]
            });
        }
}]);

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
}]);

app.controller("LoginController", ["$scope", "PrimoService", "$rootScope", "$state", "$stateParams", "$q", "$cookies", "SessionService", "$mdDialog", "AuthService", "ngToast", function($scope, PrimoService, $rootScope, $state,$stateParams,
                                           $q, $cookies,
                                           SessionService, $mdDialog, AuthService, ngToast) {


  $scope.user       = {};
  $scope.isRemember = false;

  var localHashKey = localStorage.getItem('hashkey');
  if($stateParams.hashkey)
  {
    if(localHashKey == $stateParams.hashkey){
      localStorage.removeItem('hashkey');
      $scope.registrationSuccessfull = "Registration successfully done, Please login here";
    }
  }
  //fetch remembered user from cookie if exists
  if ($cookies.getObject('username')) {
    $scope.user.username = $cookies.getObject('username');
    $scope.isRemember    = true;
  }

  /**
   * Call primo to check username
   */
  $scope.verifyUsername = function(user) {
    $scope.errorMsg = false;
    $scope.invalidMsg = false;

    if(user.email && user.password){
        $rootScope.isProcessing = true;
        AuthService.doLogin($scope.user).then(function(data){
                $rootScope.isProcessing = false;
                if(data.status == 0){
                    $scope.errorMsg = true;
                }

                if(data.status == 1){
                  localStorage.setItem("token",data.token);
                  localStorage.setItem('user',JSON.stringify(data.data));
                  localStorage.setItem('userId',data.data.name);
                  $scope.errorMsg = false;
                  var state = $rootScope.lastState || "profile";
                  $state.go(state, $rootScope.stateParams);
                }

        })
        .catch(function(err){
            $rootScope.isProcessing = false;
            $scope.invalidMsg = true;
        });
    }
  };

  /**
   * Call Primo service login and get account info. Store to localStorage
   */
  $scope.login = function() {
    AuthService.doLogin($scope.user, {
      epay: function(data) {
        $state.go('register', {claim: data.alias, token: data.login_token});
      },
      normal: function() {
        var state = $rootScope.lastState || "profile";
        $state.go(state, $rootScope.stateParams);
        $mdDialog.cancel();
      }
    })
  };

  $scope.cancelDialog = function() {
    $mdDialog.cancel();
  };

  $scope.register = function() {
    AuthService.register();
  };

  $scope.forgotPassword = function() {
    AuthService.forgotPassword();
  };

  $scope.forgotUsername = function() {
    AuthService.forgotUsername();
  };

  $rootScope.$on('$stateChangeStart', function() {
    $mdDialog.cancel();
  });

  $scope.setSelectedTab = function(index) {
    $scope.selectedTab = index;

    if (index == 1) {
      //focus password
      setTimeout(function() {
        $('#inputPassword').focus();
      }, 200);
    }
  };


  /**
   * Add username to cookie
   */
  $scope.rememberMe = function() {
    $cookies.putObject('username', $scope.user.username);
  };

  /**
   * Remove username from cookie
   */
  $scope.forgetUser = function() {
    $cookies.remove('username');
  };

}]);
app.controller("RegisterController", ["$scope", "PrimoService", "$rootScope", "SessionService", "$stateParams", "$q", "$sce", "$state", "$timeout", "$filter", "$mdDialog", "AuthService", "ngToast", function ($scope, PrimoService, $rootScope, SessionService,
                                               $stateParams, $q, $sce,
                                               $state, $timeout, $filter, $mdDialog, AuthService, ngToast) {
        $scope.selected = undefined;
        
        $scope.education_levels = [];
        $scope.states     = [];
        $scope.colleges      = [];

        var vm = this;
        $scope.state = "";
        $scope.primary_school = "";
        $scope.step1 = true;
        $scope.step2 = false;
        vm.ForgetPasswordRequest = ForgetPasswordRequest;
        vm.ResetPasswordRequest = ResetPasswordRequest;

        initController();
        
        function initController() {

            //AuthenticationService.ClearCredentials();
            $rootScope.isUserLoggedIn = false;

            PrimoService.GetStateEducationlevelCollege()
                .then(function (response) {
                    
                    if (response.status == 200 ) {

                        angular.forEach(response.data.education_levels, function(value, key) {
                          this.push(value);
                        }, $scope.education_levels);

                        angular.forEach(response.data.states, function(value, key) {
                          this.push(value);
                        }, $scope.states);

                        angular.forEach(response.data.colleges, function(value, key) {
                            this.push(value);
                        }, $scope.colleges);

                        console.log($scope.colleges);
                        
                    } else {
                        $scope.primarySchool = ['High School', 'Intermediate', 'Bachelor','Master Degree'];
                        $scope.stateList = ['California', 'Florida', 'texas','Hawaii'];
                        $scope.colleges = ['you can add new college'];
                    }
                });
            
        }

        $scope.goToStep2 = function() {
            $scope.step1 = false;
            $scope.step2 = true;
        }

        $scope.goToStep1 = function() {
            $scope.step1 = true;
            $scope.step2 = false;
        }

        $scope.register = function(user) {

            $rootScope.isProcessing = true;
            $scope.errors = [];
            $scope.validation = {};
            
            if(user.state.title == '' || user.state.title == undefined)
            {
                $scope.validation['state'] = "State is required*";
            }

            if(user.education_level.title == '' || user.education_level.title == undefined)
            {
                $scope.validation['education_level'] = "Education Level is required*";
            }

            if(user.college.title == '' || user.college.title == undefined)
            {
                $scope.validation['college'] = "College is required*";
            }

            if(user.gender == '' || user.gender == undefined)
            {
                $scope.validation['gender'] = "Gender is required*";
            }

            if(!$.isEmptyObject($scope.validation))
            {
                $rootScope.isProcessing = false;
                angular.forEach($scope.validation, function(key,value){
                    $scope.errors.push(key);
                });
                return false;
            }

            var data = {};
            data = {
                      name:user.name,
                      email:user.email,
                      first_name:user.first_name,
                      last_name:user.last_name,
                      gender:user.gender,
                      password:user.password,
                      state:user.state.title,
                      education_level:user.education_level.title,
                      college:user.college.title
                    };

            PrimoService.CreateRegistration(data)
                .then(function (response) {
                    $rootScope.isProcessing = false;
                    
                    if (response.data.status == 1) {
                        var haskey = randomHashKey();
                        localStorage.setItem('hashkey',haskey);
                        debugger;
                        $state.go('login',{hashkey:haskey});
                    } else if(response.data.status == 0){

                        angular.forEach(response.data.message,function(key,value){
                            $scope.errors.push(key);
                        });
                        
                    } else {
                        $scope.errors = "Sorry, There is a problem in registration process please contact to admin";
                    }
                });
        }

        function ForgetPasswordRequest () {
            
            PrimoService.ForgetPasswordRequest(vm.user)
                .then(function (response){
                    console.log(response);
                })
        }

        function ResetPasswordRequest () {
            
            PrimoService.ResetPasswordRequest(vm.user)
                .then(function (response){
                    console.log(response);
                })
        }

        function randomHashKey() {
          var text = "";
          var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

          for (var i = 0; i < 32; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

          return text;
        }

        $scope.withoutSanitize = function (message) {
                return $sce.trustAsHtml(message);
        };
    
  // $scope.claimNumber    = $stateParams.claim;
  // $scope.isClaimAccount = $stateParams.claim && $stateParams.claim != "";
  // $scope.token          = $stateParams.token;

  // $scope.resolve = function () {
  //   if ($scope.isClaimAccount) {
  //     $scope.claimAccount();
  //   } else {
  //     $scope.register();
  //   }
  // };

  // $scope.register = function () {
  //   $scope.user['tos_accepted']  = ($scope.isAgreed) ? 1 : 0;
  //   $scope.user['signup_source'] = "WEB";

  //   AuthService.doRegister($scope.user, {
  //     withMSISDN: function () {
  //       $rootScope.alert = {
  //         show: true,
  //         context: {type: 'success', msg: $filter('translate')('SUCCESS_REGISTER')}
  //       };
  //       $timeout(function () {
  //         AuthService.verifyMSISDN();
  //       }, 2000);
  //     },
  //     withoutMSISDN: function () {
  //       $state.go('profile');
  //     }
  //   });
  // };

  // $scope.claimAccount = function () {
  //   AuthService.doClaimAccount($scope.claimNumber, $scope.token, $scope.user, function () {
  //     $state.go('profile');
  //   });
  // };

  // $scope.cancelDialog = function () {
  //   $mdDialog.cancel()
  // };

  // $scope.login = function () {
  //   AuthService.login();
  // };

  // $rootScope.$on('$stateChangeStart', function () {
  //   $mdDialog.cancel();
  // });


}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller("VerifyMSISDNController", ["$scope", "PrimoService", "$rootScope", "$state", "$stateParams", "$timeout", "$filter", "$mdDialog", function ($scope, PrimoService, $rootScope, $state, $stateParams, $timeout, $filter, $mdDialog) {

  $scope.user          = {};
  $scope.user.username = $stateParams.username || $rootScope.user.username;
  $scope.user.msisdn   = $stateParams.msisdn || $rootScope.user.msisdn;
  $scope.user.country  = $stateParams.country || $rootScope.user.country;
  $scope.user.pin      = "";

  /**
   * Call primo api to verify msisdn
   */
  $scope.verify = function () {
    PrimoService.verifyMSISDN($scope.user).success(function (data, status, headers) {
      $scope.$root.alert = {
        show: true,
        context: {type: 'success', msg: $filter('translate')('SUCCESS_VERIFIED')}
      };
      $timeout(function () {
        $mdDialog.cancel();
      }, 2000);
    });
  };

  $scope.cancelDialog = function () {
    $mdDialog.cancel()
  };

  $rootScope.$on('$stateChangeStart', function () {
    $mdDialog.cancel();
  });

}]);
app.controller("ForgotPasswordController", ["$scope", "PrimoService", "$state", "$window", "$timeout", "$filter", "$rootScope", "$mdDialog", "AuthService", function($scope, PrimoService, $state, $window,
                                                    $timeout, $filter, $rootScope, $mdDialog, AuthService) {

  $scope.buttonText = "Send Password Reset Link";
  $scope.forgotPassword = function(user) {
    $scope.success = "";
    $scope.error = "";
    $rootScope.isProcessing = true;
    PrimoService.ForgetPasswordRequest({email: user.email}).success(function(response) {
      $rootScope.isProcessing = false;
      if(response.status == 1){

        $scope.success = response.message;
        $scope.buttonText = "Password Reset Link Sent";
      }

      if(response.status == 0){
        $scope.error = response.message;
      }
    });
  };

}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller("ForgotUsernameController", ["$scope", "$state", "PrimoService", "$rootScope", "$filter", "$mdDialog", "AuthService", "_", "CountryService", "ngToast", function($scope,
                                                    $state,
                                                    PrimoService,
                                                    $rootScope, $filter, $mdDialog, AuthService, _, CountryService, ngToast) {

  //initial values
  $scope.user = {
    email: "",
    msisdn: ""
  };

  /**
   * Call Primo API to retrieve forgotten username
   * You can retrieve by email or msisdn
   */
  $scope.forgotUsername = function() {
    var params = {};
    if ($scope.user.email) {
      params['email'] = $scope.user.email;
    } else if ($scope.user.msisdn) {
      params['msisdn'] = $scope.user.msisdn;
    }

    if (params['email'] || params['msisdn']) {
      PrimoService.recoverUsername(params).success(function(data) {
        if (params['email']) {
          $rootScope.alert = {
            show: true,
            context: {type: 'success', msg: $filter('translate')("RECOVER_USERNAME_EMAIL_SENT")}
          };
        } else {
          $rootScope.alert = {
            show: true,
            context: {type: 'success', msg: $filter('translate')("RECOVER_USERNAME_SMS_SENT")}
          };
        }

      });
    }

  };

  $scope.cancelDialog = function() {
    $mdDialog.cancel();
  };

  $scope.forgotPassword = function() {
    AuthService.forgotPassword();
  };

  $rootScope.$on('$stateChangeStart', function() {
    $mdDialog.cancel();
  });

  $scope.$watch('user', function(newValue) {
    $scope.isDisabledMsisdn = !!newValue.email;
    console.log('$scope.isDisabledMsisdn', $scope.isDisabledMsisdn)
    $scope.isDisabledEmail = !!newValue.msisdn;
    console.log('$scope.isDisabledEmail', $scope.isDisabledEmail)
  }, true)

}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.directive('homeSearchBar', function() {
  return {
    restrict: 'E',
    templateUrl: "partials/home-search-bar.html",
    replace: true,
    controller: ["$scope", "$element", "$rootScope", "$state", "$stateParams", "InternationalRatesService", "mParticleService", "ServiceProductsService", "PrimoService", "$q", "$filter", "UiService", "AuthService", "$anchorScroll", "_", function($scope, $element, $rootScope,
                         $state, $stateParams,
                         InternationalRatesService,
                         mParticleService,
                         ServiceProductsService,
                         PrimoService, $q, $filter, UiService, AuthService, $anchorScroll, _) {

      $scope.promo        =  'SUB_10';

      $scope.serviceProducts = ServiceProductsService.getServiceProducts();

      angular.forEach($scope.serviceProducts.groups, function(val, key) {
        if (val.group == 'CREDIT') {
          $scope.credit_products       = val.products;
          $scope.selectedCreditProduct = $scope.credit_products[0];
        }
      });

      /**
       * Fetch the list of popular destinations
       */
      $scope.getPopularCountries = function() {
        ServiceProductsService.getPopularCountries($scope.promo).then(function(data) {
          $scope.popularCountries = data;
        });
      };

      $scope.getPopularCountries($scope.promo);

      $scope.selectedItemChange   = selectedItemChange;
      $scope.searchForLocation    = searchForLocation;
      $scope.trySearchForLocation = trySearchForLocation;

      //selected item default empty
      $scope.selected = null;

      //default search text value
      if ($stateParams.country) {
        trySearchForLocation($stateParams.country);
      } else {
        $scope.searchText = "";
      }

      $scope.countryPlanFilter = function(product) {
        return product.type === 'COUNTRY';
      };
      /**
       * Select item on click
       * @param name
       */
      $scope.selectCountryOnClick = function(name) {
        selectedItemChange(searchForLocation(name)[0]);
      };

      $scope.showDialog = function(filter) {
        UiService.showAlertDialog('FreeCountriesController', 'partials/free-countries-list.html', function() {
        }, filter)
      };

      /**
       * Opens a dialog which is used to invite friends
       * User has to be authenticated in order to invite friends
       */
      $scope.inviteFriends = function() {
        //non authenticated user is transferred to login page
        if (!$rootScope.user) {
          AuthService.login();
          return;
        }
        UiService.showConfirmDialog('AccountInviteFriendsController', 'partials/account-invite-friends.html', function(clb) {
          if (clb) {
            PrimoService.inviteFriends({invited_emails: clb}).then(function(response) {
              $scope.$root.alert = {
                show: true,
                context: {type: 'success', msg: $filter('translate')('INVITE_SUCCESSFULLY SENT')}
              }
            });
          }
        });
      };

      $scope.formatAutocompleteItem = function(item) {
        return item.country + " |||"
      };

      $anchorScroll.yOffset = 50;

      $scope.$watch('selected', function(newValue) {
        if (newValue) {
          UiService.scrollToElement($element, 70);
        }
      });

      function trySearchForLocation(searchText) {
        var results = searchForLocation(searchText);
        if (results && angular.isArray(results) && results.length) {
          selectedItemChange(results[0])
        } else {
          $scope.$root.alert = {
            show: true,
            context: {
              type: 'danger',
              msg: $filter('translate')('NO_MATCHING_STATES') + ' "' + $scope.searchText + '" ' + $filter('translate')('WERE_FOUND')
            }
          }
        }
      }

      function searchForLocation(text) {
        if (text) {
          return InternationalRatesService.getNewCountryByName(text);
        } else {
          return InternationalRatesService.getRatePlanCountries();
        }
      }

      function selectedItemChange(item) {
        $scope.selected = item;
      }

    }]
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.directive('rewardListCard', ["$state", function ($state) {
  return {
    restrict: 'E',
    templateUrl: "partials/reward-list-card.html",
    item: "=",
    replace: true
  }
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.directive('rewardDetails', function () {
  return {
    restrict: 'E',
    templateUrl: "partials/reward-details.html",
    scope: {
      item: "="
    },
    replace: true
  }
});
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller('AccountFAQController', ["$rootScope", "$scope", "$filter", "$state", "PrimoService", "SessionService", "$stateParams", "articles", "UiService", "AuthService", function ($rootScope, $scope,
                                                 $filter,
                                                 $state,
                                                 PrimoService,
                                                 SessionService,
                                                 $stateParams,
                                                 articles, UiService, AuthService) {


  $scope.scrollToElement = UiService.scrollToElement;

  //all sections received from zendesk with articles inside
  $scope.sections = articles;
  $scope.search   = '';

  //all articles flattened
  $scope.allArticles = [];

  var _moveArticles = function (fromCat, toCat) {
    angular.forEach(fromCat.articles, function (val, key) {
      toCat.articles.push(val);
    })
  };
  //repack sections
  var primoPhoneNumberCat,
      creditAndBillingHistoryCat,
      callingCat,
      p2pCat,
      myAccountCat,
      ratePlansCat = {};

  angular.forEach($scope.sections, function (val, key) {
    if (val.id == '205098987') primoPhoneNumberCat = val;
    if (val.id == '205099187') ratePlansCat = val;
    if (val.id == '203902818') creditAndBillingHistoryCat = val;
    if (val.id == '205090368') myAccountCat = val;
    if (val.id == '203861597') callingCat = val;
    if (val.id == '203902308') p2pCat = val;

    //add articles to list of all articles
    $scope.allArticles = [].concat($scope.allArticles, val.articles);
  });

  _moveArticles(primoPhoneNumberCat, myAccountCat);
  _moveArticles(creditAndBillingHistoryCat, ratePlansCat);
  _moveArticles(p2pCat, ratePlansCat);


  /**
   * Remove uwanted categories
   */
  $scope.sections = $scope.sections.filter(function (el) {
    return el.id != '205098987' && el.id != '203902818' && el.id != '203902308'
  });

  $scope.selected = ($stateParams.selected) ? $scope.sections[$stateParams.selected] : $scope.sections[0];

  $scope.selectedArticle = null;
  /**
   * Select faq item
   * @param item
   */
  $scope.select = function (item) {
    $scope.search   = '';
    $scope.selected = item;
    $scope.scrollToElement('#group-articles');
  };


  $scope.isSelected = function (item) {
    return ($scope.selected && $scope.selected.id == item.id);
  };


  //ARTICLE SELECTORS

  $scope.selectArticle = function (article) {
    $scope.selectedArticle = article;
  };

  $scope.isSelectedArticle = function (item) {
    return ($scope.selectedArticle && $scope.selectedArticle.id == item.id);
  };


  /**
   * Opens a dialog and submit values received from dialog
   */
  $scope.createTicketDialog = function () {
    //if (!$rootScope.user) {
    //  AuthService.login();
    //  return;
    //}
    UiService.showAlertDialog('AccountTicketCreateController', 'partials/account-ticket-create.html', function (val) {
      if (val) {
        if ($rootScope.user) {
          val.username = $rootScope.userId;
          val.name     = SessionService.getFirstName() + " " + SessionService.getLastName();
        }
        PrimoService.createTicket(val).then(function () {
          $rootScope.alert = {
            show: true,
            context: {type: 'success', msg: $filter('translate')('SUCCESSFULLY_CREATED_TICKET')}
          };
        });
      }
    })
  };
}]);
/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.filter('filterArticles', function () {
  return function (input, criteria) {
    var result = [];
    var regex  = new RegExp(criteria, 'i');
    angular.forEach(input, function (article) {
      if (regex.exec(article.name)) {
        return result.push(article);
      }
      if (regex.exec(article.body)) {
        return result.push(article);
      }
    });
    return result;
  }
});