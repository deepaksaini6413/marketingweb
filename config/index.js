/**
 * fetch config file by passed environment variable name.
 * if there is no environment variable, test environment config file is being loaded
 * @type {*|string}
 */

var fs = require('fs');

function fileExists(filePath)
{
    try
    {
        return fs.statSync(filePath).isFile();
    }
    catch (err)
    {
        return false;
    }
}

var env = process.env.NODE_ENV || 'qa';

var logger = require('../lib/logger');
logger.info('Initializing [' + env + '] environment');

var cfg = null;

var config_service = '../config/config-service.json';
if (fileExists(config_service))
{
  cfg = require(config_service);
}

if (cfg == null)
{
  cfg = require('../config/config.' + env + '.js');
}

module.exports = cfg;
