#!/bin/bash
sudo yum install bzip2
sudo yum install nodejs -y
sudo yum install npm -y
sudo npm update npm -g

sudo npm cache clean -f
sudo npm install n -g
sudo n 6.2.2
sudo ln -sf /usr/local/n/versions/node/6.2.2/bin/node /usr/bin/node
sudo npm install gulp -g
sudo npm install phantomjs -g

echo "INSTALLING DEPENDENCIES"
echo `pwd`
npm install
echo "RUNNING DEPLOY"
gulp deploy
echo `pwd`
cd ..
echo `pwd`
echo "CREATING ARCHIVE"
tar -zcvf build.tar.gz primo-app/
echo "ARCHIVE CREATED"
