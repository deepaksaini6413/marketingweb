var
  gulp         = require('gulp'),
  uglify       = require('gulp-uglify'),
  concat       = require('gulp-concat'),
  annotate     = require('gulp-ng-annotate'),
  flatten      = require('gulp-flatten'),
  inject       = require('gulp-inject'),
  series       = require('stream-series'),
  plumber      = require('gulp-plumber'),
  runSequence  = require('run-sequence'),
  //less         = require('gulp-less'),
  config       = require('./config'),
  gulpNgConfig = require('gulp-ng-config'),
  fs           = require('fs'),
  bower        = require('gulp-bower'),
  es           = require('event-stream'),
  b2v          = require('buffer-to-vinyl'),
  minifyCss    = require('gulp-minify-css'),
  htmlclean    = require('gulp-htmlclean'),
  spritesmith  = require('gulp.spritesmith'),
  jsonminify   = require('gulp-jsonminify');

var path          = require('path');
var child_process = require('child_process');

var root = "";

//build javascript, concatenate all files and do the mangle
gulp.task('build_js', function(cb) {
  var env = (process.env['NODE_ENV']) ? process.env['NODE_ENV'] : 'dev`';

  var build = gulp.src('./public/app/**/*.js')
    .pipe(plumber())
    .pipe(concat('app.js'))
    .pipe(annotate());
  if (env == 'prod') {
    build.pipe(uglify({mangle: true, compress: true}));
  }
  build.pipe(gulp.dest('./build' + root))
    .on('end', cb);
});

// copy partials
gulp.task('build_html', function() {
  gulp.src('**/*.html', {cwd: './public/app/'})
    .pipe(flatten({newPath: 'partials'}))
    .pipe(htmlclean())
    .pipe(gulp.dest('./build' + root))
});

gulp.task('build_sprites', function() {
  var spriteData = gulp.src([
      'public/resources/images/logo.png',
      'public/resources/images/logo_small.png',
      'public/resources/images/ios_badge.png',
      'public/resources/images/google_badge.png',
      'public/resources/images/amazon_badge.png',
      'public/resources/images/web_app_badge_teal.png',
      'public/resources/images/web_app_badge_black.png',
      'public/resources/images/facebook_icon.png',
      'public/resources/images/twitter_icon.png',
      'public/resources/images/instagram_icon.png',
      'public/resources/images/search_icon.png'
    ])
    .pipe(spritesmith({
      /* this whole image path is used in css background declarations */
      imgName: '../images/_sprite.png',
      cssName: '_sprite.css'
    }));
  spriteData.img.pipe(gulp.dest('public/resources/images'));
  //spriteData.css.pipe(gulp.dest('public/resources/less/utils'));
});

//include dependency modules
gulp.task('build_deps', function(cb) {
  var i = 0;

  gulp.src([
    './public/resources/vendor/angular/angular.min.js',
    './public/resources/vendor/angular-ui-router/release/angular-ui-router.min.js',
    './public/resources/vendor/angular-animate/angular-animate.min.js',
    './public/resources/vendor/angular-loading-bar/build/loading-bar.min.js',
    './public/resources/vendor/angular-translate/angular-translate.js',
    './public/resources/vendor/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
    './public/resources/vendor/angular-aria/angular-aria.js',
    './public/resources/vendor/angular-mocks/angular-mocks.js',
    './public/resources/vendor/angular-cookies/angular-cookies.min.js',
    './public/resources/vendor/momentjs/min/moment.min.js',
    './public/resources/vendor/moment-timezone/builds/moment-timezone.min.js',
    './public/resources/vendor/jquery/dist/jquery.min.js',
    './public/resources/vendor/re-tree/re-tree.js',
    './public/resources/vendor/ng-device-detector/ng-device-detector.js',
    './public/resources/vendor/jquery-ui/ui/minified/effect.js',
    './public/resources/remote/angular-material-sidenav-menu/material-menu-sidenav.min.js',
    './public/resources/vendor/ngSticky/dist/sticky.min.js',
    './public/resources/mparticle/mparticle.provider.js',
    './public/resources/vendor/ng-flow/dist/ng-flow-standalone.min.js',
    './public/resources/vendor/angular-sanitize/angular-sanitize.js',
    './public/resources/vendor/angular-messages/angular-messages.js',
    './public/resources/new-design/js/bootstrap.min.js',
    './public/resources/new-design/js/jquery-ui.min.js',
    './public/resources/new-design/js/angucomplete-alt.min.js',
  ]).pipe(concat('dependencies_part_1.js'))
    .pipe(annotate())
    .pipe(uglify())
    .pipe(gulp.dest('./build' + root + '/resources/vendor'))
    .on('end', function() {
      callback('dependencies_part_1.js')
    });

  gulp.src([
    './public/resources/vendor/angular-material/angular-material.js'
  ]).pipe(concat('dependencies_part_2.js'))
    .pipe(annotate())
    .pipe(uglify())
    .pipe(gulp.dest('./build' + root + '/resources/vendor'))
    .on('end', function() {
      callback('dependencies_part_2.js')
    });

  gulp.src([
    './public/resources/vendor/libphonenumber/dist/libphonenumber.js',
    './public/resources/vendor/braintree-angular/dist/braintree-angular.js',
    './public/resources/vendor/ng-csv/build/ng-csv.min.js',
    './public/resources/vendor/lodash/dist/lodash.min.js',
    './public/resources/new-design/js/ui-bootstrap-tpls-2.5.0.js',
    './public/resources/new-design/js/bootstrap-datetimepicker.min.js',
    './public/resources/new-design/js/ngToast.min.js',
    './public/resources/new-design/js/angular-searchAndSelect.js',
  ]).pipe(concat('dependencies_part_3.js'))
    .pipe(annotate())
    .pipe(uglify())
    .pipe(gulp.dest('./build' + root + '/resources/vendor'))
    .on('end', function() {
      callback('dependencies_part_3.js')
    });

  gulp.src([
    './public/resources/vendor/angular-material/angular-material.min.css',
    // './public/resources/vendor/ng-emoticons/dist/ng-emoticons.min.css',
    // './public/resources/vendor/angular-loading-bar/build/loading-bar.min.css',
    // './public/resources/remote/angular-material-sidenav-menu/material-menu-sidenav.css',
    './public/resources/new-design/css/bootstrap.min.css',
    './public/resources/new-design/css/jquery-ui.css',
    './public/resources/new-design/css/main.css',
    './public/resources/new-design/css/style-2.css',
    './public/resources/new-design/css/custom.css',
    './public/resources/new-design/css/bootstrap-datetimepicker.min.css',
    './public/resources/vendor/angular-loading-bar/build/loading-bar.min.css',
    './public/resources/new-design/css/ngToast.min.css',
    './public/resources/new-design/css/ngToast-animations.min.css'
  ]).pipe(concat('dependencies.css'))
    .pipe(minifyCss())
    .pipe(gulp.dest('./build' + root + '/resources/vendor'))
    .on('end', function() {
      callback('dependencies.css')
    });

  function callback(name) {
    console.log(' Finished ' + name);
    i++;
    if (i == 4) {
      return cb()
    }
  }
});

//copy static assets to build folder
gulp.task('build_assets', function() {
  //copy iso - 3166 file
  gulp.src('./public/resources/countries/*.json')
    .pipe(jsonminify())
    .pipe(gulp.dest('./build' + root + '/resources/countries'));
  //copy font files
  gulp.src('./public/resources/new-design/fonts/*.*')
    .pipe(gulp.dest('./build' + root + '/resources/fonts'));
  //copy image assets
  gulp.src('./public/resources/images/**/*')
    .pipe(gulp.dest('./build' + root + '/resources/images'));
  //copy internationalization files
  gulp.src('./public/resources/i18n/*.json')
    .pipe(jsonminify())
    .pipe(gulp.dest('./build' + root + '/resources/i18n'));
  //copy docs
  gulp.src('./public/resources/docs/*.*')
    .pipe(gulp.dest('./build' + root + '/resources/docs'));
});

// inject dependencies to index.html
gulp.task('inject_deps', function() {
  var sourcesVendor = gulp.src([
      'resources/vendor/dependencies_part_1.js',
      'resources/vendor/dependencies_part_2.js',
      'resources/vendor/dependencies_part_3.js',
      'resources/vendor/dependencies.css'],
    {read: false, cwd: "./build"});

  var sourceApp = gulp.src([
      'resources/css/style.css',
      './app.js'],
    {read: false, cwd: "./build"});

  var templates = gulp.src([
      'partials/nav-top.html',
      'partials/nav-side.html',
      'partials/footer-view.html',
      'partials/alert-template.html',
      'partials/home.html',
      'partials/home-carousel.html',
      'partials/home-search-bar.html',
      'partials/country-plans-search-bar.html',
      'partials/account-profile.html',
      'partials/app-download-badges.html',
      'partials/msisdn-control.html'
    ],
    {cwd: "./build"});

  var json = gulp.src([
      'resources/countries/countryCodes.json'
    ],
    {cwd: "./build"});

  gulp.src('./public/index.html')
    .pipe(inject(series(sourcesVendor, sourceApp)))
    .pipe(inject(templates, {
      starttag: '<!-- inject:templates -->',
      transform: function(filePath, file) {
        return '<script type="text/ng-template" id="' + filePath.substring(1) + '">' + file.contents.toString() + '</script>';
      }
    }))
    .pipe(inject(json, {
      starttag: '<!-- inject:json -->',
      transform: function(filePath, file) {
        return '<script type="application/json" id="' + filePath.substring(1) + '">' + file.contents.toString() + '</script>';
      }
    }))
    .pipe(gulp.dest('./build' + root));
});

gulp.task("build_config", function(cb) {
  var json = JSON.stringify({
    "config": {
      "primo": {"host": config.primo.host, "username": config.primo.username, "password": config.primo.password,"mediumImagePath":config.primo.mediumImagePath,"smallImagePath":config.primo.smallImagePath,"fullImagePath":config.primo.fullImagePath,"noImagePath":config.primo.noImagePath,"profileImagePath":config.primo.profileImagePath},
      "google": {
        "analytics": {"trackingCode": config.google.analytics.trackingCode}
      },
      "mparticle": {"key": config.mparticle.key, "isSandbox": config.mparticle.isSandbox}
    }
  });

  return b2v.stream(new Buffer(json), 'config.js')
    .pipe(gulpNgConfig('config'))
    .pipe(gulp.dest('./public/app'));
});

gulp.task('bower', function() {
  return bower({cwd: './public'})
});

gulp.task('include_config', function() {
  gulp.src('./public/app/config.json')
    .pipe(gulpNgConfig('config'))
    .pipe(gulp.dest('./public/app/'))
});

gulp.task('watch', ['build_js', 'build_html', 'build_assets'], function() {
  gulp.watch('./public/app/**/*.js', ['build_js']);
  gulp.watch('./public/app/**/**/*.js', ['build_js']);
  gulp.watch('./public/app/resources/*.js', ['build_js']);
  gulp.watch('./public/resources/i18n/*.json', ['build_assets']);
  gulp.watch('./public/app/**/*.html', ['build_html', 'inject_deps']);
  gulp.watch('./public/app/**/**/*.html', ['build_html', 'inject_deps']);
  gulp.watch('./public/app/**/**/**/*.html', ['build_html', 'inject_deps']);
});

gulp.task('deploy', function() {
  return runSequence('build_config', ['build_js', 'build_html', 'build_assets'], 'include_config', 'build_deps', 'inject_deps');
});