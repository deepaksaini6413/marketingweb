var winston = require('winston');

/**
 * define custom levels and colors
 */
var customLevels = {
  levels: {
    debug: 0,
    info: 1,
    warning: 2,
    error: 3
  },
  colors: {
    info: 'blue',
    debug: 'green',
    warning: 'yellow',
    error: 'red'
  }
};

/**
 * define different logger transports ie console, file,...
 */
var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      level: 'debug', // Only write logs of info level or higher
      levels: customLevels.levels,
      colorize: true,
      timestamp: function () {
        return new Date(Date.now()).toISOString();
      },
      formatter: function (options) {
        // Return string will be passed to logger.
        return   options.timestamp() + ' ' + winston.config.colorize(options.level, options.level.toUpperCase()) + ' ' + (undefined !== options.message ? options.message : '') +
          (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '' );
      }
    }),
    new (winston.transports.File)({
      filename: './web-app.log',
      maxsize: 1024 * 1024 * 10, // 10MB
      level: 'debug', //should be increased later
      levels: customLevels.levels,
      json: false,
      timestamp: true
    })
  ],
  levels: customLevels.levels
});

module.exports = logger;
