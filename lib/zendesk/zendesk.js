/*
 * Copyright (C) 2016 zendesk, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of zendesk and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of zendesk.
 *
 */

var rest        = require('restler-q'),
    UserAgentZD = 'Node-zendesk';

var zendesk = {};

//predefined zendesk options which are populated when RestAPI method is invoked
zendesk.options          = {};
zendesk.options.host     = "";
zendesk.options.username = "";
zendesk.options.password = "";

/**
 * Generate zendesk Error element
 * @param msg
 * @constructor
 */
function zendeskError(msg) {
  Error.call(this);
  Error.captureStackTrace(this, arguments.callee);
  this.message = (msg || '') + '\n';
  this.name    = 'zendeskError';
}

zendeskError.prototype = Error.prototype;

/**
 * Generate zendesk request headers elements
 * @returns {{Authorization: string, User-Agent: string}}
 * @private
 */
_generateRequestHeadersZD = function () {
  var auth = 'Basic ' + new Buffer(zendesk.options.username + ':' + zendesk.options.password).toString('base64');

  var headers = {
    'Authorization': auth,
    'User-Agent': UserAgentZD
  };

  return headers;
};

/**
 * Generates valid zendesk request based on passed parameters
 * @param url - zendesk url
 * @param method
 * @param data
 * @param callback
 */
var _requestZD = function (method, url, data, callback) {
  //generate URL from passed parameters
  var url = zendesk.options.host + url;


  var options = {
    headers: _generateRequestHeadersZD()
  };

  if (method == 'POST') {
    options.data = data;
  }
  //call zendesk server
  rest[method.toLowerCase()](url, options)
    .then(function (response) {
      callback(200, response);
    })
    .fail(function (err) {
      callback(500, new zendeskError(err)); //propagate error
    });
};

zendesk.getSections = function (callback) {
  var url    = '/api/v2/help_center/sections.json';
  var method = 'GET';

  _requestZD(method, url, null, callback);
};

/**
 * Fetch all articles defined in zendesk
 * @param callback
 */
zendesk.getArticles = function (callback) {
  var url    = '/api/v2/help_center/articles.json?per_page=50';
  var method = 'GET';

  _requestZD(method, url, null, callback);
};


zendesk.getUsers = function (name, callback) {
  var url    = '/api/v2/users/search.json?query=' + name;
  var method = 'GET';

  _requestZD(method, url, null, callback);
};

/**
 * Fetch all tickets created by particular user
 * @param id
 * @param callback
 */
zendesk.getUserTickets = function (id, callback) {
  var url    = '/api/v2/search.json?query=type:ticket requester_id:' + id;
  var method = 'GET';

  _requestZD(method, url, null, callback);
};

zendesk.createTicket = function (data, callback) {
  var url       = '/api/v2/tickets.json';
  var method    = 'POST';
  var inputData = {
    ticket: {
      requester: {
        name: data.username,
        email: data.email
      },
      tags: [data.name, data.phone],
      subject: data.subject,
      description: data.description,
      comment: {
        body: data.description
      }
    }
  };

  _requestZD(method, url, inputData, callback);

};

/**
 * Load passed configuration and expose zendesk RestAPI
 * @param config
 * @returns {{}}
 * @constructor
 */
exports.RestAPI = function (config) {
  if (!config) {
    throw new zendeskError('Username and password must be provided.');
  }

  if (typeof config != 'object') {
    throw new zendeskError('Config for RestAPI must be provided as an object.');
  }

  if (!config.host) {
    throw new zendeskError('Host must be provided.');
  }

  if (!config.username || !config.password) {
    throw new zendeskError('Username and password must be provided.');
  }

  //override default config according to the config provided.
  for (var key in config) {
    zendesk.options[key] = config[key];
  }

  return zendesk;
};