var logger = require('../lib/logger');

exports.logRequest = function (req, res, next) {
  var url  = req.originalUrl || "";
  var data = {
    type: req.method,
    url: url
  };
  if (req.params) {
    data.params = req.params;
  }

  if (req.query) {
    data.query = req.query;
  }

  if (req.body) {
    data.body = req.body;
  }


  logger.info(data);
  next();
};

exports.logErrors = function (err, req, res, next) {
  var data = {
    type: req.method,
    url: req.originalUrl || ""
  };

  if (req.params) {
    data.params = req.params;
  }

  if (req.query) {
    data.query = req.query;
  }

  if (req.body) {
    data.body = req.body;
  }

  if (err) {
    data.err   = err;
    data.stack = err.stack;
  }

  logger.error(data);
  next(err);
};