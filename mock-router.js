/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
var config = require('./config'),
    primo  = require('./lib/primo/primo.js');

var primoClient = primo.RestAPI(config.primo);

module.exports = function (router) {
  router.post('/login', function (req, res, next) {
    //if (req.body.username == 'invalidUser') {
    //  res.status(500).send(require('./test/mocks/login/login.invalid.js'));
    //} else {
    res.status(500).send(require('./test/mocks/login/loginEPAY.valid.js'));
    //}
  });

  router.post('/verify-username', function (req, res, next) {
    res.status(500).send(require('./test/mocks/login/verifyUsername.valid.js'));
  });

  router.post('/register', function (req, res, next) {
    if (req.body.username == 'invalidUser') {
      res.status(500).send(require('./test/mocks/register/register.invalid.js'));
    } else {
      res.status(200).send(require('./test/mocks/register/register.valid.js'));
    }
  });

  router.post('/get-account-info', function (req, res, next) {
    res.status(200).send(require('./test/mocks/login/getAccountInfo.valid.js'));
  });e

  /**
   * Call primo to Add selected contact to contact list
   */
  router.post('/add-to-friends', function (req, res, next) {
    res.status(200).send(require('./test/mocks/contact/addFriend.valid.js'));
  });

  /**
   * Call primo to remove selected contact to contact list
   */
  router.post('/remove-from-friends', function (req, res, next) {
    primoClient.removeFromFriends(req.body, function (code, response) {
      res.status(code).send(response);
    });
  });

  router.post('/search-users', function (req, res, next) {
    res.status(200).send(require('./test/mocks/contact/searchUsers.valid.js'));
  });

  router.post('/set-account-info', function (req, res, next) {
    if (req.body.first_name) {
      res.status(200).send(require('./test/mocks/account/profile/updateProfile.valid.js'));
    } else {
      res.status(500).send(require('./test/mocks/account/profile/updateProfile.novalues.js'));
    }
  });

  router.post('/retrieve-contact-info', function (req, res, next) {
    var contact        = require('./test/mocks/contact/retrieveContactInfo.valid.js');
    contact.contact.un = req.body.contact_login_name;
    contact.contact.fn = req.body.contact_login_name;
    contact.contact.ln = req.body.contact_login_name;
    res.status(200).send(contact);

  });

  router.get('/get-international-rates', function (req, res, next) {
    res.status(200).send(require('./test/mocks/account/getInternationalRates.valid.js'));
  });

  router.post('/change-password', function (req, res, next) {
    if (req.body.old_password == 'test') {
      res.status(200).send(require('./test/mocks/account/changePassword/changePassword.valid'));
    } else {
      res.status(500).send(require('./test/mocks/account/changePassword/changePassword.invalid'));
    }
  });

  router.post('/get-billing-history', function (req, res, next) {
    res.status(200).send(require('./test/mocks/account/getBillingHistory.valid.js'));
  });

  router.post('/get-call-history', function (req, res, next) {
    res.status(200).send(require('./test/mocks/account/getCallHistory.valid.js'));
  });

  router.post('/attach-user-did', function (req, res, next) {
    res.status(200).send(require('./test/mocks/account/attachDid.valid'));
  });

  router.post('/get-reward-triggers', function (req, res, next) {
    res.status(200).send(require('./test/mocks/getRewardTriggers.js'));
  });

  router.post('/get-reward-details', function (req, res, next) {
    res.status(200).send(require('./test/mocks/getRewardDetailsAnniversary.js'));
  });

  router.post('/get-profile-image', function (req, res, next) {
    res.status(200).send(require('./test/mocks/account/getProfileImage.valid.js'));
  });

  router.post('/set-profile-image', function (req, res, next) {
    var data = req.files.file[0];
    fs.readFile(data.path, function read(err, fileContent) {
      if (err) {
        throw err;
      }

      req.body.base64_data = new Buffer(fileContent).toString('base64');
      primoClient.setProfileImage(req.body, function (code, response) {
        res.status(code).send(response);
      });
    });

  });

  /**
   * Call factual to receive world locations
   */
  router.get('/world-location/:text', function (req, res, next) {
    //search text must be provided
    if (!req.params.text) return res.status(404).send({});
    res.status(200).send(require('./test/mocks/account/profile/getFactualWorldGeographies.valid.js').data);

  });

  /**
   * Call Primo api to get braintree client token
   */
  router.get('/get-client-token', function (req, res, next) {
    res.status(200).send(require('./test/mocks/account/addCredit/getBraintree.valid.js').client_token);
  });

  /**
   * Call Primo api to complete braintree payment with nounce
   */
  router.post('/charge-payment', function (req, res, next) {
    res.status(200).send(require('./test/mocks/account/addCredit/purchase.valid'));

  });

  return router;
};