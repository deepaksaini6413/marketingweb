/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

var express        = require('express'),
    logger         = require('./lib/logger'),
    app            = express(),
    bodyParser     = require('body-parser'),
    cors           = require('cors'),
    corsMiddleware = require('./middleware/corsMiddleware'),
    logRequest     = require('./middleware/loggerMiddleware').logRequest,
    logErrors      = require('./middleware/loggerMiddleware').logErrors,
    routes         = require('./mock-router.js')(express.Router());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app
  .use(logRequest)
  .use(cors())
  .use(corsMiddleware);

logger.info('Loading routes...');
app.use(routes);
logger.info('Routes loaded...');

//load statics
app.use('/', express.static(__dirname + '/build'));

//attach log errors middleware
app.use(logErrors);
//set port
app.set('port', process.env.port || 3001);

//init server
app.listen(app.get("port"), function () {
  logger.info("Server running on port [" + (app.get("port")) + "]");
});

module.exports = app;
