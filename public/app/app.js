/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

var app = angular.module("Primo",
  [
    'ui.router',
    'ng.deviceDetector',
    'ngAnimate',
    'ngMaterial',
    'ngMessages',
    'ngSanitize',
    'ngCookies',
    'config',
    'angular-loading-bar',
    'pascalprecht.translate',
    'mparticleWidget',
    'braintree-angular',
    'ngMenuSidenav',
    'flow',
    'ngCsv',
    'sticky',
    'ui.bootstrap',
    'angucomplete-alt',
    'ngToast',
    'angular-search-and-select'
  ]);

app.constant('clientTokenPath', '/get-client-token');
app.config(function($stateProvider, $urlRouterProvider,
                    config,
                    $httpProvider, $translateProvider,
                    $mdThemingProvider,
                    deviceDetectorProvider,
                    mparticleWidgetProvider,
                    flowFactoryProvider,
                    $compileProvider,
                    $locationProvider) {

  // Disabling Debug Data
  $compileProvider.debugInfoEnabled(false);

  $locationProvider.html5Mode(true);


  mparticleWidgetProvider.init({
    app_key: config.mparticle.key,
    isSandbox: config.mparticle.isSandbox
  });

  //translate provider
  $translateProvider.useSanitizeValueStrategy(null);
  $translateProvider.useStaticFilesLoader({
    prefix: 'resources/i18n/',
    suffix: '.json'
  });


  //theme provider
  $mdThemingProvider.definePalette('primoPrimary', {
    '50': '#515151',
    '100': '#444444',
    '200': '#373737',
    '300': '#2a2a2a',
    '400': '#1e1e1e',
    '500': '#111',
    '600': '#040404',
    '700': '#000000',
    '800': '#000000',
    '900': '#000000',
    'A100': '#5d5d5d',
    'A200': '#6a6a6a',
    'A400': '#777777',
    'A700': '#000000',
    'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                        // on this palette should be dark or light
    'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
      '200', '300', '400', 'A100'],
    'contrastLightColors': undefined    // could also specify this if default was 'dark'
  });

  $mdThemingProvider.definePalette('primoAccent', {
    '50': '#39feff',
    '100': '#20feff',
    '200': '#06feff',
    '300': '#00ebec',
    '400': '#00d1d2',
    '500': '#00b8b9',
    '600': '#009f9f',
    '700': '#008586',
    '800': '#006c6c',
    '900': '#005353',
    'A100': '#53feff',
    'A200': '#6cfeff',
    'A400': '#86feff',
    'A700': '#003939',
    'contrastDefaultColor': 'light'
  });
  //theme configuration
  $mdThemingProvider.theme('default')
    .primaryPalette('primoPrimary')
    .accentPalette('primoAccent');

  $mdThemingProvider.alwaysWatchTheme(true);

  $urlRouterProvider.otherwise(function($injector) {
    var $state = $injector.get('$state');
    $state.go("/");
  });

  $httpProvider.interceptors.push('sessionInterceptor');

  $stateProvider
    .state("login", {
      url: "/login?:hashkey",
      isPublic: true,
      isNavVisible: true,
      isAuthPage: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/login.html",
          controller: 'LoginController'
        }
      }
    })
    .state("registration", {
      url: "/sign-up",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/register.html",
          controller: "RegisterController"
        }
      }
    })
    .state("verifyMSISDN", {
      url: "/verify-msisdn/:username/:msisdn/:country",
      isPublic: true,
      isNavVisible: true,
      isAuthPage: true,
      onEnter: function(AuthService) {
        AuthService.verifyMSISDN();
      }
    })
    .state("forgotPassword", {
      url: "/forgot-password",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/forgot-password.html",
          controller: "ForgotPasswordController"
        }
      }
    })
    .state("passwordReset", {
      url: "/password/reset/:key",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/forgot-password.html",
          controller: "ForgotPasswordController"
        }
      }
    })
    
    .state("forgotUsername", {
      url: "/forgot-username",
      isPublic: true,
      isNavVisible: true,
      isAuthPage: true,
      onEnter: function(AuthService) {
        AuthService.forgotUsername();
      }
    })
    .state("tos", {
      url: "/terms-of-service",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/tos.html",
          controller: "TosController"
        }
      }
    })
    .state("privacyPolicy", {
      url: "/privacy-policy",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/privacy-policy.html",
          controller: "PrivacyController"
        }
      }
    })
    .state("/", {
      url: "/",
      isPublic: false,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/home.html",
          controller: 'HomeController'
        }
      }
    })
    .state("landing-page", {
      url: "/landing-page",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/landing.html",
          controller: 'HomeController'
        }
      }
    })
    .state("/textbooks", {
      url: "/textbooks",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/home.html",
          controller: 'HomeController'
        }
      }
    })
    .state("dashboard", {
      url: "/dashboard",
      isPublic: false,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/dashboard.html",
          controller: 'DashboardController'
        }
      }
    })
    .state("profile", {
      url: "/profile",
      isPublic: false,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/account-profile.html",
          controller: 'AccountProfileController'
        }
      }
    })
    .state("changePassword", {
      url: "/change-password",
      isPublic: false,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/account-change-password.html",
          controller: 'AccountChangePasswordController'
        }
      }
    })
    .state("inviteFriends", {
      url: "/invite-friends",
      isPublic: false,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/account-invite-friends.html",
          controller: 'AccountInviteFriendsController'
        }
      }
    })
    .state("faq", {
      url: "/faq/:selected",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/account-faq.html",
          controller: 'AccountFAQController',
          resolve: {
            articles: function(FaqService) {
              return FaqService.getFAQ().then(function(res) {
                return res;
              })
            }
          }
        }
      }
    })
    
    // Core Start From Here
    .state("news", {
      url: "/news",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/news.html",
          controller: "NewsController"
        }
      }
    })
    .state("newsDetail", {
      url: "/news/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.news.html",
          controller: "NewsController"
        }
      }
    })

    .state("jobs", {
      url: "/jobs/",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/jobs.html",
          controller: "JobsController"
        }
      }
    })

    .state("jobDetail", {
      url: "/job/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.job.html",
          controller: "JobsController"
        }
      }
    })

    /*** scholarships ***/
    .state("scholarship", {
      url: "/scholarships",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/scholarships.html",
          controller: "ScholarshipController"
        }
      }
    })
    .state("scholarshipDetail", {
      url: "/scholarship/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.scholarship.html",
          controller: "ScholarshipController"
        }
      }
    })
    .state("scholarshipSearch", {
      url: "/scholarships-search",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/scholarships.search.html",
          controller: "ScholarshipController"
        }
      }
    })
    /*** colleges ***/
    .state("colleges", {
      url: "/colleges",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.html",
          controller: "CollegeController"
        }
      }
    })
    .state("collegeDetail", {
      url: "/college/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.college.html",
          controller: "CollegeController"
        }
      }
    })
    .state("collegeSearch", {
      url: "/colleges/search",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.search.html",
          controller: "CollegeController"
        }
      }
    })
    .state("/campus-life", {
      url: "/colleges/:campuslife",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.campus-life.html",
          controller: "ArticleController"
        }
      }
    })
    .state("/types-of-scholarships", {
      url: "/scholarships/:campuslife",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.campus-life.html",
          controller: "ArticleController"
        }
      }
    })
    .state("/become-a-writer", {
      url: "/news/article/:campuslife",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.campus-life.html",
          controller: "ArticleController"
        }
      }
    })
    .state("/add-event-acricle", {
      url: "/events/:campuslife",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.campus-life.html",
          controller: "ArticleController"
        }
      }
    })
    .state("/post-a-job", {
      url: "/jobs/:campuslife",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.campus-life.html",
          controller: "ArticleController"
        }
      }
    })
    .state("collegeSearchWithTitle", {
      url: "/college-search/:title",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.search.html",
          controller: "CollegeController"
        }
      }
    })
    .state("collegeSearchWithTitleState", {
      url: "/college-search/:title/:state",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.search.html",
          controller: "CollegeController"
        }
      }
    })
    .state("collegeSearchWithTitleStateCountry", {
      url: "/college-search/:title/:state/:country",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/colleges.search.html",
          controller: "CollegeController"
        }
      }
    })
    /*** books ***/
    .state("books", {
      url: "/books",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/books.html",
          controller: "BookController"
        }
      }
    })
    .state("bookDetail", {
      url: "/book/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.book.html",
          controller: "BookController"
        }
      }
    })
    .state("bookSearch", {
      url: "/book-search/:title",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/search.book.html",
          controller: "BookController"
        }
      }
    })
    /*** events ***/
    .state("events", {
      url: "/events",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/events.html",
          controller: "EventController"
        }
      }
    })
    .state("eventDetail", {
      url: "/event/:slug",
      isPublic: true,
      isNavVisible: true,
      views: {
        "nav": {},
        "contentView": {
          templateUrl: "partials/single.event.html",
          controller: "EventController"
        }
      }
    })

});
