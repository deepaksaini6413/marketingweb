app.constant('COUNTRIES', [
  {
    name: "Argentina",
    iso: "AR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Australia",
    iso: "au",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Austria",
    iso: "AT",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Bahrain",
    iso: "BH"
  },
  {
    name: "Bangladesh",
    iso: "BD",
    un: "BGD",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: false,
    unlimited_for_country: true
  },
  {
    name: "Belgium",
    iso: "BE",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Brazil",
    iso: "BR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Bulgaria",
    iso: "BG",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Canada",
    iso: "CA",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Chile",
    iso: "CL",
    un: "",
    rural_excluded: true,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "China",
    iso: "CN",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Colombia",
    iso: "CO",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Costa Rica",
    iso: "CR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Croatia",
    iso: "HR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Cyprus",
    iso: "CY",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Czech Republic",
    iso: "CZ",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Denmark",
    iso: "DK",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Dominican Republic",
    iso: "DO",
    un: "DOM",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: true
  },
  {
    name: "Estonia",
    iso: "EE",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Finland",
    iso: "FI",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "France",
    iso: "FR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Georgia",
    iso: "GE"
  },
  {
    name: "Germany",
    iso: "DE",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Greece",
    iso: "GR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Guadeloupe",
    iso: "GP",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Guam",
    iso: "GU",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Hong Kong",
    iso: "HK",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Hungary",
    iso: "HU",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Iceland",
    iso: "IS",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "India",
    iso: "IN",
    un: "IND",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: true,
    unlimited_for_country: true
  },
  {
    name: "Indonesia",
    iso: "ID"
  },
  {
    name: "Ireland",
    iso: "IE",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Israel",
    iso: "IL",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Italy",
    iso: "IT",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Japan",
    iso: "JP",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Kazakhstan",
    iso: "KZ",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Kenya",
    iso: "KE"
  },
  {
    name: "Latvia",
    iso: "LV",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Lithuania",
    iso: "LT"
  },
  {
    name: "Luxembourg",
    iso: "LU",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Malaysia",
    iso: "MY"
  },
  {
    name: "Malta",
    iso: "MT",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Martinique",
    iso: "MQ",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Mexico",
    iso: "MX",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Morocco",
    iso: "MA",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Netherlands",
    iso: "NL",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "New Zealand",
    iso: "NZ",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Nigeria",
    iso: "NG",
    un: "NGA",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: false,
    unlimited_for_country: true
  },
  {
    name: "Norway",
    iso: "NO",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Panama",
    iso: "PA",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Pakistan",
    iso: "PK",
    un: "PAK",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: false,
    unlimited_for_country: true
  },
  {
    name: "Paraguay",
    iso: "PY",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Peru",
    iso: "PE",
    un: "",
    rural_excluded: true,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Philippines",
    iso: "PH",
    un: "PHL",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: false,
    unlimited_for_country: true
  },
  {
    name: "Poland",
    iso: "PL",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Portugal",
    iso: "PT",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Puerto Rico",
    iso: "PR",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Romania",
    iso: "RO",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Singapore",
    iso: "SG",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Slovakia",
    iso: "SK",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Slovenia",
    iso: "SI",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "South Africa",
    iso: "ZA",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "South Korea",
    iso: "KR",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Spain",
    iso: "ES",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Sri Lanka",
    iso: "LK"
  },
  {
    name: "Sweden",
    iso: "SE",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Switzerland",
    iso: "CH",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Taiwan",
    iso: "TW",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Thailand",
    iso: "TH",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Turkey",
    iso: "TR"
  },
  {
    name: "Ukraine",
    iso: "UA"
  },
  {
    name: "United States",
    iso: "US",
    un: "",
    rural_excluded: false,
    mobile_included: true,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "U.S. Virgin Islands",
    iso: "VI",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "United Kingdom",
    iso: "GB",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Venezuela",
    iso: "VE",
    un: "",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: true,
    sweet_60_free_minutes: true,
    unlimited_for_country: false
  },
  {
    name: "Vietnam",
    iso: "VN",
    un: "VNM",
    rural_excluded: false,
    mobile_included: false,
    sweet_60_rate_plan: false,
    sweet_60_free_minutes: false,
    unlimited_for_country: true
  },
]);