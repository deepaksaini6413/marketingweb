
app.directive('activeLink', ['$location', function (location) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs, controller) {
      var clazz = attrs.activeLink;
      var path  = attrs.href || attrs.ngHref;
      path      = path || "";
      path      = path.substr(1, path.length); //remove hashtag

      scope.location = location;
      scope.$watch('location.path()', function (newPath) {
        //match top level menu links ie. match account side nave when /account/list is selected
        var regex   = new RegExp('^/.*/');
        var subPath = newPath.match(regex); //remove hashtag
        //remove trailing / from path
        subPath = (subPath && subPath[0]) ? subPath[0].substr(0, subPath[0].length - 1) : "nosub";

        if (path === newPath || path == subPath) {
          element.addClass(clazz);
        } else {
          element.removeClass(clazz);
        }
      });
    }
  };
}]);