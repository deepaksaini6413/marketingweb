
app.directive('alerts', function ($timeout, $filter) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: "partials/alert-template.html",
    controller: function ($scope) {

      $scope.isVisible = false;
      $scope.message   = '';

      $scope.$watch('alert', function (val) {
        if (val == undefined || val.context == undefined || val.context.msg == undefined) return;

        $scope.isVisible = true;
        $scope.message   = $filter('formatAlert')(val.context.msg);

        $scope.color = val.context.type || 'success';

        //fadeout
        $timeout(function () {
          $scope.isVisible = false;
        }, 3000);

      });
    }
  }
});