app.directive('emptyResult', function() {
  return {
    restrict: 'E',
    transclude: true,
    templateUrl: "partials/empty-result.html",
    scope: {
      message: "@"
    }
  }
});