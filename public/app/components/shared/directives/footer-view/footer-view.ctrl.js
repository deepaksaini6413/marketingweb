app.directive('footerView', function() {
  return {
    transclude: true,
    restrict: "E",
    templateUrl: "partials/footer-view.html",
    controller: function($scope, $rootScope, $state, UiService, $filter, PrimoService, $window, AuthService) {

      /**
       * Opens a dialog and submit values received from dialog
       */
      $scope.createTicketDialog = function() {
        UiService.showAlertDialog('AccountTicketCreateController', 'partials/account-ticket-create.html', function(val) {
          if (val) {
            PrimoService.createTicket(val).then(function() {
              $rootScope.alert = {
                show: true,
                context: {type: 'success', msg: $filter('translate')('SUCCESSFULLY_CREATED_TICKET')}
              };
            });
          }
        })
      };

      $scope.showDestinations = function() {
        UiService.showAlertDialog('FreeCountriesController', 'partials/free-countries-list.html', function() {
        }, 'sweet_60_rate_plan')
      };

      /**
       * Resolve download url based on access device
       */
      $scope.resolveDownloadUrl = function() {
        if (UiService.isAndroidDevice()) $window.open('https://play.google.com/store/apps/details?id=com.primo.mobile.android.app');
        if (UiService.isIOSDevice()) $window.open('https://itunes.apple.com/app/id976647948');
        $state.go('download');
      }

    }
  }
});