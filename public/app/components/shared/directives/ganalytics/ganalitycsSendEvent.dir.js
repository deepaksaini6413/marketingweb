app.directive('gaSendEvent', function (mParticleService) {
  return {
    scope: {
      eventCategory: '@',
      eventAction: '@',
      eventLabel: '@',
      mparticleUnlimitedBuy: "="
    },
    link: function (scope, iElement, attrs) {
      iElement.bind('click', function () {
        //send to Google Analytics
        if (attrs.eventLabel) {
          ga('send', {
            hitType: 'event',
            eventCategory: attrs.eventCategory,
            eventAction: attrs.eventAction,
            eventLabel: attrs.eventLabel
          });
        }

        //send to mParticle
        if (attrs.mparticleLabel) {
          mParticleService.logEvent(attrs.mparticleLabel, mParticle.EventType.Other)
        }

        if (attrs.mparticleBuyCredit) {
          mParticleService.logBuyCreditEvent(attrs.mparticleBuyCredit)
        }

        if (attrs.mparticleMonthlyBuy) {
          var data = JSON.parse(attrs.mparticleMonthlyBuy);
          mParticleService.logMonthlyBuyNowEvent(data[0], data[1]);
        }

        if (attrs.mparticleUnlimitedBuy) {
          var data = JSON.parse(attrs.mparticleUnlimitedBuy);
          mParticleService.logMonthlyBuyNowEvent(data[0], data[1]);
        }
      });
    }
  };
});