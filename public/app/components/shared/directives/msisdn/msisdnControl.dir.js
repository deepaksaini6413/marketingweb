app.directive('msisdnControl', function (phoneUtils, CountryService, $http, _) {
  return {
    templateUrl: "partials/msisdn-control.html",
    restrict: 'E',
    require: "?ngModel",
    scope: {
      isDisabled: '=ngDisabled'
    },
    link: function (scope, element, attrs, ctrl) {

      scope.countries = CountryService.getCountryCodes();

      scope.model = {
        prefix: '',
        number: ''
      };

      var ngModelHolderName = attrs.ngModel.substr(0, attrs.ngModel.indexOf('.'));

      ctrl.$render = function () {

        if (ctrl.$viewValue && /[0-9]{7,}/.test(ctrl.$viewValue)) {
          updateRegionNumber(phoneUtils.getRegionCodeForNumber('+' + ctrl.$viewValue));
        } else {
          $http.jsonp('https://freegeoip.net/json/?callback=JSON_CALLBACK').success(function (response) {
            updateRegionNumber(response.country_code);
          }).error(function () {
            updateRegionNumber('US');
          });
        }

        function updateRegionNumber(regionCode) {
          if (!regionCode) {
            return;
          }

          var country = _.find(scope.countries, {iso: regionCode});

          updateParent(country.iso);

          var regionNumber = country.code;

          scope.model = {
            prefix: regionNumber,
            number: ctrl.$viewValue ? ctrl.$viewValue.slice(regionNumber.length) : ''
          }
        }

      };

      scope.$watch('model', function () {

        var rawNumber       = getRawNumber(),
            formattedNumber = '+' + rawNumber;

        var country = _.find(scope.countries, {code: scope.model.prefix});
        if (country) {
          updateParent(country.iso);
        }

        var isNumber = /^[0-9]{3,}$/.test(scope.model.number);
        ctrl.$setValidity('number', isNumber);

        var isPossible = isNumber ? phoneUtils.isPossibleNumber(formattedNumber) : false;
        ctrl.$setValidity('possible', isPossible);

        var isValid = isPossible ? phoneUtils.isValidNumber(formattedNumber) : false;
        ctrl.$setValidity('valid', isValid);

        if (isNumber && isPossible && isValid) {
          ctrl.$setViewValue(rawNumber);
        } else {
          ctrl.$setViewValue(null);
        }

      }, true);

      function updateParent(iso) {
        // force update parent model object with "msisdn_country_code
        scope.$parent[ngModelHolderName]['msisdn_country_code'] = iso;
        scope.$parent[ngModelHolderName]['country']             = iso;
        scope.$parent[ngModelHolderName]['new_country']         = iso;
        scope.$parent[ngModelHolderName]['new_msisdn']          = getRawNumber();
      }

      function getRawNumber() {
        return (scope.model.prefix || '') + (scope.model.number || '');
      }

    }
  }
});
