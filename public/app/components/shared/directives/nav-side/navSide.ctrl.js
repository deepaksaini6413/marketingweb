/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.directive('navSide', function () {
  return {
    restrict: "E",
    templateUrl: "partials/nav-side.html",
    controller: function ($rootScope, $scope, $filter, PrimoService, SessionService, $state) {
      $scope.state = $state;
      $scope.items = [
        {
          name: $filter('translate')('MY_ACCOUNT'),
          url: "account",
          image: "resources/images/nav-side/menu_myaccount.png",
          imageSelected: "resources/images/nav-side/menu_myaccount_sel.png"
        },
        {
          name: $filter('translate')('ADD_CREDIT'),
          url: "creditChoosePlan",
          image: "resources/images/nav-side/menu_credit.png",
          imageSelected: "resources/images/nav-side/menu/menu_credit.png"
        },
        {
          name: $filter('translate')('LOGOUT'),
          onclick: "logout()",
          image: "resources/images/account/menu/my_account_sign_out.png",
          imageSelected: "resources/images/account/menu/my_account_sign_out_sel.png"
        }
      ];

      $scope.$on('pps-get-sip-balance', function (ev, args) {
        PrimoService.getSIPBalanace().then(function (data) {
          $rootScope.user.balance      = data.data.balance;
          $rootScope.user.intl_minutes = data.data.intl_minutes;
          SessionService.updateSession($rootScope.user);
          return data;
        });
      });


      $scope.$on('pps-balance-new', function (ev, args) {
        $rootScope.user.balance      = data.bal;
        $rootScope.user.intl_minutes = data.intlmin;
        SessionService.updateSession($rootScope.user);
        return data;
      });
      /**
       * determines selected item from local storage saved menu state
       * @type {boolean}
       */
      $scope.selected = ($rootScope.menu && $rootScope.menu['mm']) ? $scope.items[$rootScope.menu['mm']] : $scope.items[0];
      /**
       * Set selected item
       * @param item
       */
      $scope.select = function (item) {
        $scope.selected = item;
      };


      /**
       * Invoke Primo service attach did method
       */
      $scope.attachDid = function () {
        PrimoService.attachUserDid({}).then(function (data) {
          //update rootscope user and session
          $rootScope.user.direct_dial_in = data.data.direct_dial_in;
          SessionService.updateSession($rootScope.user);
        });
      };
    }
  }
});

