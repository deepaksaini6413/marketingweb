app.directive('navTop', function () {
  return {
    transclude: true,
    restrict: "E",
    templateUrl: "partials/nav-top.html",
    controller: function ($scope, $rootScope, SessionService, AuthService, $state,$stateParams,config,UiService) {

      $scope.config   = config;
      $scope.state    = $state;
      
      if($rootScope.userId){
        $rootScope.user = JSON.parse(localStorage.getItem('user'));
      }

      $scope.init = function(){

        if($stateParams){

            if($stateParams.current != undefined && $stateParams.current){
                if($stateParams.current.name == 'colleges'){
                $scope.menuItems=[{'title':'College Search','url':'/college-search'},{'title':'Campus Life','url':'/colleges/campus-life'},{'title':'Campus Visit 101','url':'/colleges/campus-visit'}];
                $scope.currentMenuItem = 'Colleges';
                }else if($stateParams.current.name == 'scholarship'){
                    $scope.menuItems=[{'title':'Types of Scholarships','url':'/scholarships/types-of-scholarships'},{'title':'Financial Aid','url':'/scholarships/financial-aid'},{'title':'Student Loans','url':'/scholarships/students-loans'}];
                    $scope.currentMenuItem = 'Scholarship';
                }else if($stateParams.current.name == 'news'){
                    $scope.menuItems=[{'title':'Sections','url':'/'},{'title':'Become a Writer','url':'/news/article/become-a-writer'}];
                    $scope.currentMenuItem = 'News';
                }else if($stateParams.current.name == 'events'){
                    $scope.menuItems=[{'title':'Search','url':'/events-search'},{'title':'Category','url':'/'},{'title':'Add Event','url':'/events/add-event-article'}];
                    $scope.currentMenuItem = 'Events';
                }else if($stateParams.current.name == 'jobs'){
                    $scope.menuItems=[{'title':'Career Resources','url':'/career-resources'},{'title':'Post a Job','url':'/jobs/post-a-job'}];
                    $scope.currentMenuItem = 'Jobs';
                }else if ($stateParams.current.name == '/') {
                    $scope.menuItems=[{'title':'Colleges','url':'/colleges'},{'title':'Scholarship','url':'/scholarship'},{'title':'News','url':'/news'},{'title':'Events','url':'/events'},{'title':'Jobs','url':'/jobs'},{'title':'Textbooks','url':'/textbooks'}];
                }    
            }else{

                if(window.location.pathname == '/colleges'){
                $scope.menuItems=[{'title':'College Search','url':'/college-search'},{'title':'Campus Life','url':'/colleges/campus-life'},{'title':'Campus Visit 101','url':'/colleges/campus-visit'}];
                $scope.currentMenuItem = 'Colleges';
                }else if(window.location.pathname == '/scholarships'){
                    $scope.menuItems=[{'title':'Types of Scholarships','url':'/scholarships/types-of-scholarships'},{'title':'Financial Aid','url':'/scholarships/financial-aid'},{'title':'Student Loans','url':'/scholarships/students-loans'}];
                    $scope.currentMenuItem = 'Scholarship';
                }else if(window.location.pathname == '/news'){
                    $scope.menuItems=[{'title':'Sections','url':'/'},{'title':'Become a Writer','url':'/news/article/become-a-writer'}];
                    $scope.currentMenuItem = 'News';
                }else if(window.location.pathname == '/events'){
                    $scope.menuItems=[{'title':'Search','url':'/events-search'},{'title':'Category','url':'/'},{'title':'Add Event','url':'/events/add-event-article'}];
                    $scope.currentMenuItem = 'Events';
                }else if(window.location.pathname == '/jobs'){
                    $scope.menuItems=[{'title':'Career Resources','url':'/career-resources'},{'title':'Post a Job','url':'/jobs/post-a-job'}];
                    $scope.currentMenuItem = 'Jobs';
                }else if (window.location.pathname == '/') {
                    $scope.menuItems=[{'title':'Colleges','url':'/colleges'},{'title':'Scholarship','url':'/scholarship'},{'title':'News','url':'/news'},{'title':'Events','url':'/events'},{'title':'Jobs','url':'/jobs'},{'title':'Textbooks','url':'/textbooks'}];
                }

                //$scope.menuItems=[{'title':'Colleges','url':'/colleges'},{'title':'Scholarship','url':'/scholarship'},{'title':'News','url':'/news'},{'title':'Events','url':'/events'},{'title':'Jobs','url':'/jobs'},{'title':'Textbooks','url':'/'}];
            }
            
        }

      };

      $scope.openMenu = function ($mdOpenMenu, ev) {
        originatorEv = ev;
        $mdOpenMenu(ev);
      };

      $scope.logOut = function () {

        SessionService.destroySession();
        localStorage.removeItem('user');
        localStorage.removeItem('userId');
      };

      $scope.setHeadeMenuItem = function(currentMenuItem,value){

            $scope.currentMenuItem = currentMenuItem;

            $scope.setDRPMenuItem=["colleges","scholarship","news","events","jobs","textBooks"];

            switch(currentMenuItem){
                case 'colleges':
                $scope.menuItems=[{'title':'College Search','url':'/college-search'},{'title':'Campus Life','url':'/colleges/campus-life'},{'title':'Campus Visit 101','url':'/colleges/campus-visit'}];
                $scope.currentMenuItem = 'Colleges';
                $state.go("colleges");
                break;

                case 'scholarship':
                $scope.menuItems=[{'title':'Types of Scholarships','url':'/scholarships/types-of-scholarships'},{'title':'Financial Aid','url':'/scholarships/financial-aid'},{'title':'Student Loans','url':'/scholarships/students-loans'}];
                $scope.currentMenuItem = 'Scholarship';
                $state.go("scholarship");
                break;

                case 'news':
                $scope.menuItems=[{'title':'Sections','url':'/'},{'title':'Become a Writer','url':'/news/article/become-a-writer'}];
                $scope.currentMenuItem = 'News';
                $state.go("news");
                break;

                case 'events':
                $scope.menuItems=[{'title':'Search','url':'/events-search'},{'title':'Category','url':'/'},{'title':'Add Event','url':'/events/add-event-article'}];
                $scope.currentMenuItem = 'Events';
                $state.go("events");
                break;

                case 'jobs':
                $scope.menuItems=[{'title':'Career Resources','url':'/career-resources'},{'title':'Post a Job','url':'/jobs/post-a-job'}];
                $scope.currentMenuItem = 'Jobs';
                
                $state.go("jobs");
                break;

                case 'textBooks':
                $scope.menuItems=[{'title':'Sell/Post Textbook','url':'/'},{'title':'Textbook Resources','url':'/'}];
                $scope.currentMenuItem = 'textBooks';
                $state.go("/textbooks");
                break;
            }
        };

        $scope.changeCollege = function() {
              UiService.showConfirmDialog('CollegeSelectController', 'partials/college-select.html', function(clb) {
                if (clb) {
                  PrimoService.inviteFriends({invited_emails: clb}).then(function(response) {
                    $scope.$root.alert = {
                      show: true,
                      context: {type: 'success', msg: $filter('translate')('INVITE_SUCCESSFULLY SENT')}
                    }
                  });
                }
              })
        };

      $scope.login = AuthService.login;

      $scope.init();
    },
    link: function (scope) {
      (function () {

        // Create mobile element
        // var mobile       = document.createElement('div');
        // mobile.className = 'nav-mobile';
        // document.querySelector('.nav').appendChild(mobile);

        // // hasClass
        // function hasClass(elem, className) {
        //   return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
        // }

        // // toggleClass
        // function toggleClass(elem, className) {
        //   var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
        //   if (hasClass(elem, className)) {
        //     while (newClass.indexOf(' ' + className + ' ') >= 0) {
        //       newClass = newClass.replace(' ' + className + ' ', ' ');
        //     }
        //     elem.className = newClass.replace(/^\s+|\s+$/g, '');
        //   } else {
        //     elem.className += ' ' + className;
        //   }
        // }

        // // Mobile nav function
        // var mobileNav     = document.querySelector('.nav-mobile');
        // var toggle        = document.querySelector('.nav-list');
        // mobileNav.onclick = function () {
        //   toggleClass(this, 'nav-mobile-open');
        //   toggleClass(toggle, 'nav-active');
        // };

        // $('.nav-item a:not([ng-click])').click(function () {
        //   toggleClass(this, 'nav-mobile-open');
        //   toggleClass(toggle, 'nav-active');
        // });

      })();
    }

    //test menu code
  }
});