/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 *
 * Directive used to display empty result Image and text. It is being displayed on all pages without results
 *
 */
app.directive('seeCountriesCard', function () {
  return {
    restrict: 'E',
    transclude: true,
    templateUrl: "partials/see-countries-card.html",
    controller: function ($scope, UiService) {
      $scope.showDialog = function (filter) {
        UiService.showAlertDialog('FreeCountriesController', 'partials/free-countries-list.html', function () {
        }, filter)
      }
    }
  }
});