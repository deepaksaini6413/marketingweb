/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */


app.directive('primoRateCard', function () {
  return {
    restrict: 'E',
    transclude: true,
    templateUrl: "partials/rate-card.html",
    scope: {
      title: "@",
      titleImage: "@",
      text: "@",
      text2: "@",
      text3: "@",
      buttonAction: "@",
      mparticleLabel: "@",
      mparticleMonthlyBuy: "="
    }
  }
});