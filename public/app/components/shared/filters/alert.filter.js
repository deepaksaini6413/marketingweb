/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Display received alerts in human readable form
 */
app.filter('formatAlert', function () {
  return function (alerts) {
    var retVal = "";
    if (alerts.constructor === Array) {
      angular.forEach(alerts, function (alert) {
        alert = alert.replace(/\/r/g, '"');
        retVal += alert + '\n';
      });
    } else {
      retVal = alerts;
    }

    return retVal;
  }
});