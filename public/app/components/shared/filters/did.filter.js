/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Display Primo DID in formatted manner.
 * Data is received as plain number 15187778057 and we want number to be formatted as "+ 1 (518) 777 - 8057"
 *
 */
app.filter('formatDid', function () {
  /**
   * Check if this is a number. In case of a number we format the value, otherwise return as it is
   * @param did
   * @returns {boolean}
   */
  function isPSTN(did) {
    var regex = new RegExp('^[0-9]{1,6}');
    return regex.test(did);
  }

  return function (did) {
    if (did.indexOf('@') != -1) {
      did = did.substr(0, did.indexOf('@'));
    }

    if (isPSTN(did)) {
      did = did.replace(/\D/g, "")
        .replace(/(\d{1})(\d{3})(\d{3})(\d{4})/, "+$1 ($2) $3 - $4");
    }

    return did;
  }
});