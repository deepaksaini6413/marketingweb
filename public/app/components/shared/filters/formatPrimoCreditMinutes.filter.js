/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Minutes are rounded to 2 decimals
 */
app.filter('formatPrimoCreditMinutes', function ($filter) {
  return function (rate) {
    if (!rate) {
      return $filter('translate')('SEARCH_COUNTRY_CHECK');
    }

    var retVal = (parseFloat(rate).toFixed(1));
    if (retVal == parseInt(retVal)) retVal = parseInt(retVal);
    return retVal;
  }
});