/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Display Primo Rates in cents. Rates are rounded to 2 decimals
 * If rates ends with .0 it is rounded to fixed integer value
 */
app.filter('formatSMSRate', function ($filter) {
  return function (rate) {
    if (!rate) {
      return $filter('translate')('FREE');
    }

    var retVal = (parseFloat(rate * 100).toFixed(1));
    if (retVal == parseInt(retVal)) retVal = parseInt(retVal);
    return retVal + '¢/' + $filter('translate')('msg');
  }
});