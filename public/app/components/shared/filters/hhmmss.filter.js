/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.filter('formatHHMMSS', function () {
  function pad(num) {
    return ("0" + num).slice(-2);
  }

  return function (secs) {
    var minutes = Math.floor(secs / 60);
    secs        = secs % 60;
    var hours   = Math.floor(minutes / 60);
    minutes     = minutes % 60;
    return pad(hours) + ":" + pad(minutes) + ":" + pad(secs);
  }
});