/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Directive used to filter international rates
 */
app.filter('filteredRates', function () {
  return function (input, criteria) {
    var result = [];
    var regex  = new RegExp(criteria, 'i');
    angular.forEach(input, function (rate) {
      if (regex.exec(rate.country)) {
        return result.push(rate);
      }
    });
    return result;
  }
});