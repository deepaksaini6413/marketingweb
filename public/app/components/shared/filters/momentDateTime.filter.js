/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.filter('realTime', function () {
  /**
   * Returns my current time offset
   * @returns {number}
   */
  function getMyTimezoneOffset() {
    var d = new Date();
    return d.getTimezoneOffset();
  }

  function getServerTimeOffset() {
    return moment().tz("America/Los_Angeles")._offset;
  }

  return function (time, filter) {
    return moment(time * 1000 + (Math.abs(getServerTimeOffset() + getMyTimezoneOffset()))*60*1000).format(filter);
  }
});