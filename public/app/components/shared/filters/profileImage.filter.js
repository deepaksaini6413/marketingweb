/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Return profile image to png or default profile avatar
 */
app.filter('profileImageFilter', function () {
  return function (data) {
    return (data) ? 'data:image/png;base64,' + data : 'resources/images/profile_avatar.png';
  }
});