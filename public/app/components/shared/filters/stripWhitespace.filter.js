/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

/**
 * Strips whitespace from a value
 */
app.filter('stripWhitespace', function ($filter) {
  return function (value) {
    if (value) {
      return value.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    }
  }
});