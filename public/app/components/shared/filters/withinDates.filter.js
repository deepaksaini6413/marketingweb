/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.filter('withinDatesFilter', function () {
  return function(items, field, timeStart, timeEnd) {
    timeEnd = timeEnd || Date.now(); 
    return items.filter(function(item) {
      return (parseInt(item[field]) * 1000 > timeStart && parseInt(item[field]) * 1000 < timeEnd);
    });
  }
});