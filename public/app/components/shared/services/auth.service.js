app.service('AuthService', function ($mdDialog, $state, PrimoService, mParticleService,
                                     SessionService, $q, $rootScope, $filter, $timeout) {
  return {
    doVerifyUsername: function (user, successHandler) {
      PrimoService.verifyUsername({username: user.username, language: 'en'}).then(function (response) {
        if (successHandler) {
          successHandler(response, response.data.error_codes && response.data.error_codes[0] === 'AC0001');
        }
      });
    },
    login: function ($event) {
      return $mdDialog.show({
        templateUrl: 'partials/login.html',
        controller: 'LoginController',
        targetEvent: $event,
        fullscreen: true,
        onRemoving: function () {
          checkState('login');
        }
      });
    },
    doLogin: function (user, successHandler) {

      // attach UUID to "user" object
      //user['registration_id'] = _guid();
      var deferred = $q.defer();
      PrimoService.login(user)
        .success(function (data, status, headers) {
          deferred.resolve(data);
        }).error(function(err){

          deferred.reject(err);
        });
        return deferred.promise;
    },
    register: function ($event) {
      return $mdDialog.show({
        templateUrl: 'partials/register.html',
        controller: 'RegisterController',
        targetEvent: $event,
        fullscreen: true,
        onRemoving: function () {
          checkState('register');
        }
      });
    },
    doRegister: function (user, successHandler) {
      PrimoService.register(user).success(function (data, status, headers) {
        PrimoService.login(user).success(function (data, status, headers) {
          PrimoService.getAccountInfo({
            username: data.alias,
            login_token: data.login_token
          }).success(function (accountData) {
            SessionService.storeSession(data, accountData);

            if (successHandler) {
              if (angular.isObject(successHandler)) {
                if (successHandler.hasOwnProperty('withMSISDN') && accountData.msisdn) {
                  successHandler.withMSISDN();
                } else if (successHandler.hasOwnProperty('withoutMSISDN') && !accountData.msisdn) {
                  successHandler.withoutMSISDN();
                }
              } else {
                successHandler();
              }
            }

          });

        });
      });
    },
    doClaimAccount: function (claimNumber, token, user, successHandler) {
      var deferred = $q.defer();

      //prepare data for claim account call
      var dataToSend = {
        username: claimNumber,
        new_username: user.username,
        new_password: user.password,
        email: user.email,
        first_name: user.first_name,
        last_name: user.last_name
      };

      PrimoService.claimAccount(dataToSend, claimNumber, token)
        .success(function (data) {
          PrimoService.getAccountInfo({
            username: data.alias,
            login_token: data.login_token
          }).success(function (accountData) {
            SessionService.storeSession(data, accountData);
            if (successHandler) {
              successHandler();
            }

            deferred.resolve();
          });
        });
      return deferred.promise;
    },
    verifyMSISDN: function ($event) {
      return $mdDialog.show({
        templateUrl: "partials/verify-msisdn.html",
        controller: 'VerifyMSISDNController',
        targetEvent: $event,
        fullscreen: true,
        onRemoving: function () {
          checkState('verifyMSISDN');
        }
      });
    },
    forgotPassword: function ($event) {
      return $mdDialog.show({
        templateUrl: "partials/forgot-password.html",
        controller: 'ForgotPasswordController',
        targetEvent: $event,
        fullscreen: true,
        onRemoving: function () {
          checkState('forgotPassword');
        }
      });
    },
    forgotUsername: function ($event) {
      return $mdDialog.show({
        templateUrl: "partials/forgot-username.html",
        controller: 'ForgotUsernameController',
        targetEvent: $event,
        fullscreen: true,
        onRemoving: function () {
          checkState('forgotUsername');
        }
      });
    }
  };

  function checkState(name) {
    $timeout(function () {
      if ($state.is(name)) {
        $state.go('/')
      }
    })
  };

  /**
   * Generate UUID used as registration_id
   * @returns {string}
   */
  function _guid() {
    var d    = new Date().getTime();
    var uuid = 'web-app-xxxxxxxx-xxxxxxxx-xxxxxxxx-xxxxxxxx-xxxxxxxx-xxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d     = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });

    return uuid;
  };

});