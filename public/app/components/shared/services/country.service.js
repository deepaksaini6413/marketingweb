app.service('CountryService', function ($http, _, COUNTRIES) {

  var service = {
    load: function () {
      return $http({
        method: 'GET',
        url: 'resources/countries/countryCodes.json'
      });
    },
    getCountryCodes: function () {
      return JSON.parse(document.getElementById('resources/countries/countryCodes.json').innerHTML);
    },
    getCountryByName: function (name) {
      return _.find(COUNTRIES, {name: name})
    },
    getCountryCodeByName: function (name) {
      var country = service.getCountryByName(name);
      if (country) {
        return country.iso;
      }
    }
  };

  return service;

});