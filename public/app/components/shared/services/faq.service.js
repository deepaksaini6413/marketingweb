app.service('FaqService', function (PrimoService, $window, $q) {
  return {
    /**
     * Calls primo service to load faq
     * If faq is loaded in last hour return data from localStorage
     * @returns {*}
     */
    getFAQ: function () {
      var deferred = $q.defer();
      //if value is cached more than one hour it will be refreshed
      if (!$window.localStorage.faqRefreshDate || (Date.now() - $window.localStorage.faqRefreshDate > 3600 * 1000)) {
        PrimoService.getFAQ().success(function (data) {
          $window.localStorage.faq            = JSON.stringify(data);
          $window.localStorage.faqRefreshDate = Date.now();

          deferred.resolve(data);
        });
      } else {
        deferred.resolve(JSON.parse($window.localStorage.faq));
      }

      return deferred.promise;
    }
  }
});