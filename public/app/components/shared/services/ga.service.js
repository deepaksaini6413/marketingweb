/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.service("GAManager", function ($window) {
  return {
    /**
     * Send custom event to ga
     * @param cat
     * @param action
     * @param label
     */
    sendEventMessage: function (cat, action, label) {
      ga('send', {
        hitType: 'event',
        eventCategory: cat,
        eventAction: action,
        eventLabel: label
      });
    },
    sendConversion: function (amount) {
      $window.google_trackConversion({
        google_conversion_id: '870417705',
        google_conversion_language: 'en',
        google_conversion_format: '3',
        google_conversion_color: 'ffffff',
        google_conversion_label: 'UrjiCJWA42wQqYqGnwM',
        google_conversion_value: amount,
        google_conversion_currency: 'USD',
        google_remarketing_only: false
      });
    }
  }
});