app.factory('_', function ($window) {
  if(!$window._){
    // If lodash is not available you can now provide a
    // mock service, try to load it from somewhere else,
    // redirect the user to a dedicated error page, ...
    console.warn('Lodash is not available!')
  }

  var _ = $window._;

  // delete global lodash to prevent direct usage
  delete($window._);

  // return original
  return _;

});