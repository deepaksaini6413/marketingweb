/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.service('mParticleService', function(SessionService, $rootScope) {
  var service = {
    sendSignInEvent: function() {
      mParticle.logEvent('Sign in', mParticle.EventType.Other);
      mParticle.setUserIdentity(SessionService.getPUID(), mParticle.IdentityType.CustomerId);
      mParticle.setUserIdentity(SessionService.getEmail(), mParticle.IdentityType.Email);
      mParticle.setUserAttribute("Username", $rootScope.userId);
      mParticle.setUserAttribute("$FirstName", SessionService.getFirstName());
      mParticle.setUserAttribute("$LastName", SessionService.getLastName());
      mParticle.setUserAttribute("Signin Date", Date.now());
      mParticle.setUserAttribute("Signin Channel", "Web");
      mParticle.setUserAttribute("Primo US Phone Number", SessionService.getDID());
      mParticle.setUserAttribute("$Mobile", SessionService.getMSISDN());

      mParticle.setUserAttribute("Country Code", SessionService.getMSISDNCountryCode());
      mParticle.setUserAttribute("Email Address", SessionService.getEmail());
      mParticle.setUserAttribute("Credits Balance", SessionService.getCreditBalance());
      mParticle.setUserAttribute("Minutes Balance", SessionService.getAvailableMinutes());

      mParticle.setUserAttribute("Play notification sound", true);
      mParticle.setUserAttribute("Display chat notifications", true);

      mParticle.setUserAttribute("Residence Place", SessionService.getResidencePlace());
      mParticle.setUserAttribute("Date", SessionService.getDidExpiration());

      mParticle.setUserAttribute("Reward Minutes Balance", SessionService.getRewardMinutes());
      mParticle.setUserAttribute("Plan Minutes Balance", SessionService.getPlanMinutes());

      mParticle.setUserAttribute("Rate Plan", SessionService.getCurrentPlan());
      mParticle.setUserAttribute("Rate Plan Expiration", SessionService.getCurrentPlanExpirationDate());


    },
    sendSignUpEvent: function() {
      mParticle.logEvent('Sign up', mParticle.EventType.Other);
      mParticle.setUserIdentity(SessionService.getPUID(), mParticle.IdentityType.CustomerId);
      mParticle.setUserIdentity(SessionService.getEmail(), mParticle.IdentityType.Email);
      mParticle.setUserAttribute("$FirstName", SessionService.getFirstName());
      mParticle.setUserAttribute("$LastName", SessionService.getLastName());
      mParticle.setUserAttribute("Signup Date", Date.now());
      mParticle.setUserAttribute("Signup Channel", "Web");
      mParticle.setUserAttribute("Primo US Phone Number", SessionService.getDID());
      mParticle.setUserAttribute("$Mobile", SessionService.getMSISDN());
    },
    logEvent: function(text, type, data) {
      if (text && data) {
        mParticle.logEvent(text, type, data);
      } else {
        mParticle.logEvent(text, type);
      }
    },
    logPageView: function() {
      if (mParticle.logPageView) mParticle.logPageView();
    },

    logSearchEvent: function(title, searchTerm) {
      window.mParticle.logEvent(
        title,
        mParticle.EventType.Search,
        {'Search Term': searchTerm}
      );
    },
    logDestinationEvent: function(destination) {
      window.mParticle.logEvent(
        'International Rates - View Destinations',
        mParticle.EventType.Other,
        {
          'Destination': destination

        });
    },
    logBuyCreditEvent: function(value) {
      window.mParticle.logEvent(
        'Primo Credit - Buy Now',
        mParticle.EventType.Other,
        {
          'Price': value

        });
    },
    logMonthlyBuyNowEvent: function(referrer, price) {
      window.mParticle.logEvent(
        'Monthly Minute Plans - Buy Now',
        mParticle.EventType.Other,
        {
          'Plan Referrer': referrer,
          'Price': price

        });
    },
    logEditProfile: function(data) {
      window.mParticle.logEvent('Edit Profile', mParticle.EventType.UserPreference);

      window.mParticle.setUserAttribute("$FirstName", data.first_name);
      window.mParticle.setUserAttribute("$LastName", data.last_name);
      window.mParticle.setUserIdentity($rootScope.userId, mParticle.IdentityType.Other);
      window.mParticle.setUserAttribute("Signup Date", Date.now());
      window.mParticle.setUserAttribute("Signup Channel", "Web");
      window.mParticle.setUserAttribute("$Mobile", data.msisdn);
      window.mParticle.setUserAttribute("Primo US Phone Number", data.direct_dial_in);
      window.mParticle.setUserAttribute("Country Code", data.home_country);
      window.mParticle.setUserAttribute("Email Address", data.email);
      window.mParticle.setUserAttribute("Birthplace", data.home_city);
      window.mParticle.setUserAttribute("Credits Balance", data.balance);
      window.mParticle.setUserAttribute("Minutes Balance", data.intl_minutes);
    },

    addToCartCredit: function(product, isRenewal) {
      window.mParticle.eCommerce.setCurrencyCode("USD");


      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
          // attributes
          {
            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal
          }
        );

      window.mParticle.eCommerce.Cart.add(mpProduct, true)
    },
    addToCartDid: function(product, isRenewal) {
      window.mParticle.eCommerce.setCurrencyCode("USD");


      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
          // attributes
          {
            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal
          }
        );

      window.mParticle.eCommerce.Cart.add(mpProduct, true)
    },
    addToCartUnlimited: function(product, isRenewal) {
      window.mParticle.eCommerce.setCurrencyCode("USD");


      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
          // attributes
          {
            //set for Primo Unlimited and Primo Monthly Minute Plans
            'Minutes': product.minutes.rated,

            //set for Primo Unlimited
            'Primo to Primo': product.minutes.cost,

            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal
          }
        );

      window.mParticle.eCommerce.Cart.add(mpProduct, true)
    },
    addToCartMonthly: function(product, isRenewal) {
      window.mParticle.eCommerce.setCurrencyCode("USD");

      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
// attributes
          {
            //set for Primo Unlimited and Primo Monthly Minute Plans
            'Minutes': product.minutes.rated,
            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal,
            //set for Primo Monthly Minute Plans
            'Per Minute Rate': product.minutes.cost
          }
        );

      window.mParticle.eCommerce.Cart.add(mpProduct, true)
    },
    removeFromCartCredit: function(product, isRenewal) {

      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
          // attributes
          {
            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal
          }
        );

      window.mParticle.eCommerce.Cart.remove(mpProduct, true)
    },
    removeFromCartDid: function(product, isRenewal) {
      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity
          null,
          null,
          null,
          null,
          null,
          // attributes
          {
            'Payment Method': 'Braintree',
            'Auto-Renewal': isRenewal
          }
        );

      window.mParticle.eCommerce.Cart.remove(mpProduct, true)
    },
    removeFromCartMonthly: function(product) {

      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity,
          null,
          null,
          null,
          null,
          null,
          {
            //set for Primo Unlimited and Primo Monthly Minute Plans
            'Minutes': product.minutes.rated,
            //set for Primo Monthly Minute Plans
            'Per Minute Rate': product.minutes.cost
          }
        );

      window.mParticle.eCommerce.Cart.remove(mpProduct, true)
    },
    removeFromCartUnlimited: function(product) {

      var mpProduct = window.mParticle.eCommerce
        .createProduct(
          product.title,
          product.product, //sku
          product.rate, //price
          1, //quantity,
          null,
          null,
          null,
          null,
          null,
          {
            //set for Primo Unlimited and Primo Monthly Minute Plans
            'Minutes': product.minutes.rated,
            //set for Primo Unlimited
            'Primo to Primo': product.minutes.cost
          }
        );

      window.mParticle.eCommerce.Cart.remove(mpProduct, true)
    },

    logTransaction: function(data, total, items, isRenew) {
      window.mParticle.eCommerce.setCurrencyCode("USD");


      var mpProduct = items.map(function(item) {
        return window.mParticle.eCommerce
          .createProduct(
            item.name,
            item.id, //sku
            item.value, //price
            1, //quantity
            null,
            null,
            null,
            null,
            null,
// attributes
            {
              //set for Primo Unlimited and Primo Monthly Minute Plans
              'Minutes': item.minutes.rated,

              //set for Primo Unlimited
              'Primo to Primo': item.minutes.cost,

              //set for Primo Monthly Minute Plans
              'Per Minute Rate': item.minutes.cost
            }
          );
      });

      var transactionId = "";
      if (data && data.purchases && data.purchases[0] && data.purchases[0].transaction) {
        transactionId = data.purchases[0].transaction;
      }

      var transactionAttributes = window.mParticle.eCommerce
        .createTransactionAttributes(
          transactionId, // id
          total, // revenue
          0
        );


      // logs a Purchase using products in cart
      window.mParticle.eCommerce
        .logPurchase(transactionAttributes, mpProduct, true,
          {
            'Purchase Channel': 'Web',
            'Payment Method' : 'Braintree',
            'Auto-Renewal': isRenew
          }
        );
    }
  };

  return service;
});