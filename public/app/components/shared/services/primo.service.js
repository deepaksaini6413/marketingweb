app.service('PrimoService', function($http, $state, $rootScope, config, Base64, $httpParamSerializerJQLike) {

  var token = localStorage.getItem('token');
  if(token){
    $http.defaults.headers.common['Authorization'] = token;
  }

  return {
    verifyUsername: function(user) {
      return $http({
        method: 'POST',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__VerifyUsername',
        data: $httpParamSerializerJQLike(user)
      });
    },
    getClientToken: function() {
      var user         = {};
      user.login_token = $rootScope.token;
      user.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/billingintl/3/primo/api/billingintl__ConveyCreditCardRegistration',
        data: $httpParamSerializerJQLike(user)
      });
    },
    login: function(user) {
      return $http({
        method: 'POST',
        url: config.primo.host + 'auth/login',
        data: $httpParamSerializerJQLike(user)
      });
    },
    logout: function() {
      var user         = {};
      user.login_token = $rootScope.token;
      user.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__LogoutPinlessAccount',
        data: $httpParamSerializerJQLike(user)
      });
    },
    
    verifyMSISDN: function(data) {
      data.login_token = $rootScope.token;
      data.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__VerifyMSISDN',
        data: $httpParamSerializerJQLike(data)
      });
    },
    getAccountInfo: function(user) {
      return $http({
        method: 'POST',
        url: config.primo.host + 'get-profile-info',
        data: $httpParamSerializerJQLike(user)
      });
    },
    
    setAccountInfo: function(user) {
      user.login_token = $rootScope.token;
      user.username    = $rootScope.userId;
      delete user.partner;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__SetPinlessAccountFields',
        data: $httpParamSerializerJQLike(user)
      });
    },
    
    searchUsers: function(user) {
      user.login_token = $rootScope.token;
      user.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/directoryintl/3/primo/api/directoryintl__SearchUserDirectory',
        data: $httpParamSerializerJQLike(user)
      });
    },
    changePassword: function(contact) {
      contact.login_token = $rootScope.token;
      contact.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__ChangePinlessAccountPassword',
        data: $httpParamSerializerJQLike(contact)
      });
   },
    inviteFriends: function(data) {
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      
      return $http.post(config.primo.host+'invite-friends-via-email',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    
    getPublicURLs: function(data) {
      return $http({
        method: 'GET',
        url: config.primo.host + '/pr/publicintl/3/primo/api/publicintl__GetPublicURLs'
      })
    },
    resetPassword: function(data) {
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__ResetPinlessAccountPassword',
        data: $httpParamSerializerJQLike(data)
      })
    },
    submitDealerApplication: function(data) {
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/dealer/3/primo/api/dealer__SubmitApplication',
        data: $httpParamSerializerJQLike(data)
      })
    },
    recoverUsername: function(data) {
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__RecoverUsername',
        data: $httpParamSerializerJQLike(data)
      })
    },
    deactivateAccount: function(data) {
      data.login_token = $rootScope.token;
      data.username    = $rootScope.userId;
      return $http({
        method: 'POST',
        url: config.primo.host + '/pr/manageintl/3/primo/api/manageintl__DeletePinlessAccount',
        data: $httpParamSerializerJQLike(data)
      })
    },
    getFAQ: function() {
      return $http({
        method: 'GET',
        url: '/list-faq'
      })
    },
    /*** Code Start From Here*/
    GetBookCategories: function(){
      return $http({
        method: 'GET',
        url: config.primo.host + 'books/get-book-categories',
      });
    },
    GetBookCategoriesWithParent: function(){
      return $http({
        method: 'GET',
        url: config.primo.host + 'books/get-book-categories-with-parent',
      });
    },
    // GetBooks: function(data){
    //   return $http({
    //     method: 'GET',
    //     url: config.primo.host + 'get-module/books?page='+data,
    //   });
    // },

    Books: function(lastpage) {
      return $http({
        method: 'GET',
        url: config.primo.host + 'get-module/books?page='+lastpage,
      });
          
    },
    getNews: function(){
      return $http.get(config.primo.host +'get-news').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    News: function(lastpage){
      return $http.get(config.primo.host +'get-module/news?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    singleNews: function(slug){
      return $http.get(config.primo.host +'get-single-module/news/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    newsActivity: function(id,action){
      return $http.get(config.primo.host +'news-activity/'+id+'/'+action).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    getNewsByCategory: function(slug){
      return $http.get(config.primo.host +'get-category-news/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    newsLoadMoreData: function(lastpage){
      return $http.get(config.primo.host +'get-news?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    /*** Jobs service ****/
    searchJobsBasedOnLocation: function(locations,data){
            var searchText= data.searchText ? data.searchText :'';
            var location= data.location ? data.location :'';
            var selectedExperience= data.selectedExperience ? data.selectedExperience :0;
            var selectedSalary= data.selectedSalary ? data.selectedSalary :0;

            return $http.post(config.primo.host + 'job-on-location', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Jobs: function(lastpage){
      return $http.get(config.primo.host+'get-module/jobs?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    jobDescriptionBySlug: function(slug){
      return $http.get(config.primo.host + 'job-description/' + slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    jobApply: function(data){
      return $http.post(config.primo.host + 'apply-job', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    jobAddToFavorite: function(data){
      return $http.post(config.primo.host + 'add-job-to-favourite', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    seeRecruiterNumber: function(jobId){
      var data={
                jobid:jobId
            };
         return $http.post(config.primo.host+'get-recuiter-details',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    globalJobSearch: function(searchText,location,selectedExperience,selectedSalary){
      var data={
                location:location,
                searchText:searchText,
                selectedSalary:selectedSalary,
                selectedExperience:selectedExperience
            };
            return $http.post(config.primo.host + 'jobs/search',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    Job: function(slug){
      return $http.get(config.primo.host + 'get-single-module/jobs/' + slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    UserJobs: function(lastpage){
      return $http.get(config.primo.host + 'get-module/jobs?page=' + lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    searchJob: function(data){
      return $http.post(config.primo.host + 'jobs/search', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    loadMoreData:function(lastpage, data) {
        return $http.post(config.primo.host + 'get-jobs?page=' + lastpage, $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    GetUserJobs: function(lastpage){
        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }

        return $http.get(config.primo.host+'get-user-jobs?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    CreateJobs: function(data){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'jobs/create', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    UpdateJobs: function(data,job_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'jobs-update/'+job_id+'/update', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    DeleteJobs: function(job_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      
      return $http.post(config.primo.host+'jobs/'+job_id+'/delete').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    /*****Scholarship Service *******/
    loadTopScholarship:function(){
          return $http.get(config.primo.host + 'get-module/scholarship?featured=true').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
      
    Scholarships: function(lastpage) {
         return $http.get(config.primo.host+'get-module/scholarship?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Scholarship: function(slug) {

          return $http.get(config.primo.host+'get-single-module/scholarship/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    searchScholarship: function(lastpage,college){
        return $http.get(config.primo.host+'get-module/scholarship?page='+lastpage+'&college='+college).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    testimonialsTeam: function(){
          // return $http.get(config.primo.host+'sampleData/testimonialsTeam.json').catch(function(response){
          //       if(response.status == 500){
          //         localStorage.removeItem('token'); 
          //         alert("Session has expired, please login again");
          //         $state.go('login')
          //       }
          //     });
    },

    /**** Colleges Service *****/
    getAllColleges: function(){
      return $http.get(config.primo.host+'get-all-colleges').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    leftNavigationColleges: function() {
       return $http.get(config.primo.host+'sampleData/leftSidenavigation.json').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    beseColleges: function(){
       return $http.get(config.primo.host+'sampleData/bestCollegeData.json').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    searchColleges: function(searchText){
        return $http.get(config.primo.host+'get-module/colleges?searchText='+searchText).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    sortCollegeByAlphabates: function(lastpage) {
       return $http.get(config.primo.host+'college-sort-data?sort=asend').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Colleges: function(lastpage) {
       return $http.get(config.primo.host+'get-module/colleges?page='+lastpage+"&best=true").catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    College: function(slug) {
        return $http.get(config.primo.host+'get-single-module/colleges/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    AddToWishList: function(id) {
        return $http.get(config.primo.host+'college/add-to-wishlist/'+id).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    searchCollegeByLocation: function(data){
        var data = {city:data.cureentAddress};
        return $http.post(config.primo.host+'get-module/colleges?page=1',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    collegeSearchPage: function(data){
      return $http.get(config.primo.host+'get-module/colleges?page='+lastpage+"&best=true").catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    searchCollegesWithTitleStateCountry: function(data,lastpage){
      return $http.get(config.primo.host+'get-module/colleges?page='+lastpage+"&title="+data.title+"&state="+data.state+"&country="+data.country).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    /**** book service *****/

    sendAMessage: function(bookId){
        return $http.get(config.primo.host+'sendMessage',{bookid:bookId}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    cateAndSubcatList: function(){
        return $http.get(config.primo.host+'sampleData/textbookLeftSidecategory.json').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    
    customeBroughtTextBooks: function(slug){
       return $http.get(config.primo.host+'sampleData/customeBoughtTextbook.json').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetBooks: function(lastpage,category_id) {
       return $http.get(config.primo.host+'get-module/books?page='+lastpage+'&category_id='+category_id).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    GetBooksByTitle: function(lastpage,title) {
       return $http.get(config.primo.host+'get-module/books?page='+lastpage+'&title='+title).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    GetBook: function(slug) {
        return $http.get(config.primo.host+'get-single-module/books/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    searchTextBook: function(searchText){
       return $http.get(config.primo.host+'sampleData/customeBoughtTextbook.json').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    loadMoreTextBooks: function(lastpage){
        return $http.get(config.primo.host+'get-module/books?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              }); 
    },

    GetUserBooks: function(lastpage){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
        return $http.get(config.primo.host+'get-user-books?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    CreateBooks: function(data){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'books/create', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    UpdateBooks: function(data,book_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'books-update/'+book_id+'/update', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    DeleteBooks: function(book_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      
      return $http.post(config.primo.host+'books/'+book_id+'/delete').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    
    /**** event service *****/

    Events: function(lastpage) {
       return $http.get(config.primo.host+'get-module/events?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Event: function(slug) {
        return $http.get(config.primo.host+'get-single-module/events/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    CreateEvent: function(data) {
        return $http.post(config.primo.host+'events/create',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    UserEvents: function(lastpage){
        return $http.get(config.primo.host+'get-module/events?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Update: function(data){
        return $http.post(config.primo.host+'events/'+data.id+'/update',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetUserEvents: function(lastpage){

        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }

        return $http.get(config.primo.host+'get-user-events?page='+lastpage).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    CreateEvents: function(data){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'events/create', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    UpdateEvents: function(data,event_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'events-update/'+event_id+'/update', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    DeleteEvents: function(event_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      
      return $http.post(config.primo.host+'events/'+event_id+'/delete').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    getEventsHost: function(data){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      return $http.get(config.primo.host+'events/get-events-host').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    AddEventHost: function(data){

        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }

        return $http.post(config.primo.host+'events/host/create',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    //searchEvents: function(data,lastpage = 1){
        // return $http.get(config.primo.host +'events/search?page='+lastpage+"&title="+data.title+"&state="+data.state+"&date="+data.date).catch(function(response){
        //         if(response.status == 500){
        //           localStorage.removeItem('token'); 
        //           alert("Session has expired, please login again");
        //           $state.go('login')
        //         }
        //       });
        
    //},

    /**** Profile Service *********/
    GetAll: function() {
            return $http.get(config.primo.host+'users').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetById: function(id) {
        return $http.get(config.primo.host+'users/' + id).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Profile: function(data) {
      
      return $http.post(config.primo.host+'get-profile-info',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    updateInfo: function(data){
        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }
        
        return $http.post(config.primo.host+'users',$httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    updateProfileImage: function(data){

      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      fd.append('image', data);
      
      return $http.post(config.primo.host+'profile-image-update', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetByUsername: function(username) {
        return $http.post(config.primo.host+'auth/login',username).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    CreateRegistration: function(data) {
        return $http.post(config.primo.host+'auth/register', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    UserUpdate: function(data) {
        return $http.put(config.primo.host+'users/' + user.id, $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    Delete: function(id) {
        return $http.delete(config.primo.host+'users/' + id).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetState: function(){
        return $http.get(config.primo.host+'get/state').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetEducationLevel: function(){
        return $http.get(config.primo.host+'get/education-level').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    GetStateEducationlevelCollege: function(){
        return $http.get(config.primo.host+'get/signup-step-one').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    ForgetPasswordRequest: function (data) {
        return $http.post(config.primo.host +'password/email', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    ResetPasswordRequest: function (data) {
        return $http.post(config.primo.host +'password/reset', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    getInbox: function() {
        return $http.post(config.primo.host + 'get-inbox').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    getContactUser: function() {
        return $http.post(config.primo.host + 'get-contact-user').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    getGroup: function(userId) {
        var data = {user: userId};
        return $http.post(config.primo.host + 'get-group', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    sendMessage: function(data) {
        return $http.post(config.primo.host + 'send-message', $httpParamSerializerJQLike(data)).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    /* Classified Service */

    GetUserClassified: function(lastpage){

        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }

        return $http({
          method: 'GET',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          url: config.primo.host + 'get-user-classified?page='+lastpage,
        });
    },

    CreateClassified: function(data){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'classified/create', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    UpdateClassified: function(data,classfied_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
        $http.defaults.headers.common['Content-Type'] = undefined;
      }
      
      var fd = new FormData();
      angular.forEach(data, function(value,key) {
          fd.append(key, value);
      });
      
      return $http.post(config.primo.host+'classified-update/'+classfied_id+'/update', fd, { transformRequest: angular.identity}).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },
    DeleteClassified: function(classfied_id){
      var token = localStorage.getItem('token');
      if(token){
        $http.defaults.headers.common['Authorization'] = token;
      }
      
      return $http.post(config.primo.host+'classified/'+classfied_id+'/delete').catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    },

    /* Get All Single Module By Id */

    GetSingleModuleById: function(module,id){

        var token = localStorage.getItem('token');
        if(token){
          $http.defaults.headers.common['Authorization'] = token;
        }

        return $http({
          method: 'GET',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          url: config.primo.host + 'get-single-module-by-id/'+module+'/'+id,
        });
    },
    
    /**** Get Article ******/

    getArticle: function(slug){
      return $http.get(config.primo.host+'get/article/'+slug).catch(function(response){
                if(response.status == 500){
                  localStorage.removeItem('token'); 
                  alert("Session has expired, please login again");
                  $state.go('login')
                }
              });
    }
  } 
});