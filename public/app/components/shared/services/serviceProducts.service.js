app.service('ServiceProductsService', function(PrimoService, $window, $q) {
  return {
    /**
     * Calls primo service to load international rates
     * If rates are loaded in last hour return data from localStorage
     * @returns {*}
     */
    loadDidAndCreditProducts: function(promo) {
      var deferred = $q.defer();
      var lastTime = $window.localStorage.didAndCreditProductsRefreshDate;
      var timePass = Date.now() - lastTime;

      console.log('didAndCreditProductsRefreshDate last time: ' + lastTime + ' and timePass: ' + timePass);
      //if value is cached more than one hour it will be refreshed
      if (!$window.localStorage.didAndCreditProductsRefreshDate || (timePass > 360000)) {
        console.log('didAndCreditProductsRefreshDate refreshing..');
        PrimoService.getServiceProducts(promo).then(function(data) {
          $window.localStorage.didAndCreditProducts            = JSON.stringify(data.data);
          $window.localStorage.didAndCreditProductsRefreshDate = Date.now();

          deferred.resolve(data.data);
        });
      } else {
        deferred.resolve(JSON.parse($window.localStorage.didAndCreditProducts));
      }

      return deferred.promise;
    },

    loadRatePlanProducts: function() {
      var deferred = $q.defer();
      var lastTime = $window.localStorage.ratePlanProductsRefreshDate;
      var timePass = Date.now() - lastTime;

      console.log('ratePlanProductsRefreshDate last time: ' + lastTime + ' and timePass: ' + timePass);

      if (!$window.localStorage.ratePlanProductsRefreshDate || (timePass > 360000)) {
        //get countries products
        PrimoService.getServiceProductsCountry().then(function(data) {
          $window.localStorage.ratePlanProducts            = JSON.stringify(data.data);
          $window.localStorage.ratePlanProductsRefreshDate = Date.now();
          deferred.resolve(data.data);
        });
      } else {
        deferred.resolve(JSON.parse($window.localStorage.ratePlanProducts));
      }

      return deferred.promise;
    },

    /**
     * Fetch the list of popular countries
     */
    getPopularCountries: function(promo) {
      var deferred = $q.defer();

      var retVal = [];
      this.getServiceProductsForCountryWithPromoCode(promo).then(function(data) {
        if (data.groups) {
          angular.forEach(data.groups[0].countries, function(val, key) {
            if (val.popular == true) {
              retVal.push(val);
            }
          });
        }
        deferred.resolve(retVal);
      });

      return deferred.promise;
    },

    /**
     * Returns a complete list of serviceProducts
     * @returns {Array}
     */
    getServiceProducts: function() {
      return ($window.localStorage.didAndCreditProducts) ? JSON.parse($window.localStorage.didAndCreditProducts) : [];
    },

    /**
     * Fetch DID products from local storage
     */
    getDidProducts: function(promo) {
      var deferred = $q.defer();
      var retVal   = [];
      this.loadDidAndCreditProducts(promo).then(function(data) {
        angular.forEach(data.groups, function(val, key) {
          if (val.group === "DID") {
            retVal = val.products;
            console.log("did product:: ");
            console.log(val.products);
          }
        });

        deferred.resolve(retVal);
      });
      return deferred.promise;
    },


    /**
     * Fetch the list of Credit products
     * @returns {*|promise}
     */
    getCreditProducts: function(promo) {
      var deferred = $q.defer();
      var retVal   = [];
      this.loadDidAndCreditProducts(promo).then(function(data) {
        angular.forEach(data.groups, function(val, key) {
          if (val.group === "CREDIT") {
            retVal = val.products;
          }
        });

        deferred.resolve(retVal);
      });
      return deferred.promise;
    },
    /**
     * Call services to fetch Service products with promotion for specific promo code
     * @param  {[type]} promo [description]
     * @return {[type]}       [description]
     */
    getServiceProductsForCountryWithPromoCode: function(promo) {
      var deferred = $q.defer();

      if (!$window.localStorage.ratePlanProductsRefreshDate || (Date.now() - $window.localStorage.ratePlanProductsRefreshDate > 3600 * 1000)) {
        //get countries products
        PrimoService.getServiceProductsCountry(promo).then(function(data) {
          $window.localStorage.ratePlanProducts            = JSON.stringify(data.data);
          $window.localStorage.ratePlanProductsRefreshDate = Date.now();
          deferred.resolve(data.data);
        });
      } else {
        deferred.resolve(JSON.parse($window.localStorage.ratePlanProducts));
      }
      return deferred.promise;
    },

    /**
     * Returns the list of rate plans
     * @returns {Array}
     */
    getRatePlanProducts: function() {
      return ($window.localStorage.ratePlanProducts) ? JSON.parse($window.localStorage.ratePlanProducts) : [];
    }
  }
});