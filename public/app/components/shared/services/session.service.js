/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.service("SessionService", function($rootScope, $window, $injector, UtilHelperService) {
  var service = {
    storeSession: function(data, user) {
      if (data.alias && data.refresh_token) {
        $rootScope.data = data;
      }

      if (data.login_token) {
        $rootScope.token = data.login_token;
      }

      if (data.alias) {
        $rootScope.userId = data.alias;
      }

      if (user) {
        $rootScope.user            = user;
        $rootScope.user.first_name = UtilHelperService.uniCodeToString($rootScope.user.first_name);
        $rootScope.user.last_name  = UtilHelperService.uniCodeToString($rootScope.user.last_name);
      }

      $window.localStorage.token  = $rootScope.token;
      $window.localStorage.userId = $rootScope.userId;
      $window.localStorage.data   = JSON.stringify($rootScope.data);
      $window.localStorage.user   = JSON.stringify($rootScope.user);
    },
    destroySession: function() {
      var state = $injector.get('$state');
      delete $rootScope.token;
      delete $rootScope.userId;
      delete $rootScope.user;

      delete $window.localStorage.token;
      delete $window.localStorage.data;
      delete $window.localStorage.userId;
      delete $window.localStorage.user;
      delete $window.localStorage.contacts;

      // Manual inject
      if (state.current.name !== '/') {
        state.go('/');
      }
    },
    updateSession: function(user) {
      //copy all properties from user
      angular.forEach(user, function(val, key) {
        $rootScope.user[key] = val;
      });
      $window.localStorage.user = JSON.stringify($rootScope.user);
    },
    restoreSession: function() {
      if ($window.localStorage.token) {
        $rootScope.token  = $window.localStorage.token;
        $rootScope.userId = $window.localStorage.userId;
        $rootScope.user   = JSON.parse($window.localStorage.user).data;
        //$rootScope.data   = JSON.parse($window.localStorage.data);

        return true;
      } else {
        return false;
      }
    },
    isAuthenticated: function() {
      if (!$rootScope.userId) {
        return this.restoreSession();
      } else {
        return true;
      }
    },
    getPUID: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.partner.PUID;
      }
    },
    getEmail: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.email;
      }
    },
    getUsername: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.alias;
      }
    },
    getFirstName: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.first_name;
      }
    },
    getLastName: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.last_name;
      }
    },
    getDID: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.did;
      }
    },
    getMSISDN: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.msisdn;
      }
    },
    getMSISDNCountryCode: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.msisdn_country_code;
      }
    },
    getCreditBalance: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.balance;
      }
    },
    getAvailableMinutes: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.intl_minutes;
      }
    },
    getRewardMinutes: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.free_minutes) ? user.free_minutes : 0;
      }
    },
    getPlanMinutes: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.plan_minutes) ? user.plan_minutes : 0;
      }
    },
    getDidExpiration: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return user.did_expires;
      }
    },
    getResidencePlace: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.residence_location_string) ? user.residence_location_string : "-";
      }
    },
    getCurrentPlan: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.current_plan) ? user.current_plan.short_title : "-";
      }
    },
    getCurrentPlanExpirationDate: function() {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.current_plan && user.current_plan.expires) ? new Date(user.current_plan.expires * 1000) : "-";
      }
    },
    hasActiveUnlimitedPlan: function(ratePlan) {
      if ($window.localStorage.user) {
        var user = JSON.parse($window.localStorage.user);
        return (user.plan_state && user.plan_state == "Active" && user.current_plan && user.current_plan.plan != ratePlan.product);
      }
    }
  };

  return service;
});