/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.service("UiService", function ($rootScope, $window, $translate, SessionService, $mdSidenav,
                                   InternationalRatesService,
                                   ServiceProductsService,
                                   $mdDialog,
                                   deviceDetector,
                                   $mdMedia,
                                   PublicURLService,
                                   CountryService) {
  return {
    init: function () {
      moment.tz.add('America/Los_Angeles|PST PDT|80 70|0101|1Lzm0 1zb0 Op0');
      //load country list
      $rootScope.countries = CountryService.getCountryCodes();

      //load international rates
      InternationalRatesService.init();

      $rootScope.deviceDetector = deviceDetector;

      //load public routes
      // PublicURLService.load().then(function (data) {
      //   $rootScope.publicURLs = data;
      // });

      // Alert-box UI params
      $rootScope.alert = {show: false, context: {}};

      // Logout fn
      $rootScope.logout = function () {
        SessionService.destroySession();
      };

      //initialize i18n
      $translate.use('en_EN');
      $rootScope.activeLanguage = 'en_EN';
      $rootScope.UiService      = this;
    },
    /**
     * Smooth scroll to element
     * @param el
     */
    scrollToElement: function (el, detract) {
      detract = detract || 150;
      $('html, body').animate({
        scrollTop: $(el).offset().top - detract
      }, 700, 'easeInOutCubic');
    },
    isAuthPage: function (isAuthPage) {
      $rootScope.isAuthPage = isAuthPage;
    },
    /**
     * Display alert message
     * @param title
     * @param text
     */
    showAlert: function (title, text) {
      $rootScope.alert = {
        show: true,
        context: {title: title, msg: text}
      };
    },

    /**
     * Return detected device type
     */
    isAndroidDevice: function () {
      return $rootScope.deviceDetector.raw.device.android;
    },
    isIOSDevice: function () {
      return ($rootScope.deviceDetector.raw.device.iphone || $rootScope.deviceDetector.raw.device.ipad);
    },
    showAlertDialog: function (controller, templateUrl, clb, filter) {
      $mdDialog.show({
        controller: controller,
        templateUrl: templateUrl,
        clickOutsideToClose: true,
        resolve: {
          filter: function () {
            return filter;
          }
        }
      }).then(function (val) {
        clb(val);
      }, function (val) {
        clb(val);
      });

    },
    showConfirmDialog: function (controller, templateUrl, clb) {
      $mdDialog.show({
        controller: controller,
        templateUrl: templateUrl,
        clickOutsideToClose: false
      }).then(function (answer) {
        clb(answer);
      }, function () {
        clb(false);
      });
    }
  };
});