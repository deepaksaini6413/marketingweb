/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.service('UtilHelperService', function () {
  var service = {
    /**
     * Converts unicode to string
     * @param source
     * @returns {*}
     */
    uniCodeToString: function (source) {
      var r = /\\u([\d\w]{4})/gi;
      if (source) {
        source = source.replace(r, function (match, grp) {
          return String.fromCharCode(parseInt(grp, 16));
        });
        source = unescape(source);
      }
      return source;
    },
    /**
     * Converst unicode character to ascii format
     * @param str
     * @returns {string}
     */
    unicodeToAscii: function (str) {
      var out = "";
      for (var i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) < 0x80) {
          out += str.charAt(i);
        } else {
          var u = "" + str.charCodeAt(i).toString(16);
          out += "\\u" + (u.length === 2 ? "00" + u : u.length === 3 ? "0" + u : u);
        }
      }
      return out;
    },
    /**
     * Extracts username/pstn from string received from sip. Substring before first @ symbol
     * @param str
     * @returns {string}
     */
    extractUsernameFromSIP: function (str) {
      var retVal = str.substr(0, str.indexOf('@'));
      retVal     = retVal.replace('sip:', '');
      return retVal;
    },
    mergeToSIPuser: function (user, domain) {
      return user + '@' + domain;
    },
    sortAsc: function (messages) {
      messages.sort(function (a, b) {
        return parseFloat(a.date) - parseFloat(b.date);
      });

      return messages;
    },
    sortDesc: function (messages) {
      messages.sort(function (a, b) {
        return parseFloat(b.date) - parseFloat(a.date);
      });

      return messages;
    }
  };

  return service;
});