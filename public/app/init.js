app.run(function ($injector, $window, $location, $rootScope, config, SessionService, $mdDialog, UiService, $state, AuthService, _) {
  //google analytics
  $window.ga('create', config.google.analytics.trackingCode, 'auto');
  $window.ga('require', 'ecommerce');

  $rootScope.$on('$stateChangeSuccess', function (event) {
    $window.ga('send', 'pageview', $location.path());
  });

  $rootScope.$on('destroy-session', function () {
    SessionService.destroySession();
  });

  SessionService.restoreSession();

  // Load UI config
  UiService.init();

  $rootScope.$on('$stateChangeStart', function (event, next, stateParamsOpts) {

    //close all opened md dialogs
    $mdDialog.cancel();

    //prevent access to login page when authenticated
    if (next.name === 'login' && SessionService.isAuthenticated()) {
      event.preventDefault();
      return $state.go('/');
    }

    if (next.isAuthPage) {
      UiService.isAuthPage(true);
    } else {
      UiService.isAuthPage(false);
    }

    if (next.isPublic === false) {
      
      if(next.name === '/'){
        event.preventDefault();
        return $state.go("landing-page");
      }

      if (SessionService.isAuthenticated()) { //authenticated user is transferred to last state
        if ($rootScope.lastState) {
          event.preventDefault();
          var lastState = $rootScope.lastState;
          delete $rootScope.lastState;
          return $state.go(lastState);
        }
      } else { //non authenticated user is transferred to login
        event.preventDefault();
        $rootScope.lastState = next.name;
        $rootScope.stateParams = stateParamsOpts || {};
        return AuthService.login();
      }
    }
  });

});