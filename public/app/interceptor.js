
app.factory('sessionInterceptor', function ($rootScope, $q, $filter, config) {
  var sessionInterceptor = {
    request: function (cfg) {
      if (cfg.url == '/create-ticket' || cfg.url.includes('world-')) {
        cfg.headers['Content-Type'] = 'application/json';
      } else {
        cfg.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      }

      if (cfg.url.includes('/books/create')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/books-update')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/events/create')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/events-update')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/classified/create')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/classified-update')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/jobs/create')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/jobs-update')) {
        cfg.headers['Content-Type'] = undefined;
      }

      if (cfg.url.includes('/profile-image-update')) {
        cfg.headers['Content-Type'] = undefined;
      }

      return cfg;
    },
    response: function (response) {
      var deferred = $q.defer();
      if (response.data && response.data.error_codes && response.data.error_codes.length > 0) {
        //when invalid login token is received destroy session
        if (response.data && response.data.error_codes &&
          response.data.error_codes[0] == 'SE0007' ||
          response.data.error_codes[0] == 'SE0013' ||
          response.data.error_codes[0] == 'VV0031') {
          if (response.data.error_codes[0] == 'VV0031') {
            $rootScope.alert = {
              show: true,
              context: {type: 'danger', msg: response.data.user_errors}
            };
          }
          $rootScope.$broadcast('destroy-session');
          return $q.reject(response);
        }
        else if (response.data.error_codes[0] == 'AC0001' || //this is not an error, its epay user first login sms sent as password message
          response.data.error_codes[0] == 'AC0002' //first epay success login
        ) {

          if (response.data.error_codes[0] == 'AC0001') {
            $rootScope.alert = {
              show: true,
              context: {type: 'success', msg: response.data.user_errors}
            };
          }

          return $q.resolve(response);
        }
        //display error message
        else if (response.data && response.data.user_errors && response.data.user_errors.length > 0) {
          $rootScope.alert = {
            show: true,
            context: {type: 'danger', msg: response.data.user_errors[0]}
          };
          deferred.reject(response);
        }
      } else if (response.config.url.includes('GetPartnerToken')) {
        //braintree needs a client token as a single paramtere received from response...
        if (response.data.tokens && response.data.tokens.braintree) {
          response.data = response.data.tokens.braintree.token;
        }
        deferred.resolve(response);
      } else {
        deferred.resolve(response);
      }
      return deferred.promise;
    },
    responseError: function (rejection) {
      try {
        //when received invalid login token destroy session
        if (rejection.data.error_codes[0] == 'SE0013' || rejection.data.error_codes[0] == 'VV0031') {
          if (rejection.data.error_codes[0] == 'VV0031') {
            $rootScope.alert = {
              show: true,
              context: {type: 'danger', msg: rejection.data.user_errors}
            };
          }
          $rootScope.$broadcast('destroy-session');
          return $q.reject(rejection);
        } else if (rejection.data.error_codes[0] == 'AC0001' || //this is not an error, its epay user first login sms sent as password message
          rejection.data.error_codes[0] == 'AC0002' //first epay success login
        ) {

          if (rejection.data.error_codes[0] == 'AC0001') {
            $rootScope.alert = {
              show: true,
              context: {type: 'success', msg: rejection.data.user_errors}
            };
          }

          return $q.resolve(rejection);
        } else {
          // Error handler
          if (rejection.data && rejection.data && rejection.data.user_errors) {
            $rootScope.alert = {
              show: true,
              context: {type: 'danger', msg: UtilHelperService.uniCodeToString(rejection.data.user_errors[0])}
            };
          }
          return $q.reject(rejection);

        }

      } catch (e) {
        $rootScope.alert = {show: true, context: {type: 'danger', msg: $filter('translate')("FATAL_ERROR")}};

        return $q.reject(rejection);
      }
    }
  };

  return sessionInterceptor;
});