/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller('AccountChangePasswordController', function ($scope, PrimoService, $filter) {

  $scope.changePassword = function () {
    var data = {
      old_password: $scope.checkSecret,
      new_password: $scope.secret
    };

    PrimoService.changePassword(data)
      .success(function () {
        $scope.$root.alert = {
          show: true,
          context: {type: 'success', msg: $filter('translate')('PASSWORD_CHANGE_SUCCESS')}
        };
      })
  };

});