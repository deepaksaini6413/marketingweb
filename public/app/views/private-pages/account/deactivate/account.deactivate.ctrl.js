/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller('AccountDeactivateController', function ($rootScope, $scope, PrimoService) {

  $scope.deactivate = function () {
    PrimoService.deactivateAccount({}).success(function () {
      $rootScope.logout();
    });
  }
});