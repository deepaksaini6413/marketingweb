/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller('AccountFAQController', function ($rootScope, $scope,
                                                 $filter,
                                                 $state,
                                                 PrimoService,
                                                 SessionService,
                                                 $stateParams,
                                                 articles, UiService, AuthService) {


  $scope.scrollToElement = UiService.scrollToElement;

  //all sections received from zendesk with articles inside
  $scope.sections = articles;
  $scope.search   = '';

  //all articles flattened
  $scope.allArticles = [];

  var _moveArticles = function (fromCat, toCat) {
    angular.forEach(fromCat.articles, function (val, key) {
      toCat.articles.push(val);
    })
  };
  //repack sections
  var primoPhoneNumberCat,
      creditAndBillingHistoryCat,
      callingCat,
      p2pCat,
      myAccountCat,
      ratePlansCat = {};

  angular.forEach($scope.sections, function (val, key) {
    if (val.id == '205098987') primoPhoneNumberCat = val;
    if (val.id == '205099187') ratePlansCat = val;
    if (val.id == '203902818') creditAndBillingHistoryCat = val;
    if (val.id == '205090368') myAccountCat = val;
    if (val.id == '203861597') callingCat = val;
    if (val.id == '203902308') p2pCat = val;

    //add articles to list of all articles
    $scope.allArticles = [].concat($scope.allArticles, val.articles);
  });

  _moveArticles(primoPhoneNumberCat, myAccountCat);
  _moveArticles(creditAndBillingHistoryCat, ratePlansCat);
  _moveArticles(p2pCat, ratePlansCat);


  /**
   * Remove uwanted categories
   */
  $scope.sections = $scope.sections.filter(function (el) {
    return el.id != '205098987' && el.id != '203902818' && el.id != '203902308'
  });

  $scope.selected = ($stateParams.selected) ? $scope.sections[$stateParams.selected] : $scope.sections[0];

  $scope.selectedArticle = null;
  /**
   * Select faq item
   * @param item
   */
  $scope.select = function (item) {
    $scope.search   = '';
    $scope.selected = item;
    $scope.scrollToElement('#group-articles');
  };


  $scope.isSelected = function (item) {
    return ($scope.selected && $scope.selected.id == item.id);
  };


  //ARTICLE SELECTORS

  $scope.selectArticle = function (article) {
    $scope.selectedArticle = article;
  };

  $scope.isSelectedArticle = function (item) {
    return ($scope.selectedArticle && $scope.selectedArticle.id == item.id);
  };


  /**
   * Opens a dialog and submit values received from dialog
   */
  $scope.createTicketDialog = function () {
    //if (!$rootScope.user) {
    //  AuthService.login();
    //  return;
    //}
    UiService.showAlertDialog('AccountTicketCreateController', 'partials/account-ticket-create.html', function (val) {
      if (val) {
        if ($rootScope.user) {
          val.username = $rootScope.userId;
          val.name     = SessionService.getFirstName() + " " + SessionService.getLastName();
        }
        PrimoService.createTicket(val).then(function () {
          $rootScope.alert = {
            show: true,
            context: {type: 'success', msg: $filter('translate')('SUCCESSFULLY_CREATED_TICKET')}
          };
        });
      }
    })
  };
});