/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.filter('filterArticles', function () {
  return function (input, criteria) {
    var result = [];
    var regex  = new RegExp(criteria, 'i');
    angular.forEach(input, function (article) {
      if (regex.exec(article.name)) {
        return result.push(article);
      }
      if (regex.exec(article.body)) {
        return result.push(article);
      }
    });
    return result;
  }
});