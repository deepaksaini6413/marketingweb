
app.controller('AccountInviteFriendsController', function ($rootScope, $scope, $mdDialog, $filter) {

  $scope.emailTemplateText = $filter('translate')('EMAIL_TEMPLATE_TEXT');

  $scope.email        = "";
  $scope.inviteEmails = [];

  //index of selected item
  $scope.selectedItemIndex = -1;

  $scope.answer = function (answer) {
    if (answer) {
      $mdDialog.hide($scope.inviteEmails);
    } else {
      $mdDialog.hide(false);
    }
  };

  /**
   * add to emails list
   */
  $scope.addToList = function () {
    //TODO: fetch limit from services
    if ($scope.inviteEmails.length == 5) {
      $rootScope.alert = {show: true, context: {type: 'info', msg: $filter('translate')("LIMIT_INVITE_PERIOD")}};
      return;
    }
    //already added email can not be added
    if ($scope.inviteEmails.indexOf($scope.email) !== -1) {
      $rootScope.alert = {show: true, context: {type: 'danger', msg: $filter('translate')("EMAIL_ALREADY_ADDED")}};
      return;
    }

    //evaluate agains email reges
    var patt = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
    var res  = new RegExp(patt).test($scope.email);

    //add to the list
    if (res) {
      $scope.inviteEmails.push($scope.email);
      $scope.email = "";
    }
  };

  /**
   * Remove element from array
   * @param index
   */
  $scope.removeFromList = function (index) {
    if ($scope.selectedItemIndex == index) {
      $scope.inviteEmails.splice(index, 1);
      $scope.selectedItemIndex = -1
    }
  };

  $scope.select = function (index) {
    $scope.selectedItemIndex = ($scope.selectedItemIndex != index) ? index : -1;
  };

  /**
   * checks if item is selected
   * @param index
   * @returns {boolean}
   */
  $scope.isSelected = function (index) {
    return $scope.selectedItemIndex == index;
  }
});