/**
 * Used to view/edit currently logged user details
 */
app.controller('AccountProfileController', function($rootScope,
                                                    UiService,
                                                    $scope, $q, $http,
                                                    UtilHelperService,
                                                    PrimoService, SessionService, $filter, AuthService, config, Base64) {
        $rootScope.isProcessing = false;
        $scope.editProfile  = true;
        $scope.myMessages   = false;
        $scope.myContents   = false;
        $scope.disableForm  = true;
        $scope.addForm      = 'books';
        $scope.conditions   = {0 : "New", 1 : "Used"};
        $scope.trades       = {0 : "Money Transaction", 1 : "Money & Trade", 2 : "Trade"};
        $scope.want_sell    = {0 : "Want to sell", 1 : "Sold"};
        $scope.statusArr    = {0 : "Inactive", 1 : "Active"};
        $scope.industries   = {"Customer Service" : "Customer Service", "Manufacturing" : "Manufacturing","Retail":"Retail","Other":"Other"};
        $scope.job_types    = {"Full Time" : "Full Time", "Part Time" : "Part Time", "Other" : "Other"};
        $scope.locations    = {"Within campus" : "Within campus", "Outside campus" : "Outside campus","Online":"Online"};
        $scope.event_types  = {"School event" : "School event", "Other event" : "Other event"};
        $scope.publicArr    = {0 : "Public", 1 : "Private"};
        $scope.user         = {};
        $scope.data         = {};
        $scope.module       = "";
        $scope.lastpage     = 1;
        $scope.updateForm   = false;
        $scope.loadingImage ='';
        $rootScope.isProcessing=false;
        $scope.user           = JSON.parse(localStorage.getItem('user'));

        initController();
        
        function initController(){
            $scope.loadingImage="././images/loader.gif";
            
            $scope.selectedCollegeId=window.localStorage.getItem('selectedCollegeOrUniversity');
            $rootScope.isProcessing=true;
            var data = {userid:$scope.user.id};
            PrimoService.Profile(data).then(function(res){
                var response = res.data;
                if(response.status == 1){
                    $rootScope.isProcessing=false;
                    $scope.user.first_name=response.data.first_name;
                    $scope.user.last_name=response.data.last_name;
                    $scope.user.email=response.data.email;
                    $scope.user.gender=response.data.gender;
                    $scope.user.birthday=response.data.birthday;
                    $scope.user.gender=response.data.gender;
                    $scope.user.phone=response.data.phone ? response.data.phone: 'XXXXXXX-XXX';
                    $scope.user.education=response.data.education ? response.data.birthday :'None';
                    $scope.user.biography=response.data.biography;

                    $scope.user.scholarship_apply=response.data.scholarship_apply;
                    $scope.user.applied=response.data.job.applied.length;
                    $scope.user.favourite=response.data.job.favourite.length;
                    $scope.user.posted=response.data.job.posted.length;
                
                }

            
            });

        }

        $scope.Profile = function(){
            //return "Deepak Saini";
        }

        $scope.EditProfile = function(){
          $rootScope.isProcessing = true;
          $scope.myMessages   = false;
          $scope.myContents   = false;
          $scope.editProfile  = true;
        }
        /**Message module starts**/
        $scope.showingInbox = true;
        $scope.MyMessages = function(){
            $rootScope.isProcessing = true;
            $scope.myMessages   = true;
            $scope.myContents   = false;
            $scope.editProfile  = false;
            PrimoService.getInbox().then(function(response) {
                $scope.inboxList = response.inbox;
            });
        }

        $scope.showContact = function() {
            PrimoService.getContactUser().then(function(response) {
                if (response.status == 1) {
                    $scope.userContacts = response.users;
                    $scope.showingContact = true;
                    $scope.showingInbox = false;
                }else{
                    $socpe.userContacts = [];
                }
            });
        }

        $scope.showInbox = function() {
            $scope.showingInbox = true;            
            $scope.showingContact = false;
        }

        $scope.getGroup = function(userId) {
            PrimoService.getGroup(userId).then(function(response) {
                $scope.receiver = response.message[0].receiver;
                $scope.sender = response.message[0].sender;
                if (response.message[0].receiver.email == $scope.email) {
                    $scope.sender = response.message[0].receiver;
                    $scope.receiver = response.message[0].sender;
                }else if(response.message[0].sender.email == $scope.email){
                    $scope.sender = response.message[0].sender;
                    $scope.receiver = response.message[0].receiver;
                }
                $scope.chatStream = response.message[0].message;
                $scope.group_id = response.message[0].id;
                $scope.sender = response.message[0].sender;
                $scope.receiver = response.message[0].receiver;
            });
        }

        $scope.sendMsg = function(msg) {
            console.log(msg);
            var data = {group_id: $scope.group_id, message: msg.message};
            $('#get-msg').val('');
            PrimoService.sendMessage(data).then(function(response) {
                if (response.status == 1) {
                    if (response.message.length != 0) {
                        console.log(response.message.length);
                        $scope.chatStream.push(response.message);
                    }
                    $scope.fileThumbnail = false;
                    var list = $('#message');
                    var scrollHeight = list.prop('scrollHeight')
                    list.animate({scrollTop: scrollHeight}, 500);
                }
            });
            // var send = MAIN.sendMessage(data);
            // send.success(function(response) {
            //     console.log(response);
            //     if (response.status == 1) {
            //         $scope.emojiMessage.rawhtml = null;
            //         if (response.fileMsg.length != 0) {
            //             $scope.chatStream.push(response.fileMsg);
            //         }
            //         if (response.message.length != 0) {
            //             console.log(response.message.length);
            //             $scope.chatStream.push(response.message);
            //         }
            //         $scope.fileThumbnail = false;
            //         var list = $('#message');
            //         var scrollHeight = list.prop('scrollHeight')
            //         list.animate({scrollTop: scrollHeight}, 500);
            //     }
            // });
        }

        /**Message module ends**/


        $scope.MyContents = function(){
            $scope.enableForm = false;
          $rootScope.isProcessing = true;
          $scope.myMessages   = false;
          $scope.myContents   = true;
          $scope.editProfile  = false;
            
            BookService.UserBooks($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    $scope.module       = 'module/templates/book/list.view.html';
                    $scope.items        = res.data.result.data;
                }
                
                if(res.status == 0){
                    $scope.module       = 'module/templates/book/error.view.html';
                    alert("Something went wrong");
                }
            });

        }

        $scope.editForm = function(){
          $scope.disableForm = false;
        }
        
        $scope.saveForm = function(){
          $scope.disableForm = true;

          PrimoService.updateInfo($scope.user).then(function(response){
            console.log(response);
          });
          console.log($scope.user);
        }

        $scope.getUserBooks = function(){
            $scope.enableForm   = false;
          $rootScope.isProcessing = true;
            $scope.myMessages   = false;
            $scope.myContents   = true;
            $scope.editProfile  = false;
            
            BookService.UserBooks($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    $scope.module       = 'module/templates/book/list.view.html';
                    $scope.items        = res.data.result.data;
                }
                
                if(res.status == 0){
                    $scope.module       = 'module/templates/book/error.view.html';
                    alert("Something went wrong");
                }
            });
        }

        $scope.getUserClassified = function(){
            $scope.enableForm   = false;
            $rootScope.isProcessing = true;
            $scope.myMessages   = false;
            $scope.myContents   = true;
            $scope.editProfile  = false;
            
            BookService.UserBooks($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    $scope.module       = 'module/templates/book/list.view.html';
                    $scope.items        = res.data.result.data;
                }
                
                if(res.status == 0){
                    $scope.module       = 'module/templates/book/error.view.html';
                    alert("Something went wrong");
                }
            });
        }

        $scope.getUserJobs = function(){
            $scope.enableForm   = false;
            $rootScope.isProcessing = true;
            $scope.myMessages   = false;
            $scope.myContents   = true;
            $scope.editProfile  = false;
            
            JobService.UserJobs($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    $scope.module       = 'module/templates/job/list.view.html';
                    $scope.items        = res.data.result.data;
                }
                
                if(res.status == 0){
                    $scope.module       = 'module/templates/job/error.view.html';
                    alert("Something went wrong");
                }
            });
        }
        $scope.getUserEvents = function(){
            $scope.enableForm   = false;
            $rootScope.isProcessing = true;
            $scope.myMessages   = false;
            $scope.myContents   = true;
            $scope.editProfile  = false;
            
            EventService.UserEvents($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }

                    $scope.module       = 'module/templates/event/list.view.html';
                    $scope.items        = res.data.result.data;
                }
                
                if(res.status == 0){
                    $scope.module       = 'module/templates/event/error.view.html';
                    alert("Something went wrong");
                }
            });
        }

        $scope.uploadImage =function(event){
         if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = (imagesrc) => {
              data.image = imagesrc.target.result;
              console.log(data.image);
            }
            reader.readAsDataURL(event.target.files[0]);
         }
        }
        
        $scope.Save = function(){
            BookService.Create($scope.data).then(function(response) {
                console.log(response);
            if (response.data.status == 1) {
                           
                        FlashService.Success('Book Created Successfully', true);
                        $scope.data = {};
                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating books process please contact to admin");
                        vm.dataLoading = false;
                    }
          });
        }

        $scope.saveJob = function(){
            JobService.Create($scope.data).then(function(response) {
                console.log(response);
                if (response.data.status == 1) {
                           
                        FlashService.Success('Job Created Successfully', true);
                        $scope.data = {};
                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating job please contact to admin");
                        vm.dataLoading = false;
                    }
            });
        }

        $scope.saveEvent = function(){
            EventService.Create($scope.data).then(function(response) {
                console.log(response);
                if (response.data.status == 1) {
                           
                        FlashService.Success('Event Created Successfully', true);
                        $scope.data = {};
                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating event please contact to admin");
                        vm.dataLoading = false;
                    }
            });
        }

        $scope.doEnable = function(){
            console.log("enableForm");
            $scope.enableForm = true;
            $scope.updateForm = false;
            $scope.data = {};
        }

        $scope.doDisable = function(){
            console.log("disableForm");
            $scope.enableForm = false;
        }

        $scope.loadMoreBooks = function(){
                $scope.lastpage +=1;

                BookService.Books($scope.lastpage).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
        }
        $scope.loadMoreJobs = function(){
                $scope.lastpage +=1;

                JobService.Jobs($scope.lastpage).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
        }

        $scope.loadMoreEvents = function(){
                $scope.lastpage +=1;

                EventService.Events($scope.lastpage).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
        }

        $scope.EditBook = function(book_slug){
            
            BookService.Book(book_slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.data = res.data.result;
                    $scope.data.trade_type = res.data.result.trade_type.toString();
                    $scope.data.status = res.data.result.status.toString();
                    $scope.data.sold = res.data.result.sold.toString();
                    $scope.data.condition = res.data.result.condition.toString();

                }
                   
            });
            $scope.enableForm = true;
            $scope.updateForm = true;
        }

        $scope.UpdateBook = function(){
            BookService.Update($scope.data).then(function(response) {
                console.log(response);
                if (response.data.status == 1) {
                        
                        FlashService.Success('Book Updated Successfully', true);

                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating books process please contact to admin");
                        vm.dataLoading = false;
                    }
            });
        }

        $scope.DeleteBook = function(){
            console.log("To Do");
        }

        $scope.EditEvent = function(event_slug){
            
            EventService.Event(event_slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.data = res.data.result;
                    $scope.data.event_type = res.data.result.event_type.toString();
                    $scope.data.public = res.data.result.public.toString();
                    $scope.data.status = res.data.result.status.toString();
                    $scope.data.start_date = "20-07-2017";
                }
                   
            });
            $scope.enableForm = true;
            $scope.updateForm = true;
        }

        $scope.UpdateEvent = function(){
            EventService.Update($scope.data).then(function(response) {
                console.log(response);
                if (response.data.status == 1) {
                        
                        FlashService.Success('Event Updated Successfully', true);

                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating books process please contact to admin");
                        vm.dataLoading = false;
                    }
            });
        }

        $scope.DeleteEvent = function(){
            console.log("To Do");
        }

        $scope.EditJob = function(job_slug){
            
            JobService.Job(job_slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.data = res.data.result;
                    $scope.data.job_type = res.data.result.job_type.toString();
                    $scope.data.status = res.data.result.status.toString();
                    $scope.data.industry = res.data.result.industry.toString();
                    $scope.data.job_location = res.data.result.job_location.toString();

                }
                   
            });
            $scope.enableForm = true;
            $scope.updateForm = true;
        }

        $scope.UpdateJob = function(){
            JobService.Update($scope.data).then(function(response) {
                console.log(response);
                if (response.data.status == 1) {
                        
                        FlashService.Success('Job Updated Successfully', true);

                    } else if(response.data.status == 0){

                        var errorsHtml = "<ul>";
                        angular.forEach(response.data.message,function(key,value){
                            errorsHtml += "<li>"+ response.data[value] + "</li>";
                        });
                        errorsHtml += "</ul>";
                        FlashService.Error(errorsHtml);
                        vm.dataLoading = false;
                    
                    } else {
                        FlashService.Error("Sorry, There is a problem in creating books process please contact to admin");
                        vm.dataLoading = false;
                    }
            });
        }

    $scope.DeleteJob = function(){
        console.log("To Do");
    }
    $scope.logout = function(){
        AuthenticationService.ClearCredentials();
        $rootScope.isUserLoggedIn = false;
    }
    
    function moduleDiv(){
      return {
          restrict: 'EA',
          template: "module/templates/{{name}}/create.view.html"
        }
    }

});