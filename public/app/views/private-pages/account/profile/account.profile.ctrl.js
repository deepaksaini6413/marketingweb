/**
 * Used to view/edit currently logged user details
 */
app.controller('AccountProfileController', function($rootScope,
                                                    UiService,
                                                    $scope, $q, $http,
                                                    UtilHelperService,
                                                    PrimoService, SessionService, $filter, AuthService, config, Base64,$uibModal,ngToast) {
        
        $scope.config               = config;
        $rootScope.isProcessing     = false;
        $scope.user                 = {};
        $scope.data                 = {};
        $scope.user                 = JSON.parse(localStorage.getItem('user'));
        $scope.jobsLastPage         = 1;
        $scope.eventsLastPage       = 1;
        $scope.booksLastPage        = 1;
        $scope.classifiedLastPage   = 1;
        $scope.bookCategories       = [];
        $scope.errors               = [];
        $scope.industries           = {"Customer Service" : "Customer Service", "Manufacturing" : "Manufacturing","Retail":"Retail","Other":"Other"};
        $scope.job_types            = {"Full Time" : "Full Time", "Part Time" : "Part Time", "Other" : "Other"};
        $scope.locations            = {"Within campus" : "Within campus", "Outside campus" : "Outside campus","Online":"Online"};
        $scope.job_experince_array  = {0 : "0 Year", 1 : "1 Year",2:"2 Year",3 : "3 Year", 4 : "4 Year",5:"5 Year",6 : "6 Year", 7 : "7 Year",8:"8 Year",9 : "9 Year", 10 : "10 Year",'more':"10+ Year"};
        $scope.job_salary_type_array  = {1 : "Hourly", 2 : "Weekly",3:"Monthly",4 : "Quarterly", 5 : "Yearly"};
        $scope.in_campus_array       = {1 : "Yes", 2 : "No"};
        initController();
        
        function initController(){
            
            $scope.selectedCollegeId=window.localStorage.getItem('selectedCollegeOrUniversity');
            $rootScope.isProcessing=true;
            if($rootScope.userId){
                $rootScope.user = JSON.parse(localStorage.getItem('user'));
            }
            if($rootScope.user.id){
              var data = {userid:$rootScope.user.id};
            }else{
              var data = {userid:$rootScope.user.user_id};
            }
            
            PrimoService.Profile(data).then(function(res){
                
                var response = res.data;
                if(response.status == 1){
                    localStorage.setItem('user',JSON.stringify(response.data));
                    if($rootScope.userId){
                        $rootScope.user = JSON.parse(localStorage.getItem('user'));
                    }

                    $rootScope.isProcessing =false;
                    $scope.user.first_name      =   response.data.first_name;
                    $scope.user.last_name       =   response.data.last_name;
                    $scope.user.email           =   response.data.email;
                    $scope.user.gender          =   response.data.gender;
                    $scope.user.birthday        =   response.data.birthday;
                    $scope.user.gender          =   response.data.gender;
                    $scope.user.phone           =   response.data.phone ? response.data.phone: 'XXXXXXX-XXX';
                    $scope.user.education       =   response.data.education ? response.data.birthday :'None';
                    $scope.user.biography       =   response.data.biography;
                    $scope.user.scholarship_apply=  response.data.scholarship_apply;
                    $scope.user.applied         =   response.data.job.applied.length;
                    $scope.user.favourite       =   response.data.job.favourite.length;
                    $scope.user.posted          =   response.data.job.posted.length;
                    $scope.user.books           =   response.data.books;
                    $scope.user.classified      =   response.data.classified;
                    $scope.user.events          =   response.data.events;
                    $scope.user.jobs            =   response.data.job.posted;
                    $scope.user.image            =   response.data.image;
                }

            
            });

        }

        /**
         * load All books of current user
         * 
         */
        $scope.userBooksLoadMore = function(){

            $scope.booksLastPage +=1;
            $rootScope.isProcessing=true;
            PrimoService.GetUserBooks($scope.booksLastPage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_books_disable = true;
                    }

                    if(!res.data.result){
                        $scope.is_books_disable = true;
                    }

                    $scope.user.books = $scope.user.books.concat(res.data.result.data);
                }
            });
        }

        /**
         * load All events of current user
         * 
         */
        $scope.userEventsLoadMore = function(){
            $scope.eventsLastPage +=1;
            $rootScope.isProcessing=true;
            PrimoService.GetUserEvents($scope.eventsLastPage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_events_disable = true;
                    }

                    if(!res.data.result){
                        $scope.is_events_disable = true;
                    }

                    $scope.user.events = $scope.user.events.concat(res.data.result.data);
                }
            });
        }

        /**
         * load All classified of current user
         * 
         */

        $scope.userClassifiedLoadMore = function(){
            $scope.classifiedLastPage +=1;
            $rootScope.isProcessing   =true;
            PrimoService.GetUserClassified($scope.classifiedLastPage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_classified_disable = true;
                    }

                    if(!res.data.result){
                        $scope.is_classified_disable = true;
                    }

                    $scope.user.classified = $scope.user.classified.concat(res.data.result.data);
                }
            });
        }

        /**
         * load All Jobs of current user
         * 
         */

        $scope.userJobsLoadMore = function(){
            $scope.jobsLastPage +=1;
            $rootScope.isProcessing=true;
            PrimoService.GetUserJobs($scope.jobsLastPage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_jobs_disable = true;
                    }

                    if(!res.data.result){
                        $scope.is_jobs_disable = true;
                    }

                    $scope.user.jobs = $scope.user.jobs.concat(res.data.result.data);
                }
            });
        }

        /**
         * Add Book
         * 
         */

        $scope.userAddBooksPopup = function(){

            $uibModal.open({
            templateUrl: 'partials/add.books.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log) {
                    PrimoService.GetBookCategoriesWithParent().then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.bookCategories = res.data.result;
                        }
                        
                        if(res.status == 0){
                            $scope.bookCategories = [];   
                        }
                    });

                    $scope.userAddBook = function () {
                        
                        $rootScope.isProcessing = true;

                        PrimoService.CreateBooks($scope.book).then(function(response){
                            $rootScope.isProcessing = false;

                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Book has been added");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }
            });
        }

        /**
         * update Book
         * 
         */
        $scope.userUpdateBooksPopup = function(book_id){

            $uibModal.open({
            templateUrl: 'partials/update.books.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log) {

                    PrimoService.GetBookCategoriesWithParent().then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.bookCategories = res.data.result;
                        }
                        
                        if(res.status == 0){
                            $scope.bookCategories = [];   
                        }
                    });

                    PrimoService.GetSingleModuleById('books',book_id).then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.book = res.data.result;
                            $scope.image_preview = $scope.book.image;
                        }
                        
                        if(res.status == 0){
                            $scope.book = [];   
                        }
                    });

                    $scope.userUpdateBook = function () {
                        
                        $rootScope.isProcessing = true;
                        
                        PrimoService.UpdateBooks($scope.book,$scope.book.id).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Book has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }
            });
        }

        /**
         * Delete Book
         * 
         */
        $scope.userDeleteBooks = function(book_id){


            PrimoService.DeleteBooks(book_id).then(function(response){
                    $rootScope.isProcessing = false;
                    
                    if(response.status == 200){
                        if(response.data.status == 1){
                            initController();
                            alert("Book has been deleted.");
                        }

                        if(response.data.status == 0){
                            alert("You don't have permission to delete this book.");
                        }
                    }
                        
                });
        }

        /**
         * Add Events
         * 
         */

        $scope.userAddEventsPopup = function(){

            $uibModal.open({
            templateUrl: 'partials/add.events.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log) {
                    
                    PrimoService.getEventsHost().then(function(response){
                        if(response.status == 200){
                                if(response.data.status == 1){
                                    $scope.hosts = response.data.result;

                                    if(response.data.result.length == 0){
                                        $uibModalInstance.dismiss('cancel');
                                        alert("Please add host first");
                                        
                                    }
                                }
                            }
                    });

                    setTimeout(function() {
                            $('#datetimepicker1').datetimepicker({minDate: new Date(),format:'YYYY-MM-DD HH:MM A'});
                            $('#datetimepicker2').datetimepicker({minDate: new Date(),format:'YYYY-MM-DD HH:MM A'});
                        }, 10);

                    $scope.userAddEvent = function () {
                        
                        var start_date = $('input[name=start_date]').val();

                        if(!start_date){
                            alert("Start date is required");
                            return false;
                        }
                        
                        var end_date = $('input[name=end_date]').val();
                        
                        if(!end_date){
                            alert("End date is required");
                            return false;
                        }

                        $scope.event['start_date'] = start_date;
                        $scope.event['end_date'] = end_date;

                        $rootScope.isProcessing = true;

                        PrimoService.CreateEvents($scope.event).then(function(response){
                            $rootScope.isProcessing = false;

                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Event has been added");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }
            });
        }

        /**
         * update Events
         * 
         */
        $scope.userUpdateEventsPopup = function(event_id){

            $uibModal.open({
            templateUrl: 'partials/update.events.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log) {

                    PrimoService.getEventsHost().then(function(response){
                        if(response.status == 200){
                                if(response.data.status == 1){
                                    $scope.hosts = response.data.result;

                                    if(response.data.result.length == 0){
                                        alert("Please add host first");
                                        $uibModalInstance.dismiss('cancel');
                                    }
                                }
                            }
                    });
                    
                    setTimeout(function() {
                            $('#datetimepicker1').datetimepicker({minDate: new Date(),format:'YYYY-MM-DD HH:MM A'});
                            $('#datetimepicker2').datetimepicker({minDate: new Date(),format:'YYYY-MM-DD HH:MM A'});
                        }, 10);

                    PrimoService.GetSingleModuleById('events',event_id).then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.event = res.data.result;
                            $scope.image_preview = $scope.event.image;
                        }
                        
                        if(res.status == 0){
                            $scope.event = [];   
                        }
                    });

                    $scope.userUpdateEvent = function () {
                        
                        var start_date = $('input[name=start_date]').val();

                        if(!start_date){
                            alert("Start date is required");
                            return false;
                        }
                        
                        var end_date = $('input[name=end_date]').val();
                        
                        if(!end_date){
                            alert("End date is required");
                            return false;
                        }

                        $scope.event['start_date'] = start_date;
                        $scope.event['end_date'] = end_date;

                        $rootScope.isProcessing = true;
                        
                        PrimoService.UpdateEvents($scope.event,$scope.event.id).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Event has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }
            });
        }

        /**
         * Events Book
         * 
         */
        $scope.userDeleteEvents = function(event_id){


            PrimoService.DeleteEvents(event_id).then(function(response){
                    $rootScope.isProcessing = false;
                    
                    if(response.status == 200){
                        if(response.data.status == 1){
                            initController();
                            alert("Event has been deleted.");
                        }

                        if(response.data.status == 0){
                            alert("You don't have permission to delete this event.");
                        }
                    }
                        
                });
        }

        /**
         * Add Classified
         * 
         */

        $scope.userAddClassifiedPopup = function(){

            $uibModal.open({
            templateUrl: 'partials/add.classified.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log) {
                    
                    $scope.userAddClassified = function () {
                        
                        $rootScope.isProcessing = true;

                        PrimoService.CreateClassified($scope.classified).then(function(response){
                            $rootScope.isProcessing = false;

                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Classified has been added");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }
            });
        }

        /**
         * update Classified
         * 
         */
        $scope.userUpdateClassifiedPopup = function(classified_id){

            $uibModal.open({
            templateUrl: 'partials/update.classified.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log) {

                    PrimoService.GetSingleModuleById('classifieds',classified_id).then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.classified = res.data.result;
                            $scope.image_preview = $scope.book.image;
                        }
                        
                        if(res.status == 0){
                            $scope.classified = [];   
                        }
                    });

                    $scope.userUpdateClassified = function () {
                        
                        $rootScope.isProcessing = true;
                        
                        PrimoService.UpdateClassified($scope.classified,$scope.classified.id).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Classified has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }
            });
        }

        /**
         * Classified Book
         * 
         */
        $scope.userDeleteClassified = function(classified_id){


            PrimoService.DeleteClassified(classified_id).then(function(response){
                    $rootScope.isProcessing = false;
                    
                    if(response.status == 200){
                        if(response.data.status == 1){
                            initController();
                            alert("Classified has been deleted.");
                        }

                        if(response.data.status == 0){
                            alert("You don't have permission to delete this classified.");
                        }
                    }
                        
                });
        }

        /**
         * Add Jobs
         * 
         */

        $scope.userAddJobsPopup = function(){

            $uibModal.open({
            templateUrl: 'partials/add.jobs.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log) {
                    
                    $scope.userAddJob = function () {
                        
                        $rootScope.isProcessing = true;

                        PrimoService.CreateJobs($scope.job).then(function(response){
                            $rootScope.isProcessing = false;

                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Job has been added");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }
            });
        }

        /**
         * update Jobs
         * 
         */
        $scope.userUpdateJobsPopup = function(job_id){

            $uibModal.open({
            templateUrl: 'partials/update.jobs.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log) {

                    PrimoService.GetSingleModuleById('jobs',job_id).then(function (response){
                        var res = response;
                        if(res.data.status == 1){
                            $scope.job = res.data.result;
                            $scope.image_preview = $scope.job.image;
                        }
                        
                        if(res.status == 0){
                            $scope.job = [];   
                        }
                    });

                    $scope.userUpdateJob = function () {
                        
                        $rootScope.isProcessing = true;
                        
                        PrimoService.UpdateJobs($scope.job,$scope.job.id).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Job has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }
            });
        }

        /**
         * Jobs Book
         * 
         */
        $scope.userDeleteJobs = function(job_id){


            PrimoService.DeleteJobs(job_id).then(function(response){
                    $rootScope.isProcessing = false;
                    
                    if(response.status == 200){
                        if(response.data.status == 1){
                            initController();
                            alert("Job has been deleted.");
                        }

                        if(response.data.status == 0){
                            alert("You don't have permission to delete this job.");
                        }
                    }
                        
                });
        }

        /**
         * Add Events Host
         */
        $scope.AddEventHostPopup = function(){

            $uibModal.open({
            templateUrl: 'partials/add.host.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log) {

                    $scope.AddEventHost = function () {
                        
                        $rootScope.isProcessing = true;
                        
                        PrimoService.AddEventHost($scope.host).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Host has been added");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }
            });
        }

        /**
         * Invite Friends Module
         */

        $scope.inviteFriends = function() {
          UiService.showConfirmDialog('AccountInviteFriendsController', 'partials/account-invite-friends.html', function(clb) {
            if (clb) {
              PrimoService.inviteFriends({invited_emails: clb}).then(function(response) {
                $scope.$root.alert = {
                  show: true,
                  context: {type: 'success', msg: $filter('translate')('INVITE_SUCCESSFULLY SENT')}
                }
              });
            }
          })
        }

        /**
         * Update Profile
         */

        $scope.updateProfilePop = function() {
          $uibModal.open({
            templateUrl: 'partials/edit.profile.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log) {

                    $scope.user.birthday = new Date($scope.user.birthday);

                    $scope.updateProfile = function () {
                        
                        $rootScope.isProcessing = true;
                        
                        PrimoService.updateInfo({birthday:$scope.user.birthday,
                                                first_name:$scope.user.first_name,
                                                last_name:$scope.user.last_name,
                                                gender:$scope.user.gender
                                                }).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Profile has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }
            });
        }

        $scope.changeProfilePictureClickedEvent = function(){
            $uibModal.open({
            templateUrl: 'partials/edit.profile.image.html',
            backdrop: true,
            scope:$scope,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log) {

                    
                    $scope.changeProfilePicture = function () {
                        console.log($scope.image);
                        data = $scope.image;
                        $rootScope.isProcessing = true;
                        
                        PrimoService.updateProfileImage(data).then(function(response){
                            $rootScope.isProcessing = false;
                            
                            if(response.status == 200){
                                if(response.data.status == 1){
                                    initController();
                                    $uibModalInstance.dismiss('cancel');
                                    alert("Profile has been updated");
                                    
                                }

                                if(response.data.status == 0){
                                    angular.forEach(response.data.message,function(key,value){
                                        $scope.errors.push(key);
                                    });
                                }
                            }
                                
                        });
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel'); 
                    };
                }
            });
        }
});

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
}]);
