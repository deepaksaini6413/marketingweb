app.controller("ArticleController", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, config,ngToast, $sce) {

  
    $scope.config = config;
    $scope.is_disable = false;
    
    function initController() {
        
        if($stateParams){
            if($stateParams.campuslife != undefined && $stateParams.campuslife){
                fetchArticle($stateParams.campuslife);
            }

            if($stateParams.campusvisit != undefined && $stateParams.campusvisit){
                fetchCollegesCampusVisitArticle();
            }
        }

    }

    function fetchArticle(slug){
        
        $rootScope.isProcessing=true;

        if(slug == 'campus-visit'){
            slug = 'campus-visit-101';
        }

        PrimoService.getArticle(slug).then(function (response){
            var res = response;
            $rootScope.isProcessing=false;
            if(res.status == 200){
              if(res.data.status == 1){
                if(res.data.article){
                  $scope.data = res.data.article
                }else{
                  ngToast.create("Page not found");
                  $state.go('/');
                }
              }
            }

            if(res.status == 0){
                ngToast.creat("Something went wrong");
            }

        });
    }

    $scope.deliberatelyTrustDangerousSnippet = function(data) {
         return $sce.trustAsHtml(data);
    };

    initController();

});