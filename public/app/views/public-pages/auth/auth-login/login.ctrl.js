app.controller("LoginController", function($scope, PrimoService, $rootScope, $state,$stateParams,
                                           $q, $cookies,
                                           SessionService, $mdDialog, AuthService, ngToast) {


  $scope.user       = {};
  $scope.isRemember = false;

  var localHashKey = localStorage.getItem('hashkey');
  if($stateParams.hashkey)
  {
    if(localHashKey == $stateParams.hashkey){
      localStorage.removeItem('hashkey');
      $scope.registrationSuccessfull = "Registration successfully done, Please login here";
    }
  }
  //fetch remembered user from cookie if exists
  if ($cookies.getObject('username')) {
    $scope.user.username = $cookies.getObject('username');
    $scope.isRemember    = true;
  }

  /**
   * Call primo to check username
   */
  $scope.verifyUsername = function(user) {
    $scope.errorMsg = false;
    $scope.invalidMsg = false;

    if(user.email && user.password){
        $rootScope.isProcessing = true;
        AuthService.doLogin($scope.user).then(function(data){
                $rootScope.isProcessing = false;
                if(data.status == 0){
                    $scope.errorMsg = true;
                }

                if(data.status == 1){
                  localStorage.setItem("token",data.token);
                  localStorage.setItem('user',JSON.stringify(data.data));
                  localStorage.setItem('userId',data.data.name);
                  $scope.errorMsg = false;
                  var state = $rootScope.lastState || "profile";
                  $state.go(state, $rootScope.stateParams);
                }

        })
        .catch(function(err){
            $rootScope.isProcessing = false;
            $scope.invalidMsg = true;
        });
    }
  };

  /**
   * Call Primo service login and get account info. Store to localStorage
   */
  $scope.login = function() {
    AuthService.doLogin($scope.user, {
      epay: function(data) {
        $state.go('register', {claim: data.alias, token: data.login_token});
      },
      normal: function() {
        var state = $rootScope.lastState || "profile";
        $state.go(state, $rootScope.stateParams);
        $mdDialog.cancel();
      }
    })
  };

  $scope.cancelDialog = function() {
    $mdDialog.cancel();
  };

  $scope.register = function() {
    AuthService.register();
  };

  $scope.forgotPassword = function() {
    AuthService.forgotPassword();
  };

  $scope.forgotUsername = function() {
    AuthService.forgotUsername();
  };

  $rootScope.$on('$stateChangeStart', function() {
    $mdDialog.cancel();
  });

  $scope.setSelectedTab = function(index) {
    $scope.selectedTab = index;

    if (index == 1) {
      //focus password
      setTimeout(function() {
        $('#inputPassword').focus();
      }, 200);
    }
  };


  /**
   * Add username to cookie
   */
  $scope.rememberMe = function() {
    $cookies.putObject('username', $scope.user.username);
  };

  /**
   * Remove username from cookie
   */
  $scope.forgetUser = function() {
    $cookies.remove('username');
  };

});