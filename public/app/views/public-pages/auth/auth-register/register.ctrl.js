app.controller("RegisterController", function ($scope, PrimoService, $rootScope, SessionService,
                                               $stateParams, $q, $sce,
                                               $state, $timeout, $filter, $mdDialog, AuthService, ngToast) {
        $scope.selected = undefined;
        
        $scope.education_levels = [];
        $scope.states     = [];
        $scope.colleges      = [];

        var vm = this;
        $scope.state = "";
        $scope.primary_school = "";
        $scope.step1 = true;
        $scope.step2 = false;
        vm.ForgetPasswordRequest = ForgetPasswordRequest;
        vm.ResetPasswordRequest = ResetPasswordRequest;

        initController();
        
        function initController() {

            //AuthenticationService.ClearCredentials();
            $rootScope.isUserLoggedIn = false;

            PrimoService.GetStateEducationlevelCollege()
                .then(function (response) {
                    
                    if (response.status == 200 ) {

                        angular.forEach(response.data.education_levels, function(value, key) {
                          this.push(value);
                        }, $scope.education_levels);

                        angular.forEach(response.data.states, function(value, key) {
                          this.push(value);
                        }, $scope.states);

                        angular.forEach(response.data.colleges, function(value, key) {
                            this.push(value);
                        }, $scope.colleges);

                        console.log($scope.colleges);
                        
                    } else {
                        $scope.primarySchool = ['High School', 'Intermediate', 'Bachelor','Master Degree'];
                        $scope.stateList = ['California', 'Florida', 'texas','Hawaii'];
                        $scope.colleges = ['you can add new college'];
                    }
                });
            
        }

        $scope.goToStep2 = function() {
            $scope.step1 = false;
            $scope.step2 = true;
        }

        $scope.goToStep1 = function() {
            $scope.step1 = true;
            $scope.step2 = false;
        }

        $scope.register = function(user) {

            $rootScope.isProcessing = true;
            $scope.errors = [];
            $scope.validation = {};
            
            if(user.state.title == '' || user.state.title == undefined)
            {
                $scope.validation['state'] = "State is required*";
            }

            if(user.education_level.title == '' || user.education_level.title == undefined)
            {
                $scope.validation['education_level'] = "Education Level is required*";
            }

            if(user.college.title == '' || user.college.title == undefined)
            {
                $scope.validation['college'] = "College is required*";
            }

            if(user.gender == '' || user.gender == undefined)
            {
                $scope.validation['gender'] = "Gender is required*";
            }

            if(!$.isEmptyObject($scope.validation))
            {
                $rootScope.isProcessing = false;
                angular.forEach($scope.validation, function(key,value){
                    $scope.errors.push(key);
                });
                return false;
            }

            var data = {};
            data = {
                      name:user.name,
                      email:user.email,
                      first_name:user.first_name,
                      last_name:user.last_name,
                      gender:user.gender,
                      password:user.password,
                      state:user.state.title,
                      education_level:user.education_level.title,
                      college:user.college.title
                    };

            PrimoService.CreateRegistration(data)
                .then(function (response) {
                    $rootScope.isProcessing = false;
                    
                    if (response.data.status == 1) {
                        var haskey = randomHashKey();
                        localStorage.setItem('hashkey',haskey);
                        debugger;
                        $state.go('login',{hashkey:haskey});
                    } else if(response.data.status == 0){

                        angular.forEach(response.data.message,function(key,value){
                            $scope.errors.push(key);
                        });
                        
                    } else {
                        $scope.errors = "Sorry, There is a problem in registration process please contact to admin";
                    }
                });
        }

        function ForgetPasswordRequest () {
            
            PrimoService.ForgetPasswordRequest(vm.user)
                .then(function (response){
                    console.log(response);
                })
        }

        function ResetPasswordRequest () {
            
            PrimoService.ResetPasswordRequest(vm.user)
                .then(function (response){
                    console.log(response);
                })
        }

        function randomHashKey() {
          var text = "";
          var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

          for (var i = 0; i < 32; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

          return text;
        }

        $scope.withoutSanitize = function (message) {
                return $sce.trustAsHtml(message);
        };
    
  // $scope.claimNumber    = $stateParams.claim;
  // $scope.isClaimAccount = $stateParams.claim && $stateParams.claim != "";
  // $scope.token          = $stateParams.token;

  // $scope.resolve = function () {
  //   if ($scope.isClaimAccount) {
  //     $scope.claimAccount();
  //   } else {
  //     $scope.register();
  //   }
  // };

  // $scope.register = function () {
  //   $scope.user['tos_accepted']  = ($scope.isAgreed) ? 1 : 0;
  //   $scope.user['signup_source'] = "WEB";

  //   AuthService.doRegister($scope.user, {
  //     withMSISDN: function () {
  //       $rootScope.alert = {
  //         show: true,
  //         context: {type: 'success', msg: $filter('translate')('SUCCESS_REGISTER')}
  //       };
  //       $timeout(function () {
  //         AuthService.verifyMSISDN();
  //       }, 2000);
  //     },
  //     withoutMSISDN: function () {
  //       $state.go('profile');
  //     }
  //   });
  // };

  // $scope.claimAccount = function () {
  //   AuthService.doClaimAccount($scope.claimNumber, $scope.token, $scope.user, function () {
  //     $state.go('profile');
  //   });
  // };

  // $scope.cancelDialog = function () {
  //   $mdDialog.cancel()
  // };

  // $scope.login = function () {
  //   AuthService.login();
  // };

  // $rootScope.$on('$stateChangeStart', function () {
  //   $mdDialog.cancel();
  // });


});