/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller("VerifyMSISDNController", function ($scope, PrimoService, $rootScope, $state, $stateParams, $timeout, $filter, $mdDialog) {

  $scope.user          = {};
  $scope.user.username = $stateParams.username || $rootScope.user.username;
  $scope.user.msisdn   = $stateParams.msisdn || $rootScope.user.msisdn;
  $scope.user.country  = $stateParams.country || $rootScope.user.country;
  $scope.user.pin      = "";

  /**
   * Call primo api to verify msisdn
   */
  $scope.verify = function () {
    PrimoService.verifyMSISDN($scope.user).success(function (data, status, headers) {
      $scope.$root.alert = {
        show: true,
        context: {type: 'success', msg: $filter('translate')('SUCCESS_VERIFIED')}
      };
      $timeout(function () {
        $mdDialog.cancel();
      }, 2000);
    });
  };

  $scope.cancelDialog = function () {
    $mdDialog.cancel()
  };

  $rootScope.$on('$stateChangeStart', function () {
    $mdDialog.cancel();
  });

});