app.controller("ForgotPasswordController", function($scope, PrimoService, $state, $window,
                                                    $timeout, $filter, $rootScope, $mdDialog, AuthService) {

  $scope.buttonText = "Send Password Reset Link";
  $scope.forgotPassword = function(user) {
    $scope.success = "";
    $scope.error = "";
    $rootScope.isProcessing = true;
    PrimoService.ForgetPasswordRequest({email: user.email}).success(function(response) {
      $rootScope.isProcessing = false;
      if(response.status == 1){

        $scope.success = response.message;
        $scope.buttonText = "Password Reset Link Sent";
      }

      if(response.status == 0){
        $scope.error = response.message;
      }
    });
  };

});