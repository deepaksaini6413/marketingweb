/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller("ForgotUsernameController", function($scope,
                                                    $state,
                                                    PrimoService,
                                                    $rootScope, $filter, $mdDialog, AuthService, _, CountryService, ngToast) {

  //initial values
  $scope.user = {
    email: "",
    msisdn: ""
  };

  /**
   * Call Primo API to retrieve forgotten username
   * You can retrieve by email or msisdn
   */
  $scope.forgotUsername = function() {
    var params = {};
    if ($scope.user.email) {
      params['email'] = $scope.user.email;
    } else if ($scope.user.msisdn) {
      params['msisdn'] = $scope.user.msisdn;
    }

    if (params['email'] || params['msisdn']) {
      PrimoService.recoverUsername(params).success(function(data) {
        if (params['email']) {
          $rootScope.alert = {
            show: true,
            context: {type: 'success', msg: $filter('translate')("RECOVER_USERNAME_EMAIL_SENT")}
          };
        } else {
          $rootScope.alert = {
            show: true,
            context: {type: 'success', msg: $filter('translate')("RECOVER_USERNAME_SMS_SENT")}
          };
        }

      });
    }

  };

  $scope.cancelDialog = function() {
    $mdDialog.cancel();
  };

  $scope.forgotPassword = function() {
    AuthService.forgotPassword();
  };

  $rootScope.$on('$stateChangeStart', function() {
    $mdDialog.cancel();
  });

  $scope.$watch('user', function(newValue) {
    $scope.isDisabledMsisdn = !!newValue.email;
    console.log('$scope.isDisabledMsisdn', $scope.isDisabledMsisdn)
    $scope.isDisabledEmail = !!newValue.msisdn;
    console.log('$scope.isDisabledEmail', $scope.isDisabledEmail)
  }, true)

});