app.controller("BookController", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService,$cookies,config,ngToast) {

        $scope.config = config;  
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $rootScope.isProcessing=false;
        $scope.isDataFound=false;
        $scope.loadingImage='';
        $scope.filteredTodos = [];
        $scope.currentPage = 1;
        $scope.numPerPage = 10;
        $scope.maxSize = 5;
        $scope.search = [];
        initController();

        function initController() {
              
            $scope.loadingImage=$rootScope.loadingImage;
            $rootScope.isUserLoggedIn=true;
            $scope.changeTextBook ="Send A Message";
            var slug = $stateParams.slug;

            if(slug == undefined){
                loadGetAllTextBooks();
            }

            if($stateParams.title){
                $scope.title = $stateParams.title;
                $scope.search.title = $stateParams.title;
                searchBookByTitle();
            }

            if(slug){
                LoadSingleTextBook(slug);
            }
        }

        $scope.sendAMessage = function(item){
            $rootScope.isProcessing = true;
            var bookid = 1;
            PrimoService.sendAMessage(bookid).then(function(res){
                $rootScope.isProcessing = true;
                if(res.status==200){
                    //ngToast.create('Message sent successfully');
                    $scope.changeTextBook="Message sent";
                }
            });
        };

        $scope.getDataOnSubcat = function(catid,catname){
            $scope.selectedCtaName=catname;
            $rootScope.isProcessing=true;
            $scope.items=[];
            PrimoService.GetBooks(catid).then(function (response){
                var res = response;
                $rootScope.isProcessing=true;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                    $scope.items = res.data.Data;
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        //ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    //ngToast.create("Something went wrong");
                }
            });
        }


        $scope.seeBookProfile = function(book_id){
            if(book_id){
                //window.location.href='#!/textBooks/'+book_id;
                $location.path('textBooks/'+book_id);
            }
            else{
              //ngToast.create('Sorry No data found.');
            }
            
        }
         function cateAndSubcatList(){
             $rootScope.isProcessing=true;
            PrimoService.cateAndSubcatList().then(function (response){
                var res = response;
                $scope.itemList=[];
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                      $scope.itemList = res.data.Data;
                      $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                    }

                }
                if(res.status != 200){
                    //ngToast.create('Something went wrong');
                }
            });
         }

        $scope.searchTextBookById =function(category_id){
            $rootScope.isProcessing=true;
            PrimoService.searchTextBookById(category_id).then(function (response){
                var res = response;
                $scope.items=[];
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                      $scope.items = res.data.Data;
                      $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                    }

                }
                
                if(res.status != 200){
                    //ngToast.create('Something went wrong');
                }
            });
        }

        function customeBroughtTextBooks(){
            $rootScope.isProcessing=true;
            PrimoService.customeBroughtTextBooks($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length){
                      $scope.boughtitems = res.data.Data;
                      $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                    }

                }
                
                if(res.status != 200){
                    //ngToast.create('Something went wrong');
                }
            });
        }

        function loadGetAllTextBooks()
        {
            $rootScope.isProcessing=true;
            PrimoService.GetBooks($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    // var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    // , end = begin + $scope.numPerPage;
                    // $scope.items = $scope.items.slice(begin, end);
        
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        //ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    //ngToast.create("Something went wrong");
                }
            });
        }

        function LoadSingleTextBook(slug){
            $rootScope.isProcessing=true;
            PrimoService.GetBook(slug).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result){
                        $scope.isDataFound=true;
                        $scope.item = res.data.result;
                    }
                    else{
                        $scope.isDataFound=false;
                        //ngToast.create('No data found');
                    }
                    
                }
                if(res.status != 200){
                    //ngToast.create('Something went wrong');
                }
            });
        }

        $scope.loadMore = function() 
        {
             console.log('loadMore');
                $scope.lastpage +=1;
                $rootScope.isProcessing=true;
                PrimoService.loadMoreTextBooks($scope.lastpage).then(function (response){
                    var res = response;
                    $rootScope.isProcessing=false;
                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.isDataFound=true;
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                        $scope.totalTextBook=$scope.items.length;
                    }
                    
                    if(res.status == 0){
                        //ngToast.create("Something went wrong");
                    }
                });
            }

        function  searchTextBookData(searchText){
            var slug = $stateParams.slug;
            $scope.searchText=slug;
           PrimoService.searchTextBook($scope.searchText).then(function(response){
                    if(response.status==200){
                        $rootScope.isProcessing=false;
                        if(response.data){
                          $scope.isDataFound=true;
                          $scope.searchTextBook=response.data.Data;
                          $scope.totalTextBook=$scope.searchTextBook.length;
                        }
                        else{
                            $scope.isDataFound=false;
                            $scope.totalCollege=0;
                            $scope.searchText='';
                        }
                    }
                    else{
                        $scope.totalCollege=0;
                        $scope.searchText='';
                        $rootScope.isProcessing=false;
                        //ngToast.create('cant load data from server');
                    }
                });
        }

         $scope.searchCollege = function(searchText){
            if($scope.searchText){
            $rootScope.isProcessing=true;
                PrimoService.searchTextBook($scope.searchText).then(function(response){
                    if(response.status==200){
                        //$rootScope.isProcessing=false;
                        if(response.data){
                          $scope.isDataFound=true;
                          $scope.searchTextBook=response.data.Data;
                          $scope.totalTextBook=$scope.searchTextBook.length;
                          if($scope.totalTextBook){
                            //window.location.href='#!/textBooks/search/'+$scope.searchText;
                            $location.path('textBooks/search/'+$scope.searchText);
                          }
                          $scope.searchText='';
                        }
                        else{
                            $scope.isDataFound=false;
                            $scope.totalCollege=0;
                            $scope.searchText='';
                        }
                    }
                    else{
                        $scope.totalCollege=0;
                        $scope.searchText='';
                        $rootScope.isProcessing=false;
                        //ngToast.create('cant load data from server');
                    }
                });
            }
            else{
                //ngToast.dismiss();
                //ngToast.create('Please Enter The Search Text');
            }
        }

        function searchBookByTitle()
        {
            $rootScope.isProcessing=true;
            PrimoService.GetBooksByTitle($scope.lastpage,$stateParams.title).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        //ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    //ngToast.create("Something went wrong");
                }
            });
        }

        $scope.searchBookByTitleLoadMore = function() 
        {
            $scope.lastpage +=1;
            $rootScope.isProcessing=true;
            PrimoService.GetBooksByTitle($scope.lastpage,$stateParams.title).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.isDataFound=true;
                        $scope.is_disable = true;
                    }

                    $scope.items = $scope.items.concat(res.data.result.data);
                    $scope.totalTextBook=$scope.items.length;
                }
                
                if(res.status == 0){
                    //ngToast.create("Something went wrong");
                }
            });
        }

        $scope.bookSearchByTitle = function(search){
          $lastpage = 1;
          if(search){
            $state.go('bookSearch',{title:search.title});
          }else{
            alert("Please fill the textbox first");
          }
          
        }

});