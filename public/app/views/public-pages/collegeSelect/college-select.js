
app.controller('CollegeSelectController', function ($rootScope, $scope, $mdDialog, $filter,selectColleges) {

  $scope.answer = function (answer) {
    if (answer) {
      $mdDialog.hide($scope.inviteEmails);
    } else {
      $mdDialog.hide(false);
    }
  };

  var fetchingRecords = false;

    $scope.getCountries = function (searchKey, pagenumber) {

        if (fetchingRecords) return;
        fetchingRecords = true;

        selectColleges.getCountries(searchKey, pagenumber)
                    .then(function (result) {
                        if (pagenumber === 1) {
                            $scope.totalRecords = result.TotalRecords;
                            $scope.countries = result.Records;
                        }
                        else {
                            $scope.countries = $scope.countries.concat(result.Records);
                        }
                        fetchingRecords = false;
                    },
                    
                    function (errorMessage) {
                        window.console.warn(errorMessage);
                        fetchingRecords = false;
                    });
    };

    $scope.countries = [];

    $scope.country = {
        Key: "Alabama A & M University",
        Value: "1"
    };

    $scope.getCountries("", 1);

});