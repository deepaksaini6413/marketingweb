
app.controller("CollegeController", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, $cookies, config) {
        
        $scope.config = config;
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $scope.sortAplhaVar = "true";
        $scope.leftCategoryList=[];
        $scope.sortAplhabates=false;
        $scope.collegeData=[];
        $scope.totalCollege=0;
        $rootScope.isProcessing=false;
        $scope.isDataFound=false;
        $scope.loadingImage='';
        $scope.currentPage = 1;
        $scope.numPerPage = 2;
        $scope.maxSize = 5;
        $scope.newItems=[];
        $scope.counter=0;

        initController();

        vm.pager = {};
        
        function initController() {

            //If serach College url hit

            if($stateParams.title){
                collegeSearchPage();
            }

            $scope.selectedCollegeId=window.localStorage.getItem('selectedCollegeOrUniversity');
            var slug = $stateParams.slug;
            if(slug == undefined){
                //loadColleges();
                loadTopScholarship();
            }else{
                LoadSinglePage(slug);
                loadNewsData();
                loadGetAllTextBooks();
                loadTopScholarship();
            }
            //call for getting left side list
            
            leftSideCollegeNavigation();

            //bestCollegesData();
            loadTopScholarship();
        }
        
        function showModal() {
          getLocation();
        }

        function getLocation() {
        if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition,showError);
        } else {
        ngToast.create("Geolocation is not supported by this browser.");
        }
        }

        function loadNewsData() {
            $rootScope.isProcessing=true;
            PrimoService.getNews().then(function(response) {
                var data = response.data;
                $rootScope.isProcessing=false;
                if (data.status == 1) {
                    $scope.isDataFound = true;
                    $scope.breakingNews = data.featured_news;
                    $scope.newsCategories = data.categories;
                    $scope.currentNews = data.news.data;
                    $scope.nextUrl = data.news.next_page_url;
                }
            });    
        }

        //for recent textbook added here:
        function loadGetAllTextBooks()
        {
            $rootScope.isProcessing=true;
            PrimoService.GetBooks($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    // var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    // , end = begin + $scope.numPerPage;
                    // $scope.items = $scope.items.slice(begin, end);
        
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    ngToast.create("Something went wrong");
                }
            });
        }

        // for top scholarship 
        function loadTopScholarship(){
            $rootScope.isProcessing=true;
            PrimoService.loadTopScholarship().then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    else{
                     $scope.is_disable = false;   
                    }
                    $scope.items = res.data.result;
                    $scope.states = res.data.states;
                }
                
                if(res.status == 0){
                    //$scope.isDataFound=false;
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
                
            });
        }

        function showPosition(position) {
          ngToast.dismiss();
          $scope.lat=position.coords.latitude;
          $scope.lng=position.coords.longitude;
          var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    var geocoder = geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                $scope.currentAddress=results[1].formatted_address;
                                $('#myModal').modal();
                                $scope.$apply();
                                //ngToast.create("Location: " + results[1].formatted_address);
                            }
                        }
                        else{
                            ngToast.create("Location:Not Found");
                        }
                    });
        }

        function showError(error) {
        switch(error.code) {
        case error.PERMISSION_DENIED:
          x.innerHTML = "User denied the request for Geolocation."
          break;
        case error.POSITION_UNAVAILABLE:
          x.innerHTML = "Location information is unavailable."
          break;
        case error.TIMEOUT:
          x.innerHTML = "The request to get user location timed out."
          break;
        case error.UNKNOWN_ERROR:
          x.innerHTML = "An unknown error occurred."
          break;
        }
        }


        $scope.searchSearchLocation = function(){
            $rootScope.isProcessing=true;
            var data={
                "lat":$scope.lat,
                "lng":$scope.lng,
                "cureentAddress":$scope.currentAddress
            };

            PrimoService.searchCollegeByLocation(data).then(function (response){
                if(response.status==200){
                    $rootScope.isProcessing=false;
                    if(response.data){
                      $scope.collegeData=response.data.Data;
                      $scope.totalCollege=$scope.collegeData.length;
                    }
                    else{
                        ngToast.dismiss();
                        ngToast.create('Data Not found!.');    
                    }
                }
                else{
                    $rootScope.isProcessing=false;  
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
            });
        }

        $scope.onKeySearch =function(text){
            if(text){}
            else{
                loadColleges();
            }    
        }

        $scope.seeCollegeByCategory = function(catId){
            showModal();
            //$rootScope.isProcessing=true;
            // PrimoService.seeCollegeByCategory(catId).then(function (response){
            //     if(response.status==200){
            //         $rootScope.isProcessing=false;
            //         if(response.data){
            //           $scope.collegeData=response.data.Data;
            //           $scope.totalCollege=$scope.collegeData.length;
            //         }
            //         else{
            //             ngToast.dismiss();
            //             ngToast.create('Data Not found!.');    
            //         }
            //     }
            //     else{
            //         $rootScope.isProcessing=false;
            //         ngToast.dismiss();
            //         ngToast.create('cant load data from server');
            //     }
            // });
        }

        function leftSideCollegeNavigation(){
            $rootScope.isProcessing=true;
            PrimoService.leftNavigationColleges().then(function (response){
                if(response.status==200){
                    $rootScope.isProcessing=false;
                    if(response.data){
                      $scope.leftCategoryList=response.data.Data;
                    }
                }
                else{
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
            });
        }

        function loadColleges()
        {
            $scope.counter++;
            $rootScope.isProcessing=true;
            $scope.items=[];
            $scope.newItems=[];
            PrimoService.Colleges($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    else{
                     $scope.is_disable = false;   
                    }
                    $scope.items = res.data.result;
                    $scope.totalCollege=res.data.total;
                    $scope.states = res.data.states;
                    setrating($scope.items);
                    $scope.isDataFound = true;
                    console.log($scope.items);
                }
                
                if(res.status == 0){
                    //$scope.isDataFound=false;
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
        }

        function setrating(data){
            $scope.newItems=[];
            for(var i=0;i<data.length;i++){
                $scope.newItems.push(
                {
                  "Id": $scope.items[i].id,
                  "title": $scope.items[i].title,
                  "college_rank": $scope.items[i].college_rank,
                  "college_image": $scope.items[i].college_image,
                  "address":$scope.items[i].address,
                  "time_period":$scope.items[i].time_period,
                  "no_of_students":$scope.items[i].no_of_students,
                  "net_price":$scope.items[i].net_price,
                  "rating":$scope.items[i].rating,
                  "votes":$scope.items[i].votes,
                  "reviews":$scope.items[i].reviews,
                  "slug":$scope.items[i].slug,
                  "className": ($scope.items[i].rating < 2 ? "bg_red" : (($scope.items[i].rating >= 2 && $scope.items[i].rating <= 3.5) ? "bg_yellow" : "bg_green"))
                });
            }

            $rootScope.saveSearchdata=$scope.newItems;
            if($scope.newItems.length > 0){
                //$scope.newItems=$scope.newItems.slice(0,5);
                //$scope.totalCollege=$scope.newItems.length;
            }else{

            }
        }

        function LoadSinglePage(slug){
            $rootScope.isProcessing=true;
            PrimoService.College(slug).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    $scope.item = res.data.result;
                    $scope.item.className = ($scope.item.rating < 2 ? "bg_red" : (($scope.item.rating >= 2 && $scope.item.rating <= 3.5) ? "bg_yellow" : "bg_green"));
                }
                console.log(res);    
            });
        }

        function loadTopScholarship(){
            $rootScope.isProcessing=true;
            PrimoService.loadTopScholarship().then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length)
                    {
                        $scope.isDataFound=true;
                        $scope.topScholarship = res.data.Data;
                    }
                    else{
                        $scope.isDataFound=false;
                    }
                    
                }
                
                if(res.status == 0){
                    ngToast.creat("Something went wrong");
                }
            });
        }

        $scope.loadMore = function() 
        {
             console.log('loadMore');
                $scope.lastpage +=1;
                $rootScope.isProcessing=true;
                PrimoService.Colleges($scope.lastpage).then(function (response){
                    var res = response;
                    $rootScope.isProcessing=false;
                    if(res.data.status == 1){
                        if(res.data.current_page == res.data.last_page)
                        {
                            $scope.is_disable = true;
                        }
                        $scope.totalCollege=res.data.to;
                        $scope.newItems = $scope.newItems.concat(res.data.result);
                    }
                    if(res.status == 0){
                        ngToast.dismiss();
                        ngToast.create("Something went wrong");
                    }
                });
             // $scope.newItems=$rootScope.saveSearchdata;
             // $scope.totalCollege=$scope.newItems.length;
             //$scope.newItems=$rootScope.saveSearchdata.slice(5,$rootScope.saveSearchdata.length -1);
        }

        $scope.addToWishList = function(item_id)
        {
            if(!$rootScope.isUserLoggedIn)
            {
                ngToast.create("Please login to add this school into your wishlist");
            }

            if($rootScope.isUserLoggedIn)
            {
                PrimoService.AddToWishList(item_id).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        // To Do task    
                    }
                    
                    if(res.status == 0){
                        ngToast.dismiss();
                        ngToast.create("Something went wrong");
                    }
                });
            }
        }
        $scope.sortAplha = function()
        {
            $scope.newItems=[];
            $scope.sortAplhabates=true;
            $scope.sortType = 'title';
            $rootScope.isProcessing=true;
            PrimoService.sortCollegeByAlphabates($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=true;
                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.items = res.data.result;
                    $scope.totalCollege=res.data.result.length;
                    setrating($scope.items);
                    $scope.isDataFound = true;
                    console.log($scope.items);
                }
                else{
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
                if(res.status == 0){
                    //$scope.isDataFound=false;
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
        }
        $scope.bestCollege= function (){
            //bestCollegesData();
            loadColleges();
        }

         function bestCollegesData(){
            $scope.collegeData=[];
            $scope.sortAplhabates=true;
            $rootScope.isProcessing=true;
            PrimoService.beseColleges().then(function(response){
                if(response.status==200){
                    $rootScope.isProcessing=false;
                    if(response.data){
                      $scope.collegeData=response.data.Data;
                      //$scope.totalCollege=$scope.collegeData.length;
                    }
                }
                else{
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create('cant load data from server');
                }
            });
        }

        $scope.searchCollege = function(){

            if($scope.searchText){
                $rootScope.isProcessing=true;
                $scope.items=[];
                $scope.newItems=[];
                PrimoService.Colleges($scope.lastpage).then(function (response){
                    var res = response;
                    $rootScope.isProcessing=false;
                    if(res.data.status == 1){
                        if(res.data.current_page == res.data.last_page)
                        {
                            $scope.is_disable = true;
                        }
                        else{
                         $scope.is_disable = false;   
                        }
                        $scope.items = res.data.result;
                        $scope.totalCollege=res.data.result.length;
                        setrating($scope.items);
                        $scope.isDataFound = true;
                        console.log($scope.items);
                    }
                    
                    if(res.status == 0){
                        //$scope.isDataFound=false;
                        $rootScope.isProcessing=false;
                        ngToast.dismiss();
                        ngToast.create("Something went wrong");
                    }
                });
            }
            else{
                 ngToast.dismiss();
                 ngToast.create('Please Enter The Search Text');
            }
        }

       $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
        , end = begin + $scope.numPerPage;
        //l$scope.totalCollege=$scope.newItems.length;
        $scope.newItems = $scope.items.slice(begin, end);
    
      });    

       $scope.searchCollege = function(search){
        debugger;
        $state.go('collegeSearch', { 'title':search.title, 'state':search.state });

       }

       $scope.collegeSearchPage = function(){

        $scope.isProcessing = true;
        $scope.counter++;
        $scope.items=[];
        $scope.newItems=[];
        var data = { title: $stateParams.title };
        PrimoService.collegeSearchPage(data).then(function (response){
            var res = response;
            $rootScope.isProcessing=false;
            if(res.data.status == 1){
                if(res.data.current_page == res.data.last_page)
                {
                    $scope.is_disable = true;
                }
                else{
                 $scope.is_disable = false;   
                }
                $scope.items = res.data.result;
                $scope.totalCollege=res.data.total;
                $scope.states = res.data.states;
                setrating($scope.items);
                $scope.isDataFound = true;
                console.log($scope.items);
            }
            
            if(res.status == 0){
                //$scope.isDataFound=false;
                $rootScope.isProcessing=false;
                ngToast.dismiss();
                ngToast.create("Something went wrong");
            }
        });

       }

});

app.filter('extractFirstAndSecondLetter', function() {
    return function(input) {
      var split = input.split(" ");

      var response;
      if(split.length >= 2)
      {
        response = split[0].charAt(0) + split[1].charAt(0);
      }

      if(split.length == 1)
      {
        response = split[0].substring(0,2);
      }
      return response.toUpperCase();
    }
});