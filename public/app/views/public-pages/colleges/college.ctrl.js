app.controller("CollegeController", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, $cookies, config, ngToast) {
        
    
    $scope.config = config;
    $scope.data = {title:'',state:'',country:''};
    $scope.lastpage = 1;
    $scope.sortAplhabates=true;
    $scope.loadMoreOnlyTextButton = false;
    $scope.loadMoreButton = false;
    $scope.newItems=[];

    function initController() {
        
        if($stateParams.slug){
            LoadSinglePage($stateParams.slug);
        }else if ($stateParams.title){
            $scope.loadMoreButton = true;
            $scope.loadMoreOnlyTextButton = false;
            searchCollegesWithTitleStateCountry();
        }else{
            loadTopScholarship();
        }
    }        

    initController();

    function loadTopScholarship(){
        $rootScope.isProcessing=true;
        PrimoService.loadTopScholarship().then(function (response){
            var res = response;
            $rootScope.isProcessing=false;
            if(res.status == 200){
                if(res.data.result.data && res.data.result.data.length)
                {
                    $scope.isDataFound=false;
                    $scope.topScholarship = res.data.result.data;
                    $scope.states = res.data.states;
                    $scope.county_name = res.data.county_name;
                }
                else{
                    $scope.isDataFound=true;
                }
                
            }
            
            if(res.status == 0){
                ngToast.creat("Something went wrong");
            }
        });

    }


    $scope.searchCollegeForm = function(search){

        if(search){

            if(search.title && search.state && search.country){
                $state.go('collegeSearchWithTitleStateCountry',{'title':search.title,'state':search.state,'country':search.country});
            }else if(search.title && search.state){
                $state.go('collegeSearchWithTitleState',{'title':search.title,'state':search.state});
            }else {
                $state.go('collegeSearchWithTitle',{'title':search.title});   
            }

        }else{
            alert("Please fill atleast one field");
        } 
    }
    
    function searchCollegesWithTitleStateCountry(){
        $rootScope.isProcessing = true;
        
        if($stateParams.title){
            $scope.data.title = $stateParams.title;
        }

        if($stateParams.state){
            $scope.data.state = $stateParams.state;
        }

        if($stateParams.country){
            $scope.data.country = $stateParams.country;
        }

        if($stateParams){
            PrimoService.searchCollegesWithTitleStateCountry($scope.data,$scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                $scope.isDataFound=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.totalCollege = res.data.total;
                    $scope.newItems = $scope.newItems.concat(res.data.result);
                }
                if(res.status == 0){
                    $scope.isDataFound=true;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
        }
        
    }

    $scope.loadMore = function() 
        {
            $scope.loadMoreButton = true;
            $scope.loadMoreOnlyTextButton = false;

            if($stateParams.title){
                $scope.data.title = $stateParams.title;
            }

            if($stateParams.state){
                $scope.data.state = $stateParams.state;
            }

            if($stateParams.country){
                $scope.data.country = $stateParams.country;
            }

            $scope.lastpage +=1;
            $rootScope.isProcessing=true;

            PrimoService.searchCollegesWithTitleStateCountry($scope.data,$scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                $scope.isDataFound=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.totalCollege=res.data.to;
                    $scope.newItems = $scope.newItems.concat(res.data.result);
                }
                if(res.status == 0){
                    $scope.isDataFound=true;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
        }

    $scope.searchCollege = function(){
        $scope.loadMoreButton = false;
        $scope.loadMoreOnlyTextButton = true;
        debugger;
        if($scope.searchText){
            $rootScope.isProcessing=true;
            $scope.items=[];
            $scope.newItems=[];
            PrimoService.searchColleges($scope.searchText).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    else{
                     $scope.is_disable = false;   
                    }
                    $scope.items = res.data.result;
                    $scope.totalCollege=res.data.result.length;
                    setrating($scope.items);
                    $scope.isDataFound = false;
                    console.log($scope.items);
                }
                
                if(res.status == 0){
                    $scope.isDataFound = true;
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
        }
        else{
             ngToast.dismiss();
             ngToast.create('Please Enter The Search Text');
        }
    }

    function setrating(data){
            $scope.newItems=[];
            for(var i=0;i<data.length;i++){
                $scope.newItems.push(
                {
                  "Id": $scope.items[i].id,
                  "title": $scope.items[i].title,
                  "college_rank": $scope.items[i].college_rank,
                  "college_image": $scope.items[i].college_image,
                  "address":$scope.items[i].address,
                  "time_period":$scope.items[i].time_period,
                  "no_of_students":$scope.items[i].no_of_students,
                  "net_price":$scope.items[i].net_price,
                  "rating":$scope.items[i].rating,
                  "votes":$scope.items[i].votes,
                  "reviews":$scope.items[i].reviews,
                  "slug":$scope.items[i].slug,
                  "className": ($scope.items[i].rating < 2 ? "bg_red" : (($scope.items[i].rating >= 2 && $scope.items[i].rating <= 3.5) ? "bg_yellow" : "bg_green"))
                });
            }

            $rootScope.saveSearchdata=$scope.newItems;
            if($scope.newItems.length > 0){
                //$scope.newItems=$scope.newItems.slice(0,5);
                //$scope.totalCollege=$scope.newItems.length;
            }else{

            }
        }

    $scope.sortAplha = function()
    {
        $scope.sortAplhabates=true;
        $scope.sortType = 'title';
        $rootScope.isProcessing=true;
        PrimoService.sortCollegeByAlphabates($scope.lastpage).then(function (response){
            var res = response;
            $rootScope.isProcessing=true;
            if(res.data.status == 1){
                if(res.data.result.current_page == res.data.result.last_page)
                {
                    $scope.is_disable = true;
                }
                $scope.items = res.data.result;
                $scope.totalCollege=res.data.result.length;
                setrating($scope.items);
                $scope.isDataFound = false;
                console.log($scope.items);
            }
            else{
                $rootScope.isProcessing=false;
                ngToast.dismiss();
                ngToast.create('cant load data from server');
            }
            if(res.status == 0){
                $scope.isDataFound=true;
                $rootScope.isProcessing=false;
                ngToast.dismiss();
                ngToast.create("Something went wrong");
            }
        });
    }

    $scope.loadMoreOnlyTextButtonFun = function(){
       $scope.loadMoreButton = false;
            $scope.loadMoreOnlyTextButton = true;

            if($stateParams.title){
                $scope.data.title = $stateParams.title;
            }

            // if($stateParams.state){
            //     $scope.data.state = $stateParams.state;
            // }

            // if($stateParams.country){
            //     $scope.data.country = $stateParams.country;
            // }

            if($scope.searchText){
                $scope.data.title = $scope.searchText;
            }

            $scope.lastpage +=1;
            $rootScope.isProcessing=true;

            PrimoService.searchCollegesWithTitleStateCountry($scope.data,$scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                $scope.isDataFound=false;
                if(res.data.status == 1){
                    if(res.data.current_page == res.data.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.totalCollege=res.data.to;
                    $scope.newItems = $scope.newItems.concat(res.data.result);
                }
                if(res.status == 0){
                    $scope.isDataFound=true;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
    }

    function LoadSinglePage(slug){
        $rootScope.isProcessing=true;
        PrimoService.College(slug).then(function (response){
            var res = response;
            $rootScope.isProcessing=false;
            if(res.status == 200){
                $scope.item = res.data.result;
                $scope.item.className = ($scope.item.rating < 2 ? "bg_red" : (($scope.item.rating >= 2 && $scope.item.rating <= 3.5) ? "bg_yellow" : "bg_green"));
            }
            console.log(res);    
        });
    }

});

app.filter('extractFirstAndSecondLetter', function() {
    return function(input) {
      var split = input.split(" ");

      var response;
      if(split.length >= 2)
      {
        response = split[0].charAt(0) + split[1].charAt(0);
      }

      if(split.length == 1)
      {
        response = split[0].substring(0,2);
      }
      return response.toUpperCase();
    }
});