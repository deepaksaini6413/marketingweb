app.controller("EventController", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, $cookies, config, ngToast) {
        
        $scope.config = config;
        
        var vm          = this;
        vm.selected     = 'event';
        $scope.lastpage = 1;
        $scope.searchlastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $scope.sortAplhaVar = "true";
        $scope.leftCategoryList=[];
        $scope.sortAplhabates=false;
        $rootScope.isProcessing=false;
        $scope.isDataFound=false;
        $scope.loadingImage='';
        $scope.currentPage = 1;
        $scope.numPerPage = 2;
        $scope.maxSize = 5;
        $scope.newItems=[];
        $scope.counter=0;
        $scope.search = {};
        $scope.loadMoreButton = true;
        $scope.loadMoreWithSearchingButton = false;
        initController();

        function initController() {
            $scope.loadingImage=$rootScope.loaderImageUrl;
            var slug = $stateParams.slug;
            if(slug == undefined){
                LoadEvents();
            }else{
                LoadSinglePage(slug);
            }
        }

        function LoadEvents(){

            $scope.counter++;
            $rootScope.isProcessing=true;
            $scope.items=[];
            $scope.newItems=[];
            PrimoService.Events($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    else{
                     $scope.is_disable = false;   
                    }
                    $scope.items = res.data.result.data;
                    $scope.totalEvents=res.data.result.total;
                    $scope.isDataFound = true;
                    $scope.states = res.data.states;
                }
                
                if(res.status == 0){
                    //$scope.isDataFound=false;
                    $rootScope.isProcessing=false;
                    ngToast.dismiss();
                    ngToast.create("Something went wrong");
                }
            });
            // PrimoService.Events($scope.lastpage).then(function (response){
            //     var res = response;

            //     if(res.data.status == 1){
            //         if(res.data.result.current_page == res.data.result.last_page)
            //         {
            //             $scope.is_disable = true;
            //         }
            //         $scope.items = res.data.result.data;
            //         console.log($scope.items);
            //     }
                
            //     if(res.status == 0){
            //         alert("Something went wrong");
            //     }
            // });
        }

        function LoadSinglePage(slug){
            
            PrimoService.Event(slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.item = res.data.result;
                }
                console.log(res);    
            });
        }

        $scope.loadMore = function() 
        {
            console.log('loadMore');
            $scope.lastpage +=1;

            PrimoService.Events($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    
                    $scope.items = $scope.items.concat(res.data.result.data);
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });
        }

        $scope.searchEvents = function(search){
            $scope.loadMoreWithSearchingButton = true;
            $scope.loadMoreButton = false;
            $scope.searchlastpage = 1;
            $scope.counter++; 
            $scope.search = search;

            if(search != undefined){
                $rootScope.isProcessing = true;
                PrimoService.searchEvents(search).then(function (response){
                    debugger;
                    $rootScope.isProcessing = false;

                    if(response.data.result.total > 0 ){

                        if(response.data.result.current_page == response.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }
                        else{

                            $scope.is_disable = false;   
                        }

                        $scope.items = response.data.result.data;
                        $scope.totalEvents = response.data.result.total;

                        console.log($scope.items);
                    }else{

                        $scope.totalEvents = 0;
                        $scope.items = [];
                    }

                });
            }else{
                ngToast.create({className: 'danger',content: 'Please fill atleast one option'});
            }

        }

        $scope.loadMoreWithSearching = function() 
        {
            $scope.searchlastpage +=1;
            PrimoService.searchEvents($scope.search,$scope.searchlastpage).then(function (response){
                $rootScope.isProcessing = false;

                    if(response.data.result.total > 0 ){

                        if(response.data.result.current_page == response.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }
                        else{

                            $scope.is_disable = false;   
                        }

                        $scope.items = $scope.items.concat(response.data.result.data);
                        $scope.totalEvents = response.data.result.total;

                        console.log($scope.items);
                    }else{

                        $scope.totalEvents = 0;
                        $scope.items = [];
                    }
            });
        }
});