/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.directive('homeSearchBar', function() {
  return {
    restrict: 'E',
    templateUrl: "partials/home-search-bar.html",
    replace: true,
    controller: function($scope, $element, $rootScope,
                         $state, $stateParams,
                         InternationalRatesService,
                         mParticleService,
                         ServiceProductsService,
                         PrimoService, $q, $filter, UiService, AuthService, $anchorScroll, _) {

      $scope.promo        =  'SUB_10';

      $scope.serviceProducts = ServiceProductsService.getServiceProducts();

      angular.forEach($scope.serviceProducts.groups, function(val, key) {
        if (val.group == 'CREDIT') {
          $scope.credit_products       = val.products;
          $scope.selectedCreditProduct = $scope.credit_products[0];
        }
      });

      /**
       * Fetch the list of popular destinations
       */
      $scope.getPopularCountries = function() {
        ServiceProductsService.getPopularCountries($scope.promo).then(function(data) {
          $scope.popularCountries = data;
        });
      };

      $scope.getPopularCountries($scope.promo);

      $scope.selectedItemChange   = selectedItemChange;
      $scope.searchForLocation    = searchForLocation;
      $scope.trySearchForLocation = trySearchForLocation;

      //selected item default empty
      $scope.selected = null;

      //default search text value
      if ($stateParams.country) {
        trySearchForLocation($stateParams.country);
      } else {
        $scope.searchText = "";
      }

      $scope.countryPlanFilter = function(product) {
        return product.type === 'COUNTRY';
      };
      /**
       * Select item on click
       * @param name
       */
      $scope.selectCountryOnClick = function(name) {
        selectedItemChange(searchForLocation(name)[0]);
      };

      $scope.showDialog = function(filter) {
        UiService.showAlertDialog('FreeCountriesController', 'partials/free-countries-list.html', function() {
        }, filter)
      };

      /**
       * Opens a dialog which is used to invite friends
       * User has to be authenticated in order to invite friends
       */
      $scope.inviteFriends = function() {
        //non authenticated user is transferred to login page
        if (!$rootScope.user) {
          AuthService.login();
          return;
        }
        UiService.showConfirmDialog('AccountInviteFriendsController', 'partials/account-invite-friends.html', function(clb) {
          if (clb) {
            PrimoService.inviteFriends({invited_emails: clb}).then(function(response) {
              $scope.$root.alert = {
                show: true,
                context: {type: 'success', msg: $filter('translate')('INVITE_SUCCESSFULLY SENT')}
              }
            });
          }
        });
      };

      $scope.formatAutocompleteItem = function(item) {
        return item.country + " |||"
      };

      $anchorScroll.yOffset = 50;

      $scope.$watch('selected', function(newValue) {
        if (newValue) {
          UiService.scrollToElement($element, 70);
        }
      });

      function trySearchForLocation(searchText) {
        var results = searchForLocation(searchText);
        if (results && angular.isArray(results) && results.length) {
          selectedItemChange(results[0])
        } else {
          $scope.$root.alert = {
            show: true,
            context: {
              type: 'danger',
              msg: $filter('translate')('NO_MATCHING_STATES') + ' "' + $scope.searchText + '" ' + $filter('translate')('WERE_FOUND')
            }
          }
        }
      }

      function searchForLocation(text) {
        if (text) {
          return InternationalRatesService.getNewCountryByName(text);
        } else {
          return InternationalRatesService.getRatePlanCountries();
        }
      }

      function selectedItemChange(item) {
        $scope.selected = item;
      }

    }
  }
});