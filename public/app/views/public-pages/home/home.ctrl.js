/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller("HomeController", function ($scope, $rootScope, $state, UiService, $filter, AuthService, PrimoService, config,ngToast) {

  
        var vm = this;
        $scope.config = config;
        $scope.is_disable = false;
        vm.user = null;
        vm.allUsers = [];
        vm.books = null;
        $scope.books = [];  
        $scope.lastpage = 1;
        $scope.selectedCategoryId = '';
        
        function initController() {
           //forceSSL();
            loadGetAllTextBooks();            
            loadBookCategories();
        }

        function loadGetAllTextBooks()
        {
            $rootScope.isProcessing=true;
            PrimoService.GetBooks($scope.lastpage,$scope.selectedCategoryId).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    ngToast.create("Something went wrong");
                }
            });
        }

        function loadBookCategories(){
            
            PrimoService.GetBookCategories().then(function (response){

                var res = response;
                if(res.data.status == 1){
                    $scope.itemList = res.data.result;
                }
                
                if(res.status == 0){
                    $scope.itemList = [];   
                }
            });
        }

        $scope.loadMore = function() 
        {
             console.log('loadMore');
                $scope.lastpage +=1;
                $rootScope.isProcessing=true;
                PrimoService.GetBooks($scope.lastpage,$scope.selectedCategoryId).then(function (response){
                    var res = response;
                    $rootScope.isProcessing=false;
                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.isDataFound=true;
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                        $scope.totalTextBook=$scope.items.length;
                    }
                    
                    if(res.status == 0){
                        ngToast.create("Something went wrong");
                    }
                });
        }


        $scope.searchBookByCategoryId = function(id){

            $scope.selectedCategoryId = id;
            $rootScope.isProcessing=true;
            PrimoService.GetBooks($scope.lastpage,$scope.selectedCategoryId).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.result.data && res.data.result.data.length){
                    $scope.items = res.data.result.data;
                    $scope.totalTextBook=$scope.items.length;
                    $scope.isDataFound=true;
                    }
                    else{
                        $scope.isDataFound=false;
                        ngToast.create('No data found');
                    }
                }
                if(res.status != 200){
                    ngToast.create("Something went wrong");
                }
            });
        }

    $scope.bookSearchByTitle = function(search){
      
      if(search){
        $state.go('bookSearch',{title:search.title});
      }else{
        alert("Please fill the textbox first");
      }
      
    }
    
    initController();

});