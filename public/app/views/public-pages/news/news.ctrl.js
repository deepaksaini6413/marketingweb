app.controller("NewsController", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, $cookies, config,ngToast) {

        $scope.config = config;  
        var vm = this;
        $scope.is_disable = false;
        vm.user = null;
        vm.allUsers = [];
        vm.items = null;
        $scope.items = [];  
        $scope.lastpage = 1;
        $rootScope.isProcessing=false;
        $scope.loadingImage='';


        
        function initController() {
            
            if($stateParams.slug){
                LoadSinglePage($stateParams.slug);
            }else{
                loadNewsData();
            }
            loadNewsData();
        }

        function loadNewsData() {
            $rootScope.isProcessing=true;
            PrimoService.getNews().then(function(response) {
                var data = response.data;
                $rootScope.isProcessing=false;
                if (data.status == 1) {
                    $scope.isDataFound = true;
                    $scope.breakingNews = data.featured_news;
                    $scope.newsCategories = data.categories;
                    $scope.currentNews = data.news.data;
                    $scope.nextUrl = data.news.next_page_url;
                }
            });    
        }

        function loadNews() {
            $rootScope.isProcessing=true;
            if(angular.equals({}, $rootScope.globals.currentUser)){
                $scope.user = $rootScope.globals.currentUser.username;
            }
            
            PrimoService.News($scope.lastpage).then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.items = res.data.result.data;
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });
        }
        
        function LoadSinglePage(slug){
            $rootScope.isProcessing=true;
            PrimoService.singleNews(slug).then(function (response){
                $rootScope.isProcessing=false;
                var res = response;
                if(res.data.status == 1){
                    $scope.item = res.data.news;
                    $scope.related_news = res.data.related_news;
                    $scope.stats = res.data.stats;
                }
            });
        }

        $scope.newsActivity = function(id,action){

            $rootScope.isProcessing=true;
            var newsObj = $cookies.getObject('newsactivity');

            if(newsObj != undefined){
                
                if(newsObj.indexOf(id) !== -1) {

                    alert("you already performed action");
                    return false;

                }else{
                    newsObj.push(id);
                    $cookies.putObject('newsactivity',newsObj);
                }

            }else{
                
                $cookies.putObject('newsactivity',[id]);
            }

            PrimoService.newsActivity(id,action).then(function (response){
                $rootScope.isProcessing=false;
                var res = response;

                if(res.data.status == 1){
                    $scope.stats = res.data.stats;
                }
                  
            });
        }

        $scope.loadMore = function() {
                $rootScope.isProcessing=true;
                $scope.lastpage +=1;

                PrimoService.News($scope.lastpage).then(function (response){
                    $rootScope.isProcessing=false;
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
            }


        $scope.getCategoryNews = function(slug) {
            $rootScope.isProcessing=true;
            $scope.lastpage = 1;
            $scope.currentNews = [];
            $rootScope.isProcessing=true;
            PrimoService.getNewsByCategory(slug).then(function(response) {
                $rootScope.isProcessing=false;
                var data = response.data;
                $rootScope.isProcessing=false;
                $scope.currentNews = data.news.data;
                $scope.newsCategory = data.category;
                if(data.news.data.length > 0){
                    $scope.isDataFound = true;
                }
                else{
                    $scope.isDataFound = false;
                }
                $scope.nextUrl = data.news.next_page_url;
            });
        }


        $scope.loadMoreNews = function() {
                
                $rootScope.isProcessing=true;
                $scope.lastpage +=1;

                $scope.loading = true;
                PrimoService.newsLoadMoreData($scope.lastpage).then(function(response) {
                    $rootScope.isProcessing=false;
                    var data = response.data;
                    $scope.loading = false;
                    if(data.status == 1){
                            
                        if(data.news.current_page == data.news.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        angular.forEach(data.news.data, function(value, key){
                            $scope.currentNews.push(value);
                        });
                    }

                    if(data.status == 0){
                        alert("Something went wrong");
                    }
                });
            }

    initController();

});