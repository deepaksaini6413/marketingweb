/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

app.controller('RetailersController', function ($scope, UiService, PrimoService, $rootScope, $filter) {

  $scope.becomeRetailer = function () {
    UiService.showConfirmDialog('RetailersContactController', 'partials/retailers-contact-form.html', function (clb) {
      if (clb) {
        PrimoService.submitDealerApplication(clb).success(function (data) {
          $rootScope.alert = {
            show: true,
            context: {type: 'success', msg: $filter('translate')('SUCCESS_RETAIL_APP')}
          };
        });
      }
    });

  }
});