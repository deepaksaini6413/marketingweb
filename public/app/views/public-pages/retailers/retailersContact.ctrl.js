/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller('RetailersContactController', function($rootScope, $scope, $mdDialog,
                                                      PrimoService,
                                                      $filter) {

  $scope.initUser = function() {
    $scope.user = {
      first_name: '',
      last_name: '',
      email: '',
      phone: '',
      company: '',
      address_1: '',
      city: '',
      state_province: '',
      postal_code: '',
      message: '',
      epay_pos: 0
    }
  };

  $scope.initUser();

  $scope.storeTypes = {
    "single": "Single Location",
    "multiple": "Multiple Location",
    "other": "Other"
  };

  $scope.source = {
    "Friend": "Friend",
    "Store": "Store",
    "A Primo Retailer": "A Primo Retailer",
    "Search Engine/Online": "Search Engine/Online",
    "Other": "Other"
  };

  $scope.answer = function() {
    PrimoService.submitDealerApplication($scope.user).success(function(data) {
      $rootScope.alert = {
        show: true,
        context: {type: 'success', msg: $filter('translate')('SUCCESS_RETAIL_APP')}
      };

      //reset user info on success
      $scope.initUser();
      $scope.dealerForm.$setPristine();
      $scope.dealerForm.$setUntouched();
    });
  };
});