
app.controller("ScholarshipController", function ($scope, $rootScope, $state, $stateParams, UiService, $filter, AuthService, PrimoService, $cookies, config,ngToast) {

        $scope.config = config;
        var vm = this;
        vm.selected = 'college';
        $scope.lastpage = 1;
        $scope.is_disable = false;
        $scope.items = [];
        $rootScope.isProcessing=false;
        $scope.isDataFound=false;

        initController();

        function initController() {
            $scope.loadingImage=$rootScope.loadingImage;
            var slug = $stateParams.slug;
            var currenturl = $state.current;

            if(slug){
                loadScholarships();
            }else if (currenturl.url == '/scholarships-search'){

                scholarshipsSearchPage();
            }else{
                LoadSinglePage(slug);
            }
            //testimonialsTeam();
            loadTopScholarship();
        }

        $scope.UCCHUB = function(clickAction){

        }

        function loadTopScholarship(){

            $rootScope.isProcessing=true;
            PrimoService.loadTopScholarship().then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.data.status == 1){
                    if(res.data.result.data && res.data.result.data.length)
                    {
                        $scope.isDataFound=true;
                        $scope.topScholarship = res.data.result.data;
                        
                    }
                    else{
                        $scope.isDataFound=false;
                    }
                    
                }
                
                if(res.status == 0){
                    ngToast.creat("Something went wrong");
                }
            });
        }

        $scope.startSearch = function(){
            $('#myModal').modal();
            // $rootScope.isProcessing=true;
            // PrimoService.startSearch().then(function(response){
            //     var res= response;
            //     $rootScope.isProcessing=false;
            //     if(res.status==200){
            //         if(res.data){
            //             //business logic here
            //         }
            //     }
            // });
        }

        $scope.collegeDictory = function(){
            $location.path('/colleges'); 
            // $rootScope.isProcessing=true;
            // PrimoService.collegeDictory().then(function(response){
            //     var res= response;
            //     $rootScope.isProcessing=false;
            //     if(res.status==200){
            //         if(res.data){
            //             //business logic here
            //         }
            //     }
            // });
        }

        $scope.matchMaker = function(){
            $rootScope.isProcessing=true;
            PrimoService.matchMaker().then(function(response){
                var res= response;
                $rootScope.isProcessing=false;
                if(res.status==200){
                    if(res.data){
                        //business logic here
                    }
                }
            });
        }

        function testimonialsTeam(){
            $rootScope.isProcessing=true;
            PrimoService.testimonialsTeam().then(function (response){
                var res = response;
                $rootScope.isProcessing=false;
                if(res.status == 200){
                    if(res.data.Data && res.data.Data.length)
                    {
                        $scope.isDataFound=true;
                        $scope.testimonialsTeam = res.data.Data;
                    }
                    else{
                        $scope.isDataFound=false;
                    }
                    
                }
                
                if(res.status == 0){
                    ngToast.creat("Something went wrong");
                }
            });
        }

        function loadScholarships()
        {
            
            PrimoService.Scholarships($scope.lastpage).then(function (response){
                var res = response;

                if(res.data.status == 1){
                    if(res.data.result.current_page == res.data.result.last_page)
                    {
                        $scope.is_disable = true;
                    }
                    $scope.items = res.data.result.data;
                    
                }
                
                if(res.status == 0){
                    alert("Something went wrong");
                }
            });
        }

        function LoadSinglePage(slug){
            
            PrimoService.Scholarship(slug).then(function (response){
                var res = response;
                if(res.data.status == 1){
                    $scope.item = res.data.result;
                }
                
            });
        }

        $scope.loadMore = function() 
        {
                $scope.lastpage +=1;
                PrimoService.Scholarships($scope.lastpage).then(function (response){
                    var res = response;

                    if(res.data.status == 1){
                        
                        if(res.data.result.current_page == res.data.result.last_page)
                        {
                            $scope.is_disable = true;
                        }

                        $scope.items = $scope.items.concat(res.data.result.data);
                    }
                    
                    if(res.status == 0){
                        alert("Something went wrong");
                    }
                });
            }

        function scholarshipsSearchPage(){
            $rootScope.isProcessing = true;
            $scope.items = [];

            PrimoService.Scholarships(1).then(function(response){
                if(response.data.status == 1){
                    $scope.items = response.data.result.data;
                    $scope.totalScholarship = response.data.result.total;
                    $scope.pagination = false;
                }
            });


            
            PrimoService.getAllColleges().then(function(response){
                $rootScope.isProcessing = false;
                if(response.data.status == 1){
                    $scope.colleges = response.data.result;
                }
            });
        }

        $scope.searchScholarship = function(search){

            $scope.lastpage = 1;
            $scope.isProcessing = true;
            $scope.items = [];
            
            PrimoService.searchScholarship($scope.lastpage,search.college.title).then(function(response){
                $scope.isProcessing = false;
                if(response.data.status == 1){

                    if(response.data.result.current_page == response.data.result.last_page)
                    {
                            $scope.is_disable = true;
                    }
                    $scope.pagination = true;
                    $scope.items = response.data.result.data;
                    $scope.totalScholarship = response.data.result.total;
                }
            });
        }
});