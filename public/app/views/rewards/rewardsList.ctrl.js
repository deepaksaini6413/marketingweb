/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
app.controller('RewardsListController', function ($scope, $filter, $state, $rootScope, PrimoService, data) {
  $scope.rewardItems = [];

  //populate incomplete on top
  angular.forEach(data.incomplete, function (item) {
    item.type = 'incomplete';
    $scope.rewardItems.push(item);
  });

  //populate completed rewards
  angular.forEach(data.complete, function (item) {
    item.type = 'complete';
    $scope.rewardItems.push(item);
  });

  /**
   * determines selected item from local storage saved menu state
   * @type {boolean}
   */
  $scope.selected = false;

  $scope.isMSISDNVerified = function() {
    var retVal = false;
    angular.forEach($scope.rewardItems, function(val, key) {
      if ($scope.rewardItems[key].reward_name == "MSISDN_VERIFIED" && $scope.rewardItems[key].type == 'complete') {
        retVal = true;
      }
    });

    return retVal;
  };

  /**
   /**
   * Set selected item
   * @param item
   */
  $scope.select = function (item, index) {
    if (item.type == 'complete') {
      PrimoService.getRewardDetails({reward_name: item.reward_name}).then(function (data) {
        $scope.selected          = data.data.reward;
        $scope.selected[0].type  = item.type;
        $scope.selected[0].image = item.icon_base64_data;
      });
    } else {
      if (item.reward_name == "REFERRAL_REFERRER") {
        $state.go('dashboard');
      } else if (item.reward_name == "ANNIVERSARY") {
        //nothing to do here
      } else {
        $state.go('account.profile');
      }
    }
  };
});

