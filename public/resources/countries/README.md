# README
Comprehensive country code information, including ISO 3166 codes, ITU dialing codes, ISO 4217 currency codes, and many others. Provided as a Simple Data Format Data Package.

# Data
Data comes from multiple sources as follows.

ISO 3166 offical English and French short names are from iso.org Note: ISO is no longer providing these code lists for free.

ISO 4217 currency codes are from currency-iso.org

# Field Information
- name	-	string	Country's official English short name
- name_fr	-	string	Country's offical French short name
- ISO3166-1-Alpha-2	-	string	Alpha-2 codes from ISO 3166-1
- ISO3166-1-Alpha-3	-	string	Alpha-3 codes from ISO 3166-1 (synonymous with World Bank Codes)
- ISO3166-1-numeric	-	integer	Numeric codes from ISO 3166-1 (synonymous with UN Statistics M49 Codes)
- ITU	-	string	Codes assigned by the International Telecommunications Union
- MARC	7	string	MAchine-Readable Cataloging codes from the Library of Congress
- WMO	- string	Country abbreviations by the World Meteorological Organization
- DS	-	string	Distinguishing signs of vehicles in international traffic
- Dial	-	string	Country code from ITU-T recommendation E.164, sometimes followed by area code
- FIFA	-	string	Codes assigned by the Fédération Internationale de Football Association
- FIPS	-	string	Codes from the U.S. standard FIPS PUB 10-4
- GAUL	-	string	Global Administrative Unit Layers from the Food and Agriculture Organization
- IOC	-	string	Codes assigned by the International Olympics Committee
- currency_alphabetic_code	-	string	ISO 4217 currency alphabetic code
- currency_country_name	-	string	ISO 4217 country name
- currency_minor_unit	- integer	ISO 4217 currency number of minor units
- currency_name	-	string	ISO 4217 currency name
- currency_numeric_code	- integer	ISO 4217 currency numeric code
- is_independent -	string	Country status, based on the CIA World Factbook

