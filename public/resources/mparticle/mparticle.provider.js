/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
(function (angular) {

  angular.module('mparticleWidget', [])
    .run([
      '$window',
      'mparticleWidgetSettings',
      function ($window, mparticleWidgetSettings) {
        if (!mparticleWidgetSettings.app_key) {
          throw new Error('Missing app_key. Please set in app config via mparticleWidgetProvider');
        }

        if (mparticleWidgetSettings.isSandbox == undefined) {
          throw new Error('Missing isSandbox. Please set in app config via mparticleWidgetProvider');
        }

        console.log('is sandbox', mparticleWidgetSettings.isSandbox);
        var window = $window;

        window.mParticle = {
          config: {
            isSandbox: mparticleWidgetSettings.isSandbox
          }
        };

        //pasted from mparticle widget
        (function (apiKey) {
          window.mParticle           = window.mParticle || {};
          window.mParticle.config    = window.mParticle.config || {};
          window.mParticle.config.rq = [];
          window.mParticle.ready     = function (f) {
            window.mParticle.config.rq.push(f);
          };
          var mp                     = document.createElement('script');
          mp.type                    = 'text/javascript';
          mp.async                   = true;
          mp.src                     = ('https:' == document.location.protocol ? 'https://jssdkcdns' : 'http://jssdkcdn') + '.mparticle.com/js/v1/' + apiKey + '/mparticle.js';
          var s                      = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(mp, s);

          window.mParticle.ready(function () {
            //mParticle is ready to log events, etc.
          });

        })(mparticleWidgetSettings.app_key);
      }
    ]);

})(angular);

(function (angular) {
  var
    settings = {
      app_key: '',
      isSandbox: true,
      beforePageLoad: angular.noop
    };

  angular.module('mparticleWidget')
    .value('mparticleWidgetSettings', settings)
    .provider('mparticleWidget', [function () {
      this.init = function (opts) {
        angular.extend(settings, opts);
      };

      this.$get = [
        '$window',
        function ($window) {
          function mparticleWidgetApi() {
          }

          angular.forEach(apiMethods, function (method) {
            mparticleWidgetApi.prototype[method] = function () {
              $window.mParticle[method].apply($window.mParticle, arguments);
            };
          });

          return new mparticleWidgetApi();
        }
      ];
    }]);

})(angular);
