/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */
var config        = require('./config');
var zendesk       = require('./lib/zendesk/zendesk.js');
var zendeskClient = zendesk.RestAPI(config.zendesk),
    _             = require('lodash'),
    Factual       = require('factual-api'),
    factual       = new Factual(config.factual.apiKey, config.factual.secretKey);

module.exports = function (router) {

  /**
   * Call factual to receive world locations
   */
  router.get('/world-location/:text', function (req, res, next) {
    //search text must be provided
    if (!req.params.text) return res.status(404).send({});
    //call factual
    factual.get('/t/world-geographies?limit=20&include_count=t&offset=0&q=+' + req.params.text + '&KEY=' + config.factual.apiKey, function (error, data) {
      //send error message received from factual
      if (error) return res.status(500).send(error);
      //filter only relevant fields received from factual
      var retVal = _.map(data.data, function (item) {
        return {
          contextname: item.contextname,
          country: item.country,
          longitude: item.longitude,
          latitude: item.latitude
        }
      });

      //send response
      res.status(200).send(retVal);
    });
  });


  /**
   * Call Primo to fetch service plans
   */
  router.post('/get-service-plans', function (req, res, next) {
    /* TODO: temporary fix, until this API gets public */
    /*primoClient.getServicePlans(req.headers, req.body, function (code, response) {
     res.status(code).send(response);
     })*/
    res.status(200).send({service_plans: service_plans});
  });


  router.get('/list-faq', function (req, res, next) {
    zendeskClient.getSections(function (code, resp) {
      var sections = resp.sections;
      zendeskClient.getArticles(function (code, response) {
        var articles = response.articles;
        for (var i in articles) {
          for (var j in sections) {
            if (sections[j].id == articles[i].section_id) {
              if (!sections[j].articles) sections[j].articles = [];
              sections[j].articles.push(articles[i]);
            }
          }
        }
        res.status(200).send(sections);
      });
    });
  });

  router.get('/list-tickets/:name', function (req, res, next) {
    zendeskClient.getUsers(req.params.name, function (code, usersResponse) {
      var user = usersResponse.users[0];

      if (user) {
        zendeskClient.getUserTickets(user.id, function (code, response) {
          res.status(code).send(response.results);
        });
      } else {
        res.status(200).send({});
      }
    });
  });

  router.post('/create-ticket', function (req, res, next) {
    zendeskClient.createTicket(req.body, function (code, response) {
      res.status(code).send(response.results);
    });
  });


  return router;
};