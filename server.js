var express        = require('express'),
    logger         = require('./lib/logger'),
    app            = express(),
    bodyParser     = require('body-parser'),
    cors           = require('cors'),
    http2https     = require('./middleware/http2httpsMiddleware'),
    corsMiddleware = require('./middleware/corsMiddleware'),
    logRequest     = require('./middleware/loggerMiddleware').logRequest,
    logErrors      = require('./middleware/loggerMiddleware').logErrors,
    routes         = require('./router.js')(express.Router());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app
  //.use(http2https)
  .use(logRequest)
  .use(cors())
  .use(corsMiddleware);

logger.info('Loading routes...');
app.use(routes);
logger.info('Routes loaded...');

//HTML5 routes
app.get([
  '/',
  'textbooks',
  '/login',
  '/login/:haskey',
  '/register',
  '/verify-msisdn/:username/:msisdn/:country',
  '/forgot-password',
  '/password/reset/:key',
  '/forgot-username',
  '/terms-of-service',
  '/privacy-policy',
  '/profile',
  '/change-password',
  '/faq/',
  '/news',
  '/news/:slug',
  '/jobs',
  '/job/:slug',
  '/scholarships',
  '/scholarships-search',
  '/scholarship/:slug',
  '/events',
  '/event/:slug',
  '/colleges',
  '/college/:slug',
  '/books',
  '/book/:slug',
  '/book-search/:title',
  '/college-search',
  '/college-search/:title',
  '/college-search/:title/:state',
  '/college-search/:title/:state:country',
  '/sign-up',
  '/colleges/:campuslife',
  '/scholarships/:campusvisit',
  '/news/:campusvisit',
  '/events/:campusvisit',
  '/jobs/:campusvisit',
  '/landing-page'
], function (req, res) {
  res.sendFile(__dirname + '/build/index.html')
});

//load statics
app.use('/', express.static(__dirname + '/build'));

//attach log errors middleware
app.use(logErrors);
//set port
app.set('port', process.env.port || 3000);

//init server
app.listen(app.get("port"), function () {
  logger.info("Server running on port [" + (app.get("port")) + "]");
});

module.exports = app;
