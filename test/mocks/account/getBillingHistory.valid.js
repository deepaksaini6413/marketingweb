/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

module.exports = {
  warnings: [],
  success: true,
  error_codes: [],
  user_errors: [],
  billing_history: [{
    history_epoch: 1458020642,
    order_id: 11192,
    type: 'Other',
    history_source: 'Other',
    AMOUNT: '0.00',
    MINUTES: 100,
    description: 'Anniversary | 100 Minutes',
    result: 'COMPLETE'
  }]
};