/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */


module.exports = {
  warnings: [],
  success: true,
  error_codes: [],
  user_errors: [],
  call_history: [{
    rate_plan: 'Primo SMS Outbound',
    history_epoch_pst: 1459212933,
    destination: 'United States',
    destination_number: '15187778056',
    minutes: 0,
    rate: 0
  },
    {
      rate_plan: 'Primo SMS Outbound',
      history_epoch_pst: 1458190942,
      destination: 'United States',
      destination_number: '15187778046',
      minutes: 0,
      rate: 0
    }]
}
