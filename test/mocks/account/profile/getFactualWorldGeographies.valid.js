/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

module.exports = {
  data: [{
    ancestors: [Object],
    contextname: 'MSA New York-Northern New Jersey-Long Island, Earth, US',
    country: 'US',
    factual_id: '09078efa-8f76-11e1-848f-cfd5bf3ef515',
    latitude: 40.562511,
    longitude: -74.420418,
    name: 'MSA New York-Northern New Jersey-Long Island',
    neighbors: [Object],
    parent: [Object],
    placetype: 'zone',
    variants: [Object]
  },
    {
      ancestors: [Object],
      contextname: 'America/New_York, Earth, US',
      country: 'US',
      factual_id: '0977c22e-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 36.2052,
      longitude: -76.272987,
      name: 'America/New_York',
      parent: [Object],
      placetype: 'timezone',
      variants: [Object]
    },
    {
      ancestors: [Object],
      children: [Object],
      contextname: 'New York, US',
      country: 'US',
      factual_id: '0864a1f4-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 42.85535,
      longitude: -76.501671,
      name: 'New York',
      neighbors: [Object],
      parent: [Object],
      placetype: 'region',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'New York, Ballard, US',
      country: 'US',
      factual_id: '089446ca-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 36.989841,
      longitude: -88.970619,
      name: 'New York',
      neighbors: [Object],
      parent: [Object],
      placetype: 'locality',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'Little New York, Gonzales, US',
      country: 'US',
      factual_id: '088c9484-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 29.5508,
      longitude: -97.306396,
      name: 'Little New York',
      parent: [Object],
      placetype: 'locality',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'New York, Santa Rosa, US',
      country: 'US',
      factual_id: '0894479c-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 30.838011,
      longitude: -87.194489,
      name: 'New York',
      neighbors: [Object],
      parent: [Object],
      placetype: 'locality',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'New York, Cibola, US',
      country: 'US',
      factual_id: '08944738-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 35.059429,
      longitude: -107.526619,
      name: 'New York',
      neighbors: [Object],
      parent: [Object],
      placetype: 'locality',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'Little New York, Marshall, US',
      country: 'US',
      factual_id: '09316e3c-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 34.3862,
      longitude: -86.186089,
      name: 'Little New York',
      parent: [Object],
      placetype: 'neighborhood',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'West New York, Hudson, US',
      country: 'US',
      factual_id: '08ce45aa-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 40.78833,
      longitude: -74.015259,
      name: 'West New York',
      neighbors: [Object],
      parent: [Object],
      placetype: 'locality',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'York New Salem, York, US',
      country: 'US',
      factual_id: '08afd688-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 39.965511,
      longitude: -76.771973,
      name: 'York New Salem',
      neighbors: [Object],
      parent: [Object],
      placetype: 'locality',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'Central New York State, US',
      country: 'US',
      factual_id: '0977dca0-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 42.654079,
      longitude: -75.268433,
      name: 'Central New York State',
      parent: [Object],
      placetype: 'colloquial',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'New York, Henderson, US',
      country: 'US',
      factual_id: '0894476a-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 32.168499,
      longitude: -95.669189,
      name: 'New York',
      neighbors: [Object],
      parent: [Object],
      placetype: 'locality',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: '212 New York, US',
      country: 'US',
      factual_id: '0907c2ee-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 40.79097,
      longitude: -73.965927,
      name: '212 New York',
      neighbors: [Object],
      parent: [Object],
      placetype: 'zone',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'New York, Wayne, US',
      country: 'US',
      factual_id: '08944698-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 40.85154,
      longitude: -93.255211,
      name: 'New York',
      neighbors: [Object],
      parent: [Object],
      placetype: 'locality',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'MD New York-White Plains-Wayne, Earth, US',
      country: 'US',
      factual_id: '09079328-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 41.0844,
      longitude: -73.992249,
      name: 'MD New York-White Plains-Wayne',
      neighbors: [Object],
      parent: [Object],
      placetype: 'zone',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'New York, Saint Ann, JM',
      country: 'JM',
      factual_id: '12dc06cc-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 18.25,
      longitude: -77.183296,
      name: 'New York',
      parent: [Object],
      placetype: 'locality',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'Western New York State, US',
      country: 'US',
      factual_id: '0977e646-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 42.688278,
      longitude: -78.364281,
      name: 'Western New York State',
      parent: [Object],
      placetype: 'colloquial',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'New York, Caldwell, US',
      country: 'US',
      factual_id: '08944706-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 39.68486,
      longitude: -93.917931,
      name: 'New York',
      neighbors: [Object],
      parent: [Object],
      placetype: 'locality',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'New York Mills, Otter Tail, US',
      country: 'US',
      factual_id: '08c8dbe2-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 46.517719,
      longitude: -95.373367,
      name: 'New York Mills',
      neighbors: [Object],
      parent: [Object],
      placetype: 'locality',
      variants: [Object]
    },
    {
      ancestors: [Object],
      contextname: 'New York\'s North Country, US',
      country: 'US',
      factual_id: '097802fc-8f76-11e1-848f-cfd5bf3ef515',
      latitude: 44.216648,
      longitude: -74.781174,
      name: 'New York\'s North Country',
      parent: [Object],
      placetype: 'colloquial',
      variants: [Object]
    }],
  included_rows: 20,
  total_row_count: 43273
};
