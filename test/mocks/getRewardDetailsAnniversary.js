/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */


module.exports = '{"reward": [{"reward_name":"EMAIL_VERIFIED","description":"Add Email Address","details":null,"reward_type":"minutes","reward_value":"200","reward_date":1456214941}]}';