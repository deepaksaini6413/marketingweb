/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */


module.exports = {
  warnings: [],
  success: true,
  error_codes: [],
  user_errors: [],
  first_name: 'Nebojsa',
  last_name: 'Brankovic',
  email: 'nbrankovic@hyperether.com',
  city: null,
  msisdn_country_code: 'BE',
  home_city: 'Sombor',
  home_country: 'RS',
  from_location_string: 'Sombor, Zapadna Bachka, RS',
  from_longitude: '19.09259',
  from_latitude: '45.789989',
  residence_location_string: 'Sombor, Zapadna Bachka, RS',
  residence_longitude: '19.09259',
  residence_latitude: '45.789989',
  preferred_language: 'EN',
  marketing_email_option: true,
  marketing_sms_option: false,
  balance: 0,
  gift_credit: 0,
  intl_minutes: 700,
  cos_id: 1,
  plan_state: 'Active',
  did: '',
  has_profile_pic: 1,
  ixnx_domain: 'dc1qaixnx02.primo.me',
  direct_dial_in: '',
  msisdn: '38163583149'
};