/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */


module.exports = {
  "warnings": [],
  "success": true,
  "error_codes": [],
  "user_errors": [],
  "ixnx": "dc1qaixnx02.primo.me",
  "alias": "nebojsa",
  "password": "6A001E6C68D8DEDD8BD8580DC66DB847",
  "access_number": "",
  "login_token": "1461236621|$2y$10$8f0ix0muODq.6yKHY1zk2eIxUK56bWuQuLCJTJsfJaQAIqfxyVeta|5718b38d95ce83.97555157|MTcwMjQ3OTU2ODE=",
  "refresh_token": "1459939845|$2y$10$vFGhhXVahIkk9bOx5OJD8eOO0LcI1tryE4qx8zbJpipjLnj/dIO.G|5704ea05549443.35837999|MTcwMjQ3OTU2ODE=",
  "pps_token": "15acb3d995a4430e2b231a0c6705056bb97bae9d022d6c7145aea0df3852a1b2$c912c40a3efc754723957831315f030b$7b05e54253a6747853865628771a5ed60989b084b3c4e0a61abe579388e78118",
  "ixnx_token": "e=1459943445\nT=2\nt=jTbyTIkRFrjnovWoHe-MiA\ni=16ma6G3TgSynoLgT\nc=H3-4qBZ1YuigO804Ze_QhqN8h19ZtZlAGIDjdhIST9YnfuJ36IvRTn1I_gw",
  "buddies": [{"contact_login_name": "iwatest1"}, {"contact_login_name": "motoman"}, {"contact_login_name": "nebatest"}, {"contact_login_name": "milos"}, {"contact_login_name": "moraiphone34"}, {"contact_login_name": "lacika"}, {"contact_login_name": "dragan27"}, {"contact_login_name": "brankovicbog"}, {"contact_login_name": "rade.test"}],
  "multiple_devices": true,
  "xmpp_username": "nebojsa",
  "xmpp_password": "BmIswaas"
};