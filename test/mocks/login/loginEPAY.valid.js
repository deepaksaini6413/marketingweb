/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

module.exports = {
  "warnings": [],
  "success": true,
  "error_codes": ["AC0002"],
  "user_errors": [],
  "user_identifier": "58170aee903ce49d35244b2c",
  "ixnx": "dc1qaixnx02.primo.me",
  "alias": "+1313732273026",
  "password": "A8DA97A25A6564B8E50D61CD48F2DACE",
  "access_number": null,
  "login_token": "1477920662|$2y$10$RbrRzX4PTC7Jn1m/32VvGey6yIZoDcaCb5EqUP1Kgb4nFiHhEiyJW|58174796e03902.49242917|MTcwMjQ3OTU2ODE=",
  "refresh_token": "1477920662|$2y$10$MHy7fD2AYJ8vNzWJJr6jLuSKvqoFigNHF37ZRxkJXVRszBwYmAWA2|58174796f25296.20125604|MTcwMjQ3OTU2ODE=",
  "pps_token": "4c70f8b0acc5f9727ddd7f873ee12d4b452687dde84055a1f65c77ac1fad8ad4$0c4fbb08a2cb22423f060be49f9516cd$083912ee6f4c76d2ad1e413a9fcb816903ea2e807db5071f5e35f816423c4d17",
  "ixnx_token": "e=1477924263\nT=2\nt=LC8cL53jEf5DuJBkcJ5kkQ\ni=GKOL0r5KZGRBeRYD\nc=AKKxwDvu2JJU4-1VlXI-21mcnMM9t9JC91fIjgMEIddEMHnKlPk0-U85Y_R5MRZntHRY",
  "buddies": null,
  "multiple_devices": false,
  "msisdn_reward_value": "25",
  "xmpp_username": null,
  "xmpp_password": null
};