/*
 * Copyright (C) 2016 Primo, All Rights Reserved.
 * This source code and any compilation or derivative thereof is the
 * proprietary information of Primo and is confidential in nature.
 *
 * Under no circumstances is this software to be exposed to or placed
 * under an Open Source License of any type without the expressed written
 * permission of Primo.
 *
 */

module.exports = {
  warnings: [],
  success: true,
  error_codes: [],
  user_errors: [],
  access_number: '',
  ixnx_domain: 'dc1qaixnx02.primo.me',
  direct_dial_in: '15187778040'
};